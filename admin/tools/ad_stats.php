<?php

	/*
	 #======================================================
	 |    Trellis Desk
	 |    =====================================
	 |    By DJ Tarazona (dj@accord5.com)
	 |    (c) 2010 ACCORD5
	 |    http://www.trellisdesk.com/
	 |    =====================================
	 |    Email: sales@accord5.com
	 #======================================================
	 |    @ Version: v1.0.4 Final Build 10440094
	 |    @ Version Int: 104.4.0.094
	 |    @ Version Num: 10440094
	 |    @ Build: 0094
	 #======================================================
	 |    | Admin Custom Department Fields
	 #======================================================
	 */

	class ad_stats
	{
		function auto_run()
		{
			$this -> DBconnector();

			$this -> ifthd -> skin -> set_section('Ticket Control');
			$this -> ifthd -> skin -> set_description('Manage your tickets,  departments, custom department fields and canned replies.');

			switch( $this->ifthd->input['code'] )
			{
				case 'country' :
					$this -> list_country();
					break;
				default :
					$this -> list_stats();
					break;
			}
		}

		function DBconnector()
		{
			// Create connection
			$dbhandle = mysql_connect('localhost', 'louiscruises', 'magicj') or die("Unable to connect to MySQL");
			//echo "Connected to MySQL<br>";

			//select a database to work with
			$selected = mysql_select_db("louiscruise", $dbhandle) or die("Could not select examples");
		}

		function list_stats()
		{
			/*
			 * Calendar
			 */

			$query = "SELECT `settings` FROM `countries` WHERE `code` = 'en' LIMIT 1";

			$result = mysql_query($query);

			while ( $row = mysql_fetch_array($result) )
				$settings = $row[settings];

			$settings = json_decode($settings, true);

			$days = $settings[calendars][standard][days][names];

			$curr_year_month = date('Y-m', time());

			$curr_date = date('Y-m-d', time());

			/*
			 * Visits
			 */

			$query = "SELECT * FROM `countdetail` WHERE `date` LIKE \"%$curr_year_month%\"";

			$result = mysql_query($query);

			$data = array();

			while ( $row = mysql_fetch_array($result) )
			{
				$data[] = $row;
			}

			$numItems = count($days);
			$i = 0;

			foreach ( $data as $key1  => $track )
			{
				foreach ( $days as $key2  => $day )
				{
					if ( $key2 == $track[day] )
					{
						$store[$key2][] = $track;
					}
				}
			}

			/* By Date */

			$query = "SELECT `date`, COUNT(id) AS `count` FROM `countdetail` id GROUP BY id.date";

			$result = mysql_query($query);

			while ( $row = mysql_fetch_assoc($result) )
				$count_by_date[] = $row;
			/*
			 *
			 * Array
			 (
			 [0] => Array
			 (
			 [date] => 2013-07-10
			 [count] => 2989
			 )

			 [1] => Array
			 (
			 [date] => 2013-07-11
			 [count] => 3644
			 )
			 *
			 *
			 */
			$c = 0;
			$num = count($count_by_date);
			$res_html = '';

			foreach ( $count_by_date as $filter )
			{
				if ( ++$c !== $num )
				{
					$res_html .= "['" . $filter['date'] . "', " . $filter['count'] . "], ";
				}
				else
				{
					$res_html .= "['" . $filter['date'] . "', " . $filter['count'] . "]";
				}
			}

			//$res_html = "['2008-08-12 4:00PM',4], ['2008-09-12 4:00PM',5]";

			/*
			 * Bookings
			 */

			$query = "SELECT `country`, `day` FROM `countdetail` WHERE `visits` LIKE \"%AUTHORISED%\" AND `date` LIKE \"%$curr_year_month%\"";

			$result = mysql_query($query);

			while ( $row = mysql_fetch_array($result) )
			{
				$bookings[] = $row;
			}

			$auth = array();

			foreach ( $bookings as $key1  => $booking )
			{
				foreach ( $days as $key2  => $day )
				{
					if ( $key2 == $booking[day] )
					{
						$auth[$key2][] = $booking;
					}
				}
			}

			/*
			 * Schema collection
			 */

			$all_days = '';
			$count_tracks = '';

			foreach ( $days as $key  => $day )
			{
				if ( ++$i !== $numItems )
				{
					$all_days .= "\"$day\",";
					$count_tracks .= count($store[$key]) . ",";
					$count_books .= count($auth[$key]) . ",";

					$data_array .= "[" . $key . ", " . count($store[$key]) . "], ";
				}
				else
				{
					$all_days .= "\"$day\"";
					$count_tracks .= count($store[$key]);
					$count_books .= count($auth[$key]);

					$data_array .= "[" . $key . ", " . count($store[$key]) . "]";
				}
			}

			/*

			 $this -> output = '
			 <canvas id="canvas" height="450" width="980"></canvas>
			 ';

			 $this -> output .= '<script type="text/javascript">
			 var lineChartData = {
			 labels : [' . $all_days . '],
			 datasets : [
			 {
			 fillColor : "rgba(220,220,220,0.5)",
			 strokeColor : "rgba(220,220,220,1)",
			 pointColor : "rgba(220,220,220,1)",
			 pointStrokeColor : "#fff",
			 data : [' . $count_books . ']
			 },
			 {
			 fillColor : "rgba(151,187,205,0.5)",
			 strokeColor : "rgba(151,187,205,1)",
			 pointColor : "rgba(151,187,205,1)",
			 pointStrokeColor : "#fff",
			 data : [' . $count_tracks . ']
			 }
			 ]
			 }

			 var myLine = new Chart(document.getElementById("canvas").getContext("2d")).Line(lineChartData);
			 </script>
			 ';
			 *
			 */

			$this -> output .= '
							<div id="chart" style="width:700px; height:300px"> </div>
							
							<script class="code" type="text/javascript">
							    jQuery(document).ready(function()
							    {
								  var s1 = [' . $res_html . '];
									
								  plot1 = jQuery.jqplot(\'chart\', [s1], {
								      title: \'Default Date Axis\',
								      axes:{
									        xaxis:{
									            renderer:jQuery.jqplot.DateAxisRenderer,
									            tickOptions:{formatString:\'%b %#d, %#I\'},
									        }
								    	},
								     series:[{lineWidth:4, markerOptions:{style:\'square\'}}]
								  });
								});
							</script>
							<script src="<! HD_URL !>/includes/dist/plugins/jqplot.dateAxisRenderer.min.js" type="text/javascript"></script>
			';

			$this -> ifthd -> skin -> add_output($this -> output);

			$this -> ifthd -> skin -> do_output(array(
				'nav'  => $this -> nav,
				'title'  => 'View Stats'
			));
		}

		function list_country()
		{

			$res_country_html = '';
			$res_date_html = '';

			$query = "SELECT `country`, `date`, COUNT(id) AS `count` FROM `countdetail` id GROUP BY id.country";

			$result = mysql_query($query);

			while ( $row = mysql_fetch_assoc($result) )
				$count_by_country[] = $row;

			$num = count($count_by_date);
			$c = 0;

			foreach ( $count_by_country as $filter )
			{
				if ( $filter['count'] >= 300 )
				{
					$res_country_html .= "['" . $filter['country'] . "'," .  $filter['count'] . "], ";
					$res_date_html    .= "'" . $filter['date'] . "', ";
					$res_count_html   .= "'" . $filter['count'] . "', ";
				}
			}
			
			//$res_country_html = "[2002, 112000], [2003, 122000], [2004, 104000], [2005, 99000], [2006, 121000],[2007, 148000], [2008, 114000], [2009, 133000], [2010, 161000], [2011, 173000]";
			//$res_date_html = "[2002, 10200], [2003, 10800], [2004, 11200], [2005, 11800], [2006, 12400],[2007, 12800], [2008, 13200], [2009, 12600], [2010, 13100]";

			$this -> output .= '
							<div id="chart4" style="width:900px; height:500px"> </div>
							
							<script class="code" type="text/javascript">
								jQuery(document).ready(function(){
								
								    var line = [' . $res_country_html . '];
								 	
								    var line2 = [' . $res_country_html . '];
								 
								    var plot4 = jQuery.jqplot(\'chart4\', [line, line2], {
								        title: \'Concern vs. Occurrance\',
								        series:[{renderer: jQuery.jqplot.BarRenderer}, {xaxis:\'x2axis\', yaxis:\'y2axis\'}],
								        axes: {
								            xaxis: {
								                renderer: jQuery.jqplot.CategoryAxisRenderer,
								                label: \'Warranty Concern\',
								                labelRenderer: jQuery.jqplot.CanvasAxisLabelRenderer,
								                tickRenderer: jQuery.jqplot.CanvasAxisTickRenderer,
								                tickOptions: {
								                    angle: 30
								                }
								            },
								            x2axis: {
								                renderer: jQuery.jqplot.CategoryAxisRenderer,
								                label: \'\',
								                labelRenderer: jQuery.jqplot.CanvasAxisLabelRenderer,
								                tickRenderer: jQuery.jqplot.CanvasAxisTickRenderer,
								                tickOptions: {
								                    angle: 30
								                }
								            },
								            yaxis: {
								                autoscale:true,
								                label: \'Occurance\',
								                labelRenderer: jQuery.jqplot.CanvasAxisLabelRenderer,
								                tickRenderer: jQuery.jqplot.CanvasAxisTickRenderer,
								                tickOptions: {
								                    angle: 30
								                }
								            },
								            y2axis: {
								                autoscale:true,
								                label: \'Bookings\',
								                labelRenderer: jQuery.jqplot.CanvasAxisLabelRenderer,
								                tickRenderer: jQuery.jqplot.CanvasAxisTickRenderer,
								                tickOptions: {
								                    angle: 30
								                }
								            }
								           }
								    });
								});
							</script>	
							<script type="text/javascript" src="<! HD_URL !>/includes/dist/plugins/jqplot.logAxisRenderer.min.js"></script>
							<script type="text/javascript" src="<! HD_URL !>/includes/dist/plugins/jqplot.canvasTextRenderer.min.js"></script>
							<script type="text/javascript" src="<! HD_URL !>/includes/dist/plugins/jqplot.canvasAxisLabelRenderer.min.js"></script>
							<script type="text/javascript" src="<! HD_URL !>/includes/dist/plugins/jqplot.canvasAxisTickRenderer.min.js"></script>
							<script type="text/javascript" src="<! HD_URL !>/includes/dist/plugins/jqplot.dateAxisRenderer.min.js"></script>
							<script type="text/javascript" src="<! HD_URL !>/includes/dist/plugins/jqplot.categoryAxisRenderer.min.js"></script>
							<script type="text/javascript" src="<! HD_URL !>/includes/dist/plugins/jqplot.barRenderer.min.js"></script>
			';

			$this -> ifthd -> skin -> add_output($this -> output);

			$this -> ifthd -> skin -> do_output(array(
				'nav'  => $this -> nav,
				'title'  => 'View Stats By Country'
			));
		}

	}
?>
