<?php

$depart = $_GET['depart'];

$action = $_GET['action'];

if($action == 'form')
{
	$url = 'http://help.marketers.gr/index.php?act=tickets&code=open&step=2&department=' . $depart;
}
elseif($action == 'knowledge_base')
{
	$url = 'http://help.marketers.gr/index.php?act=kb';
}


?>
<!DOCTYPE html>
<html>
<head>
	<title>Test Document</title>
	<style>
		@import url(http://www.marketers.gr/scripts/kendo/styles/kendo.common.min.css);
		@import url(http://www.marketers.gr/scripts/kendo/styles/kendo.metro.min.css);
		@import url(http://www.marketers.gr/css/site/style.css);
		@import url(http://www.marketers.gr/css/site/grid.css);
		@import url(http://www.marketers.gr/css/site/booking.css);
		/*@import url(style.css);*/
	</style>
	<script src='http://help.marketers.gr/includes/scripts/jquery-1.7.2.min.js' type='text/javascript'></script>
	<script src="http://help.marketers.gr/includes/scripts/kendo/kendo.web.min.js"></script>
	<script src="http://help.marketers.gr/includes/scripts/kendo/console.js"></script>
	<script type="text/javascript" src="js/script.js"></script>
</head>

<body>
<script type="text/javascript">
			var xmlhttp;
			if (window.XMLHttpRequest)
			{
				xmlhttp = new XMLHttpRequest();
			} 
			else 
			{
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange = function() {
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
					document.getElementById("load-knowledge-base").innerHTML = xmlhttp.responseText;
				}
			}
			xmlhttp.open("GET", "<?php print $url; ?>&view=component&tpl=louis", true);
			xmlhttp.send();
		</script>
<div id="results"></div>
<div class="wrapperHeader">
	<div class="container_14 containerHeader">
		<div id="header">
			<div class="grid_4 languages">
				<span class="text1">
					<label>Country:</label>
					<strong>England</strong>
				</span>
				<div class="text2">
					<label>Language:</label>
					<span>
						<strong><img width="20" height="11" alt="" id="country-flag" src="http://www.marketers.gr/images/flags/en.png"></strong>
						<select class="select-top" id="all-countries" name="">
							<option selected="selected" value="en" data-url="en/bookNow.php">English</option>
							<option value="gr" data-url="gr/bookNow.php">Greek</option>
						</select>
					</span>
				</div>
			</div>
			<div class="grid_12 menu">
				<h1 class="grid_2 logo"> <a href="/en/index.html"><img width="180" height="100" alt="img" src="http://www.marketers.gr/images/site/logo.png"></a> </h1>
				<div>
					<div class="nav">
						<ul>
							<li data-id="42" class="menu42"> <a class=" " title="Plan Your Cruise" href="#"> <strong> <i class="img1 "></i> <span class="cont1 ">Plan Your Cruise</span> </strong> </a> </li>
							<li data-id="43" class="menu43"> <a class=" " title="Welcome On Board" href="#"> <strong> <i class="img1 img2"></i> <span class="cont1 cont2">Welcome On Board</span> </strong> </a> </li>
							<li data-id="44" class="menu44"> <a class=" " title="Customer Support" href="#"> <strong> <i class="img1 img3"></i> <span class="cont1 cont3">Customer Support</span> </strong> </a> </li>
						</ul>
					</div>
					<div class="search"> <span class="search-box">
						<input type="text" value="enter your search ..." class="field" name="">
						</span>
						<input type="button" class="butn" value="SEARCH" name="">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="wrapper">
	<div class="container_12 container">
		<div class="grid_12 main_content">
			<div class="wrap">
				<div class="grid_12 main_content">
					<div class="left_column">
						<div class="booking">
							<div id="load-knowledge-base"></div>
							<div id="load-knowledge-base-categories"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Window HTML -->
<div id="window" style="display:none; top:40px;"></div>
<div class="k-overlay" style="display: none; z-index: 10002; opacity: 0.5;"></div>

</body>
</html>