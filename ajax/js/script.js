$(document).ready(function()
{	
	$('.row1-mini a, .row2-mini a').live( "click", function(event) {
		event.preventDefault()
	})

	$('.subbox a').live( "click" , function(event)
	{
		event.preventDefault()
		$process = $(this).attr('href')
			
		$.post($process + '&view=content', function(data)
		{
			$('#load-knowledge-base-categories').empty().append(data)
		})
	})
	
	$('form').live("submit", function(e)
	{
		e.preventDefault();

		$process = $(this).attr('action')

		$.post($process, $(this).serialize(), function(data)
		{
			if ($(data).find('.critical').length != 0)
			{	
				 // update captcha
				 var jqt = $('#captcha');
			     var src = jqt.attr('src');
			     
			     src =  src + '?_ts=' + new Date().getTime(); //recaching src
			     jqt.fadeOut(400)
			     setTimeout(function(){
			     	jqt.attr('src',src);
			     }, 400)
			     jqt.fadeIn(400)
				
				//get message
				$message = $(data).find('.critical').text()
				$error   = '<div class="message critical" style="display:none;">' + $message + '</div>';
				$('#results').empty().append($error)
				$('.critical').fadeIn(400)
				
				//window.empty().append($message)
				//window.data("kendoWindow").open();
			
				setTimeout(function()
				{
					//window.data("kendoWindow").close();
				} ,2000)
			} 
			else
			{
				//$success = $(data).find('.alert').text()
				$done = '<div class="message alert" style="display:none;">Ticket Submited</div>';
				$('#results').empty().append($done)
				$('.alert').fadeIn(400)
			}
		});
		return false;
	});
	
	//init after ajax loading
	$('#load-knowledge-base').ajaxComplete(function(e, xhr, settings)
	{
		initDatepicker( '.datepicker' )
		
		var start = $(".date-start").kendoDatePicker({
			// defines the start view
			start : "year",
			// defines when the calendar should return date
			depth : "year",
			change : startChange
		}).data("kendoDatePicker");
		
		var end = $(".date-end").kendoDatePicker({
			// defines the start view
			start : "year",
			// defines when the calendar should return date
			depth : "year",
			change : endChange
		}).data("kendoDatePicker");
		
		start.max(end.value());
		end.min(start.value());
		
		function startChange() {
		var startDate = start.value();
	
		if (startDate) {
				startDate = new Date(startDate);
				startDate.setDate(startDate.getDate() + 1);
				end.min(startDate);
			}
		}
		
		function endChange() {
			var endDate = end.value();
		
			if (endDate) {
				endDate = new Date(endDate);
				endDate.setDate(endDate.getDate() - 1);
				start.max(endDate);
			}
		}
	});
	
	function initDatepicker($selector) {
	// create DatePicker from input HTML element
		$($selector).kendoDatePicker()
	}    
})