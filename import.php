<?php

	session_start();
	
	/*# ================================= #*/
	/*# ======	DECLARE VARIABLES  =====  #*/
	/*# ================================= #*/
	
	$username = 'root';
	$password = 'root';
	
	$random1 = 'secret_key1';
	$random2 = 'secret_key2';
	
	$hash = md5($random1 . $pass . $random2);
	
	$self = $_SERVER['REQUEST_URI'];

	/*# ================================= #*/
	/*# ========= USER LOGOUT  ========== #*/
	/*# ================================= #*/

	if (isset($_GET['logout']))
	{
		unset($_SESSION['login']);
	}
	
	/*# ================================= #*/
	/*# ============ SETTINGS  ========== #*/
	/*# ================================= #*/
	
	$host = 'localhost';
	$port = '3306';
?>
<!DOCTYPE html>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Import Data</title>

<style>
	@import url("import/css/bootstrap.min.css");
	@import url("import/css/theme.css");
</style>

<script src="http://code.jquery.com/jquery.js"></script>
<script src="import/js/bootstrap.min.js"></script>

<body>
	
<?php if (isset($_SESSION['login']) && $_SESSION['login'] == $hash) {
	 
	navbar($username)
?>

<div class="container-fluid">
<?php
		if( isset($_POST['submit']) )
		{ 
			
			$name  = $_POST['database'];
			
			$link = mysql_connect( $host . ':' . $port, $_POST['datauser'], $_POST['datapass'] );
			
			if (!$link) {
			?>	
			<a href="<?php echo $self; ?>" class="btn">Try again!</a>
			<br /><br />
			<div class="alert alert-error">
			    <?php die('Could not connect: ' . mysql_error()); ?>
			</div>
			<?php
		}
		mysql_select_db($name, $link);
		mysql_query( "SET NAMES 'utf8'" );
		
		$message = <<<EOF
		
		<div class="alert alert-success">
			Connected <strong>successfully</strong><br /> Selected Database : <strong> {$name} </strong>
		</div>
EOF;
		$csv_contents = parse_csv_file("import/files/france.csv");
	
		/*
		 echo '<div class="container-fluid">';
		 echo '<pre class="span8">';
		 print_r($csv_contents);
		 echo '</pre>';
		 echo '</div>';
		 */
	
		/* ================================== */
		/* ===== Update Languages Table  ==== */
		/* ================================== */
	
		$no_updates_arr = array();
	
		foreach ($csv_contents as $row)
		{
			if ($row['value'] !== '')
			{
				$sql_update = "UPDATE `languages` SET `value` = '$row[value]' WHERE `name` = '$row[name]' AND `code` = 'fr'";
				mysql_query($sql_update);
				$updates_arr[] = $row;
			}
			else
			{
				$no_updates_arr[] = $row;
			}
		}
		?>
		<div class="alert">
			<?php printf("Now updated languages: %d\n", mysql_affected_rows()); ?>
		</div>
		<?php

		/* ================================ */
		/* ===== Fetch from Database  ===== */
		/* ================================ */
	
		$sql_query = "SELECT * FROM `languages` WHERE `code` = 'fr'";
	
		$result = mysql_query($sql_query);
	
		while ($data = mysql_fetch_array($result, MYSQL_ASSOC))
		{
			$languages[] = $data;
		}
?>

<?php echo $message ?>
</div>
	<div class="container-fluid">
		<div class="row-fluid">
			<?php display_sidebar() ?>
			<div class="span9">
				<div class="block">
					<div class="block-heading">EMPTY VALUES</div>
					<div class="block-body">
						<table class="table table-striped alert-error">
							<tr>
								<th>Variables</th>
							</tr>
							<?php foreach( $no_updates_arr as $key => $no_update ) : ?>
							<tr>
								<td><?php print $no_update[name]; ?></td>
							</tr>
							<?php endforeach; ?>
						</table>
					</div>
				</div>
				<div class="block">
					<div class="block-body">
						<legend>UPDATED DATA</legend>
						<table class="table table-striped alert-success">
							<tr>
								<th>Variable</th>
								<th>Value</th>
							</tr>
							<?php foreach( $updates_arr as $key => $update ) : ?>
							<tr>
								<td><?php print $update[name]; ?></td>
								<td><?php print $update[value]; ?></td>
							</tr>
							<?php endforeach; ?>
						</table>
					</div>
				</div>
				<div class="block">
					<div class="block-body">
						<legend>Data Table</legend>
						<table class="table table-striped">
							<tr>
								<th>#</th>
								<th>code</th>
								<th>name</th>
								<th>value</th>
							</tr>
							<?php foreach( $languages as $key => $language ) : ?>
							<tr>
								<td><?php print $language['id']; ?></td>
								<td><?php print $language['code']; ?></td>
								<td><?php print $language['name']; ?></td>
								<td><?php print $language['value']; ?></td>
							</tr>
							<?php endforeach; ?>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php
	}
	else
	{
?>
	<div class="container-fluid">
		<div class="row-fluid">
			<?php display_sidebar() ?>
		</div>
	</div>
<?php } ?>

<?php

	} //if login
	else if (isset($_POST['submit']))
	{
		if ($_POST['username'] == $username && $_POST['password'] == $password)
		{
			//IF USERNAME AND PASSWORD ARE CORRECT SET THE LOG-IN SESSION
			$_SESSION["login"] = $hash;
			header("Location: $_SERVER[PHP_SELF]");
		} 
		else 
		{
			// DISPLAY FORM WITH ERROR
			echo '<div class="alert alert-error">Username or password is invalid</div>';
			display_login_form();
		}
	}
	else 
	{
	display_login_form();
	}
?>

</body>
</html>

<?php 
	function parse_csv_file($csvfile) 
	{
	    $csv = Array();
	    $rowcount = 0;
	    if (($handle = fopen($csvfile, "r")) !== FALSE) 
	    {
	        $max_line_length = defined('MAX_LINE_LENGTH') ? MAX_LINE_LENGTH : 10000;
	        $header = fgetcsv($handle, $max_line_length);
	        $header_colcount = count($header);
	        while (($row = fgetcsv($handle, $max_line_length)) !== FALSE) 
	        {
	            $row_colcount = count($row);
	            if ($row_colcount == $header_colcount) 
	            {
	                $entry = array_combine($header, $row);
	                $csv[] = $entry;
	            }
	            else 
	            {
	                error_log("csvreader: Invalid number of columns at line " . ($rowcount + 2) . " (row " . ($rowcount + 1) . "). Expected=$header_colcount Got=$row_colcount");
	                return null;
	            }
	            $rowcount++;
	        }
	        //echo "Totally $rowcount rows found\n";
	        fclose($handle);
	    }
	    else {
	        error_log("csvreader: Could not read CSV \"$csvfile\"");
	        return null;
	    }
	    return $csv;
	}
	
	function dirToArray($dir) 
	{
	   $result = array();
	
	   $cdir = scandir($dir);
	   foreach ($cdir as $key => $value)
	   {
	      if (!in_array($value,array(".","..")))
	      {
	         if (is_dir($dir . DIRECTORY_SEPARATOR . $value))
	         {
	            $result[$value] = dirToArray($dir . DIRECTORY_SEPARATOR . $value);
	         }
	         else
	         {
	            $result[] = $value;
	         }
	      }
	   }
	   return $result;
	} 
	
	function display_login_form()
	{
	$output = <<<EOF
	<div class="container">
		<div class="content">
			<div class="row">
				<div class="login-form well">
					<h2>Login</h2>
					<form action="" method="post">
						<fieldset>
							<div class="clearfix">
								<input type="text" name="username" id="username" placeholder="Username">
							</div>
							<div class="clearfix">
								<input type="password" name="password" id="password" placeholder="Password">
							</div>
							<input type="submit" class="btn" name="submit" value="Sign in">
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
EOF;
  	echo $output;
	}
	function display_sidebar()
	{
		
	$files = dirToArray('import/files');
	
	$options = '';
	
	foreach ($files as $file)
	{
		$options .= "<option>$file</option>\n";
	}
	
	$output = <<<EOF
		<div class="span3">
		<div class="block">
			<div class="block-heading">Import Options</div>
			<div class="block-body">
				<form action="" method="post">
					<fieldset class="control-group">
						<label>Select Database</label>
						<select name="database">
							<option name="temp">temp</option>
							<option name="louiscruise">louiscruise</option>
						</select>
					</fieldset>
					<fieldset class="control-group">
						<label>Database user</label>
						<input type="text" name="datauser" placeholder="Username" />
					</fieldset>
					<fieldset class="control-group">
						<label>Database pass</label>
						<input type="password" name="datapass" placeholder="Password" />
					</fieldset>
					<fieldset class="control-group">
						<label>Table</label>
						<label class="radio inline">
							<input type="radio" selected="selected" />
							languages</label>
					</fieldset>
					<fieldset class="control-group">
						<label>Select file</label>
						<select>
							{$options}
						</select>
					</fieldset>
					<fieldset class="control-group">
						<input type="submit" class="btn btn-success" name="submit" value="GO" />
					</fieldset>
				</form>
			</div>
		</div>
	</div>
EOF;
	echo $output;
	}
	
	function navbar($username)
	{
		$output = <<<EOF
		
		<div class="navbar">
			<div class="navbar-inner">
				<div class="container-fluid">
					<p class="brand">Hello <strong>{$username}</strong>, you have successfully logged in!</p>
					<div class="nav pull-right">
					<a href="?logout=true">Logout?</a>
					</div>
				</div>
			</div>
		</div>
EOF;
	echo $output;
	}
	?>