<?php
header('Content-type: text/css');
ob_start("compress");

function compress($minify)
{
    /* remove comments */
    $minify = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $minify);

    /* remove tabs, spaces, newlines, etc. */
    $minify = str_replace(array(
        "\r\n",
        "\r",
        "\n",
        "\t",
        '  ',
        '    ',
        '    '
    ), '', $minify);

    return $minify;
}

/* css files for combining */
include ('bootstrap-3.css');
include ('reset.css');
include ('main.css');
include ('lightbox.css');
include ('flags.css');
include ('cookiecuttr.css');
include ('jquery.ui.dialog.min.css');
include ('jquery.ui.resizable.min.css');
include ('date-picker.css');
include ('override.css');
ob_end_flush();
?>