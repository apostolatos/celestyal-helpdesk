function langsHandler() {
    if ($('#langs').is(':visible')) {
        $('#langs').fadeOut();
    } else {
        $('#langs').fadeIn();
    }

    langs_array = [];

    ul = document.getElementById('langs');
    ul_childs = ul.getElementsByTagName('li');

    for (i = 0; i < ul_childs.length; i++) {
        langs_array[i] = ul_childs[i].children[0];

        $(langs_array[i]).bind('click', function(e) {
            $('#lang_btn').removeClass();
            $('#lang_btn').toggleClass(e.target.className);
            $('#langs').fadeOut();

            // Redirect User using data-url attribute
            window.location.href = e.target.getAttribute('data-url')
        });
    }
}

$(document).ready(function() {
    $('#langs').css('display', 'none');
    $('#langs').fadeOut();
   // $('#lang_btn').addClass('el');
    /*
        i18n_btn = document.getElementById('lang_btn');
        i18n_btn.on('click', langsHandler, false);
        */

    $('#lang_btn').on('click', function() {
        langsHandler();
    });

});