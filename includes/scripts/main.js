/* CUSTOM FUNCTIONS
 ----------------------------- */
var currentTallest = 0, currentRowStart = 0, rowDivs = new Array(), topPosition = 0;

function equalHeights(query) {

    $(query).each(function() {
        $el = $(this);
        topPostion = $el.position().top;
        console.log(topPostion);
        if (currentRowStart != topPostion) {

            // we just came to a new row.  Set all the heights on the completed row
            for ( currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
                rowDivs[currentDiv].height(currentTallest);
            }

            // set the variables for the new row
            rowDivs.length = 0;
            // empty the array
            currentRowStart = topPostion;

            currentTallest = $el.height();
            rowDivs.push($el);

        } else {

            // another div on the current row.  Add it to the list and check if it's taller
            rowDivs.push($el);
            currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);

        }

        // do the last row
        for ( currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
            rowDivs[currentDiv].height(currentTallest);
        }

    });

}

function toggleNav() {

    if ($('#site-wrapper').hasClass('show-nav')) {
        // if browser doesn't support CSS3 Transforms
        $('#site-wrapper').css('margin-right', '0px');
        $('#site-wrapper').removeClass('show-nav');
    } else {
        // if browser doesn't support CSS3 Transforms
        $('#site-wrapper').css('margin-right', '-260px');
        $('#site-wrapper').addClass('show-nav');
    }

}

/* CLOSE ON ESQ KEY
 ---------------------- */
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        /* CLOSE MOBILE  MENU */
        if ($('#site-wrapper').hasClass('show-nav')) {
            // trigger toggle-nav's click
            $('.toggle-nav').trigger('click');
        }
        /* CLOSE POPUPS */
        if ($(".modal").data()['bs.modal'].isShown) {
            $(".modal").modal('hide');
        }
    }

});

/* ON DOCUMENT READY
 --------------------------- */

( function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/el_GR/all.js#xfbml=1&appId=188422644620707";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
/* */
function langsHandler() {
    if ($('#langs').is(':visible')) {
        $('#langs').fadeOut();
    } else {
        $('#langs').fadeIn();
    }
    langs_array = [];
    ul = document.getElementById('langs');
    ul_childs = ul.getElementsByTagName('li');
    for ( i = 0; i < ul_childs.length; i++) {
        langs_array[i] = ul_childs[i].children[0];
        $(langs_array[i]).bind('click', function(e) {
            $('#lang_btn').removeClass();
            $('#lang_btn').toggleClass(e.target.className);
            $('#langs').fadeOut();
            // Redirect User using data-url attribute
            window.location.href = e.target.getAttribute('data-url')
        });
    }
}

head.ready(function() {
    
    $('.sliderInner').width($(window).width());
    
    $('.dropdown.yamm-fw').click(function(e) {
        console.log($(this));
        if ($(this).hasClass('open')) {
            $('.breadcrumb').show();
        } else {
            $('.breadcrumb').hide();
        }
    });

    $('.container-fluid').on('click', function() {
        $('.breadcrumb').show();
    })

    $('form#searchform').submit(function(event) {
        event.preventDefault();
        // ajax code goes here to submit data
    });

    $('.Search-btn').on('click', function(e) {
        search();
    });

    $('.title-search').pressEnter(function() {
        search();
    });

});

$.fn.pressEnter = function(fn) {
    return this.each(function() {
        $(this).bind('enterPress', fn);
        $(this).keyup(function(e) {
            if (e.keyCode == 13) {
                $(this).trigger("enterPress");
            }
        });
    });
};

function search() {
    var input = $('.title-search').val();

    setTimeout(function() {
        var action = $('input[name="defaultAction"]').val();

        var newAction = action + 'title/' + input + '/description/' + input + '.html';

        $('form#searchform').attr('action', newAction);

        $.post(newAction).done(function(data) {
            $('#results').html(data);
        });
    }, 20)
}

function fastEmbed() {
    if (!document.getElementsByClassName) {
        // If IE8
        var getElementsByClassName = function(node, classname) {
            var a = [];
            var re = new RegExp('(^| )' + classname + '( |$)');
            var els = node.getElementsByTagName("*");
            for (var i = 0, j = els.length; i < j; i++)
                if (re.test(els[i].className))
                    a.push(els[i]);
            return a;
        }
        var videos = getElementsByClassName(document.body, "youtube");
    } else {
        var videos = document.getElementsByClassName("youtube");
    }
    var nb_videos = videos.length;
    for (var i = 0; i < nb_videos; i++) {
        // Finf youtube video thumbnail id
        videos[i].style.backgroundImage = 'url(http://i.ytimg.com/vi/' + videos[i].id + '/sddefault.jpg)';
        // Custom Play icon as overlay
        var play = document.createElement("div");
        play.setAttribute("class", "play");
        videos[i].appendChild(play);
        videos[i].onclick = function() {
            // Create iframe with autoplaytrue
            var iframe = document.createElement("iframe");
            var iframe_url = "https://www.youtube.com/embed/" + this.id + "?autoplay=1&autohide=1";
            if (this.getAttribute("data-params"))
                iframe_url += '&' + this.getAttribute("data-params");
            iframe.setAttribute("src", iframe_url);
            iframe.setAttribute("frameborder", '0');
            // The height and width of the iFrame should be the same as parent
            iframe.style.width = this.style.width;
            iframe.style.height = this.style.height;
            // Replace the YouTube thumbnail with YouTube Player
            this.parentNode.replaceChild(iframe, this);
        }
    }
}

'use strict';
function r(f) {
    /in/.test(document.readyState) ? setTimeout('r(' + f + ')', 9) : f()
}

r(function() {
    fastEmbed();
});

function clearCheckBoxes() {
    document.querySelectorAll('input')[0].checked = false;
}

$(function() {
    $('.toggle-nav').on('click', function(e) {
        toggleNav();
    });
    $('#mobile-menu .dropdown-menu a').on('click', function(e) {
        // trigger toggle-nav's click
        $('.toggle-nav').trigger('click');
    });

    var screenHeight = window.screen.availHeight;
    $('#vid-container').css({
        'height' : screenHeight
    });

    /* keeps dropdown with keep_open class open on click */
    $(document).on('click', '.dropdown-menu', function(e) {
        $(this).hasClass('keep_open') && e.stopPropagation();

    });
    $('.dropdown.keep-open').on({
        "shown.bs.dropdown" : function() {
            this.closable = false;
        },
        "click" : function() {
            this.closable = true;
        },
        "hide.bs.dropdown" : function() {
            return this.closable;
        }
    });

    $('#filters > .btn-group > button').click(function() {
        $('#filters > .btn-group').removeClass('selected');
        $(this).parent().toggleClass('selected');
    });

    $('#tabs > .btn-group > a').click(function() {
        $('#tabs > .btn-group').removeClass('selected');
        $(this).parent().toggleClass('selected');
    });

    /* KEEP MODALS ON TOP */
    $('#search').on('shown.bs.modal', function(e) {
        $('#search').appendTo("body").modal('show');
    });
    $('#book1').on('shown.bs.modal', function(e) {
        $('#book1').appendTo("body").modal('show');
    });

});
