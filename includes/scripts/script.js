function initDatepicker($selector)
{
	// create DatePicker from input HTML element
	jQuery($selector).kendoDatePicker();
}

function copy_from_input()
{
	jQuery('input[name="name"]').keyup(function()
	{
		$val = jQuery(this).val().replace(/\s+/g, '_').toLowerCase();

		jQuery('input[name="fkey"]').val($val);
		
		jQuery.post("admin.php?section=tools&act=ajax&code=search_field", { key : $val}, function(data)
		{
			if(data.length > 0)
			{
				jQuery('input[name="fkey"]').val(data);
			}
		});
		
	});
}

function serializeData()
{
	var fixHelper = function(e, ui)
	{
		ui.children().each(function()
		{
			jQuery(this).width(jQuery(this).width());
		});
		return ui;
	};

	jQuery("#acppage table tbody tr").hover(function()
	{
		jQuery(this).addClass('hover');

	}, function()
	{
		jQuery(this).removeClass('hover');
	})

	jQuery("#acppage table tbody").sortable(
	{
		opacity : 0.6,
		cursor : 'move',
		handle : '.handler',
		placeholder : 'placeholder',
		helperclass : 'sortHelper',
		activeclass : 'sortableactive',
		helper : fixHelper,
		'start' : function(event, ui)
		{
			ui.placeholder.html('<td colspan="7">&nbsp;</td>');
		},
		'change' : function(e, ui)
		{

		},
		'update' : function()
		{

			var order = jQuery(this).sortable("serialize") + '&action=updateRecordsListings';
			jQuery.post("admin.php?section=tools&act=ajax", order, function(theResponse)
			{
				if (jQuery(".appended").length == 0)
				{
					jQuery("#acppage .groupbox").before('<div style="display:none;" class="alert appended"><div class="msg-icon"></div>A new order has been changed.</div>');
					jQuery(".appended").fadeIn(1200);
				}
				else
				{
					jQuery(".appended").css('display', 'none').fadeIn(1200);
				}
			});

			console.log(order)
		}
	});

	jQuery("#acppage table tbody").disableSelection();
}