head.ready ( function ( )
{

	/* ============================== */
	/* === Get current language ===== */
	/* ============================== */

	var window = $ ( '#loading' );

	var domain = document.URL;
	url_parts = domain.replace ( /\/\s*$/ , '' ).split ( '/' );
	url_parts.shift ( );

	curr_language = url_parts [ 2 ];

	$ ( 'select#cdf_country option' ).each ( function ( )
	{
		var text = $ ( this ).text ( );
		$ ( this ).attr ( 'value' , $.trim ( text ) );
	} );

	/* ============================== */
	/* ====== Custom Validation ===== */
	/* ============================== */

	var validator = $ ( '#myDiv' ).kendoValidator ( ).data ( 'kendoValidator' ) ,
	    status = $ ( '.status' );

	$ ( 'a.k-button' ).click ( function ( e )
	{
		setTimeout ( function ( )
		{
			if ( $ ( '.alert-danger' ).length > 0 )
			{
				var pos = $ ( '.alert-danger:first' );

				$ ( 'html,body' ).animate (
				{
					scrollTop : pos.offset ( ).top
				} , 'slow' );
			}
		} , 100 );
	} );
	
	$ ( '#cdf_country' ).change ( function ( )
	{
		var value = $ ( '#cdf_country option:selected' ).attr ( 'value' );

		if ( value == 'Canada' )
		{
			$ ( '#cdf_canada_provinces' ).parents ( '.field' ).slideDown ( );
			$ ( '#cdf_canada_provinces' ).parents ( '.field' ).removeClass ( 'hidden' );
			
			if ( $ ( '#cdf_canada_provinces' ).hasClass ( 'hidden' ) )
			{
				$ ( '#cdf_canada_provinces' ).removeClass ( 'hidden' );
			}
		}
		else
		{
			$ ( '#cdf_canada_provinces' ).parents ( '.field' ).slideUp ( );
			$ ( '#cdf_canada_provinces' ).parents ( '.field' ).addClass ( 'hidden' );
			$ ( '#cdf_canada_provinces' ).addClass ( 'hidden' );
		}

		if ( value == 'United States' )
		{
			$ ( '#cdf_united_states' ).parents ( '.field' ).slideDown ( );
			$ ( '#cdf_united_states' ).parents ( '.field' ).removeClass ( 'hidden' );

			if ( $ ( '#cdf_united_states' ).hasClass ( 'hidden' ) )
			{
				$ ( '#cdf_united_states' ).removeClass ( 'hidden' );
			}
		}
		else
		{
			$ ( '#cdf_united_states' ).parents ( '.field' ).slideUp ( );
			$ ( '#cdf_united_states' ).parents ( '.field' ).addClass ( 'hidden' );
			$ ( '#cdf_united_states' ).addClass ( 'hidden' );
		}
	} );
	
	/* ============================== */
	/* ======== Submit Ticket ======= */
	/* ============================== */
	
	$ ( '.next' ).click ( function ( e )
	{
		var depart = $ ( 'input[name="department"]' ).attr ( 'value' );
		
		e.preventDefault ( );
		
		$ ( 'form[name="ticket"]' ).submit ( );
		
		/*
		
		if ( validator.validate ( ) )
		{
			$ ( 'tr.hidden' ).remove ( );
			// do action
			
			$.ajax (
			{
				type : 'POST' ,
				url : '/' + curr_language + '/form/submit.html' ,
				data : $ ( 'form[name="ticket"]' ).serialize ( ) ,
				success : function ( data )
				{
					//console.log(data)
					var this_error = $ ( '.alert-danger' );

					if ( $ ( data ).find ( '.alert-danger' ).length > 0 )
					{
						var error = $ ( data ).find ( '.alert-danger' ).outerHTML ( );

						this_error.remove ( );

						$ ( '#ajax-message' ).append ( error );

						d = new Date ( );

						$ ( '#captcha2' ).attr ( 'src' , '/includes/captcha2/captcha.php?' + Math.random ( ) );
						$ ( '#captcha-form' ).focus ( );
					}
					else
					{
						//window.data ( 'kendoWindow' ).open ( );
					}
				}

			} );
		}
		*/
	} );
	
	/* ============================== */
	/* ====== Init Kendo Window ===== */
	/* ============================== */
	
	if ( ! window.data ( 'kendoWindow' ) )
	{
		window.kendoWindow (
		{
			draggable : false ,
			width : '620px' ,
			height : '350px' ,
			modal : true ,
			resizable : false ,
			open : function ( )
			{
				this.element.data ( 'kendoWindow' ).center ( );

				if ( $ ( 'body' ).hasClass ( 'ie' ) )
				{
					$ ( '.k-window' ).fadeIn ( 600 );
				}

				$ ( '#loading' ).fadeIn ( 600 );
				
				setTimeout ( function ( )
				{
					//location.href = 'http://www.celestyalcruises.com/' + curr_language + '/index.html';
				} , 4000 );

			} ,
			visible : false
		} );
	}

	/* ============================== */
	/* ====== Inint Datepicker ====== */
	/* ============================== */

	$ ( '.datepicker' ).kendoDatePicker ( );

	var start = $ ( '.date-start' ).kendoDatePicker (
	{
		// defines the start view
		start : 'year' ,
		// defines when the calendar should return date
		depth : 'year' ,
		change : startChange
	} ).data ( 'kendoDatePicker' );

	var end = $ ( '.date-end' ).kendoDatePicker (
	{
		// defines the start view
		start : 'year' ,
		// defines when the calendar should return date
		depth : 'year' ,
		change : endChange
	} ).data ( 'kendoDatePicker' );

	$ ( '#change_captcha' ).click ( function ( e )
	{
		e.preventDefault ( );
	} );

	//start.max(end.value());
	//end.min(start.value());

	function startChange ( )
	{
		var startDate = start.value ( );

		if ( startDate )
		{
			startDate = new Date ( startDate );
			startDate.setDate ( startDate.getDate ( ) + 1 );
			end.min ( startDate );
		}
	}

	function endChange ( )
	{
		var endDate = end.value ( );

		if ( endDate )
		{
			endDate = new Date ( endDate );
			endDate.setDate ( endDate.getDate ( ) - 1 );
			start.max ( endDate );
		}
	}
} );

$.fn.extend (
{
	outerHTML : function ( htmlString )
	{
		if ( htmlString )
		{
			return this.replaceWith ( htmlString );
		}

		if ( 'outerHTML' in this [ 0 ] )
		{
			return this [ 0 ].outerHTML;
		}

		return ( function ( element )
			{
				var attrs = element.attributes ,
				    i = 0 ,
				    n = attrs.length ,
				    name = element.nodeName.toLowerCase ( ) ,
				    attrlist = '';

				for ( ; i != n ; ++ i )
				{
					attrlist += ' ' + attrs [ i ].name + '="' + attrs [ i ].value + '"';
				}
				return '<' + name + attrlist + '>' + element.innerHTML + '</' + name + '>';
			} ( this [ 0 ] ) );
	}

} ); 