﻿<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | ad_lang_error.php
#======================================================
*/

$lang = array(

'error' => 'Fehler',  # 'Error',
'error_has_occured' => 'Hoppsala! Da ist etwas schief gelaufen.',  # 'Whoops! Looks like something went wrong.',
'fill_form_completely' => 'Sie müssen das Formular vollständig ausfüllen.',  # 'You must fill out the form completely before submitting.',
'fill_form_lengths' => 'Ein oder mehrere Felder sind nicht vollständig ausgefüllt.',  # 'One or more of the fields you filled out did not pass the mimimun length requirement.',
'login_no_admin' => 'Sie haben keinen Zugriff zum Administrationsbereich. ',  # 'Sorry, you do not have access to the ACP.',
'login_no_pass' => 'Das Passwort ist nicht korrekt.',  # 'Sorry, the password was incorrect.',
'login_no_user' => 'Zu diesme Benutzernamen ist kein Konto vorhanden.',  # 'We could not find a member that matched the username.',
'must_login' => 'Sie müssen sich einloggen um Zugriff auf den Administrationsbereich zu erhalten.',  # 'You must login before accessing the ACP.',
'news_disabled' => 'Das Nachrichtensystem wurde deaktiviert. <a href=\'<! HD_URL !>/admin.php?section=manage&amp;act=settings&amp;code=find&amp;group=news\'>Klicken Sie hier </a> um die Einstellungen zu bearbeiten.',  # 'The announcement system has been disabled. <a href=\'<! HD_URL !>/admin.php?section=manage&amp;act=settings&amp;code=find&amp;group=news\'>Click here</a> to manage your announcement settings.',
'no_article' => 'Der Artikel konnte nicht gefunden werden.',  # 'Sorry, the article you were looking for could not be found.',
'no_articles_found' => 'Es konnten zu dieser Auswahl keine Artikel gefunden werden.',  # 'Sorry, but we could not find any articles matching your criteria.',
'no_attachment' => 'Der Anhang konnte nicht gefunden werden.',  # 'Sorry, the attachment you were looking for could not be found.',
'no_canned' => 'Die Anwortvorlage konnte nicht gefunden werden.',  # 'Sorry, the canned reply you were looking for could not be found.',
'no_category' => 'Die Kategorie konnte nicht gefunden werden.',  # 'Sorry, the category you were looking for could not be found.',
'no_create_lang' => 'Die Sprachdateien konnten nicht erzeugt werden. Bitte ändern Sie die Rechte des \'languages\' Verzeichnisese auf 0777.',  # 'Sorry, we could not create the language files. Please CHMOD your \'languages\' folder to 0777.',
'no_delete_default_lang' => 'Die Standardsprache kann nicht gelöscht werden.',  # 'Sorry, but you cannot delete the default language.',
'no_delete_default_skin' => 'Der Standard Skin kann nicht gelöscht werden.',  # 'Sorry, but you cannot delete the default skin.',
'no_department' => 'Dieser Bereich wurde nicht gefunden.',  # 'Sorry, the department you were looking for could not be found.',
'no_dfield' => 'Das extra angepasste Bereichsfeld konnte nicht gefunden werden.',  # 'Sorry, the custom department field you were looking for could not be found.',
'no_export_templates' => 'Das Skin konnte nicht erzeugt werden weil keine Templates vorliegen.',  # 'Sorry, the skin pack could not be generated because there are no templates to export.',
'no_group' => 'Diese Gruppe wurde nicht gefunden.',  # 'Sorry, the group you were looking for could not be found.',
'no_lang' => 'Diese Sprache wurde nicht gefunden.',  # 'Sorry, the language you were looking for could not be found.',
'no_member' => 'Dieses Mitglied wurde nicht gefunden.',  # 'Sorry, the member you were looking for could not be found.',
'no_members_found' => 'Zu Ihrer Auswahl wurden keine Mitglieder gefunden.',  # 'Sorry, but we could not find any members matching your criteria.',
'no_message' => 'Bitte geben Sie eine Nachricht ein.',  # 'Please enter a message.',
'no_mm_tickets' => 'Sie haben noch keine Tickets ausgewählt.',  # 'Sorry, but you did not select any tickets.',
'no_mm_valid_tickets' => 'Aktion mit den gewählten Tickets nicht möglich.',  # 'Sorry, but none of the tickets you selected are valid for this action.',
'no_open_file' => 'Datei konnte nicht geöffnet werden.',  # 'Sorry, the file could not be opened.',
'no_page' => 'Diese Seite wurde nicht gefunden.',  # 'Sorry, the page you were looking for could not be found.',
'no_perm' => 'Sie haben keinen Zugriff auf diesen Bereich.',  # 'Sorry, you do not have access to this area.',
'no_perm_banned' => 'Sie haben nicht die Berechtigung für dieses Support Desk.',  # 'Sorry, you do not have permission to access this support desk.',
'no_pfield' => 'Das Custom Profil konnte nicht gefunden werden.',  # 'Sorry, the custom profile field you were looking for could not be found.',
'no_reply' => 'Diese Antwort wurde nicht gefunden.',  # 'Sorry, the reply you were looking for could not be found.',
'no_settings_found' => 'Diese Einstellungen wurde nicht gefunden.',  # 'Sorry, the settings you were looking for could not be found.',
'no_skin' => 'Dieser Skin wurde nicht gefunden.',  # 'Sorry, the skin you were looking for could not be found.',
'no_subject' => 'Bitte einen Betreff angeben.',  # 'Please enter a subject.',
'no_template' => 'Dieses Template wurde nicht gefunden.',  # 'Sorry, the template you were looking for could not be found.',
'no_ticket' => 'Dieses Ticket wurde nicht gefunden.',  # 'Sorry, the ticket you were looking for could not be found.',
'no_upload_lang_xml' => 'Diese Datei ist kein gültiges Trellis Desk XML Language File.',  # 'Sorry, the file you tried to upload is not a valid Trellis Desk XML Language File.',
'no_upload_move' => 'Datei konnte nicht hochgleaden werden. Bitte das \'tmp\' Verzeichnis auf 0777 CHMOden.',  # 'Sorry, we could not upload the file. Please CHMOD your \'tmp\' folder to 0777.',
'no_upload_size' => 'Die Datei enthielt keine Daten.',  # 'Sorry, the file you tried to upload did not contain any data.',
'no_upload_skin_xml' => 'Diese Datei ist kein gültiges Trellis Desk XML Skin File.',  # 'Sorry, the file you tried to upload is not a valid Trellis Desk XML Skin File.',
'not_writable' => 'Diese Datei ist nicht beschreibbar. Bitte setzen Sie Schreibrechte.',  # 'Sorry, the file you were trying to edit is not writable. Please CHMOD to 0777.',
'not_writable_img_dir' => 'Es konnte kein Skin Set erzeugt werden, da das /images/ Verzeichnis nicht beschreibbar ist.',  # 'Sorry, we could not create a skin set because the ./images/ directory is not writable. Please CHMOD to 0777.',
'not_writable_skin' => 'Es konnte kein Skin Set erzeugt werden, da das /skin/ Verzeichnis nicht beschreibbar ist.',  # 'Sorry, we could not create a skin set because the ./skin/ directory is not writable. Please CHMOD to 0777.',
'prune_no_days' => 'Sie haben keine Anzahl von Tagen angegeben.',  # 'Sorry, you didn\'t enter a day amount.',
'root_edit_mem' => 'Nur der Root Administrator kann dieses Mitglied bearbeiten.',  # 'Only the root administrator can edit this member.',
'ticket_closed_already' => 'Das Ticket wurde bereits geschlossen.',  # 'Sorry, the ticket has already been closed.',
'ticket_closed_escalate' => 'Das Ticket wurde bereits geschlossen. Es kann nicht eskaliert werden.',  # 'Sorry, but the ticket is closed therefore you cannot escalate the ticket.',
'ticket_closed_hold' => 'Das Ticket wurde bereits geschlossen. Es kann nicht auf warten gesetzt werden.',  # 'Sorry, but the ticket is closed therefore you cannot place it on hold.',
'ticket_closed_reply' => 'Das Ticket wurde bereits geschlossen, daher sind keine Antworten mehr möglich.',  # 'Sorry, but the ticket is closed therefore you cannot reply.',
'ticket_escalated_already' => 'Das Ticket wurde bereits eskaliert.',  # 'Sorry, the ticket has already been escalated.',
'ticket_hold_already' => 'Das Ticket wurde bereits auf warten gesetzt.',  # 'Sorry, the ticket has already been put on hold.',
'ticket_reopen_already' => 'Das Ticket ist bereits geöffnet.',  # 'Sorry, but the ticket is already open.',
'try_again' => 'Bitte versuchen Sie es erneut. Sollte das Problem bestehen bleiben wenden Sie sich an einen Administrator.',  # 'Please go back and try again. If the problem persists, please contact an administrator.',
'upload_bad_type' => 'Nicht erlaubter Dateityp.',  # 'The file type you were trying to upload is not allowed.',
'upload_failed' => 'Datei hochladen fehlgeschlagen. Bitte erneut versuchen.',  # 'File upload failed. Please try again.',
'upload_too_big' => 'Datei überschreitet die zulässige Dateigröße.',  # 'The file you were trying to upload exceeded the max upload size.',

);

?>