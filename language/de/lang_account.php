﻿<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | lang_account.php
#======================================================
*/

$lang = array(

'account_overview' => 'Übersicht',  # 'Your Account Overview',
'account_settings' => 'Konto Einstellungen',  # 'Account Settings',
'change_email' => 'E-Mail ändern',  # 'Change Email',
'change_email_address' => 'E-Mail Adresse ändern',  # 'Change Email Address',
'change_email_button' => 'E-Mail ändern',  # 'Change Email',
'change_pass_button' => 'Passwort ändern',  # 'Change Password',
'change_password' => 'Passwort ändern',  # 'Change Password',
'current_password' => 'Aktuelles Passwort',  # 'Current Password',
'custom_profile_fields' => 'Kunden Profil',  # 'Custom Profile Fields',
'disabled' => 'Deaktiviert',  # 'Disabled',
'dst_active' => 'DST Active',  # 'DST Active',
'email' => 'E-Mail',  # 'Email',
'email_notifications' => 'E-Mail Benachrichtigungen',  # 'Email Notifications',
'email_preferences' => 'E-Mail Einstellungen',  # 'Email Preferences',
'email_staff_new_ticket' => 'Neue Tickets in meinen Bereichen',  # 'New Tickets in My Departments',
'email_staff_ticket_reply' => 'Neue Antworten in meinen Bereichen',  # 'New Replies in My Departments',
'email_type' => 'E-Mail Typ',  # 'Email Type',
'enabled' => 'Aktiviert',  # 'Enabled',
'err_login_no_pass' => 'Das aktuelle Passwort ist nicht korrekt.',  # 'Your current password is incorrect.',
'err_no_cpfield' => 'Bitte eine Eingabe in diesem Feld vornehmen.',  # 'Please enter a value for the field:',
'err_no_email_change' => 'Diese E-Mail Adresse entspricht der aktuellen.',  # 'This email address is the same as your current one.',
'err_no_email_match' => 'Die angegebenen E-Mail Adressen stimmen nicht überein.',  # 'Your email addresses do not match.',
'err_no_email_valid' => 'Bitte eine gültige E-Mail Adresse angeben.',  # 'Please enter a valid email address.',
'err_no_new_pass_short' => 'Bitte ein neues Passwort eingeben.',  # 'Please enter a new password.',
'err_no_pass_match' => 'Ihre neuen Passwörter stimmen nicht überein.',  # 'Your new passwords do not match.',
'err_no_pass_short' => 'Bitte das aktuelle Passwort angeben.',  # 'Please enter a current password.',
'general_info' => 'Allgemeine Informationen',  # 'General Information',
'group' => 'Gruppe',  # 'Group',
'html' => 'HTML',  # 'HTML',
'joined' => 'Registriert seit',  # 'Joined',
'language' => 'Sprache',  # 'Language',
'last_visit' => 'Letzter Besuch',  # 'Last Visit',
'modify_account' => 'Konto bearbeiten',  # 'Modify Account',
'modify_account_information' => 'Kontoinformationen ändern',  # 'Modify Account Information',
'new_email_address' => 'Neue E-Mail Adresse',  # 'New Email Address',
'new_email_address_confirm' => 'Zur Kontrolle E-Mail Adresse noch einmal angeben.',  # 'Confirm New Email Address',
'new_password' => 'Neues Passwort',  # 'New Password',
'new_password_confirm' => 'Passwort bestätigen',  # 'Confirm New Password',
'new_reply' => 'Neue Antwort',  # 'New Reply',
'new_ticket' => 'Neues Ticket',  # 'New Ticket',
'notifications_for' => 'Benachrichtigungen für',  # 'Notifications For',
'optional' => '(optional)',  # '(Optional)',
'plain_text' => 'Nur Text',  # 'Plain Text',
'rich_text_editor' => 'Rich Text Editor',  # 'Rich Text Editor',
'skin' => 'Erscheinungsbild (Skin)',  # 'Skin',
'time_is_now' => 'Die aktuelle Zeit ist:',  # 'The time is now:',
'time_zone' => 'Zeitzone:',  # 'Time Zone',
'title' => 'Titel',  # 'Title',
'update_account' => 'Konto aktualisieren',  # 'Update Account',

);

?>