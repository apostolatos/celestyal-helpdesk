﻿<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | lang_article.php
#======================================================
*/

$lang = array(

'add_a_comment' => 'Diesen Artikel kommentieren',  # 'Add A Comment',
'add_comment_button' => 'Kommentar hinzufügen',  # 'Add Comment',
'categories' => 'Kategorien',  # 'Categories',
'confirm_delete' => 'Wollen Sie diesen Kommentar wirklich löschen?',  # 'Are you sure you want to delete this comment?',
'delete' => 'löschen',  # 'Delete',
'edit' => 'Bearbeiten',  # 'Edit',
'edit_comment' => 'Kommentar bearbeiten',  # 'Edit Comment',
'edit_comment_button' => 'Bearbeiten',  # 'Edit Comment',
'err_no_comment' => 'Bitte einen Kommentar eingeben.',  # 'Please enter a comment.',
'full_star' => 'Ganzer Stern',  # 'Full Star',
'half_star' => 'Halber Stern',  # 'Half Star',
'keywords_phrase' => 'Suchbegriff oder Phrase eingeben.',  # 'Enter keywords or phrase',
'no_articles' => 'Es gibt noch keine Artikel.',  # 'There are no articles to display.',
'no_articles_found' => 'Kein Artikel gefunden',  # 'No articles found.',
'no_comments' => 'Es wurden noch keine Kommentare zu diesem Artiekl abgegeben.',  # 'No comments have been made for this article yet.',
'no_star' => 'Ohne Stern',  # 'No Star',
'relevance' => 'Relevanz',  # 'Relevance',
'search_results' => 'Suchergebnisse',  # 'Search Results',
'stars' => 'Sterne',  # 'Stars',
'view_category' => 'Kategorie anzeigen',  # 'View Category',
'viewing_article' => 'Zeige Artikel',  # 'Viewing Article',

);

?>