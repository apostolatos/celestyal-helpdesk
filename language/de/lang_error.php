﻿<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | lang_error.php
#======================================================
*/

$lang = array(

'already_rated' => 'Sie haben bereits eine Bewertung für diesen Artikel abgegeben.',  # 'Sorry, but you have already rated this article.',
'already_rated_reply' => 'Sie haben bereits eine Bewertung abgegeben.',  # 'Sorry, but you have already rated this reply.',
'already_validated' => 'Ihr Konto wurde bereits aktiviert.',  # 'Your account has already been activated.',
'banned_kb' => 'Sieh haben nicht die Berechtigung um auf die Wissensdatenbank zuzugreifen.',  # 'Sorry, you do not have permission to access the knowledge base.',
'banned_kb_comment' => 'Sie haben nicht die Berechtigung Artikel zu kommentieren.',  # 'Sorry, you do not have permission to comment on articles.',
'banned_kb_rate' => 'Sie haben nicht die Berechtigung Artikel zu bewerten.',  # 'Sorry, you do not have permission to rate articles.',
'banned_news_comment' => 'Sie haben nicht die Berechtigung Nachrichten zu kommentieren.',  # 'Sorry, you do not have permission to comment on news.',
'banned_ticket' => 'Sie haben nicht die Berechtigung um auf das Ticket System zuzugreifen.',  # 'Sorry, you do not have permission to access the ticket center.',
'banned_ticket_escalate' => 'Sie haben nicht die Berechtigung ein Ticket zu eskalieren.',  # 'Sorry, you do not have permission to escalate a ticket.',
'banned_ticket_open' => 'Sie haben nicht die Berechtigung ein neues Ticket zu eröffnen.',  # 'Sorry, you do not have permission to open a new ticket.',
'banned_ticket_rate' => 'Sie haben nicht die Berechtigung Antworten von Mitarbeitern zu bewerten.',  # 'Sorry, you do not have permission to rate staff replies.',
'error' => 'Fehler',  # 'Error',
'error_has_occured' => 'Hoppsala! Da ist etwas schief gegangen.',  # 'Whoops! Looks like something went wrong.',
'fill_form_completely' => 'Das Formular muss vollständig ausgefüllt werden.',  # 'You must fill out the form completely before submitting.',
'fill_form_lengths' => 'Ein oder mehrere Felder wurden nicht vollständieg ausgefüllt.',  # 'One or more of the fields you filled out did not pass the mimimun length requirement.',
'invalid_rate_value' => 'Sie müssen eine Bewertung zwischen 1 und 5 auswählen.',  # 'Sorry, but you must choose a rating between 1 and 5.',
'invalid_rate_value_reply' => 'Sie müssen Daumen hoch oder Daumen runter auswählen.',  # 'Sorry, but you must choose thumbs up or thumbs down.',
'kb_comment_disabled' => 'Die Kommentarfunktion ist deaktiviert.',  # 'Sorry, but commenting has been disabled.',
'kb_disabled' => 'Die Wissensdatenbank ist leider deaktiviert.',  # 'Sorry, but the knowledge base has been disabled.',
'kb_rating_disabled' => 'Die Bewertungsfunktion ist leider deaktiviert.',  # 'Sorry, but rating has been disabled.',
'login_must_val' => 'Sie müssen die Anmeldung bestätigen indem Sie auf den Link klicken den Sie per E-Mail erhalten haben.  Sollten Sie keine E-Mail erhalten haben <a href="index.php?act=register&amp;code=sendval">klicken Sie bitte hier</a>.',  # 'Sorry, you must validate your account by clicking the link you received in our email to you. If you did not receive and email, <a href="index.php?act=register&amp;code=sendval">click here</a>.',
'login_must_val_admin' => 'Sie müssen warten bis Ihr Konto manuell freigeschaltet wurde. Sie erhalten eine E-Mail/-Benachrichtigung wenn dies geschehen ist.',  # 'Sorry, you must wait until an administrator has manually approved your account. You will receive an email when your account is approved.',
'login_no_pass' => 'Das Passwort ist leider nicht korrekt. <a href="index.php?act=register&amp;code=forgot">Haben Sie das Passwort vergessen?</a>',  # 'Sorry, the password was incorrect. <a href="index.php?act=register&amp;code=forgot">Forgot your password?</a>',
'login_no_user' => 'Zu diesem Namen konnten wir keinen Benutzer finden.',  # 'We could not find a member that matched the username.',
'logout_no_key' => 'Der logout key ist ungültig.',  # 'Invalid logout key.',
'must_be_guest' => 'Nur Gäste haben Zugriff auf diese Seite.',  # 'Sorry, only guests have permission to access this page.',
'must_be_user' => 'Sie müssen angemeldet sein um auf diese Seite zugreifen zu können.',  # 'Sorry, you must be logged in to access this page.',
'new_tickets_disabled' => 'Das Anlegen neuer Tickets ist zur Zeit deaktiviert.',  # 'Sorry, but an administrator has currently disabled new tickets.',
'news_comment_disabled' => 'Die Kommentarfunktion ist zur Zeit deaktiviert.',  # 'Sorry, but commenting has been disabled.',
'news_disabled' => 'Die Seite mit den Nachrichten ist zur Zeit deaktiviert.',  # 'Sorry, but the news page has been disabled.',
'no_announcement' => 'Diese Nachricht konnte nicht gefunden werden.',  # 'Sorry, the announcement you were looking for could not be found.',
'no_article' => 'Dieser Artikel konnte nicht gefunden werden.',  # 'Sorry, the article you were looking for could not be found.',
'no_category' => 'Diese Kategorie konnte nicht gefunden werden.',  # 'Sorry, the category you were looking for could not be found.',
'no_comment' => 'Dieser Kommentar konnte nicht gefunden werden.',  # 'Sorry, the comment you were looking for could not be found.',
'no_department' => 'Dieser Bereich konnte nicht gefunden werden.',  # 'Sorry, the department you were looking for could not be found.',
'no_email_val_key' => 'Dieser Validierungsschlüssel konnte nicht gefunden werden.',  # 'Sorry, but we could find not your validation key.',
'no_member' => 'Dieser Teilnehmer konnte nicht gefunden werden.',  # 'Sorry, the member you were looking for could not be found.',
'no_page' => 'Diese Seite konnte nicht gefunden werden.',  # 'Sorry, the page you were looking for could not be found.',
'no_pass_match' => 'Ihre Passwörter stimmen nicht überein.',  # 'Your passwords did not match.',
'no_perm_access' => 'Sie haben keine Erlaubnis auf diesen Bereich zuzugreifen.',  # 'Sorry, you do not have permission to access this area.',
'no_perm_banned' => 'Sie haben nicht die Berechtigung auf dieses Support Desk zuzugreifen.',  # 'Sorry, you do not have permission to access this support desk.',
'no_perm_com_delete' => 'Sie haben nicht die Berechtigung diesen Kommentar zu löschen.',  # 'Sorry, you do not have permission to delete this comment.',
'no_perm_com_edit' => 'Sie haben nicht die Berechtigung diesen Kommentar zu bearbeiten.',  # 'Sorry, you do not have permission to edit this comment.',
'no_perm_reply_delete' => 'Sie haben nicht die Berechtigung diese Antwort zu löschen.',  # 'Sorry, you do not have permission to delete this reply.',
'no_perm_reply_edit' => 'Sie haben nicht die Berechtigung diese Antwort zu bearbeiten.',  # 'Sorry, you do not have permission to edit this reply.',
'no_perm_ticket_edit' => 'Sie haben nicht die Berechtigung dieses Ticket zu bearbeiten.',  # 'Sorry, you do not have permission to edit this ticket.',
'no_reply' => 'Die Antwort konnte nicht gefunden werden.',  # 'Sorry, the reply you were looking for could not be found.',
'no_staff_rate_reply' => 'Nur Antworten von Mitarbeitern können bewertet werdem.',  # 'Sorry, only staff replies can be rated.',
'no_ticket' => 'Dieses Ticket konnte nicht gefunden werden.',  # 'Sorry, the ticket you were looking for could not be found.',
'no_ticket_guest' => 'Leider konnte zu dieser E-Mail und Ticket Key Kombination kein Ticket gefunden werden.',  # 'Sorry, we could find find a ticket matching the email address and ticket key.',
'no_valid_email' => 'Bitte eine gültige E-Mail-Adresse angeben.',  # 'Please enter a valid email address.',
'no_valid_tkey' => 'Bitte einen gültigen Ticket Key eingeben.',  # 'Please enter a valid ticket key.',
'registration_disabled' => 'Neuanmeldungen sind zur Zeit nicht möglich.',  # 'Sorry, but an administrator has currently disabled new registrations.',
'reply_rating_disabled' => 'Die Bewertungsfunktion wurde deaktiviert.',  # 'Sorry, but rating has been disabled.',
'ticket_closed_already' => 'Dieses Ticket wurde bereits geschlossen.',  # 'Sorry, but the ticket has already been closed.',
'ticket_closed_escalate' => 'Dieses Ticket wurde bereits geschlossen und kann daher nicht eskaliert werden.',  # 'Sorry, but the ticket is closed therefore you cannot escalate the ticket.',
'ticket_closed_reply' => 'Dieses Ticket wurde bereits geschlossen und kann daher nicht beantwortet werden.',  # 'Sorry, but the ticket is closed therefore you cannot reply.',
'ticket_escalate_perm' => 'Sie haben nicht die Berechtigung dieses Ticket zu eskalieren.',  # 'Sorry, you do not have permission to escalate this ticket.',
'ticket_escalated_already' => 'Dieses Ticket wurde bereits eskaliert.',  # 'Sorry, but the ticket has already been escalated.',
'ticket_no_close_perm' => 'Sie haben nicht die Berechtigung dieses Ticket zu schließen.',  # 'Sorry, you do not have permission to close this ticket.',
'token_mismatch' => 'Das Formular Token konnte nicht verifiziert werden.',  # 'Your form token could not be verified.',
'try_again' => 'Bitte gehen Sie zurück und versuchen es erneut. Sollte das Problem weiterhin bestehen kontaktieren Sie bitte einen Administrator.',  # 'Please go back and try again. If the problem persists, please contact an administrator.',

);

?>