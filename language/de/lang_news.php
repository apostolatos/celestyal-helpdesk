<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | lang_news.php
#======================================================
*/

$lang = array(

'add_a_comment' => 'Kommentar hinzufügen',  # 'Add A Comment',
'add_comment_button' => 'Kommentar eintragen',  # 'Add Comment',
'announcement' => 'Nachricht',  # 'Announcement',
'comments' => 'Kommentare',  # 'Comments',
'confirm_delete' => 'Wollen Sie diesen Kommentar wirklich löschen?',  # 'Are you sure you want to delete this comment?',
'delete' => 'löschen',  # 'L�schen',
'edit' => 'bearbeiten',  # 'Bearbeiten',
'edit_comment' => 'Kommentar bearbeiten',  # 'Kommentar bearbeiten',
'edit_comment_button' => 'Kommentar bearbeiten',  # 'Kommentar bearbeiten',
'err_no_comment' => 'Bitte einen Kommentar eingeben.',  # 'Please enter a comment.',
'no_comments' => 'Es wurden noch keine Kommentare zu dieser Nachricht abgegegeben.',  # 'No comments have been made for this announcement yet.',
'no_news' => 'Es gibt zur Zeit keine Nachrichten.',  # 'There are no announcements to display.',
'viewing_announcement' => 'Kommentare anzeigen',  # 'Viewing Announcement',

);

?>