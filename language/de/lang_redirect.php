﻿<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | lang_redirect.php
#======================================================
*/

$lang = array(

'add_rating_success' => 'Ihre Bewertung wurde erfolgreich abgegeben.',  # 'Your rating has been successfully added.',
'almost_acc_activate' => 'Ihre E-Mail-Adresse wurde erfolgreich bestätigt. Bevor Sie Ihr Konto nutzen können muss es manuell vom Administrator freigeschaltet werden. Sie erhalten dann eine weitere Bestätigung.',  # 'Your email address has been successfully verified. Before you can begin using your account, an administrator must manually approve your account. You will receive an email when your account is approved.',
'change_val_email' => 'Damit die neue E-Mail-Adresse endgültig übernommen werden kann müssen Sie den Link in der E-Mail bestätigen die an die neue Adresse gesandt wurde.',  # 'Before we can change your email, you must first click the validation link in the email that has been dispacted to your new address.',
'click_here' => 'Hier klicken',  # 'click here',
'delete_comment_success' => 'Der Kommentar wurde erfolgreich gelöscht.',  # 'The comment has been successfully deleted.',
'edit_comment_success' => 'Der Kommentar wurde erfolgreich bearbeitet',  # 'The comment has been successfully edited.',
'login_success' => 'Sie haben sich erfolgreich eingelogged.',  # 'You have been successfully logged in.',
'logout_success' => 'Sie haben sich erfolgreich ausgelogged.',  # 'You have been successfully logged out.',
'new_user_no_val' => 'Ihr Benutzerkonto wurde erfolgreich angelegt. Sie können sich nun einloggen.',  # 'Your account has been successfully created. You may now login to your account.',
'new_user_val_admin' => 'Bevor Sie Ihr Konto nutzen können muss es manuell vom Administrator freigeschaltet werden. Sie erhalten dann eine Bestätigung per E-Mail.',  # 'Before you can begin using your account, an administrator must manually approve your account. You will receive an email when your account is approved.',
'new_user_val_both' => 'Bevor Sie Ihr Konto nutzen können muss es manuell vom Administrator freigeschaltet werden. Außerdem müssen Sie auch den Link zur Bestätigung aufrufen der Ihnen per E-Mail zugesandt wurde.',  # 'Before you can begin using your account, an administrator must manually approve your account AND you must click the validation link in the email that has been dispatched to your email address.',
'new_user_val_email' => 'Bevor Sie Ihr Konto nutzen können müssen Sie den Link zur Bestätigung der Anmeldung aufrufen der Ihnen per E-Mail zugesandt wurde.',  # 'Before you can begin using your account, you must first click the validation link in the email that has been dispatched to your email address.',
'new_user_val_resend' => 'Eine neue E-Mail mit Link zur Bestätigung der Anmeldung wurde an Ihre E-Mail-Adresse versandt.',  # 'A new validation email has been successfully sent to your email.',
'please_wait' => 'Bitte warten',  # 'Please Wait',
'reply_delete_success' => 'Die Antwort wurde gelöscht.',  # 'The reply has been successfully deleted.',
'reply_edit_success' => 'Die Antwort wurde erfolgreich aktualisiert.',  # 'The reply has been successfully updated.',
'reset_pass_email_sent' => 'Um das Passwort neu zu setzen klicken Sie bitte auf den Link Ihnen per E-Mail zugesandt wurde.',  # 'To reset your password, click the link in the email that has been dispatched to your email address.',
'reset_pass_success' => 'Ihr Passwort wurde erfolgreich geändert. Sie können sich jetzt einloggen.',  # 'Your password has been successfully reset. You may now login.',
'submit_comment_success' => 'Ihr Kommentar wurde erfolgreich hinzugefügt.',  # 'Your comment has been successfully added.',
'submit_reply_success' => 'Ihre Antwort wurde erfolgreich hinzugefügt.',  # 'Your reply has been successfully added.',
'submit_ticket_success' => 'Ihr Ticket wurde erfolgreich eingetragen.',  # 'Your ticket has been successfully submitted.',
'success_acc_activate' => 'Ihr Konto wurde erfolgreich aktiviert. Sie können sich jetzt einloggen.',  # 'Your account have been successfully activated. You may now login to your account.',
'thank_you' => 'Vielen Dank.',  # 'Thank you.',
'ticket_close_success' => 'Ihr Ticket wurde geschlossen.',  # 'Your ticket has been successfully closed.',
'ticket_edit_success' => 'Ihr Ticket wurde aktualisiert.',  # 'The ticket has been successfully updated.',
'ticket_escalate_success' => 'Ihr Ticket wurde erfolgreich eskaliert.',  # 'Your ticket has been successfully escalated.',
'transfer_you' => 'Es folgt eine automatische Weiterleitung. Wenn Sie nicht warten möchten, ',  # 'Please wait while we transfer you. If you do not wish to wait,',
'update_my_account' => 'Ihr Konto wurde erfolgreich aktualisiert.',  # 'Your account has been successfully updated.',
'update_my_email' => 'Ihre E-Mail-Adresse wurde erfolgreich aktualisiert.',  # 'Your email address has been successfully updated.',
'update_my_pass' => 'Ihr Passwort wurde aktualisiert. Unter Umständen müssen Sie sich jetzt neu einloggen.',  # 'Your pass has been successfully updated. Depending on your cookie settings, you may need to login again.',

);

?>