﻿<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | lang_register.php
#======================================================
*/

$lang = array(

'captcha' => 'Captcha',  # 'Captcha',
'create_account_button' => 'Benutzerkonto anlegen',  # 'Create Account',
'email_address' => 'E-Mail Adresse',  # 'Email Address',
'err_captcha_mismatch' => 'Der Captcha-Code wurde nicht richtig eingegeben. Bitte noch einmal versuchen.',  # 'The Captcha code you entered did not match the image. Please try again.',
'err_email_in_use' => 'Diese E-Mail Adresse ist bereits registriert.',  # 'Sorry, that email is already in use. Please choose another email.',
'err_no_cpfield' => 'Bitte in diesem Feld eine Eingabe machen.',  # 'Please enter a value for the field:',
'err_no_email_valid' => 'Sie müssen eine gültige E-Mail Adresse eingeben.',  # 'You must enter a valid email address.',
'err_no_pass_match' => 'Die Passwörter stimmen nicht überein.',  # 'Your passwords do not match.',
'err_no_pass_short' => 'Bitte ein Passwort angeben.',  # 'Please enter a password.',
'err_no_user_or_email' => 'Sie müssen einen Benutzernamen oder eine E-Mail-Adresse eingeben.',  # 'You must enter a username or email address.',
'err_no_user_short' => 'Bitte einen Benutzernamen angeben.',  # 'Please enter a username.',
'err_user_already_active' => 'Ihre Anmeldung wurde bereits bestätigt. Versuchen Sie sich einzuloggen.',  # 'Your account has already been validated. Try logging in. :)',
'err_user_in_use' => 'Dieser Benutzername ist bereits vergeben. Wählen Sie bitte einen anderen.',  # 'Sorry, that username is already in use. Please choose another username.',
'err_user_not_found' => 'Es konnte kein Benutzer mit dieser Kennnung gefunden werden.',  # 'We could not find any member with the provided account information.',
'forgot_password' => 'Passwort vergessen',  # 'Forgot Password',
'new_password' => 'Neues Passwort',  # 'New Password',
'new_password_confirm' => 'Neues Passwort zur Bestätigung wiederholen',  # 'Confirm New Password',
'optional' => '(optional)',  # '(Optional)',
'or' => 'oder',  # 'OR',
'password_confirm' => 'Passwort bestätigen',  # 'Confirm Password',
'register_new_account' => 'Neues Konto anlegen',  # 'Register A New Account',
'resend_val_button' => 'Bestätigung erneut zustellen.',  # 'Resend Validation',
'reset_pass_button' => 'Passwort zurücksetzen',  # 'Reset Password',
'reset_password' => 'Passwort zurücksetzen',  # 'Reset Password',
'upgrade_account_button' => 'Konto Upgrade',  # 'Upgrade Account',
'upgrade_msg' => 'Da Sie bereits als Gast angemeldet sind können Sie einfach auf ein registriertes Konto hochstufen. Füllen Sie dazu einfach das Formular aus und senden Sie es ab. Ihre bereits eingetragenen Tickets bleiben natürlich erhalten.',  # 'Because you are already logged in as a guest, you can upgrade your account to a fully registered account while keeping all your current tickets. To upgrade your account, simply fill out the form below and click Upgrade Account.',

);

?>