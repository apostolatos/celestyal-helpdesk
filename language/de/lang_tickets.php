﻿<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | lang_tickets.php
#======================================================
*/

$lang = array(

'article_suggestions' => 'Vorgeschlagene Artikel',  # 'Articles Suggestions',
'attachment' => '(Anhang)',  # '(Attachment)',
'attachment_b' => 'Anhang',  # 'Attachment',
'attachment_max_size' => 'Maximale Größe des Anhang',  # 'Attachment max size:',
'captcha' => 'Captcha',  # 'Captcha',
'close' => 'schließen',  # 'Close',
'close_msg_a' => 'Dieses Ticket wurde geschlossen von',  # 'This ticket was closed by',
'close_msg_b' => 'und zwar mit folgender Begründung',  # 'for the following reason.',
'close_ticket' => 'Ticket schließen',  # 'Close Ticket',
'close_ticket_button' => 'Ticket schließen',  # 'Close Ticket',
'closing_ticket' => 'Ticket wird geschlossen',  # 'Closing Ticket',
'confirm_close' => 'Soll dieses Ticket wirklich geschlossen werden?',  # 'Are you sure you want to close this ticket?',
'confirm_delete_reply' => 'Soll diese Antwort wirklich gelöscht werden?',  # 'Are you sure you want to delete this reply?',
'confirm_escalate' => 'Soll dieses Ticket wirklich eskaliert werden?',  # 'Are you sure you want to escalate this ticket?',
'continue_ticket_submit' => 'Ein neues Ticket eintragen',  # 'Continue With Ticket Submission',
'download_attachment' => 'Anhang herunterladen',  # 'Download attachment:',
'edit' => 'Bearbeiten',  # 'Edit',
'edit_reply' => 'Antwort bearbeiten',  # 'Edit Reply',
'edit_reply_button' => 'Antwort bearbeiten',  # 'Edit Reply',
'edit_ticket' => 'Ticket bearbeiten',  # 'Edit Ticket',
'edit_ticket_button' => 'Ticket bearbeiten',  # 'Edit Ticket',
'email' => 'E/Mail',  # 'Email',
'email_address' => 'E-Mail Adresse',  # 'Email Address',
'enter_close_reason' => 'Bitte einen Grund für das Schließen des Tickets angeben.',  # 'Please enter a reason for closing this ticket.',
'err_captcha_mismatch' => 'Der Captcha/Code wurde nicht korrekt eingegeben. Bitte versuchen Sie es erneut.',  # 'The Captcha code you entered did not match the image. Please try again.',
'err_email_in_use' => 'Diese E-Mail-Adresse wird bereits genutzt. Bitte verwenden Sie eine andere oder loggen Sie sich ein.',  # 'That email is already in use by one of our members. Please use another email or login.',
'err_no_cdfield' => 'Bitte einen gültigen Wert eintragen.',  # 'Please enter a value for the field:',
'err_no_depart' => 'Bitte einen Bereich auswählen.',  # 'Please select a department.',
'err_no_email' => 'Bitte eine gültige E-Mail-Adresse eingeben.',  # 'Please enter a valid email address.',
'err_no_message' => 'Bitte eine Nachricht eingeben.',  # 'Please enter a message.',
'err_no_name' => 'Bitte einen Namen eingeben.',  # 'Please enter a name.',
'err_no_reason' => 'Bitte eine Begründung angeben.',  # 'Please enter a reason.',
'err_no_reply' => 'Bitte eine Antwort eingeben.',  # 'Please enter a reply.',
'err_no_subject' => 'Bitte einen Betreff eingeben.',  # 'Please enter a subject.',
'err_upload_bad_type' => 'Dieser Dateityp ist nicht gestattet.',  # 'The file type you were trying to upload is not allowed.',
'err_upload_failed' => 'Hochladen der Datei fehlgeschalgen. Bitte versuchen Sie es erneut.',  # 'File upload failed. Please try again.',
'err_upload_too_big' => 'Die Datei überschreitet die maximal zulässige Dateigröße',  # 'The file you were trying to upload exceeded the max upload size.',
'escalate' => 'Fortfahren',  # 'Escalate',
'guest_login' => 'Gast Login',  # 'Guest Login',
'guest_login_info' => 'Willkommen Gast. Bitte melden Sie sich unter Verwendung Ihrer E-Mail Adresse und des Ticket Key an um Zugriff auf Ihr Gast Ticket zu erhalten. Wenn Sie bereits registriertes Mitglied sind melden Sie sich bitte in der Loginbox rechts an.',  # 'Welcome Guest. Please login using your email address and ticket key below to access your guest ticket. If you are already a registered member, please login via the Log In box to the right.',
'guest_ticket_notification' => 'Bei Antworten von Mitarbeiter per E-Mail informieren.',  # 'Email Notification of Staff Replies',
'history' => 'Verlauf',  # 'History',
'last_replier' => 'Letzter Beantworter',  # 'Last Replier',
'last_reply' => 'Letzte Antwort',  # 'Last Reply',
'name' => 'Name',  # 'Name',
'no_replies' => 'Es gibt noch keine Antworten zu diesem Ticket.',  # 'Sorry, no replies have been made to this ticket.',
'no_suggestions_helped' => 'Keiner der vorgeschlagenen Artikel konnte mir helfen das Problem zu lösen.',  # 'None of the above suggestions helped me resolve my issue.',
'open_ticket_button' => 'Ticket eintragen',  # 'Submit Ticket',
'optional' => '(optional)',  # '(Optional)',
'relevance' => 'Relevanz',  # 'Relevance',
'replies' => 'Bisherige Antworten',  # 'Replies',
'select_depart' => 'Bitte einen Bereich auswählen',  # 'Please select a department.',
'send_reply' => 'Antwort senden',  # 'Send A Reply',
'send_reply_button' => 'Antwort absenden',  # 'Send Reply',
'suggestions_explained' => 'Es werden einige Artikel aus der Wissensdatenbank aufgeführt die geeignet sein könnten Ihre Frage zu beantworten. Bitte vergewissern Sie sich, dass Ihre Frage nicht bereits in den genannten Dokumenten behandelt wird, bevor Sie ein neues Ticket eröffnen. <i>Ihr Ticket wurde noch nicht eingetragen.</i>',  # 'Below is a list of possible knowledge base articles that might answer your inquiry. Please review the articles and verify that they do not help you before continuing with your ticket submission. <i>Your ticket has not been submitted yet.</i>',
'thumbs_down' => 'Daumen hoch',  # 'Thumbs Down',
'thumbs_up' => 'Daumen runter',  # 'Thumbs Up',
'ticket_center_overview' => 'Ticket Übersicht',  # 'Ticket Center Overview',
'ticket_id' => 'Ticket-ID',  # 'Ticket Id',
'ticket_key' => 'Ticket Key',  # 'Ticket Key',
'viewing_ticket' => 'Ticket Ansicht',  # 'Viewing A Ticket',

);

?>