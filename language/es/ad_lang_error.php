<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | ad_lang_error.php
#======================================================
*/

$lang = array(

'error' => 'Error',
'error_has_occured' => 'Ups! Algo no funcion&oacute; como lo esper&aacute;bamos.',
'fill_form_completely' => 'Debe completar completamente el formulario antes de enviarlo.',
'fill_form_lengths' => 'Uno o m&aacute;s de los campos que complet&oacute; no cumplen los requisitos de tama&ntilde;o m&iacute;nimo.',
'login_no_admin' => 'Ud. no tiene acceso al Panel de Administraci&oacute;n.',
'login_no_pass' => 'La contrase&ntilde;a es incorrecta.',
'login_no_user' => 'No es posible encontrar un usuario que concuerde con el nombre de usuario.',
'must_login' => 'Debe loguearse antes de acceder al Panel de Administraci&oacute;n.',
'news_disabled' => 'El sistema de anuncios ha sido desactivado. &lt;a href=&#039;&lt;! HD_URL !&gt;/admin.php?section=manage&amp;act=settings&amp;code=find&amp;group=news&#039;&gt;Click aqu&iacute;&lt;/a&gt; para administrar la configuraci&oacute;n de los anuncios.',
'no_article' => 'El art&iacute;culo que est&aacute; buscando no puso ser encontrado.',
'no_articles_found' => 'No se encontraron art&iacute;culos que correspondan con su criterio de b&uacute;squeda.',
'no_attachment' => 'El adjunto que est&aacute; buscando no pudo ser encontrado.',
'no_canned' => 'La respuesta predefinida que est&aacute; buscando no pudo ser encontrada.',
'no_category' => 'La categor&iacute;a que est&aacute; buscando no pudo ser encontrada.',
'no_create_lang' => 'Imposible crear archivos de lenguaje. Por favor realice un CHMOD sobre la carpeta &#039;languages&#039; a 0777.',
'no_delete_default_lang' => 'Imposible eliminar el lenguaje por defecto.',
'no_delete_default_skin' => 'Imposible eliminar el estilo por defecto.',
'no_department' => 'El departamento que est&aacute; buscando no pudo ser encontrado.',
'no_dfield' => 'El campo personalizado que est&aacute; buscando no pudo ser encontrado.',
'no_export_templates' => 'El paquete de estilo no pudo ser generado porque no se encontraron plantillas que exportar.',
'no_group' => 'El grupo que est&aacute; buscando no pudo ser encontrado.',
'no_lang' => 'El lenguaje que est&aacute; buscando no pudo ser encontrado.',
'no_member' => 'El usuario que est&aacute; buscando no pudo ser encontrado.',
'no_members_found' => 'No se encontraron usuarios de acuerdo a su criterio de b&uacute;squeda.',
'no_message' => 'Por favor, ingrese el mensaje.',
'no_mm_tickets' => 'No ha seleccionado ning&uacute;n ticket.',
'no_mm_valid_tickets' => 'Ninguno de los tickets que ha seleccionado es v&aacute;lido para esta acci&oacute;n.',
'no_open_file' => 'El archivo no pudo ser abierto.',
'no_page' => 'La p&aacute;gina que est&aacute; buscando no pudo ser encontrada.',
'no_perm' => 'Ud. no tiene acceso a esta &aacute;rea.',
'no_perm_banned' => 'Ud. no tiene permiso para utilizar este sistema de soporte.',
'no_pfield' => 'El campo personalizado que est&aacute; buscando no pudo ser encontrado.',
'no_reply' => 'La respuesta que est&aacute; buscando no pudo ser encontrada.',
'no_settings_found' => 'La configuraci&oacute;n que est&aacute; buscando no pudo ser encontrada.',
'no_skin' => 'El estilo que est&aacute; buscando no pudo ser encontrado.',
'no_subject' => 'Por favor ingrese el asunto',
'no_template' => 'La plantilla que est&aacute; buscando no pudo ser encontrada.',
'no_ticket' => 'El ticket que est&aacute; buscando no pudo ser encontrado.',
'no_upload_lang_xml' => 'El archivo que trata de subir no es un archivo XML v&aacute;lido de lenguaje para Trellis Desk.',
'no_upload_move' => 'Imposible subir el archivo. Por favor realice un CHMOD de la carpeta &#039;tmp&#039; a 0777.',
'no_upload_size' => 'El archivo que trat&oacute; de subir no contiene datos.',
'no_upload_skin_xml' => 'El archivo que trata de subir no es un archivo XML v&aacute;lido de estilo para Trellis Desk.',
'not_writable' => 'El archivo que trata de editar es s&oacute;lo lectura. C&aacute;mbielo con CHMOD a 0777.',
'not_writable_img_dir' => 'Imposible crear estilo debido a que la carpeta  ./images/  es s&oacute;lo lectura. Por favor realice un CHMOD a 0777.',
'not_writable_skin' => 'Imposible crear estilo debido a que la carpeta  ./skin/  es s&oacute;lo lectura. Por favor realice un CHMOD a 0777.',
'prune_no_days' => 'No ingres&oacute; la cantidad de d&iacute;as.',
'root_edit_mem' => 'S&oacute;lo el superadministrador puede editar este usuario.',
'ticket_closed_already' => 'Este ticket ya fue cerrado anteriormente.',
'ticket_closed_escalate' => 'El ticket est&aacute; cerrado, por lo tanto no puede ser escalado.',
'ticket_closed_hold' => 'El ticket est&aacute; cerrado, por lo tanto no puede ser puesto en espera.',
'ticket_closed_reply' => 'El ticket est&aacute; cerrado, por lo tanto no puede ser respondido.',
'ticket_escalated_already' => 'El ticket ya fue escalado previamente.',
'ticket_hold_already' => 'El ticket ya fue puesto en espera previamente.',
'ticket_reopen_already' => 'El ticket se encuentra actualmente abierto.',
'try_again' => 'Regrese y reintente. Si el problema persiste, por favor contacte un administrador.',
'upload_bad_type' => 'El tipo de archivo que est&aacute; tratando de subir no est&aacute; permitido.',
'upload_failed' => 'Carga de archivo fall&oacute;. Por favor reintente.',
'upload_too_big' => 'El tama&ntilde;o del archivo que intenta subir es mayor que el m&aacute;ximo permitido.',

);

?>