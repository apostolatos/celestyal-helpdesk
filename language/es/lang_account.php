<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | lang_account.php
#======================================================
*/

$lang = array(

'account_overview' => 'Su Cuenta',
'account_settings' => 'Configuraci&oacute;n de la cuenta',
'change_email' => 'Cambiar Email',
'change_email_address' => 'Cambiar Direcci&oacute;n de Email',
'change_email_button' => 'Cambiar Email',
'change_pass_button' => 'Cambiar Contrase&ntilde;a',
'change_password' => 'Cambiar Contrase&ntilde;a',
'current_password' => 'Contrase&ntilde;a Actual',
'custom_profile_fields' => 'Campos Personalizados de Perfil',
'disabled' => 'Desactivado',
'dst_active' => 'DST Activo',
'email' => 'Email',
'email_notifications' => 'Notificaciones por Email',
'email_preferences' => 'Preferencias de Email',
'email_staff_new_ticket' => 'Nuevos Tickets en mis Departamentos',
'email_staff_ticket_reply' => 'Nuevas Respuestas en mis Departamentos',
'email_type' => 'Tipo de Email',
'enabled' => 'Activado',
'err_login_no_pass' => 'Su contrase&ntilde;a actual es incorrecta.',
'err_no_cpfield' => 'Por favor ingrese un valor en el campo:',
'err_no_email_change' => 'Esta direcci&oacute;n de email es la misma que la actual.',
'err_no_email_match' => 'Sus direcciones de email no concuerdan.',
'err_no_email_valid' => 'Por favor ingrese una direcci&oacute;n de email v&aacute;lida.',
'err_no_new_pass_short' => 'Por favor ingrese la nueva contrase&ntilde;a.',
'err_no_pass_match' => 'Sus nuevas contrase&ntilde;as no concuerdan.',
'err_no_pass_short' => 'Por favor ingrese su contrase&ntilde;a actual.',
'general_info' => 'Informaci&oacute;n General',
'group' => 'Grupo',
'html' => 'HTML',
'joined' => 'Fecha de Alta',
'language' => 'Lenguaje',
'last_visit' => '&Uacute;ltima Visita',
'modify_account' => 'Modificar Cuenta',
'modify_account_information' => 'Modificar Informaci&oacute;n de la cuenta',
'new_email_address' => 'Nueva direcci&oacute;n de email',
'new_email_address_confirm' => 'Confirme nueva direcci&oacute;n de Email',
'new_password' => 'Nueva Contrase&ntilde;a',
'new_password_confirm' => 'Confirme Nueva Contrase&ntilde;a',
'new_reply' => 'Nueva respuesta',
'new_ticket' => 'Nuevo Ticket',
'notifications_for' => 'Notificaciones para',
'optional' => '(Opcional)',
'plain_text' => 'Texto Plano',
'rich_text_editor' => 'Editor de Texto Enriquecido',
'skin' => 'Estilo',
'time_is_now' => 'La hora actual es:',
'time_zone' => 'Zona Horaria',
'title' => 'T&iacute;tulo',
'update_account' => 'Actualizar Cuenta',

);

?>