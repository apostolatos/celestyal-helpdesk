<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | lang_article.php
#======================================================
*/

$lang = array(

'add_a_comment' => 'A&ntilde;adir un Comentario',
'add_comment_button' => 'A&ntilde;adir Comentario',
'categories' => 'Categor&iacute;as',
'confirm_delete' => '&iquest;Est&aacute; seguro que desea eliminar este comentario?',
'delete' => 'Eliminar',
'edit' => 'Editar',
'edit_comment' => 'Editar Comentario',
'edit_comment_button' => 'Editar Comentario',
'err_no_comment' => 'Por favor ingrese un comentario.',
'full_star' => 'Estrella Completa',
'half_star' => 'Media Estrella',
'keywords_phrase' => 'Ingrese palabras clave o una frase',
'no_articles' => 'No hay art&iacute;culos que mostrar.',
'no_articles_found' => 'No se encontraron art&iacute;culos.',
'no_comments' => 'A&uacute;n no se han realizado comentarios sobre este art&iacute;culo.',
'no_star' => 'Sin estrellas',
'relevance' => 'Relevancia',
'search_results' => 'Resultados de la B&uacute;squeda',
'stars' => 'Estrellas',
'view_category' => 'Ver Categor&iacute;a',
'viewing_article' => 'Visualizando Art&iacute;culo',

);

?>