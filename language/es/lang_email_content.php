<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | lang_email_content.php
#======================================================
*/

$lang['header'] = <<<EOF
Estimado <#MEM_NAME#>,
EOF;

$lang['footer'] = <<<EOF
Atentamente,

El equipo de <#HD_NAME#>.

<#HD_URL#>
EOF;

$lang['change_email_val_sub'] = "Verificación de su Email";

$lang['change_email_val'] = <<<EOF
Ud. solicit&oacute; que su email fuera cambiado a esta direcci&oacute;n. Para completar el cambio, debe verificar esta direcci&oacute;n de email haciendo click en el siguiente enlace.

---------------------------

<#VAL_LINK#>

---------------------------
EOF;

$lang['new_user_val_email_sub'] = "Verificación de su nueva cuenta";

$lang['new_user_val_email'] = <<<EOF
Bienvenido a <#HD_NAME#>.  Ud. ha solicitado una nueva cuenta en nuestro sistema de soporte.  Para completar la activaci&oacute;n de su cuenta, debe verificar esta direcci&oacute;n de email haciendo click en el siguiente enlace de validaci&oacute;n.

---------------------------

Usuario: <#USER_NAME#>
Enlace de Validaci&oacute;n: <#VAL_LINK#>

---------------------------
EOF;

$lang['new_user_val_both_sub'] = "Verificación de su nueva cuenta";

$lang['new_user_val_both'] = <<<EOF
Bienvenido a <#HD_NAME#>.  Ud. ha solicitado una nueva cuenta en nuestro sistema de soporte.  Para completar la activaci&oacute;n de su cuenta, debe verificar esta direcci&oacute;n de email haciendo click en el siguiente enlace de validaci&oacute;n.  Adicionalmente, un administrador debe aprobar su cuenta manualmente.

---------------------------

Usuario: <#USER_NAME#>
Enlace de Validaci&oacute;n: <#VAL_LINK#>

---------------------------

Recuerde que adem&aacute;s de verificar su direcci&oacute;n de email, un administrador debe aprobar manualmente su cuenta. Un mensaje de correo electr&oacute;nico le ser&aacute; enviado para notificarle cuando su cuenta est&eacute; aprobada.
EOF;

$lang['new_user_val_admin_sub'] = "Su nueva cuenta";

$lang['new_user_val_admin'] = <<<EOF
Ud. ha solicitado una nueva cuenta en nuestro sistema de soporte. Antes de poder comenzar a utilizar su cuenta, un administrador debe aprobarla manualmente. Recibir&aacute; un mensaje de correo electr&oacute;nico cuando su cuenta est&eacute; aprobada.
EOF;

$lang['acc_accivated_sub'] = "Cuenta Activada";

$lang['acc_accivated'] = <<<EOF
Su cuenta fue activada con &eacute;xito. Puede ahora ingresar al sistema.
EOF;

$lang['acc_almost_accivated_sub'] = "Esperando aprobación de cuenta";

$lang['acc_almost_accivated'] = <<<EOF
Su direcci&oacute;n de email fue verificada con &eacute;xito, pero antes de comenzar a utilizar su cuenta, un administrador debe aprobarla manualmente. Recibir&aacute; un mensaje de correo electr&oacute;nico cuando su cuenta est&eacute; aprobada.
EOF;

$lang['acc_approved_sub'] = "Cuenta Aprobada";

$lang['acc_approved'] = <<<EOF
Su cuenta ha sido aprobada por un administrador.  Puede ahora ingresar al sistema.
EOF;

$lang['acc_almost_approved_sub'] = "Cuenta esperando verificación de Email";

$lang['acc_almost_approved'] = <<<EOF
Su cuenta ha sido aprobada por un administrador, pero antes de comenzar a utilizar su cuenta, debe hacer click en el enlace de validaci&oacute;n que fue enviado a su direcci&oacute;n de email.
EOF;

$lang['new_ticket_sub'] = "Ticket #<#TICKET_ID#>";

$lang['new_ticket'] = <<<EOF
Ha enviado un nuevo ticket. Nuestro equipo revisar&aacute; su ticket en breve y le responder&aacute;  convenientemente. A continuaci&oacute;n, los detalles del ticket.

---------------------------

Ticket: <#TICKET_ID#>
Asunto: <#SUBJECT#>
Departamento: <#DEPARTMENT#>
Prioridad: <#PRIORITY#>
Fecha de env&iacute;o: <#SUB_DATE#>

---------------------------

Puede ver su ticket utilizando este enlace: <#TICKET_LINK#>
EOF;

$lang['staff_new_ticket_sub'] = "Ticket #<#TICKET_ID#>";

$lang['staff_new_ticket'] = <<<EOF
Un nuevo ticket ha sido creado en su departamento. A continuaci&oacute;n, los detalles del mismo.

---------------------------

Ticket: <#TICKET_ID#>
Usuario: <#MEMBER#>
Asunto: <#SUBJECT#>
Departamento: <#DEPARTMENT#>
Priordad: <#PRIORITY#>
Fecha de Env&iacute;o: <#SUB_DATE#>

---------------------------

<#MESSAGE#>

---------------------------

Puede administrar este ticket mediante este enlace: <#TICKET_LINK#>
EOF;

$lang['new_guest_ticket_sub'] = "Ticket #<#TICKET_ID#>";

$lang['new_guest_ticket'] = <<<EOF
Ha enviado un nuevo ticket como invitado. Nuestro equipo revisar&aacute; su ticket en breve y le responder&aacute;  convenientemente. A continuaci&oacute;n, los detalles del ticket.

---------------------------

Ticket: <#TICKET_ID#>
Asunto: <#SUBJECT#>
Departamento: <#DEPARTMENT#>
Prioridad: <#PRIORITY#>
Fecha de Env&iacute;o: <#SUB_DATE#>

Clave del Ticket: <#TICKET_KEY#>

---------------------------

Puede ver su ticket utilizando este enlace: <#TICKET_LINK#>
EOF;

$lang['staff_new_guest_ticket_sub'] = "Ticket #<#TICKET_ID#>";

$lang['staff_new_guest_ticket'] = <<<EOF
Un nuevo ticket de invitado ha sido creado en su departamento. A continuaci&oacute;n, los detalles del mismo.

---------------------------

Ticket: <#TICKET_ID#>
Usuario: <#MEMBER#> (Invitado)
Asunto: <#SUBJECT#>
Departamento: <#DEPARTMENT#>
Prioridad: <#PRIORITY#>
Fecha de Env&iacute;o: <#SUB_DATE#>

---------------------------

<#MESSAGE#>

---------------------------

Puede administrar este ticket mediante este enlace: <#TICKET_LINK#>
EOF;

$lang['ticket_escl_sub'] = "Ticket #<#TICKET_ID#> Escalado";

$lang['ticket_escl'] = <<<EOF
Uno de sus tickets ha sido escalado. Nuestros administradores revisar&aacute;n su ticket en breve. A continuaci&oacute;n, los detalles del mismo.

---------------------------

Ticket: <#TICKET_ID#>
Asunto: <#SUBJECT#>
Departamento: <#DEPARTMENT#>
Prioridad: <#PRIORITY#>
Fecha de Env&iacute;o: <#SUB_DATE#>

---------------------------

Puede ver su ticket utilizando este enlace: <#TICKET_LINK#>
EOF;

$lang['ticket_close_sub'] = "Ticket #<#TICKET_ID#> Cerrado";

$lang['ticket_close'] = <<<EOF
Uno de sus tickets ha sido cerrado. A continuaci&oacute;n, los detalles del mismo.

---------------------------

Ticket: <#TICKET_ID#>
Asunto: <#SUBJECT#>
Departamento: <#DEPARTMENT#>
Priordad: <#PRIORITY#>
Fecha de Env&iacute;o: <#SUB_DATE#>

---------------------------

Puede ver su ticket utilizando este enlace: <#TICKET_LINK#>

Si podemos ayudarle en algo m&aacute;s, no dude en hac&eacute;rnoslo saber.
EOF;

$lang['ticket_move_sub'] = "Ticket #<#TICKET_ID#> Movido";

$lang['ticket_move'] = <<<EOF
Uno de sus tickets fue movido a otro departamento. A continuaci&oacute;n, los detalles del mismo.

---------------------------

Ticket: <#TICKET_ID#>
Asunto: <#SUBJECT#>
Departamento anterior: <#OLD_DEPARTMENT#>
Nuevo Departamento: <#NEW_DEPARTMENT#>
Priordad: <#PRIORITY#>
Fecha de Env&iacute;o: <#SUB_DATE#>

---------------------------

Puede ver su ticket utilizando este enlace: <#TICKET_LINK#>

Si podemos ayudarle en algo m&aacute;s, no dude en hac&eacute;rnoslo saber.
EOF;

$lang['ticket_reply_sub'] = "Respuesta al Ticket #<#TICKET_ID#>";

$lang['ticket_reply'] = <<<EOF
Su ticket ha sido respondido.  A continuaci&oacute;n, los detalles del mismo.

---------------------------

<#REPLY#>

---------------------------

Ticket: <#TICKET_ID#>
Asunto: <#SUBJECT#>
Departamento: <#DEPARTMENT#>
Prioridad: <#PRIORITY#>
Fecha de Env&iacute;o: <#SUB_DATE#>

---------------------------

Puede ver su ticket utilizando este enlace: <#TICKET_LINK#>

Si podemos ayudarle en algo m&aacute;s, no dude en hac&eacute;rnoslo saber.
EOF;

$lang['staff_reply_ticket_sub'] = "Respuesta al Ticket #<#TICKET_ID#>";

$lang['staff_reply_ticket'] = <<<EOF
Una respuesta ha sido recibida acerca de un ticket de su departamento. A continuaci&oacute;n, los detalles del mismo.

---------------------------

<#REPLY#>

---------------------------

Ticket: <#TICKET_ID#>
Usuario: <#MEMBER#>
Asunto: <#SUBJECT#>
Departamento: <#DEPARTMENT#>
Prioridad: <#PRIORITY#>
Fecha de Env&iacute;o: <#SUB_DATE#>

---------------------------

Puede administrar este ticket mediante este enlace: <#TICKET_LINK#>
EOF;

$lang['announcement_sub'] = "<#TITLE#>";

$lang['announcement'] = <<<EOF
Un nuevo anuncio, titulado &#039;<#TITLE#>&#039; ha sido realizado.

---------------------------

<#CONTENT#>

---------------------------

Ha recibido estem mensaje porque eligi&oacute; recibir notificaciones sobre nuevos anuncios en su perfil. Si no desea recibir este tipo de mensajes, ingrese a su cuenta y actualice sus preferencias.
EOF;

$lang['reset_pass_val_sub'] = "Reinicializar su Contraseña";

$lang['reset_pass_val'] = <<<EOF
Ha solicitado reinicializar su contrase&ntilde;a en <#HD_NAME#>. Para completar este proceso, haga click en el enlace de validaci&oacute;n que se muestra m&aacute;s abajo. Si no ha solicitado reinicializar su contrase&ntilde;a, por favor ignore este mensaje.

---------------------------

Usuario: <#USER_NAME#>
Enlace de Validaci&oacute;n: <#VAL_LINK#>
EOF;

$lang['ticket_pipe_rejected_sub'] = "Ticket No Aceptado";

$lang['ticket_pipe_rejected'] = <<<EOF
Nos vemos imposiblitados de aceptar su mensaje de correo electr&oacute;nico y crear un ticket debido a que no posee los permisos necesarios para crear tickets en este departamento.

Si cree que este mensaje es un error, por favor contacte a un administrador.
EOF;

$lang['new_user_admin_val_sub'] = "Nuevo Registro: <#USER_NAME#>";

$lang['new_user_admin_val'] = <<<EOF
Un nuevo usuario se ha registrado y est&aacute; esperando la aprobaci&oacute;n de los administradores. A continuaci&oacute;n, los detalles del mismo.

---------------------------

Usuario: <#USER_NAME#>
Email: <#USER_EMAIL#>
Fecha de Registro: <#JOIN_DATE#>

---------------------------

Puede administrar los usuarios esperando aprobaci&oacute;n utiizando este enlace: <#APPROVE_LINK#>
EOF;

?>