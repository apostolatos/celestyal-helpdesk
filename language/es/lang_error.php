<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | lang_error.php
#======================================================
*/

$lang = array(

'already_rated' => 'Ud ya valor&oacute; este art&iacute;culo.',
'already_rated_reply' => 'Ud. ya valor&oacute; esta respuesta.',
'already_validated' => 'Su cuenta ya estaba activada.',
'banned_kb' => 'Ud. no tiene los permisos necesarios para acceder a la base de conocimientos.',
'banned_kb_comment' => 'Ud. no tiene los permisos necesarios para comentar en los art&iacute;culos.',
'banned_kb_rate' => 'Ud. no tiene los permisos necesarios para valorar art&iacute;culos.',
'banned_news_comment' => 'Ud. no tiene los permisos necesarios para comentar en las noticias.',
'banned_ticket' => 'Ud. no tiene los permisos necesarios para ingresar al centro de tickets.',
'banned_ticket_escalate' => 'Ud. no tiene los permisos necesarios para escalar un ticket.',
'banned_ticket_open' => 'Ud. no tiene los permisos necesarios para crear un ticket.',
'banned_ticket_rate' => 'Ud. no tiene los permisos necesarios para valorar las respuestas del equipo de soporte.',
'error' => 'Error',
'error_has_occured' => 'Ups! Parece que algo no funcion&oacute; como lo esper&aacute;bamos',
'fill_form_completely' => 'Debe completar el formulario antes de enviarlo.',
'fill_form_lengths' => 'Uno o m&aacute;s de los campos que complet&oacute; no tienen la longitud m&iacute;nima requerida.',
'invalid_rate_value' => 'Debe seleccionar una valoraci&oacute;n entre 1 y 5.',
'invalid_rate_value_reply' => 'Debe seleccionar &quot;Positivo&quot; o &quot;Negativo&quot;.',
'kb_comment_disabled' => 'Los comentarios han sido desactivados.',
'kb_disabled' => 'La base de conocimientos ha sido desactivada.',
'kb_rating_disabled' => 'La valoraci&oacute;n ha sido desactivada.',
'login_must_val' => 'Debe activar su cuenta haciendo click en el enlace que le enviamos por email. Si no ha recibido ese mensaje, &lt;a href=&quot;index.php?act=register&amp;code=sendval&quot;&gt;haga click aqu&iacute;&lt;/a&gt;.',
'login_must_val_admin' => 'Debe esperar hasta que un administrador apruebe manualmente su cuenta. Recibir&aacute; un mensaje por email cuando esto suceda.',
'login_no_pass' => 'La contrase&ntilde;a es incorrecta. &lt;a href=&quot;index.php?act=register&amp;code=forgot&quot;&gt;&iquest;Olvid&oacute; su contrase&ntilde;a?&lt;/a&gt;',
'login_no_user' => 'No es posible encontrar un usuario que corresponda con ese nombre de usuario.',
'logout_no_key' => 'Clave de salida no v&aacute;lida.',
'must_be_guest' => 'S&oacute;lo los invitados pueden acceder a esta p&aacute;gina',
'must_be_user' => 'Debe ingresar al sistema antes de poder acceder a esta p&aacute;gina.',
'new_tickets_disabled' => 'Un administrador ha desactivado la creaci&oacute;n de tickets.',
'news_comment_disabled' => 'Los comentarios han sido desactivados.',
'news_disabled' => 'La p&aacute;gina de noticias ha sido desactivada.',
'no_announcement' => 'El anuncio que estaba buscando no pudo ser encontrado.',
'no_article' => 'El art&iacute;culo que estaba buscando no pudo ser encontrado.',
'no_category' => 'La categor&iacute;a que estaba buscando no pudo ser encontrada.',
'no_comment' => 'El comentario que estaba buscando no pudo ser encontrado.',
'no_department' => 'El departamento que estaba buscando no pudo ser encontrado.',
'no_email_val_key' => 'No fue posible encontrar su clave de validaci&oacute;n.',
'no_member' => 'El usuario que estaba buscando no pudo ser encontrado.',
'no_page' => 'La p&aacute;gina que estaba buscando no pudo ser encontrada.',
'no_pass_match' => 'Las contrase&ntilde;as no coinciden.',
'no_perm_access' => 'No est&aacute; autorizado a acceder a esta &aacute;rea.',
'no_perm_banned' => 'No est&aacute; autorizado a acceder a este sistema de soporte.',
'no_perm_com_delete' => 'No est&aacute; autorizado a eliminar este comentario.',
'no_perm_com_edit' => 'No est&aacute; autorizado a editar este comentario.',
'no_perm_reply_delete' => 'No est&aacute; autorizado a eliminar esta respuesta.',
'no_perm_reply_edit' => 'No est&aacute; autorizado a editar esta respuesta.',
'no_perm_ticket_edit' => 'No est&aacute; autorizado a editar este ticket.',
'no_reply' => 'La respuesta que estaba buscando no pudo ser encontrada.',
'no_staff_rate_reply' => 'S&oacute;lo las respuestas del equipo de soporte pueden ser valoradas.',
'no_ticket' => 'El ticket que estaba buscando no pudo ser encontrado.',
'no_ticket_guest' => 'No fue posible encontrar un ticket correspondiente a la direcci&oacute;n de email y la clave de ticket proporcionadas.',
'no_valid_email' => 'Ingrese una direcci&oacute;n de email v&aacute;lida.',
'no_valid_tkey' => 'Ingrese una clave de ticket v&aacute;lida.',
'registration_disabled' => 'Un administrador ha desactivado los nuevos registros.',
'reply_rating_disabled' => 'La valoraci&oacute;n ha sido desactivada.',
'ticket_closed_already' => 'Este ticket ya hab&iacute;a sido cerrado.',
'ticket_closed_escalate' => 'Este ticket est&aacute; cerrado, y por lo tanto no puede ser escalado.',
'ticket_closed_reply' => 'Este ticket esta cerrado, por lo que no es posible responderlo.',
'ticket_escalate_perm' => 'No est&aacute; autorizado a escalar este ticket.',
'ticket_escalated_already' => 'Este ticket ya fue previamente escalado.',
'ticket_no_close_perm' => 'No est&aacute; autorizado a cerrar este ticket.',
'token_mismatch' => 'El token de su formulario no pudo ser verificado.',
'try_again' => 'Por favor regrese y reintente. Si el problema persiste, contacte a un administrador.',

);

?>