<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | lang_global.php
#======================================================
*/

$lang = array(

'code' => 'es',
'aca' => 'ERC',
'aca_full' => 'Esperando respuesta del cliente',
'account_info' => 'Informaci&oacute;n de la cuenta',
'admin' => 'Admin',
'announcements' => 'Anuncios',
'bytes' => 'Bytes',
'closed' => 'Cerrado',
'days_ago' => 'D&iacute;as',
'department' => 'Departamento',
'enter_keywords' => 'Ingrese palabras clave',
'escalated' => 'Escalado',
'forgot_pass' => '&iquest;Olvid&oacute; su Contrase&ntilde;a?',
'gmt' => 'GMT',
'gmt_n_100' => 'GMT - 1:00 Horas',
'gmt_n_1000' => 'GMT - 10:00 Horas',
'gmt_n_1100' => 'GMT - 11:00 Horas',
'gmt_n_1200' => 'GMT - 12:00 Horas',
'gmt_n_200' => 'GMT - 2:00 Horas',
'gmt_n_300' => 'GMT - 3:00 Horas',
'gmt_n_3500' => 'GMT - 3:30 Horas',
'gmt_n_400' => 'GMT - 4:00 Horas',
'gmt_n_500' => 'GMT - 5:00 Horas',
'gmt_n_600' => 'GMT - 6:00 Horas',
'gmt_n_700' => 'GMT - 7:00 Horas',
'gmt_n_800' => 'GMT - 8:00 Horas',
'gmt_n_900' => 'GMT - 9:00 Horas',
'gmt_p_100' => 'GMT + 1:00 Horas',
'gmt_p_1000' => 'GMT + 10:00 Horas',
'gmt_p_1100' => 'GMT + 11:00 Horas',
'gmt_p_1200' => 'GMT + 12:00 Horas',
'gmt_p_200' => 'GMT + 2:00 Horas',
'gmt_p_300' => 'GMT + 3:00 Horas',
'gmt_p_350' => 'GMT + 3:30 Horas',
'gmt_p_400' => 'GMT + 4:00 Horas',
'gmt_p_450' => 'GMT + 4:30 Horas',
'gmt_p_500' => 'GMT + 5:00 Horas',
'gmt_p_550' => 'GMT + 5:30 Horas',
'gmt_p_600' => 'GMT + 6:00 Horas',
'gmt_p_700' => 'GMT + 7:00 Horas',
'gmt_p_800' => 'GMT + 8:00 Horas',
'gmt_p_900' => 'GMT + 9:00 Horas',
'gmt_p_950' => 'GMT + 9:30 Horas',
'guest' => 'Invitado',
'home' => 'Inicio',
'hours_ago' => 'Horas',
'id' => 'ID',
'in_progress' => 'En Progreso',
'kb' => 'KB',
'knowledge_base' => 'Base de Conocimientos',
'less_than_a_minute_ago' => 'Hace menos de un minuto',
'log_in' => 'Ingresar',
'log_in_button' => 'Ingresar',
'logout' => 'Salir',
'mb' => 'MB',
'minutes_ago' => 'Minutos',
'most_popular_articles' => 'Art&iacute;culos m&aacute;s Populares',
'my_account' => 'Mi Cuenta',
'news' => 'Noticias',
'no' => 'No',
'no_login_tickets' => 'Por favor ingrese para ver sus tickets.',
'no_tickets' => 'No hay tickets que mostrar.',
'no_tickets_short' => 'No hay tickets que mostrar.',
'on_hold' => 'En espera',
'open' => 'Abierto',
'open_ticket' => 'Enviar un Ticket',
'password' => 'Contrase&ntilde;a',
'print' => 'Imprimir',
'priority' => 'Prioridad',
'priority_1' => 'Baja',
'priority_2' => 'Media',
'priority_3' => 'Alta',
'priority_4' => 'Urgente',
'recently_added_articles' => 'Art&iacute;culos recientemente a&ntilde;adidos',
'redirect' => 'Redirigir',
'register' => 'Registro',
'remember_me' => 'Recordarme',
'resend_val' => 'Reenviar Validaci&oacute;n',
'search' => 'Buscar',
'search_button' => 'Buscar',
'status' => 'Estado',
'subject' => 'Asunto',
'submitted' => 'Enviado',
'ticket_history' => 'Historial de Tickets',
'ticket_overview' => 'Resumen de Tickets',
'tickets' => 'Tickets',
'today' => 'Hoy',
'trellis_desk' => 'Trellis Desk',
'upgrade_account' => 'Actualizar Cuenta',
'username' => 'Usuario',
'weeks_ago' => 'Semanas',
'welcome' => 'Bienvenido',
'yes' => 'S&iacute;',
'yesterday' => 'Ayer',
'your_tickets' => 'Sus Tickets',
'is_required' => 'se requiere',
'message' => 'Μensaje',

'manage_booking' => 'Manage Booking',
'about_us' => 'About Us',
'our_mission' => 'Our Mission',
'our_fleet' => 'Our Fleet',
'cruises' => 'Cruises',
'itineraries' => 'Itineraries',
'destinations' => 'Destinations',
'Excursions' => 'Excursions',
'drinks_packages' => 'Drinks Packages',
'experience' => 'Experience',
'themed_cruises' => 'Themed cruises',
'onboard' => 'onboard',
'ashore' => 'Ashore'

);

?>