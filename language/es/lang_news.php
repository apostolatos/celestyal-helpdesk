<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | lang_news.php
#======================================================
*/

$lang = array(

'add_a_comment' => 'A&ntilde;adir Comentario',
'add_comment_button' => 'A&ntilde;adir Comentario',
'announcement' => 'Anuncio',
'comments' => 'Comentarios',
'confirm_delete' => '&iquest;Est&aacute; seguro que desea eliminar este comentario?',
'delete' => 'Eliminar',
'edit' => 'Editar',
'edit_comment' => 'Editar Comentario',
'edit_comment_button' => 'Editar Comentario',
'err_no_comment' => 'Ingrese un comentario.',
'no_comments' => 'A&uacute;n no se han realizado comentarios sobre este anuncio.',
'no_news' => 'No hay anuncios que mostrar.',
'viewing_announcement' => 'Visualizando Anuncio',

);

?>