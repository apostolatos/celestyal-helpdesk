<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | lang_redirect.php
#======================================================
*/

$lang = array(

'add_rating_success' => 'Su valoraci&oacute;n fue registrada con &eacute;xito.',
'almost_acc_activate' => 'Su direcci&oacute;n de email ha sido verificada con &eacute;xito. Antes de poder comenzar a utilizar su cuenta, un administrador debe aprobarla manualmente. Recibir&aacute; un mensaje por email cuando esto suceda.',
'change_val_email' => 'Antes de modificar su email, debe hacer click en el enlace de validaci&oacute;n que figura en el mensaje de email que fue enviado a su nueva direcci&oacute;n.',
'click_here' => 'click aqu&iacute;',
'delete_comment_success' => 'El comentario fue eliminado con &eacute;xito.',
'edit_comment_success' => 'El comentario fue editado con &eacute;xito',
'login_success' => 'Ud. ha ingresado con &eacute;xito.',
'logout_success' => 'Ud. ha salido con &eacute;xito.',
'new_user_no_val' => 'Su cuenta fue creada con &eacute;xito. Puede ahora ingresar a la misma.',
'new_user_val_admin' => 'Antes de poder comenzar a utilizar su cuenta, un administrador debe aprobarla manualmente. Recibir&aacute; un mensaje de email cuando esto ocurra.',
'new_user_val_both' => 'Antes de poder comenzar a utilizar su cuenta, un administrador debe aprobarla manualmente y Ud. debe hacer click en el enlace de validaci&oacute;n incluido en el email que fue enviado a su cuenta de correo electr&oacute;nico.',
'new_user_val_email' => 'Antes de poder comenzar a utilizar su cuenta, Ud. debe hacer click en el enlace de validaci&oacute;n incluido en el email que fue enviado a su cuenta de correo electr&oacute;nico.',
'new_user_val_resend' => 'Un nuevo email de validaci&oacute;n fue enviado a su direcci&oacute;n de correo electr&oacute;nico.',
'please_wait' => 'Por favor espere',
'reply_delete_success' => 'La respuesta fue eliminada con &eacute;xito.',
'reply_edit_success' => 'La respuesta fue actualizada con &eacute;xito.',
'reset_pass_email_sent' => 'Para reinicializar su contrase&ntilde;a, haga click en el enlace incluido en el email que fue enviado a su cuenta de correo electr&oacute;nico.',
'reset_pass_success' => 'So contrase&ntilde;a fue reinicializada con &eacute;xito. Puede ahora ingresar al sistema.',
'submit_comment_success' => 'Su comentario fue a&ntilde;adido con &eacute;xito.',
'submit_reply_success' => 'Su respuesta fue registrada correctamente.',
'submit_ticket_success' => 'Su ticket fue enviado correctamente.',
'success_acc_activate' => 'Su cuenta ha sido activada. Puede ahora ingresar al sistema.',
'thank_you' => 'Gracias.',
'ticket_close_success' => 'Su ticket fue cerrado con &eacute;xito.',
'ticket_edit_success' => 'Su ticket fue actualizado con &eacute;xito.',
'ticket_escalate_success' => 'Su ticket fue escalado con &eacute;xito.',
'transfer_you' => 'Por favor espere mientras lo transferimos. Si no desea esperar,',
'update_my_account' => 'Su cuenta fue actualizada con &eacute;xito.',
'update_my_email' => 'Su direcci&oacute;n de email fue actualizada con &eacute;xito.',
'update_my_pass' => 'So contrase&ntilde;a fue actualizada con &eacute;xito. Dependiendo de su configuraci&oacute;n de cookies, puede ser necesario que ingrese nuevamente.',

);

?>