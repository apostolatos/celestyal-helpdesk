<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | lang_register.php
#======================================================
*/

$lang = array(

'captcha' => 'Captcha',
'create_account_button' => 'Crear Cuenta',
'email_address' => 'Direcci&oacute;n de Email',
'err_captcha_mismatch' => 'El c&oacute;digo Captcha que ingres&oacute; no corresponde con la imagen. Por favor intente nuevamente.',
'err_email_in_use' => 'Esa direcci&oacute;n de email ya est&aacute; siendo utilizada. Por favor elija otra direcci&oacute;n de email.',
'err_no_cpfield' => 'Ingrese un valor para el campo:',
'err_no_email_valid' => 'Debe ingresar una direcci&oacute;n de email v&aacute;lida.',
'err_no_pass_match' => 'Las contrase&ntilde;as no coinciden.',
'err_no_pass_short' => 'Ingrese una contrase&ntilde;a.',
'err_no_user_or_email' => 'Debe ingresar un nombre de usuario o una direcci&oacute;n de email.',
'err_no_user_short' => 'Ingrese un nombre de usuario.',
'err_user_already_active' => 'Su cuenta ya fue activada previamente. Intente ingresando al sistema. :)',
'err_user_in_use' => 'Ese nombre de usuario ya est&aacute; siendo utilizado. Por favor elija otro nombre de usuario.',
'err_user_not_found' => 'No fue posible encontrar ning&uacute;n usuario con la informaci&oacute;n proporcionada.',
'forgot_password' => 'Contrase&ntilde;a Olvidada',
'new_password' => 'Nueva Contrase&ntilde;a',
'new_password_confirm' => 'Confirme Nueva Contrase&ntilde;a',
'optional' => '(Opcional)',
'or' => 'O',
'password_confirm' => 'Confirme Contrase&ntilde;a',
'register_new_account' => 'Registrar una Nueva Cuenta',
'resend_val_button' => 'Reenviar Validaci&oacute;n',
'reset_pass_button' => 'Reinicializar Contrase&ntilde;a',
'reset_password' => 'Reinicializar Contrase&ntilde;a',
'upgrade_account_button' => 'Actualizar Cuenta',
'upgrade_msg' => 'Debido a que se encuentra logueado como invitado, puede actualizar su cuenta a una cuenta con registro completo, manteniendo sus tickets actuales. Para actualizar su cuenta, simplemente complete el sguiente formulario y haga click en Actualizar Cuenta.',

);

?>