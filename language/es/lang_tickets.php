<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | lang_tickets.php
#======================================================
*/

$lang = array(

'article_suggestions' => 'Art&iacute;culos Sugeridos',
'attachment' => '(Adjunto)',
'attachment_b' => 'Adjunto',
'attachment_max_size' => 'Tama&ntilde;o m&aacute;ximo de adjunto:',
'captcha' => 'Captcha',
'close' => 'Cerrar',
'close_msg_a' => 'Este ticket fue cerrado por',
'close_msg_b' => 'por la siguiente raz&oacute;n.',
'close_ticket' => 'Cerrar Ticket',
'close_ticket_button' => 'Cerrar Ticket',
'closing_ticket' => 'Cerrando Ticket',
'confirm_close' => '&iquest;Est&aacute; seguro que desea cerrar este ticket?',
'confirm_delete_reply' => '&iquest;Est&aacute; seguro que desea eliminar esta respuesta?',
'confirm_escalate' => '&iquest;Est&aacute; seguro que desea escalar este ticket?',
'continue_ticket_submit' => 'Continuar con el env&iacute;o del Ticket',
'download_attachment' => 'Descargar adjunto:',
'edit' => 'Editar',
'edit_reply' => 'Editar Respuesta',
'edit_reply_button' => 'Editar Respuesta',
'edit_ticket' => 'Editar Ticket',
'edit_ticket_button' => 'Editar Ticket',
'email' => 'Email',
'email_address' => 'Direcci&oacute;n de Email',
'enter_close_reason' => 'Por favor ingrese una raz&oacute;n para cerrar este ticket.',
'err_captcha_mismatch' => 'El c&oacute;digo Captcha que ingres&oacute; no corresponde con la imagen. Por favor intente nuevamente.',
'err_email_in_use' => 'Esa direcci&oacute;n de email est&aacute; siendo usanda por uno de nuestros miembros. Por favor utilice otra direcci&oacute;n o inicie sesi&oacute;n.',
'err_no_cdfield' => 'Por favor ingrese un valor en el campo:',
'err_no_depart' => 'Por favor seleccione un departamento.',
'err_no_email' => 'Por favor ingrese una direcci&oacute;n v&aacute;lida de email.',
'err_no_message' => 'Por favor ingrese un mensaje.',
'err_no_name' => 'Por favor ingrese un nombre.',
'err_no_reason' => 'Por favor ingrese una raz&oacute;n.',
'err_no_reply' => 'Por favor ingrese una respuesta.',
'err_no_subject' => 'Por favor ingrese el asunto.',
'err_upload_bad_type' => 'El tipo de archivo que intenta subir no est&aacute; permitido.',
'err_upload_failed' => 'Subida de archivo fall&oacute;. Por favor reintente.',
'err_upload_too_big' => 'El archivo que est&aacute; intentando subir excede el tama&ntilde;o m&aacute;ximo permitido.',
'escalate' => 'Escalar',
'guest_login' => 'Entrar como Invitado',
'guest_login_info' => 'Bienvenido Invitado. Por favor ingrese usando su direcci&oacute;n de e-mail y la clave del ticket para acceder a su ticket. Si es un miembro registrado, por favor inicie sesi&oacute;n utilizando el cuadro de la derecha.',
'guest_ticket_notification' => 'Notificaci&oacute;n por Email de respuestas del Personal',
'history' => 'Historial',
'last_replier' => '&Uacute;ltimo en responder',
'last_reply' => '&Uacute;ltima Respuesta',
'name' => 'Nombre',
'no_replies' => 'Lo sentimos, no se han realizado respuestas a este ticket.',
'no_suggestions_helped' => 'Ninguna de las sugerencias de arriba resuelve mi problema.',
'open_ticket_button' => 'Enviar',
'optional' => '(Opcional)',
'relevance' => 'Relevancia',
'replies' => 'Respuestas',
'select_depart' => 'Por favor seleccione un departamento.',
'send_reply' => 'Enviar Respuesta',
'send_reply_button' => 'Enviar Respuesta',
'suggestions_explained' => 'La siguiente es una lista de art&iacute;culos de la base de conocimiento que podr&iacute;an resolver su inquietud. Por favor revise esos art&iacute;culos y verifique que no pueden ayudarlo antes de continuar con el env&iacute;o de su ticket. &lt;i&gt;Su ticket no ha sido enviado a&uacute;n.&lt;/i&gt;',
'thumbs_down' => 'Positivo',
'thumbs_up' => 'Negativo',
'ticket_center_overview' => 'Resumen de Tickets',
'ticket_id' => 'Id de Ticket',
'ticket_key' => 'Clave de Ticket',
'viewing_ticket' => 'Viendo un Ticket',

);

?>