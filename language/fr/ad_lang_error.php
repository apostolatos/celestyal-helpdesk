<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | ad_lang_error.php
#======================================================
*/

$lang = array(

'error' => 'Erreur',
'error_has_occured' => 'Whoops! On dirait que quelque chose a mal tourné.',
'fill_form_completely' => 'Vous devez remplir complètement le formulaire avant de le soumettre.',
'fill_form_lengths' => 'Un ou plusieurs des champs remplis ne contiennent pas la longueur minimal requise.',
'login_no_admin' => 'Désolé, vous n\'avez pas accès au PCA.',
'login_no_pass' => 'Désolé, le mot de passe est incorrect.',
'login_no_user' => 'Nous n\'avons pas pu trouver un membre qui correspond à ce nom d\'utilisateur.',
'must_login' => 'Vous devez vous identifier avant d\'accéder au PCA.',
'news_disabled' => 'L\'annonce a été désactivée. <a href=\'<! HD_URL !>/admin?.phpsection=manage&act=settings&code=find&group=news\'>Cliquez ici</a> pour gérer les paramètres de votre annonce.',
'no_article' => 'Désolé, l\'article que vous cherchez n\'a pas pu être trouvé.',
'no_articles_found' => 'Désolé, mais nous n\'avons pas trouver d\'articles qui correspondent à vos critères.',
'no_attachment' => 'Désolé, la pièce jointe que vous cherchez n\'a pas pu être trouvé.',
'no_canned' => 'Désolé, les réponses en canne que vous cherchez n\'ont pas pus être trouvés.',
'no_category' => 'Désolé, la catégorie que vous cherchez n\'a pas pu être trouvé.',
'no_create_lang' => 'Désolé, nous ne pouvions pas créer les fichiers de langue. Veuillez faire un CHMOD du dossier \'languages\' à 0777.',
'no_delete_default_lang' => 'Désolé, mais vous ne pouvez pas supprimer la langue par défaut.',
'no_delete_default_skin' => 'Désolé, mais vous ne pouvez pas supprimer le skin par défaut.',
'no_department' => 'Désolé, le département que vous cherchez n\'a pas pu être trouvé.',
'no_dfield' => 'Désolé, le département personnalisé que vous cherchez n\'a pas pu être trouvé.',
'no_export_templates' => 'Désolé, le pacquet de skin ne peut pas être généré, car il n\'existe pas de modèles à l\'exportation.',
'no_group' => 'Désolé, le groupe que vous cherchez n\'a pas pu être trouvé.',
'no_lang' => 'Désolé, la langue que vous cherchez n\'a pas pu être trouvé.',
'no_member' => 'Désolé, le membre que vous cherchez n\'a pas pu être trouvé.',
'no_members_found' => 'Désolé, nous n\'avons trouvé aucun membres correspondant à vos critères.',
'no_message' => 'Veuillez entrez un message.',
'no_mm_tickets' => 'Désolé, mais vous n\'avez pas sélectionner un ticket.',
'no_mm_valid_tickets' => 'Désolé, mais aucune des tickets que vous avez sélectionnés sont valables pour cette action.',
'no_open_file' => 'Désolé, le fichier n\'a pas pu être ouvert.',
'no_page' => 'Désolé, la page que vous cherchez n\'a pas pu être trouvée.',
'no_perm' => 'Désolé, vous n\'avez pas accès à cette zone.',
'no_perm_banned' => 'Désolé, vous n\'êtes pas autorisé à accéder à ce bureau de soutien.',
'no_pfield' => 'Désolé, le champ de profil personnalisé que vous cherchez n\'a pas pu être trouvé.',
'no_reply' => 'Désolé, la réponse que vous cherchez n\'a pas pu être trouvé.',
'no_settings_found' => 'Désolé, les paramètres que vous cherchez n\'ont pas pu être trouvés.',
'no_skin' => 'Désolé, le skin que vous cherchez n\'a pas pu être trouvé.',
'no_subject' => 'Veuillez entrez un sujet.',
'no_template' => 'Désolé, le modèle que vous cherchez n\'a pas pu être trouvé.',
'no_ticket' => 'Désolé, le ticket que vous cherchez n\'a pas pu être trouvé.',
'no_upload_lang_xml' => 'Désolé, le fichier que vous avez essayé de télécharger n\'est pas un Fichier XML Trellis Desk de Langue valide.',
'no_upload_move' => 'Désolé, nous n\'avons pas pu charger le fichier. Veuillez faire un CHMOD du répertoire \'tmp\' à 0777.',
'no_upload_size' => 'Désolé, le fichier que vous avez essayé de téléverser ne contient pas de données.',
'no_upload_skin_xml' => 'Désolé, le fichier que vous avez essayé de téléverser n\'est pas un Fichier XML Trellis Desk de Skin valide.',
'not_writable' => 'Désolé, le fichier que vous essayez de modifier n\'est pas inscriptible. Veuillez faire un CHMOD à 0777.',
'not_writable_img_dir' => 'Désolé, nous ne pouvons pas nous créer un skin parce que le répertoire ./images/ n\'est pas inscriptible. Veuillez faire un CHMOD à 0777 de ce répertoire.',
'not_writable_skin' => 'Désolé, nous ne pouvons pas nous créer un skin parce que le répertoire ./skin/ n\'est pas inscriptible. Veuillez faire un CHMOD à 0777 de ce répertoire.',
'prune_no_days' => 'Désolé, vous n\'avez pas entré un nombre de jours.',
'root_edit_mem' => 'Seule l\'administrateur peut modifier ce membre.',
'ticket_closed_already' => 'Désolé, ce ticket a déjà été fermé.',
'ticket_closed_escalate' => 'Désolé, mais ce ticket est fermé donc vous ne pouvez pas faire escalader ce ticket.',
'ticket_closed_hold' => 'Désolé, mais ce ticket est fermé donc vous ne pouvez pas le placer en attente.',
'ticket_closed_reply' => 'Désolé, mais ce ticket est fermé donc vous ne pouvez pas y répondre.',
'ticket_escalated_already' => 'Désolé, le ticket a déjà été escaladé.',
'ticket_hold_already' => 'Désolé, ce ticket a déjà été mis en attente.',
'ticket_reopen_already' => 'Désolé, mais ce ticket est déjà ouvert.',
'try_again' => 'Veuillez revenir en arrière et essayer à nouveau. Si le problème persiste, veuillez contacter un administrateur.',
'upload_bad_type' => 'Le type de fichier que vous essayez de transférer n\'est pas autorisé.',
'upload_failed' => 'échec de l\'envoi de dossier. Veuillez essayer à nouveau.',
'upload_too_big' => 'Le fichier que vous essayez de transférer dépassé la taille maximum de téléversement.',

);

?>