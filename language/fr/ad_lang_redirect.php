<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | ad_lang_redirect.php
#======================================================
*/

$lang = array(

'add_announce_success' => 'L\'annonce a été ajoutée.',
'add_article_success' => 'L\'article a été ajouté.',
'add_canned_success' => 'La réponse en canne a été ajoutée.',
'add_cat_success' => 'Cette catégorie a été ajoutée.',
'add_depart_success' => 'Le département a été ajouté.',
'add_dfield_success' => 'Le département personnalisé a été ajouté.',
'add_group_success' => 'Le groupe a été ajouté.',
'add_lang_success' => 'La langue a été ajoutée.',
'add_page_success' => 'La page personnalisée a été ajoutée.',
'add_pfield_success' => 'Le champ de profil personnalisé a été ajouté.',
'add_user_success' => 'Le membre a été ajouté et peut maintenant se connecter.',
'approve_user_success' => 'Le membre a été approuvé.',
'cache_rebuilt' => 'Le cache a été reconstruit.',
'click_here' => 'cliquez ici',
'css_skin_success' => 'Le CSS a été mis à jour.',
'default_lang_success' => 'La langue a été définie par défaut.',
'default_skin_success' => 'Le skin a été défini par défaut.',
'delete_announce_success' => 'L\'annonce a été supprimé avec succès.',
'delete_article_success' => 'L\'article a été supprimé avec succès.',
'delete_canned_success' => 'La réponse en cannea été supprimée avec succès.',
'delete_cat_success' => 'Cette catégorie a été supprimée avec succès.',
'delete_depart_success' => 'Le département a été supprimé avec succès.',
'delete_dfield_success' => 'Le champ de département personnalisé a été supprimé avec succès.',
'delete_group_success' => 'Le groupe a été supprimé avec succès.',
'delete_lang_success' => 'La langue a bien été supprimée.',
'delete_page_success' => 'La page personnalisée a été supprimée avec succès.',
'delete_pfield_success' => 'Le champ de profil personnalisé a été supprimé avec succès.',
'delete_skin_success' => 'Le skin a été supprimé avec succès.',
'delete_user_success' => 'Le membre a bien été supprimée.',
'edit_announce_success' => 'L\'annonce a été mise à jour.',
'edit_article_success' => 'L\'article a été mis à jour.',
'edit_canned_success' => 'La réponse en canne a été mise à jour.',
'edit_cat_success' => 'Cette catégorie a été mise à jour.',
'edit_depart_success' => 'Le département a été mis à jour.',
'edit_dfield_success' => 'Le champ de département personnalisé a été mis à jour.',
'edit_group_success' => 'Le groupe a été mis à jour.',
'edit_lang_success' => 'Le fichier de langue a été mis à jour.',
'edit_page_success' => 'La page personnalisée a été mise à jour.',
'edit_pfield_success' => 'Le champ de profil personnalisé a été mis à jour.',
'edit_user_success' => 'Le membre a été mis à jour.',
'group_add_skin_success' => 'Le groupe a été ajouté.',
'group_del_skin_success' => 'Le groupe a été supprimé avec succès.',
'group_edit_skin_success' => 'Le groupe a été mis à jour.',
'import_lang_success' => 'La langue a été importée.',
'import_skin_success' => 'Le skin a été importé avec succès.',
'login_success' => 'Vous avez été connecté avec succès.',
'logout_success' => 'Vous êtes maintenant déconnecté.',
'maint_recount' => 'Les articles ont été recomptés.',
'please_wait' => 'Veuillez Patienter',
'prop_lang_success' => 'La langue a été mise à jour avec succès.',
'prop_skin_success' => 'Le skin a été mis à jour avec succès.',
'prune_logs_success' => 'Les journaux ont été nettoyés.',
'reorder_depart_success' => 'Les départements ont été réorganisés avec succès.',
'reply_delete_success' => 'La réponse a été supprimé avec succès.',
'reply_edit_success' => 'La réponse a été mis à jour.',
'setting_revert_success' => 'Les paramètres ont été rétablies.',
'settings_update_success' => 'Les paramètres ont été mis à jour et mis en cache.',
'spring_clean_success' => 'Nettoyage de printemps a terminé avec succès.',
'submit_reply_success' => 'Votre réponse a été ajoutée.',
'submit_ticket_success' => 'Votre ticket a été soumis avec succès.',
'switch_lang_success' => 'Les membres ont été mis à jour.',
'switch_skin_success' => 'Les membres ont été mis à jour.',
'template_add_skin_success' => 'Le modèle a été ajoutée.',
'template_del_skin_success' => 'Le modèle a été supprimé avec succès.',
'template_skin_success' => 'Le modèle a été mis à jour.',
'thank_you' => 'Merci.',
'ticket_close_success' => 'Le ticket a été fermé.',
'ticket_delete_success' => 'Le ticket a été supprimé avec succès.',
'ticket_edit_success' => 'Le ticket a été mis à jour.',
'ticket_escalate_success' => 'Le ticket a été augmenté.',
'ticket_hold_success' => 'Le ticket a été mis en attente.',
'ticket_move_success' => 'Le ticket a été transféré avec succès.',
'ticket_multi_success' => 'Les multiples actions sur les tickets ont été appliquées avec succès.',
'ticket_reopen_success' => 'Le ticket a été rouvert.',
'transfer_you' => 'S\'il vous plaît patienter pendant que nous vous transférons. Si vous ne souhaitez pas attendre,',
'wrapper_skin_success' => 'L\'emballage a été mis à jour.',

);

?>