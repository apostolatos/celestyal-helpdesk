<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | lang_account.php
#======================================================
*/

$lang = array(

'account_overview' => 'Survol de votre Compte',
'account_settings' => 'Paramètres Compte',
'change_email' => 'Modifier Courriel',
'change_email_address' => 'Modifier Adresse Courriel',
'change_email_button' => 'Modifier Courriel',
'change_pass_button' => 'Modifier Mot de Passe',
'change_password' => 'Modifier Mot de Passe',
'current_password' => 'Mot de Passe Actuel',
'custom_profile_fields' => 'Champs de Profil Personnalisés',
'disabled' => 'Désactivé',
'dst_active' => 'DST Actif',
'email' => 'Courriel',
'email_notifications' => 'Alertes Courriel',
'email_preferences' => 'Préférences Courriel',
'email_staff_new_ticket' => 'Nouveaux Tickets dans Mes Départements',
'email_staff_ticket_reply' => 'Nouvelles Réponses dans Mes Départements',
'email_type' => 'Type Courriel',
'enabled' => 'Activé',
'err_login_no_pass' => 'Votre Mot de Passe Actuel est incorrect.',
'err_no_cpfield' => 'Veuillez entrer une valeur pour le champ:',
'err_no_email_change' => 'Cette adresse courriel est la même que celle actuelle.',
'err_no_email_match' => 'Vos adresses courriels ne concordent pas.',
'err_no_email_valid' => 'Veuillez entrer une adresse courriel valide.',
'err_no_new_pass_short' => 'Veuillez entrer un nouveau mot de passe.',
'err_no_pass_match' => 'Vos mots de passes de concordent pas.',
'err_no_pass_short' => 'Veuillez entrer votre mot de passe actuel.',
'general_info' => 'Information Générale',
'group' => 'Groupe',
'html' => 'HTML',
'joined' => 'Date d\'inscription',
'language' => 'Langue',
'last_visit' => 'Dernière Visite',
'modify_account' => 'Modifier Compte',
'modify_account_information' => 'Modifier Information Compte',
'new_email_address' => 'Nouvelle Adresse Courriel',
'new_email_address_confirm' => 'Confirmer la Nouvelle Adresse Courriel',
'new_password' => 'Nouveau Mot de Passe',
'new_password_confirm' => 'Confirmer Nouveau Mot de Passe',
'new_reply' => 'Nouvelle Réponse',
'new_ticket' => 'Nouveau Ticket',
'notifications_for' => 'Alerte Pour',
'optional' => '(Optionel)',
'plain_text' => 'Texte Brut',
'rich_text_editor' => 'Texte Enrichi',
'skin' => 'Skin',
'time_is_now' => 'Il est maintenant:',
'time_zone' => 'Fuseau Horaire',
'title' => 'Titre',
'update_account' => 'Mise à Jour Compte',

);

?>