<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | lang_article.php
#======================================================
*/

$lang = array(

'add_a_comment' => 'Ajouter un Commentaire',
'add_comment_button' => 'Ajouter Commentaire',
'categories' => 'Catégories',
'confirm_delete' => 'Êtes-vous certain de vouloir supprimer ce commentaire ?',
'delete' => 'Supprimer',
'edit' => 'Modifier',
'edit_comment' => 'Modifier Commentaire',
'edit_comment_button' => 'Modifier Commentaire',
'err_no_comment' => 'Veuillez entrer un commentaire.',
'full_star' => 'Pleine Étoile',
'half_star' => 'Moitié Étoile',
'keywords_phrase' => 'Entrer un mot clé ou une phrase',
'no_articles' => ' Il n\'y a aucun article à afficher.',
'no_articles_found' => 'Aucun article trouvé.',
'no_comments' => 'Aucun commentaire n\'a été fait sur cet article.',
'no_star' => 'Aucune Étoile',
'relevance' => 'Relevant',
'search_results' => 'Résultat de Recherche',
'stars' => 'Étoiles',
'view_category' => 'Voir Catégorie',
'viewing_article' => 'Regarde Article',

);

?>