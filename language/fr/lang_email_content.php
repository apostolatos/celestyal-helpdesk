<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | lang_email_content.php
#======================================================
*/

$lang['header'] = <<<EOF
Chèr <#MEM_NAME#>,
EOF;

$lang['footer'] = <<<EOF
Cordialement,

L'équipe <#HD_NAME#>.

<#HD_URL#>
EOF;

$lang['change_email_val_sub'] = "Vérifier Votre Email";

$lang['change_email_val'] = <<<EOF
Vous avez demandé que votre courriel soit changer. Afin de compléter ce changement, vous devez vérifier ce courriel en cliquant sur le lien de validation ci-dessous.

---------------------------

<#VAL_LINK#>

---------------------------
EOF;

$lang['new_user_val_email_sub'] = "Vérifier Votre Nouveau Compte";

$lang['new_user_val_email'] = <<<EOF
Bienvenue sur <#HD_NAME#>. Vous avez demandé un nouveau compte sur notre bureau de soutient. Afin d'activer votre compte, vous devez vérifier votre adresse courriel en cliquant sur le lien de validation ci-dessous.

---------------------------

Nom d'utilisateur: <#USER_NAME#>

Lien de Validation: <#VAL_LINK#>

---------------------------

Pour compléter la vérification de votre adresse électronique, un administrateur devra manuellement approuver votre compte. Un courrier électronique sera envoyé pour vous notifier quand votre compte sera approuvé.
EOF;

$lang['new_user_val_both_sub'] = "Vérifier Votre Nouveau Compte";

$lang['new_user_val_both'] = <<<EOF
Bienvenue chez <#HD_NAME#>. Vous avez demandé un nouveau compte sur notre bureau de soutient. Afin d'activer votre compte, vous devez vérifier votre adresse courriel en cliquant sur le lien de validation ci-dessous. De plus, un administrateur devra manuellement approuver votre compte.

---------------------------

Nom d'utilisateur: <#USER_NAME#>

Lien de Validation: <#VAL_LINK#>

---------------------------

Souvenez vous, en plus de vérifier votre adresse courriel, un administrateur doit manuellement approuver votre compte. Un courriel sera envoyé pour vous informer quand votre compte sera approuvé.
EOF;

$lang['new_user_val_admin_sub'] = "Votre Nouveau Compte";

$lang['new_user_val_admin'] = <<<EOF
Vous avez demandé un nouveau compte sur notre bureau de soutient. Afin d'activer votre compte, vous devez vérifier votre adresse courriel en cliquant sur le lien de validation ci-dessous. De plus, un administrateur devra manuellement approuver votre compte.
EOF;

$lang['acc_accivated_sub'] = "Compte Activé";

$lang['acc_accivated'] = <<<EOF
Votre compte a été activé.�Vous pouvez maintenant vous connecter.
EOF;

$lang['acc_almost_accivated_sub'] = "Compte En Attente d\'Approbation";

$lang['acc_almost_accivated'] = <<<EOF
Votre adresse courriel a été vérifier� mais avant de commencer d'utiliser votre compte, un administrateur doit manuellement l'approuver. Vous receverez un courriel lorsque votre compte sera approuvé.
EOF;

$lang['acc_approved_sub'] = "Compte Approuvé";

$lang['acc_approved'] = <<<EOF
Votre compte a été approuver par un administrateur. Vous pouvez maintenant vous connecter.
EOF;

$lang['acc_almost_approved_sub'] = "Compte En Attente de l\'Approbation Par Courriel";

$lang['acc_almost_approved'] = <<<EOF
Votre compte a été approuver par un administrateur. Mais avant de commencer à utiliser votre compte, vous devez premièrement cliquer sur le lien de validation dans le courriel qui vous a été envoyer à votre adresse courriel.
EOF;

$lang['new_ticket_sub'] = "Ticket ID #<#TICKET_ID#>";

$lang['new_ticket'] = <<<EOF
Vous avez soumis un nouveau ticket. Notre équipe devrait regarder celui-ci bientôt y répondre. Voici les détails du ticket.

---------------------------

Numéro du ticket: <#TICKET_ID#>

Sujet: <#SUBJECT#>

Service: <#DEPARTMENT#>

Priorité: <#PRIORITY#>

Date d'envoi: <#SUB_DATE#>

---------------------------

Vous pouvez voir votre ticket en utilisant ce lien: <#TICKET_LINK#>
EOF;

$lang['staff_new_ticket_sub'] = "Ticket ID #<#TICKET_ID#>";

$lang['staff_new_ticket'] = <<<EOF
Un nouveau ticket a été créer dans votre département. Voici les détails du ticket.

---------------------------

Numéro du ticket: <#TICKET_ID#>

Membre: <#MEMBER#>

Sujet: <#SUBJECT#>

Service: <#DEPARTMENT#>

Priorité: <#PRIORITY#>

Date d'envoi: <#SUB_DATE#>

---------------------------

<#MESSAGE#>

---------------------------

Vous pouvez vous occuper de ce ticket en utilisant ce lien: <#TICKET_LINK#>
EOF;

$lang['new_guest_ticket_sub'] = "Ticket ID #<#TICKET_ID#>";

$lang['new_guest_ticket'] = <<<EOF
Vous avez soumis un nouveau ticket d'invité. Notre équipe devrait bientôt le voir et y répondre. Voici les détails du ticket.

---------------------------

Numéro du ticket: <#TICKET_ID#>

Sujet: <#SUBJECT#>

Service: <#DEPARTMENT#>

Priorité: <#PRIORITY#>

Date d'envoi: <#SUB_DATE#>

Numéro du ticket: <#TICKET_KEY#>

---------------------------

Vous pouvez voir ce ticket en suivant ce lien: <#TICKET_LINK#>
EOF;

$lang['staff_new_guest_ticket_sub'] = "Ticket ID #<#TICKET_ID#>";

$lang['staff_new_guest_ticket'] = <<<EOF
Un nouveau billet d'invité a été créé dans votre département. Voici les détails du ticket.

---------------------------

Numéro du ticket: <#TICKET_ID#>

Membre: <#MEMBER#> (Guest)

Sujet: <#SUBJECT#>

Service: <#DEPARTMENT#>

Priorité: <#PRIORITY#>

Date d'envoi: <#SUB_DATE#>

---------------------------

<#MESSAGE#>

---------------------------

Vous pouvez gérer ce billet en suivant ce lien: <#TICKET_LINK#>
EOF;

$lang['ticket_escl_sub'] = "Ticket ID #<#TICKET_ID#> Escalated";

$lang['ticket_escl'] = <<<EOF
Un de vos tickets a été escalader vers un autre services. Nos dirigeants devraient revoir votre ticket rapidement. Voici les détails du ticket.

---------------------------

Numéro du ticket: <#TICKET_ID#>

Sujet: <#SUBJECT#>

Service: <#DEPARTMENT#>

Priorité: <#PRIORITY#>

Date d'envoi: <#SUB_DATE#>

---------------------------

Vous pouvez voir ce billet en suivant ce lien: <#TICKET_LINK#>
EOF;

$lang['ticket_close_sub'] = "Ticket ID #<#TICKET_ID#> Closed";

$lang['ticket_close'] = <<<EOF
Un de vos ticket a été fermer. Voici les détails du ticket.

---------------------------

Numéro du ticket: <#TICKET_ID#>

Sujet: <#SUBJECT#>

Service: <#DEPARTMENT#>

Priorité: <#PRIORITY#>

Date d'envoi: <#SUB_DATE#>

---------------------------

Vous pouvez voir ce billet en suivant ce lien: <#TICKET_LINK#>

Toutefois si nous pouvons vous aider pour un autre problème, faites le nous savoir.
EOF;

$lang['ticket_move_sub'] = "Ticket ID #<#TICKET_ID#> Moved";

$lang['ticket_move'] = <<<EOF
Un de vos tickets a été deplacer vers un autre service. Voici les détails du ticket.

---------------------------

Numéro du ticket: <#TICKET_ID#>

Sujet: <#SUBJECT#>

Ancien Service: <#Ancien_DEPARTMENT#>

New Service: <#NEW_DEPARTMENT#>

Priorité: <#PRIORITY#>

Date d'envoi: <#SUB_DATE#>

---------------------------

Vous pouvez voir ce billet en suivant ce lien: <#TICKET_LINK#>

Toutefois si nous pouvons vous aider pour un autre problème, faites le nous savoir.
EOF;

$lang['ticket_reply_sub'] = "Ticket ID #<#TICKET_ID#> Reply";

$lang['ticket_reply'] = <<<EOF
Une réponse a été apporter à votre ticket. Voici les détails du ticket.

---------------------------

<#REPLY#>

---------------------------

Numéro du ticket: <#TICKET_ID#>

Sujet: <#SUBJECT#>

Service: <#DEPARTMENT#>

Priorité: <#PRIORITY#>

Date d'envoi: <#SUB_DATE#>

---------------------------

Vous pouvez voir ce billet en suivant ce lien: <#TICKET_LINK#>

Toutefois si nous pouvons vous aider pour un autre problème, faites le nous savoir.
EOF;

$lang['staff_reply_ticket_sub'] = "Ticket ID #<#TICKET_ID#> Reply";

$lang['staff_reply_ticket'] = <<<EOF
Une réponse a été apporter sur un ticket de votre service service. Voici les détails du ticket.

---------------------------

<#REPLY#>

---------------------------

Numéro du ticket: <#TICKET_ID#>

Membre: <#MEMBER#>

Sujet: <#SUBJECT#>

Service: <#DEPARTMENT#>

Priorité: <#PRIORITY#>

Date d'envoi: <#SUB_DATE#>

---------------------------

Vous pouvez gérer ce billet en utilisant ce lien: <#TICKET_LINK#>
EOF;

$lang['announcement_sub'] = "<#TITLE#>";

$lang['announcement'] = <<<EOF
Une nouvelle annonce intitulée '<#TITLE#>' a été créer.

---------------------------

<#CONTENT#>

---------------------------

Vous avez reçu ce courrier électronique parce que vous avez choisi de recevoir des notifications de courrier électronique pour les nouvelles annonces dans votre profil. Si vous voudriez cesser ces courriers électroniques, veuillez mettre à jour vos préférences en vous connectant à votre compte.
EOF;

$lang['reset_pass_val_sub'] = "Réinitialiser Votre Mot De Passe";

$lang['reset_pass_val'] = <<<EOF
Vous avez demander de réinitialiser votre mot de passe du compte <#HD_NAME#>. Pour procéder à la réinitialisation de votre mot de passe, veuillez cliquer sur le lien figurant ci-dessous. Si vous n'avez pas demander la réinitialisation de votre mot de passe, merci d'ignorer ce courrier électronique.

---------------------------

Nom d'utilisateur: <#USER_NAME#>

Lien de validation: <#VAL_LINK#>

---------------------------

EOF;

$lang['ticket_pipe_rejected_sub'] = "Ticket Pas Accepté";

$lang['ticket_pipe_rejected'] = <<<EOF
Il nous est impossible d'enregistrer ce courriel et ce ticket car vous n'avez pas les permissions pour créer un ticket dans ce service.

Si vous pensez que c'est une erreur, merci de contacter un Administrateur.
EOF;

$lang['new_user_admin_val_sub'] = "Nouvel Enregistrement: <#USER_NAME#>";

$lang['new_user_admin_val'] = <<<EOF
Un nouveau membre a été enregistrer mais il est en attente de validation par un administrateur. Ci-dessous vous pouvez voir les détails de ce membre.

---------------------------

Membre: <#USER_NAME#>

Email: <#USER_EMAIL#>

Date d'enregistrement: <#JOIN_DATE#>

---------------------------

Vous pouvez gérer vos membres en attente de validation en utilisant ce lien: <#APPROVE_LINK#>
EOF;

?>