<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | lang_error.php
#======================================================
*/

$lang = array(

'already_rated' => 'Désolé, mais vous avez déjà évalué cet article.',
'already_rated_reply' => 'Désolé, mais vous avez déjà évalué cette réponse.',
'already_validated' => 'Votre compte a déjà été activé.',
'banned_kb' => 'Désolé, vous n\'avez pas l\'autorisation d\'accéder à la base de connaissances.',
'banned_kb_comment' => 'Désolé, vous n\'avez pas l\'autorisation de commenter sur les articles.',
'banned_kb_rate' => 'Désolé, vous n\'avez pas la permission d\'évaluer des articles.',
'banned_news_comment' => 'Désolé, vous n\'avez pas l\'autorisation de faire des commentaires sur les nouvelles.',
'banned_ticket' => 'Désolé, vous n\'avez pas l\'autorisation d\'accéder au centre de tickets.',
'banned_ticket_escalate' => 'Désolé, vous n\'avez pas l\'autorisation de produire un ticket.',
'banned_ticket_open' => 'Désolé, vous n\'avez pas l\'autorisation d\'ouvrir un nouveau ticket.',
'banned_ticket_rate' => 'Désolé, vous n\'avez pas la permission d\'évaluer les réponses du personnel.',
'error' => 'Erreur',
'error_has_occured' => 'Whoops! On dirait que quelque chose a mal tourné.',
'fill_form_completely' => 'Vous devez remplir complètement le formulaire avant de le soumettre.',
'fill_form_lengths' => 'Un ou plusieurs des champs remplis ne contient pas le minimum de caractères permis.',
'invalid_rate_value' => 'Désolé, vous devez choisir une note entre 1 et 5.',
'invalid_rate_value_reply' => 'Désolé, vous devez choisir positif ou un négatif.',
'kb_comment_disabled' => 'Désolé, les commentaires ont été désactivés.',
'kb_disabled' => 'Désolé, la base de connaissances a été désactivé.',
'kb_rating_disabled' => 'Désolé, les votes ont été désactivés.',
'login_must_val' => 'Désolé, vous devez valider votre compte en cliquant sur le lien que vous avez reçu dans notre courriel pour vous. Si vous n\'avez pas reçu de courriel, <a href=\"index.php?act=register&amp;code=sendval\">cliquez ici</a>.',
'login_must_val_admin' => 'Désolé, vous devez attendre que l\'administrateur ait approuvé manuellement votre compte. Vous recevrez un e-mail lorsque votre compte est approuvé.',
'login_no_pass' => 'Désolé, le mot de passe est incorrect. <a href=\"index.php?act=register&amp;code=forgot\">Vous avez oublié votre mot de passe?</a>',
'login_no_user' => 'Nous n\'avons pas pu trouver un membre qui correspond à ce nom d\'utilisateur.',
'logout_no_key' => 'Clé de déconnexion non valide.',
'must_be_guest' => 'Désolé, seuls les visiteurs ont la permission d\'accéder à cette page.',
'must_be_user' => 'Désolé, vous devez être identifié pour accéder à cette page.',
'new_tickets_disabled' => 'Désolé, un administrateur a désactivé nouveaux tickets.',
'news_comment_disabled' => 'Désolé, les commentaires a été désactivé.',
'news_disabled' => 'Désolé, la page de nouvelles a été désactivé.',
'no_announcement' => 'Désolé, l\'annonce que vous cherchez n\'a pas pu être trouvé.',
'no_article' => 'Désolé, l\'article que vous cherchez n\'a pas pu être trouvé.',
'no_category' => 'Désolé, la catégorie que vous cherchez n\'a pas pu être trouvé.',
'no_comment' => 'Désolé, les commentaires que vous cherchez n\'ont pas pu être trouvés.',
'no_department' => 'Désolé, le département que vous cherchez n\'a pas pu être trouvé.',
'no_email_val_key' => 'Désolé, mais nous n\'avons pas trouver votre clé de validation.',
'no_member' => 'Désolé, le membre que vous cherchez n\'a pas pu être trouvé.',
'no_page' => 'Désolé, la page que vous cherchez n\'a pas pu être trouvé.',
'no_pass_match' => 'Vos mots de passe ne correspondent pas.',
'no_perm_access' => 'Désolé, vous n\'êtes pas autorisé à accéder à cette zone.',
'no_perm_banned' => 'Désolé, vous n\'êtes pas autorisé à accéder à ce bureau de soutien.',
'no_perm_com_delete' => 'Désolé, vous n\'êtes pas autorisé à supprimer ce commentaire.',
'no_perm_com_edit' => 'Désolé, vous n\'êtes pas autorisé à modifier ce commentaire.',
'no_perm_reply_delete' => 'Désolé, vous n\'avez pas le droit de supprimer cette réponse.',
'no_perm_reply_edit' => 'Désolé, vous n\'êtes pas autorisé à modifier cette réponse.',
'no_perm_ticket_edit' => 'Désolé, vous n\'avez pas l\'autorisation d\'éditer ce ticket.',
'no_reply' => 'Désolé, la réponse que vous cherchez n\'a pas pu être trouvé.',
'no_staff_rate_reply' => 'Désolé, seul les réponses du personnel peuvent être notées.',
'no_ticket' => 'Désolé, le ticket que vous cherchez n\'a pas pu être trouvé.',
'no_ticket_guest' => 'Désolé, nous n\'avons pas trouvé un ticket correspondant à l\'adresse e-mail et à la clé ticket.',
'no_valid_email' => 'Veuillez entrer une adresse électronique valide.',
'no_valid_tkey' => 'Veuilez entrer une clé ticket valide.',
'registration_disabled' => 'Désolé, un administrateur a désactivé les nouveaux enregistrements.',
'reply_rating_disabled' => 'Désolé, les votes ont été désactivés.',
'ticket_closed_already' => 'Désolé, le ticket a déjà été fermé.',
'ticket_closed_escalate' => 'Désolé, le ticket est fermé donc vous ne pouvez pas faire escalader le billet.',
'ticket_closed_reply' => 'Désolé, le ticket est fermé donc vous ne pouvez pas y répondre.',
'ticket_escalate_perm' => 'Désolé, n\'avez pas l\'autorisation de faire escalader ce ticket.',
'ticket_escalated_already' => 'Désolé, ce ticket a déjà été escaladé.',
'ticket_no_close_perm' => 'Désolé, vous n\'avez pas l\'autorisation de fermer ce ticket.',
'token_mismatch' => 'La clé du formualaire n\'a pas pu être vérifiée.',
'try_again' => 'Veuillez revenir en arrière et essayer à nouveau. Si le problème persiste, veuillez contacter un administrateur.',

);

?>