<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | lang_news.php
#======================================================
*/

$lang = array(

'add_a_comment' => 'Ajouter un Commentaire',
'add_comment_button' => 'Ajouter Commentaire',
'announcement' => 'Annonce',
'comments' => 'Commentaires',
'confirm_delete' => 'Êtes-vous sûr de vouloir supprimer ce commentaire ?',
'delete' => 'Supprimer',
'edit' => 'Modifier',
'edit_comment' => 'Modifier Commentaire',
'edit_comment_button' => 'Modifier Commentaire',
'err_no_comment' => 'Veuillez entrer un commentaire.',
'no_comments' => 'Aucun commentaire n\'a été fait sur cette annonce.',
'no_news' => 'Il n\'y a aucun annonces à afficher.',
'viewing_announcement' => 'Regarde les Annonces',

);

?>