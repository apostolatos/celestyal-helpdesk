<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | lang_redirect.php
#======================================================
*/

$lang = array(

'add_rating_success' => 'Votre vote a été ajoutée.',
'almost_acc_activate' => 'Votre adresse courriel a été vérifiée avec succès. Avant de pouvoir commencer à utiliser votre compte, un administrateur doit approuver manuellement votre compte. Vous recevrez un courriel lorsque votre compte est approuvé.',
'change_val_email' => 'Avant de pouvoir changer votre adresse courriel, vous devez d\'abord cliquer sur le lien de validation dans le courriel qui a été envoyé à votre nouvelle adresse.',
'click_here' => 'cliquez ici',
'delete_comment_success' => 'Le commentaire a été supprimé avec succès.',
'edit_comment_success' => 'Le commentaire a été édité avec succès.',
'login_success' => 'Vous avez été connecté avec succès',
'logout_success' => 'Vous êtes maintenant déconnecté.',
'new_user_no_val' => 'Votre compte a été créé avec succès. Vous pouvez maintenant vous connecter à votre compte.',
'new_user_val_admin' => 'Avant de pouvoir commencer à utiliser votre compte, un administrateur doit approuver manuellement votre compte. Vous recevrez un courriel lorsque votre compte est approuvé.',
'new_user_val_both' => 'Avant de pouvoir commencer à utiliser votre compte, un administrateur doit approuver manuellement votre compte et vous devez cliquer sur le lien de validation dans le courriel qui a été envoyé à votre adresse courriel.',
'new_user_val_email' => 'Avant de pouvoir commencer à utiliser votre compte, vous devez d\'abord cliquer sur le lien de validation dans le courriel qui a été envoyé à votre adresse courriel.',
'new_user_val_resend' => 'Une nouvelle validation courriel a été envoyée à votre adresse courriel.',
'please_wait' => 'Veuillez Patienter',
'reply_delete_success' => 'La réponse a été supprimé avec succès.',
'reply_edit_success' => 'La réponse a été mise à jour.',
'reset_pass_email_sent' => 'Pour réinitialiser votre mot de passe, cliquez sur le lien dans le courriel qui a été envoyé à votre adresse courriel.',
'reset_pass_success' => 'Votre mot de passe a été réinitialisé. Vous pouvez maintenant vous connecter.',
'submit_comment_success' => 'Votre commentaire a été ajouté.',
'submit_reply_success' => 'Votre réponse a été ajoutée.',
'submit_ticket_success' => 'Votre billet a été soumis avec succès.',
'success_acc_activate' => 'Votre compte a été activé avec succès. Vous pouvez maintenant vous connecter à votre compte.',
'thank_you' => 'Merci.',
'ticket_close_success' => 'Votre ticket a été fermé.',
'ticket_edit_success' => 'Le ticket a été mis à jour.',
'ticket_escalate_success' => 'Votre ticket a été escaladé.',
'transfer_you' => 'Veuillez patienter pendant que nous vous transférer. Si vous ne souhaitez pas attendre,',
'update_my_account' => 'Votre compte a été mis à jour.',
'update_my_email' => 'Votre adresse courriel a été mis à jour.',
'update_my_pass' => 'Votre mot de passe a été mis à jour. En fonction de vos paramètres de cookies, vous devrez vous connecter à nouveau.',

);

?>