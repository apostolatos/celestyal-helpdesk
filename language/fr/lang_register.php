<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | lang_register.php
#======================================================
*/

$lang = array(

'captcha' => 'Captcha',
'create_account_button' => 'Créer Compte',
'email_address' => 'Adresse Courriel',
'err_captcha_mismatch' => 'Le code Captcha que vous avez entré ne correspond pas avec l\'image. Veuillez réessayer.',
'err_email_in_use' => 'Désolé, ce courriel est déjà utilisé. Veuillez choisir un autre courriel.',
'err_no_cpfield' => 'Veuillez entrer une valeur pour le champ:',
'err_no_email_valid' => 'Vous devez entrer une adresse courriel valide.',
'err_no_pass_match' => 'Vos mots de passes ne concordent pas.',
'err_no_pass_short' => 'Veuillez entrer un mot de passe.',
'err_no_user_or_email' => 'Vous devez entrer un nom d\'utilisateur ou une adresse courriel.',
'err_no_user_short' => 'Veuillez entrer un nom d\'utilisateur.',
'err_user_already_active' => 'Votre compte a déjà été validé. Essayez de vous connecter. :)',
'err_user_in_use' => 'Désolé, ce nom d\'utilisateur est déjà utilisé. Veuillez choisir un autre nom d\'utilisateur.',
'err_user_not_found' => 'Nous n\'avons pas pu trouver de membres avec les informations de compte fourni.',
'forgot_password' => 'Mot de Passe Oublié',
'new_password' => 'Nouveau Mot de Passe',
'new_password_confirm' => 'Confirmer Nouveau Mot de Passe',
'optional' => ' (Optionel)',
'or' => 'OU',
'password_confirm' => 'Confirmer Mot de Passe',
'register_new_account' => 'Créer un Nouveau Compte',
'resend_val_button' => 'Envoyer Validation à Nouveau',
'reset_pass_button' => 'Réinitialiser Mot de Passe',
'reset_password' => 'Réinitialiser Mot de Passe',
'upgrade_account_button' => 'Mise à Jour du Compte',
'upgrade_msg' => 'Puisque vous êtes déjà connecté en tant que visiteur, vous pouvez mettre à jour votre compte vers un compte complet et enregistré, tout en gardant tous vos tickets actuels. Pour mettre à jour votre compte, remplissez simplement le formulaire ci-dessous et cliquez Mise à Jour du Compte.',

);

?>