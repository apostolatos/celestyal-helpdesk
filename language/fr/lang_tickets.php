<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | lang_tickets.php
#======================================================
*/

$lang = array(

'article_suggestions' => 'Suggestions d\'articles',
'attachment' => '(Attachement)',
'attachment_b' => 'Attachement',
'attachment_max_size' => 'Taille maximale d\'attachement:',
'captcha' => 'Captcha',
'close' => 'Fermer',
'close_msg_a' => 'Ce ticket a été fermé par',
'close_msg_b' => 'pour les raisons suivantes.',
'close_ticket' => 'Fermer Ticket',
'close_ticket_button' => 'Fermer Ticket',
'closing_ticket' => 'Fermeture Ticket',
'confirm_close' => 'êtes-vous certain de vouloir fermer ce ticket?',
'confirm_delete_reply' => 'êtes-vous certains de vouloir supprimer cette réponse?',
'confirm_escalate' => 'êtes-vous certain de vouloir faire monter ce ticket?',
'continue_ticket_submit' => 'Continuer La Soumission Du Ticket',
'download_attachment' => 'Télécharger l\'attachement:',
'edit' => 'Modifier',
'edit_reply' => 'Modifier Réponse',
'edit_reply_button' => 'Modifier Réponse',
'edit_ticket' => 'Modifier Ticket',
'edit_ticket_button' => 'Modifier Ticket',
'email' => 'Email',
'email_address' => 'Adresse Email',
'enter_close_reason' => 'Veuillez entrer une raison pour fermer ce ticket.',
'err_captcha_mismatch' => 'Le code Captcha que vous avez entré ne correspond pas avec l\'image. Veuillez réessayer.',
'err_email_in_use' => 'Ce email est déjê utilisé par un de nos membres. Veuillez utiliser un autre email ou nom d\'utilisateur.',
'err_no_cdfield' => 'Veuillez entrer une valeur pour le champ:',
'err_no_depart' => 'Veuillez séléctionner un service.',
'err_no_email' => 'Veuillez entrer une adresse email valide.',
'err_no_message' => 'Veuillez entrer un message.',
'err_no_name' => 'Veuillez entrer un nom.',
'err_no_reason' => 'Veuillez entrer une raison.',
'err_no_reply' => 'Veuillez entrer une réponse.',
'err_no_subject' => 'Veuillez entrer un sujet.',
'err_upload_bad_type' => 'Le type de fichier que vous essayer d\'envoyer n\'est pas permis.',
'err_upload_failed' => 'Envoi de fichier échoué. Veuillez réessayer.',
'err_upload_too_big' => 'Le fichier que vous avez essayé d\'envoyer excède la taille maximale d\'envoi.',
'escalate' => 'Monter',
'guest_login' => 'Connexion visiteur',
'guest_login_info' => 'Bonjour Visiteur. Veuillez vous connecter en utilisant votre adresse email et numéro de ticket ci-dessous pour accéder ê votre ticket visiteur. Si vous êtes déjê membre enregistré, veuillez vous connecter via le menu de connexion sur la droite.',
'guest_ticket_notification' => 'Notification Email des Réponses de l\'équipe',
'history' => 'Historique',
'last_replier' => 'Dernier Répondant',
'last_reply' => 'Dernière Réponse',
'name' => 'Nom',
'no_replies' => 'Désolé, aucune réponse n\'a été offerte pour ce ticket.',
'no_suggestions_helped' => 'Aucune des suggestions ci-dessus ne m\'a aidé ê résoudre ce problême.',
'open_ticket_button' => 'Soumettre Ticket',
'optional' => '(Optionel)',
'relevance' => 'Pertinence',
'replies' => 'Réponses',
'select_depart' => 'Veuillez séléctionner un service.',
'send_reply' => 'Envoyer Une Réponse',
'send_reply_button' => 'Envoyer Réponse',
'suggestions_explained' => 'Ci-dessous est une liste des articles de la base de connaissance qui pourrait possiblement répondre ê votre question. Veuillez vérifier les articles et vous assurer qu\'ils ne répondent pas ê votre problême avant de continuer la soumission de votre ticket. <i>Votre ticket n\'a pas encore été soumis.</i>',
'thumbs_down' => 'Thumbs Down',
'thumbs_up' => 'Thumbs Up',
'ticket_center_overview' => 'Vue d\'Ensemble du Centre de Ticket',
'ticket_id' => 'Ticket Id',
'ticket_key' => 'Numéro Ticket',
'viewing_ticket' => 'Regarde Un Ticket',

);

?>