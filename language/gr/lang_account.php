<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | lang_account.php
#======================================================
*/

$lang = array(

'account_overview' => '&Epsilon;&pi;&iota;&sigma;&kappa;ό&pi;&eta;&sigma;&eta; &Lambda;&omicron;&gamma;&alpha;&rho;&iota;&alpha;&sigma;&mu;&omicron;ύ',
'account_settings' => '&Rho;&upsilon;&theta;&mu;ί&sigma;&epsilon;&iota;&sigmaf; &lambda;&omicron;&gamma;&alpha;&rho;&iota;&alpha;&sigma;&mu;&omicron;ύ',
'change_email' => '&Alpha;&lambda;&lambda;&alpha;&gamma;ή Email',
'change_email_address' => '&Alpha;&lambda;&lambda;&alpha;&gamma;ή &Delta;&iota;&epsilon;ύ&theta;&upsilon;&nu;&sigma;&eta;&sigmaf; Email',
'change_email_button' => '&Alpha;&lambda;&lambda;&alpha;&gamma;ή Email',
'change_pass_button' => '&Alpha;&lambda;&lambda;&alpha;&gamma;ή &kappa;&omega;&delta;&iota;&kappa;&omicron;ύ',
'change_password' => '&Alpha;&lambda;&lambda;&alpha;&gamma;ή &kappa;&omega;&delta;&iota;&kappa;&omicron;ύ',
'current_password' => '&Tau;&rho;έ&chi;&omega;&nu; Password',
'custom_profile_fields' => 'Custom Profile Fields',
'disabled' => '&Alpha;&pi;&epsilon;&nu;&epsilon;&rho;&gamma;&omicron;&pi;&omicron;&iota;&eta;&mu;έ&nu;&omicron;',
'dst_active' => 'Ώ&rho;&alpha; DST &Epsilon;&nu;&epsilon;&rho;&gamma;ή',
'email' => 'Email',
'email_notifications' => '&Epsilon;&iota;&delta;&omicron;&pi;&omicron;&iota;ή&sigma;&epsilon;&iota;&sigmaf; Email',
'email_preferences' => '&Pi;&rho;&omicron;&tau;&iota;&mu;ή&sigma;&epsilon;&iota;&sigmaf; Email',
'email_staff_new_ticket' => '&Nu;έ&alpha;  &Epsilon;&iota;&sigma;&iota;&tau;ή&rho;&iota;&alpha; &sigma;&tau;&omicron; &Tau;&mu;ή&mu;&alpha;&tau;&alpha; &mu;&omicron;&upsilon;',
'email_staff_ticket_reply' => '&Nu;έ&epsilon;&sigmaf; &Alpha;&pi;&alpha;&nu;&tau;ή&sigma;&epsilon;&iota;&sigmaf; &sigma;&tau;&omicron; &Tau;&mu;ή&mu;&alpha;&tau;&alpha; &mu;&omicron;&upsilon;',
'email_type' => '&Tau;ύ&pi;&omicron;&sigmaf; Email',
'enabled' => '&Epsilon;&nu;&epsilon;&rho;&gamma;ό',
'err_login_no_pass' => '&Omicron; &tau;&rho;έ&chi;&omicron;&nu; &kappa;&omega;&delta;&iota;&kappa;ό&sigmaf; &epsilon;ί&nu;&alpha;&iota; &lambda;ά&theta;&omicron;&sigmaf;',
'err_no_cpfield' => '&Pi;&alpha;&rho;&alpha;&kappa;&alpha;&lambda;ώ &epsilon;&iota;&sigma;ά&gamma;&epsilon;&tau;&epsilon; &mu;&iota;&alpha; &tau;&iota;&mu;ή &gamma;&iota;&alpha; &tau;&omicron; &pi;&epsilon;&delta;ί&omicron;:',
'err_no_email_change' => '&Alpha;&upsilon;&tau;ή &eta; &delta;&iota;&epsilon;ύ&theta;&upsilon;&nu;&sigma;&eta; &eta;&lambda;&epsilon;&kappa;&tau;&rho;&omicron;&nu;&iota;&kappa;&omicron;ύ &tau;&alpha;&chi;&upsilon;&delta;&rho;&omicron;&mu;&epsilon;ί&omicron;&upsilon; &epsilon;ί&nu;&alpha;&iota; &tau;&omicron; ί&delta;&iota;&omicron; &mu;&epsilon; &tau;&eta;&nu; &upsilon;&pi;ά&rho;&chi;&omicron;&upsilon;&sigma;&alpha;',
'err_no_email_match' => '&Tau;&alpha; email &sigma;&alpha;&sigmaf; &delta;&epsilon;&nu; &tau;&alpha;&iota;&rho;&iota;ά&zeta;&omicron;&upsilon;&nu;',
'err_no_email_valid' => '&Pi;&lambda;&eta;&kappa;&tau;&rho;&omicron;&lambda;&omicron;&gamma;ή&sigma;&tau;&epsilon; &mu;&iota;&alpha; έ&gamma;&kappa;&upsilon;&rho;&eta; &delta;&iota;&epsilon;ύ&theta;&upsilon;&nu;&sigma;&eta; email.',
'err_no_new_pass_short' => '&Pi;&alpha;&rho;&alpha;&kappa;&alpha;&lambda;ώ &epsilon;&iota;&sigma;ά&gamma;&epsilon;&tau;&epsilon; έ&nu;&alpha; &nu;έ&omicron; &kappa;&omega;&delta;&iota;&kappa;ό',
'err_no_pass_match' => '&Omicron;&iota; &nu;έ&omicron;&iota; &sigma;&alpha;&sigmaf; &kappa;&omega;&delta;&iota;&kappa;&omicron;ύ&sigmaf; &pi;&rho;ό&sigma;&beta;&alpha;&sigma;&eta;&sigmaf; &delta;&epsilon;&nu; &tau;&alpha;&iota;&rho;&iota;ά&zeta;&omicron;&upsilon;&nu;',
'err_no_pass_short' => '&Pi;&lambda;&eta;&kappa;&tau;&rho;&omicron;&lambda;&omicron;&gamma;ή&sigma;&tau;&epsilon; &tau;&omicron;&nu; &tau;&rho;έ&chi;&omicron;&nu;&tau;&alpha; &kappa;&omega;&delta;&iota;&kappa;ό &pi;&rho;ό&sigma;&beta;&alpha;&sigma;&eta;&sigmaf;',
'general_info' => '&Gamma;&epsilon;&nu;&iota;&kappa;έ&sigmaf; &pi;&lambda;&eta;&rho;&omicron;&phi;&omicron;&rho;ί&epsilon;&sigmaf;',
'group' => '&Omicron;&mu;ά&delta;&alpha;',
'html' => 'HTML',
'joined' => '&Delta;&eta;&mu;&iota;&omicron;&upsilon;&rho;&gamma;ή&theta;&eta;&kappa;&epsilon;',
'language' => '&Gamma;&lambda;ώ&sigma;&sigma;&alpha;',
'last_visit' => '&Tau;&epsilon;&lambda;&epsilon;&upsilon;&tau;&alpha;ί&alpha; &Epsilon;&pi;ί&sigma;&kappa;&epsilon;&psi;&eta;',
'modify_account' => '&Tau;&rho;&omicron;&pi;&omicron;&pi;&omicron;ί&eta;&sigma;&eta; &Lambda;&omicron;&gamma;&alpha;&rho;&iota;&alpha;&sigma;&mu;&omicron;ύ',
'modify_account_information' => '&Tau;&rho;&omicron;&pi;&omicron;&pi;&omicron;ί&eta;&sigma;&eta; &Pi;&lambda;&eta;&rho;&omicron;&phi;&omicron;&rho;&iota;ώ&nu; &Lambda;&omicron;&gamma;&alpha;&rho;&iota;&alpha;&sigma;&mu;&omicron;ύ',
'new_email_address' => '&Nu;έ&alpha; &Eta;&lambda;&epsilon;&kappa;&tau;&rho;&omicron;&nu;&iota;&kappa;ή &Delta;&iota;&epsilon;ύ&theta;&upsilon;&nu;&sigma;&eta;',
'new_email_address_confirm' => '&Epsilon;&pi;&iota;&beta;&epsilon;&beta;&alpha;&iota;ώ&sigma;&tau;&epsilon; &tau;&eta;&nu; &Nu;έ&alpha; &Eta;&lambda;&epsilon;&kappa;&tau;&rho;&omicron;&nu;&iota;&kappa;ή &Delta;&iota;&epsilon;ύ&theta;&upsilon;&nu;&sigma;&eta;',
'new_password' => '&Nu;έ&omicron;&sigmaf; &Kappa;&omega;&delta;&iota;&kappa;ό&sigmaf;',
'new_password_confirm' => '&Epsilon;&pi;&iota;&beta;&epsilon;&beta;&alpha;&iota;ώ&sigma;&tau;&epsilon; &tau;&omicron;&nu; &Nu;έ&omicron; &Kappa;&omega;&delta;&iota;&kappa;ό',
'new_reply' => '&Nu;έ&alpha; &alpha;&pi;ά&nu;&tau;&eta;&sigma;&eta;',
'new_ticket' => '&Nu;έ&omicron; &epsilon;&iota;&sigma;&iota;&tau;ή&rho;&iota;&omicron;',
'notifications_for' => '&Alpha;&nu;&alpha;&kappa;&omicron;&iota;&nu;ώ&sigma;&epsilon;&iota;&sigmaf; &gamma;&iota;&alpha;',
'optional' => '(&Pi;&rho;&omicron;&alpha;&iota;&rho;&epsilon;&tau;&iota;&kappa;ό)',
'plain_text' => '&Alpha;&pi;&lambda;ό &Kappa;&epsilon;ί&mu;&epsilon;&nu;&omicron;',
'rich_text_editor' => 'Rich Text Editor',
'skin' => '&Epsilon;&mu;&phi;ά&nu;&iota;&sigma;&eta;',
'time_is_now' => '&Eta; ώ&rho;&alpha; &epsilon;ί&nu;&alpha;&iota; &tau;ώ&rho;&alpha;:',
'time_zone' => '&Zeta;ώ&nu;&eta; Ώ&rho;&alpha;&sigmaf;',
'title' => '&Tau;ί&tau;&lambda;&omicron;&sigmaf;',
'update_account' => '&Epsilon;&nu;&eta;&mu;έ&rho;&omega;&sigma;&eta; &Lambda;&omicron;&gamma;&alpha;&rho;&iota;&alpha;&sigma;&mu;&omicron;ύ',

);

?>