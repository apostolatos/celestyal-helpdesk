<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | lang_article.php
#======================================================
*/

$lang = array(

'add_a_comment' => '&Pi;&rho;&omicron;&sigma;&theta;ή&kappa;&eta; &sigma;&chi;&omicron;&lambda;ί&omicron;&upsilon;',
'add_comment_button' => '&Pi;&rho;&omicron;&sigma;&theta;ή&kappa;&eta; &sigma;&chi;&omicron;&lambda;ί&omicron;&upsilon;',
'categories' => '&Kappa;&alpha;&tau;&eta;&gamma;&omicron;&rho;ί&epsilon;&sigmaf;',
'confirm_delete' => '&Epsilon;ί&sigma;&tau;&epsilon; &sigma;ί&gamma;&omicron;&upsilon;&rho;&omicron;&iota; ό&tau;&iota; &theta;έ&lambda;&epsilon;&tau;&epsilon; &nu;&alpha; &delta;&iota;&alpha;&gamma;&rho;ά&psi;&epsilon;&tau;&epsilon; &alpha;&upsilon;&tau;ό &tau;&omicron; &sigma;&chi;ό&lambda;&iota;&omicron;;',
'delete' => '&Delta;&iota;&alpha;&gamma;&rho;&alpha;&phi;ή',
'edit' => '&Epsilon;&pi;&epsilon;&xi;&epsilon;&rho;&gamma;&alpha;&sigma;ί&alpha;',
'edit_comment' => '&Epsilon;&pi;&epsilon;&xi;&epsilon;&rho;&gamma;&alpha;&sigma;ί&alpha; &Sigma;&chi;&omicron;&lambda;ί&omicron;&upsilon;',
'edit_comment_button' => '&Epsilon;&pi;&epsilon;&xi;&epsilon;&rho;&gamma;&alpha;&sigma;ί&alpha; &Sigma;&chi;&omicron;&lambda;ί&omicron;&upsilon;',
'err_no_comment' => '&Pi;&alpha;&rho;&alpha;&kappa;&alpha;&lambda;ώ &epsilon;&iota;&sigma;ά&gamma;&epsilon;&tau;&epsilon; έ&nu;&alpha; &sigma;&chi;ό&lambda;&iota;&omicron;.',
'full_star' => 'Full Star',
'half_star' => 'Half Star',
'keywords_phrase' => '&Pi;&lambda;&eta;&kappa;&tau;&rho;&omicron;&lambda;&omicron;&gamma;ή&sigma;&tau;&epsilon; &lambda;έ&xi;&epsilon;&iota;&sigmaf;-&kappa;&lambda;&epsilon;&iota;&delta;&iota;ά ή &phi;&rho;ά&sigma;&eta;',
'no_articles' => '&Delta;&epsilon;&nu; &upsilon;&pi;ά&rho;&chi;&omicron;&upsilon;&nu; ά&rho;&theta;&rho;&alpha; &gamma;&iota;&alpha; &epsilon;&mu;&phi;ά&nu;&iota;&sigma;&eta;.',
'no_articles_found' => '&Delta;&epsilon;&nu; &beta;&rho;έ&theta;&eta;&kappa;&alpha;&nu; ά&rho;&theta;&rho;&alpha;.',
'no_comments' => '&Delta;&epsilon;&nu; έ&chi;&omicron;&upsilon;&nu; &gamma;ί&nu;&epsilon;&iota; &sigma;&chi;&omicron;&lambda;&iota;&alpha; &alpha;&kappa;ό&mu;&alpha; &gamma;&iota;&alpha; &alpha;&upsilon;&tau;ό &tau;&omicron; ά&rho;&theta;&rho;&omicron;.',
'no_star' => 'No Star',
'relevance' => '&Sigma;&upsilon;&nu;&alpha;&phi;ή&sigmaf;',
'search_results' => '&Alpha;&pi;&omicron;&tau;&epsilon;&lambda;έ&sigma;&mu;&alpha;&tau;&alpha; &Alpha;&nu;&alpha;&zeta;ή&tau;&eta;&sigma;&eta;&sigmaf;',
'stars' => 'Stars',
'view_category' => '&Pi;&rho;&omicron;&beta;&omicron;&lambda;ή &Kappa;&alpha;&tau;&eta;&gamma;&omicron;&rho;ί&alpha;&sigmaf;',
'viewing_article' => '&Pi;&rho;&omicron;&beta;&omicron;&lambda;ή ά&rho;&theta;&rho;&omicron;&upsilon;',

);

?>