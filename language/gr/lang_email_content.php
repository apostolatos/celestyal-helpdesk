<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | lang_email_content.php
#======================================================
*/

$lang['header'] = <<<EOF
&Alpha;&gamma;&alpha;&pi;&eta;&tau;έ / ή <#MEM_NAME#>,
EOF;

$lang['footer'] = <<<EOF
&Chi;&alpha;&iota;&rho;&epsilon;&tau;&iota;&sigma;&mu;&omicron;ί,

&Eta; &omicron;&mu;ά&delta;&alpha; &tau;&iota;&sigmaf; Celestyal Cruises.
EOF;

$lang['change_email_val_sub'] = "Confirm your email";

$lang['change_email_val'] = <<<EOF
Έ&chi;&epsilon;&tau;&epsilon; &zeta;&eta;&tau;ή&sigma;&epsilon;&iota; &nu;&alpha; &alpha;&lambda;&lambda;ά&xi;&epsilon;&iota; &tau;&omicron; email &sigma;&alpha;&sigmaf; &sigma;&epsilon; &alpha;&upsilon;&tau;ή &tau;&eta; &delta;&iota;&epsilon;ύ&theta;&upsilon;&nu;&sigma;&eta;. &Pi;&rho;&omicron;&kappa;&epsilon;&iota;&mu;έ&nu;&omicron;&upsilon; &nu;&alpha; &omicron;&lambda;&omicron;&kappa;&lambda;&eta;&rho;&omega;&theta;&epsilon;ί &eta; &alpha;&lambda;&lambda;&alpha;&gamma;ή, &pi;&rho;έ&pi;&epsilon;&iota; &nu;&alpha; &epsilon;&pi;&alpha;&lambda;&eta;&theta;&epsilon;ύ&sigma;&epsilon;&tau;&epsilon; &alpha;&upsilon;&tau;ό &tau;&omicron; email, &kappa;ά&nu;&omicron;&nu;&tau;&alpha;&sigmaf; &kappa;&lambda;&iota;&kappa; &sigma;&tau;&omicron; &pi;&alpha;&rho;&alpha;&kappa;ά&tau;&omega; &sigma;ύ&nu;&delta;&epsilon;&sigma;&mu;&omicron; &epsilon;&pi;&iota;&kappa;ύ&rho;&omega;&sigma;&eta;.

---------------------------

<#VAL_LINK#>

---------------------------
EOF;

$lang['new_user_val_email_sub'] = "&Epsilon;&pi;&alpha;&lambda;&eta;&theta;&epsilon;ύ&sigma;&tau;&epsilon; &tau;&omicron; &nu;έ&omicron; &sigma;&alpha;&sigmaf; &lambda;&omicron;&gamma;&alpha;&rho;&iota;&alpha;&sigma;&mu;ό";

$lang['new_user_val_email'] = <<<EOF
&Kappa;&alpha;&lambda;ώ&sigmaf; ή&lambda;&theta;&alpha;&tau;&epsilon; &sigma;&tau;o  Celestyal Cruises.  Έ&chi;&epsilon;&tau;&epsilon; &zeta;&eta;&tau;ή&sigma;&epsilon;&iota; έ&nu;&alpha; &nu;έ&omicron; &lambda;&omicron;&gamma;&alpha;&rho;&iota;&alpha;&sigma;&mu;ό &sigma;&tau;&omicron; &sigma;ύ&sigma;&tau;&eta;&mu;&alpha; help desk. &Gamma;&iota;&alpha; &nu;&alpha; &epsilon;&nu;&epsilon;&rho;&gamma;&omicron;&pi;&omicron;&iota;ή&sigma;&epsilon;&tau;&epsilon; &tau;&omicron; &lambda;&omicron;&gamma;&alpha;&rho;&iota;&alpha;&sigma;&mu;ό &sigma;&alpha;&sigmaf;, &pi;&rho;έ&pi;&epsilon;&iota; &nu;&alpha; &epsilon;&pi;&iota;&beta;&epsilon;&beta;&alpha;&iota;ώ&sigma;&epsilon;&tau;&epsilon; &alpha;&upsilon;&tau;ή&nu; &tau;&eta; &delta;&iota;&epsilon;ύ&theta;&upsilon;&nu;&sigma;&eta; &eta;&lambda;&epsilon;&kappa;&tau;&rho;&omicron;&nu;&iota;&kappa;&omicron;ύ &tau;&alpha;&chi;&upsilon;&delta;&rho;&omicron;&mu;&epsilon;ί&omicron;&upsilon; &kappa;ά&nu;&omicron;&nu;&tau;&alpha;&sigmaf; &kappa;&lambda;&iota;&kappa; &sigma;&tau;&omicron;&nu; &pi;&alpha;&rho;&alpha;&kappa;ά&tau;&omega; &sigma;ύ&nu;&delta;&epsilon;&sigma;&mu;&omicron; &epsilon;&pi;&iota;&kappa;ύ&rho;&omega;&sigma;&eta;&sigmaf;.

---------------------------

Username: <#USER_NAME#>
Validation Link: http://service.nview.gr/<#VAL_LINK#>

---------------------------
EOF;

$lang['new_user_val_both_sub'] = "&Epsilon;&pi;&alpha;&lambda;&eta;&theta;&epsilon;ύ&sigma;&tau;&epsilon; &tau;&omicron; &nu;έ&omicron; &sigma;&alpha;&sigmaf; &lambda;&omicron;&gamma;&alpha;&rho;&iota;&alpha;&sigma;&mu;ό";

$lang['new_user_val_both'] = <<<EOF
&Kappa;&alpha;&lambda;ώ&sigmaf; ή&lambda;&theta;&alpha;&tau;&epsilon; &sigma;&tau;&eta;&nu;  Celestyal Cruises.  Έ&chi;&epsilon;&tau;&epsilon; &zeta;&eta;&tau;ή&sigma;&epsilon;&iota; έ&nu;&alpha; &nu;έ&omicron; &lambda;&omicron;&gamma;&alpha;&rho;&iota;&alpha;&sigma;&mu;ό &sigma;&tau;&omicron; &sigma;ύ&sigma;&tau;&eta;&mu;&alpha; help desk. &Gamma;&iota;&alpha; &nu;&alpha; &epsilon;&nu;&epsilon;&rho;&gamma;&omicron;&pi;&omicron;&iota;ή&sigma;&epsilon;&tau;&epsilon; &tau;&omicron; &lambda;&omicron;&gamma;&alpha;&rho;&iota;&alpha;&sigma;&mu;ό &sigma;&alpha;&sigmaf;, &pi;&rho;έ&pi;&epsilon;&iota; &nu;&alpha; &epsilon;&pi;&iota;&beta;&epsilon;&beta;&alpha;&iota;ώ&sigma;&epsilon;&tau;&epsilon; &alpha;&upsilon;&tau;ή&nu; &tau;&eta; &delta;&iota;&epsilon;ύ&theta;&upsilon;&nu;&sigma;&eta; &eta;&lambda;&epsilon;&kappa;&tau;&rho;&omicron;&nu;&iota;&kappa;&omicron;ύ &tau;&alpha;&chi;&upsilon;&delta;&rho;&omicron;&mu;&epsilon;ί&omicron;&upsilon; &kappa;ά&nu;&omicron;&nu;&tau;&alpha;&sigmaf; &kappa;&lambda;&iota;&kappa; &sigma;&tau;&omicron;&nu; &pi;&alpha;&rho;&alpha;&kappa;ά&tau;&omega; &sigma;ύ&nu;&delta;&epsilon;&sigma;&mu;&omicron; &epsilon;&pi;&iota;&kappa;ύ&rho;&omega;&sigma;&eta;&sigmaf;. &Epsilon;&pi;&iota;&pi;&lambda;έ&omicron;&nu;, έ&nu;&alpha;&sigmaf; &delta;&iota;&alpha;&chi;&epsilon;&iota;&rho;&iota;&sigma;&tau;ή&sigmaf; &pi;&rho;έ&pi;&epsilon;&iota; &nu;&alpha; &epsilon;&gamma;&kappa;&rho;ί&nu;&epsilon;&iota; &tau;&omicron; &lambda;&omicron;&gamma;&alpha;&rho;&iota;&alpha;&sigma;&mu;ό &sigma;&alpha;&sigmaf;.

---------------------------

Username: <#USER_NAME#>
Validation Link: <#VAL_LINK#>

---------------------------

&Nu;&alpha; &theta;&upsilon;&mu;ά&sigma;&tau;&epsilon; &omicron;&tau;&iota; &omicron; &delta;&iota;&alpha;&chi;&epsilon;&iota;&rho;&iota;&sigma;&tau;ή&sigmaf; &pi;&rho;έ&pi;&epsilon;&iota; &epsilon;&pi;ί&sigma;&eta;&sigmaf; &nu;&alpha; &epsilon;&gamma;&kappa;&rho;ί&nu;&epsilon;&iota; &tau;&omicron; &lambda;&omicron;&gamma;&alpha;&rho;&iota;&alpha;&sigma;&mu;ό &sigma;&alpha;&sigmaf;. &Theta;&alpha; &alpha;&pi;&omicron;&sigma;&tau;&alpha;&lambda;&epsilon;ί έ&nu;&alpha; &mu;ή&nu;&upsilon;&mu;&alpha; &gamma;&iota;&alpha; &nu;&alpha; &sigma;&alpha;&sigmaf; &epsilon;&iota;&delta;&omicron;&pi;&omicron;&iota;ή&sigma;&epsilon;&iota; ό&tau;&alpha;&nu; &omicron; &lambda;&omicron;&gamma;&alpha;&rho;&iota;&alpha;&sigma;&mu;ό&sigmaf; &sigma;&alpha;&sigmaf; έ&chi;&epsilon;&iota; &epsilon;&gamma;&kappa;&rho;&iota;&theta;&epsilon;ί.
EOF;

$lang['new_user_val_admin_sub'] = "&Omicron; &nu;έ&omicron;&sigmaf; &lambda;&omicron;&gamma;&alpha;&rho;&iota;&alpha;&sigma;&mu;ό &sigma;&alpha;&sigmaf;";

$lang['new_user_val_admin'] = <<<EOF
Έ&chi;&epsilon;&tau;&epsilon; &zeta;&eta;&tau;ή&sigma;&epsilon;&iota; έ&nu;&alpha; &nu;έ&omicron; &lambda;&omicron;&gamma;&alpha;&rho;&iota;&alpha;&sigma;&mu;ό &sigma;&tau;&omicron; &sigma;ύ&sigma;&tau;&eta;&mu;&alpha; help desk. &Gamma;&iota;&alpha; &nu;&alpha; &epsilon;&nu;&epsilon;&rho;&gamma;&omicron;&pi;&omicron;&iota;&eta;&theta;&epsilon;ί &omicron; &lambda;&omicron;&gamma;&alpha;&rho;&iota;&alpha;&sigma;&mu;ό &sigma;&alpha;&sigmaf;,έ&nu;&alpha;&sigmaf; &delta;&iota;&alpha;&chi;&epsilon;&iota;&rho;&iota;&sigma;&tau;ή&sigmaf; &pi;&rho;έ&pi;&epsilon;&iota; &nu;&alpha; &epsilon;&gamma;&kappa;&rho;ί&nu;&epsilon;&iota; &tau;&omicron; &lambda;&omicron;&gamma;&alpha;&rho;&iota;&alpha;&sigma;&mu;ό &sigma;&alpha;&sigmaf;.  &Theta;&alpha; &alpha;&pi;&omicron;&sigma;&tau;&alpha;&lambda;&epsilon;ί έ&nu;&alpha; &mu;ή&nu;&upsilon;&mu;&alpha; &gamma;&iota;&alpha; &nu;&alpha; &sigma;&alpha;&sigmaf; &epsilon;&iota;&delta;&omicron;&pi;&omicron;&iota;ή&sigma;&epsilon;&iota; ό&tau;&alpha;&nu; &omicron; &lambda;&omicron;&gamma;&alpha;&rho;&iota;&alpha;&sigma;&mu;ό&sigmaf; &sigma;&alpha;&sigmaf; έ&chi;&epsilon;&iota; &epsilon;&gamma;&kappa;&rho;&iota;&theta;&epsilon;ί.
EOF;

$lang['acc_accivated_sub'] = "&Lambda;&omicron;&gamma;&alpha;&rho;&iota;&alpha;&sigma;&mu;ό&sigmaf; &Epsilon;&nu;&epsilon;&rho;&gamma;&omicron;&pi;&omicron;&iota;&eta;&mu;έ&nu;&omicron;&sigmaf;";

$lang['acc_accivated'] = <<<EOF
&Omicron; &lambda;&omicron;&gamma;&alpha;&rho;&iota;&alpha;&sigma;&mu;ό&sigmaf; &sigma;&alpha;&sigmaf; έ&chi;&epsilon;&iota; &epsilon;&nu;&epsilon;&rho;&gamma;&omicron;&pi;&omicron;&iota;&eta;&theta;&epsilon;ί &epsilon;&pi;&iota;&tau;&upsilon;&chi;ώ&sigmaf;. &Mu;&pi;&omicron;&rho;&epsilon;ί&tau;&epsilon; &pi;&lambda;έ&omicron;&nu; &nu;&alpha; &sigma;&upsilon;&nu;&delta;&epsilon;&theta;&epsilon;ί&tau;&epsilon;.
EOF;

$lang['acc_almost_accivated_sub'] = "&Alpha;&nu;&alpha;&mu;&omicron;&nu;ή Έ&gamma;&kappa;&rho;&iota;&sigma;&eta;&sigmaf; &Lambda;&omicron;&gamma;&alpha;&rho;&iota;&alpha;&sigma;&mu;&omicron;ύ.";

$lang['acc_almost_accivated'] = <<<EOF
&Eta; &eta;&lambda;&epsilon;&kappa;&tau;&rho;&omicron;&nu;&iota;&kappa;ή &sigma;&alpha;&sigmaf; &delta;&iota;&epsilon;ύ&theta;&upsilon;&nu;&sigma;&eta; έ&chi;&epsilon;&iota; &epsilon;&pi;&alpha;&lambda;&eta;&theta;&epsilon;&upsilon;&tau;&epsilon;ί &mu;&epsilon; &epsilon;&pi;&iota;&tau;&upsilon;&chi;ί&alpha;.  Ό&mu;&omega;&sigmaf;, &gamma;&iota;&alpha; &nu;&alpha; &mu;&pi;&omicron;&rho;&epsilon;ί&tau;&epsilon; &nu;&alpha; &alpha;&rho;&chi;ί&sigma;&epsilon;&tau;&epsilon; &nu;&alpha; &chi;&rho;&eta;&sigma;&iota;&mu;&omicron;&pi;&omicron;&iota;&epsilon;ί&tau;&epsilon; &tau;&omicron; &lambda;&omicron;&gamma;&alpha;&rho;&iota;&alpha;&sigma;&mu;ό &sigma;&alpha;&sigmaf;, έ&nu;&alpha;&sigmaf; &delta;&iota;&alpha;&chi;&epsilon;&iota;&rho;&iota;&sigma;&tau;ή&sigmaf; &pi;&rho;έ&pi;&epsilon;&iota; &nu;&alpha; &epsilon;&gamma;&kappa;&rho;ί&nu;&epsilon;&iota; &tau;&omicron; &lambda;&omicron;&gamma;&alpha;&rho;&iota;&alpha;&sigma;&mu;ό &sigma;&alpha;&sigmaf;.  &Theta;&alpha; &alpha;&pi;&omicron;&sigma;&tau;&alpha;&lambda;&epsilon;ί έ&nu;&alpha; &mu;ή&nu;&upsilon;&mu;&alpha; &gamma;&iota;&alpha; &nu;&alpha; &sigma;&alpha;&sigmaf; &epsilon;&iota;&delta;&omicron;&pi;&omicron;&iota;ή&sigma;&epsilon;&iota; ό&tau;&alpha;&nu; &omicron; &lambda;&omicron;&gamma;&alpha;&rho;&iota;&alpha;&sigma;&mu;ό&sigmaf; &sigma;&alpha;&sigmaf; έ&chi;&epsilon;&iota; &epsilon;&gamma;&kappa;&rho;&iota;&theta;&epsilon;ί.
EOF;

$lang['acc_approved_sub'] = "&Lambda;&omicron;&gamma;&alpha;&rho;&iota;&alpha;&sigma;&mu;ό&sigmaf; &Epsilon;&gamma;&kappa;&rho;ί&nu;&epsilon;&tau;&alpha;&iota;";

$lang['acc_approved'] = <<<EOF
&Omicron; &lambda;&omicron;&gamma;&alpha;&rho;&iota;&alpha;&sigma;&mu;ό&sigmaf; &sigma;&alpha;&sigmaf; έ&chi;&epsilon;&iota; &epsilon;&gamma;&kappa;&rho;&iota;&theta;&epsilon;ί &mu;&epsilon; &epsilon;&pi;&iota;&tau;&upsilon;&chi;ί&alpha; &alpha;&pi;ό έ&nu;&alpha; &delta;&iota;&alpha;&chi;&epsilon;&iota;&rho;&iota;&sigma;&tau;ή. &Mu;&pi;&omicron;&rho;&epsilon;ί&tau;&epsilon; &pi;&lambda;έ&omicron;&nu; &nu;&alpha; &sigma;&upsilon;&nu;&delta;&epsilon;&theta;&epsilon;ί&tau;&epsilon;.
EOF;

$lang['acc_almost_approved_sub'] = "&Alpha;&nu;&alpha;&mu;&omicron;&nu;ή &Epsilon;&pi;&iota;&kappa;ύ&rho;&omega;&sigma;&eta;&sigmaf; Email &Lambda;&omicron;&gamma;&alpha;&rho;&iota;&alpha;&sigma;&mu;&omicron;ύ";

$lang['acc_almost_approved'] = <<<EOF
&Omicron; &lambda;&omicron;&gamma;&alpha;&rho;&iota;&alpha;&sigma;&mu;ό&sigmaf; &sigma;&alpha;&sigmaf; έ&chi;&epsilon;&iota; &epsilon;&gamma;&kappa;&rho;&iota;&theta;&epsilon;ί &mu;&epsilon; &epsilon;&pi;&iota;&tau;&upsilon;&chi;ί&alpha; &alpha;&pi;ό έ&nu;&alpha; &delta;&iota;&alpha;&chi;&epsilon;&iota;&rho;&iota;&sigma;&tau;ή.Ό&mu;&omega;&sigmaf;, &gamma;&iota;&alpha; &nu;&alpha; &mu;&pi;&omicron;&rho;&epsilon;ί&tau;&epsilon; &nu;&alpha; &alpha;&rho;&chi;ί&sigma;&epsilon;&tau;&epsilon; &nu;&alpha; &chi;&rho;&eta;&sigma;&iota;&mu;&omicron;&pi;&omicron;&iota;&epsilon;ί&tau;&epsilon; &tau;&omicron; &lambda;&omicron;&gamma;&alpha;&rho;&iota;&alpha;&sigma;&mu;ό &sigma;&alpha;&sigmaf;, &pi;&rho;έ&pi;&epsilon;&iota; &pi;&rho;ώ&tau;&alpha; &kappa;ά&nu;&tau;&epsilon; &kappa;&lambda;&iota;&kappa; &sigma;&tau;&omicron; &sigma;ύ&nu;&delta;&epsilon;&sigma;&mu;&omicron; &epsilon;&pi;&iota;&kappa;ύ&rho;&omega;&sigma;&eta; &sigma;&tau;&omicron; &mu;ή&nu;&upsilon;&mu;&alpha; &pi;&omicron;&upsilon; έ&chi;&epsilon;&iota; &alpha;&pi;&omicron;&sigma;&tau;&alpha;&lambda;&epsilon;ί &sigma;&tau;&eta; &delta;&iota;&epsilon;ύ&theta;&upsilon;&nu;&sigma;&eta; &eta;&lambda;&epsilon;&kappa;&tau;&rho;&omicron;&nu;&iota;&kappa;&omicron;ύ &tau;&alpha;&chi;&upsilon;&delta;&rho;&omicron;&mu;&epsilon;ί&omicron;&upsilon; &sigma;&alpha;&sigmaf;.
EOF;

$lang['new_ticket_sub'] = "Ticket ID #<#TICKET_ID#>";

$lang['new_ticket'] = <<<EOF
Έ&chi;&epsilon;&tau;&epsilon; &upsilon;&pi;&omicron;&beta;ά&lambda;&epsilon;&iota; &nu;έ&alpha; &epsilon;&iota;&sigma;&iota;&tau;ή&rho;&iota;&omicron;. &Tau;&omicron; &pi;&rho;&omicron;&sigma;&omega;&pi;&iota;&kappa;ό &mu;&alpha;&sigmaf; &theta;&alpha; &epsilon;&xi;&epsilon;&tau;ά&sigma;&epsilon;&iota; &tau;&omicron; &epsilon;&iota;&sigma;&iota;&tau;ή&rho;&iota;&omicron; &kappa;&alpha;&iota; &sigma;ύ&nu;&tau;&omicron;&mu;&alpha; &theta;&alpha; &alpha;&pi;ά&nu;&tau;&eta;&sigma;&eta; &alpha;&nu;&alpha;&lambda;ό&gamma;&omega;&sigmaf;. &Alpha;&kappa;&omicron;&lambda;&omicron;&upsilon;&theta;&omicron;ύ&nu; &omicron;&iota; &lambda;&epsilon;&pi;&tau;&omicron;&mu;έ&rho;&epsilon;&iota;&epsilon;&sigmaf; &tau;&omicron;&upsilon; &epsilon;&iota;&sigma;&iota;&tau;&eta;&rho;ί&omicron;&upsilon;.

---------------------------

Ticket ID: <#TICKET_ID#>
Subject: <#SUBJECT#>
Department: <#DEPARTMENT#>
Priority: <#PRIORITY#>
Date Submitted: <#SUB_DATE#>

---------------------------

You can view your ticket using this link: <#TICKET_LINK#>
EOF;

$lang['staff_new_ticket_sub'] = "Ticket ID #<#TICKET_ID#>";

$lang['staff_new_ticket'] = <<<EOF
Έ&nu;&alpha; &nu;έ&omicron; &epsilon;&iota;&sigma;&iota;&tau;ή&rho;&iota;&omicron; έ&chi;&epsilon;&iota; &delta;&eta;&mu;&iota;&omicron;&upsilon;&rho;&gamma;&eta;&theta;&epsilon;ί &sigma;&tau;&eta;&nu; &upsilon;&pi;&eta;&rho;&epsilon;&sigma;ί&alpha; &sigma;&alpha;&sigmaf;. &Alpha;&kappa;&omicron;&lambda;&omicron;&upsilon;&theta;&omicron;ύ&nu; &omicron;&iota; &lambda;&epsilon;&pi;&tau;&omicron;&mu;έ&rho;&epsilon;&iota;&epsilon;&sigmaf; &tau;&omicron;&upsilon; &epsilon;&iota;&sigma;&iota;&tau;&eta;&rho;ί&omicron;&upsilon;.
---------------------------

Ticket ID: <#TICKET_ID#>
Member: <#MEMBER#>
Subject: <#SUBJECT#>
Department: <#DEPARTMENT#>
Priority: <#PRIORITY#>
Date Submitted: <#SUB_DATE#>

---------------------------

<#MESSAGE#>

---------------------------

You can manage this ticket using this link: <#TICKET_LINK#>
EOF;

$lang['new_guest_ticket_sub'] = "Ticket ID #<#TICKET_ID#>";

$lang['new_guest_ticket'] = <<<EOF
Έ&chi;&epsilon;&tau;&epsilon; &upsilon;&pi;&omicron;&beta;ά&lambda;&epsilon;&iota; έ&nu;&alpha; &nu;έ&omicron; &alpha;ί&tau;&eta;&mu;&alpha;. &Tau;&omicron; &pi;&rho;&omicron;&sigma;&omega;&pi;&iota;&kappa;ό &mu;&alpha;&sigmaf; &theta;&alpha; &epsilon;&xi;&epsilon;&tau;ά&sigma;&epsilon;&iota; &tau;&omicron; &alpha;ί&tau;&eta;&mu;&alpha; &sigma;&alpha;&sigmaf; &kappa;&alpha;&iota; &sigma;ύ&nu;&tau;&omicron;&mu;&alpha; &theta;&alpha; &sigma;&alpha;&sigmaf; &alpha;&pi;&alpha;&nu;&tau;ή&sigma;&epsilon;&iota; &alpha;&nu;&alpha;&lambda;ό&gamma;&omega;&sigmaf;. &Alpha;&kappa;&omicron;&lambda;&omicron;&upsilon;&theta;&omicron;ύ&nu; &omicron;&iota; &lambda;&epsilon;&pi;&tau;&omicron;&mu;έ&rho;&epsilon;&iota;&epsilon;&sigmaf; &tau;&omicron;&upsilon; &alpha;&iota;&tau;ή&mu;&alpha;&tau;&omicron;&sigmaf;.

---------------------------

Ticket ID: <#TICKET_ID#>
Subject: <#SUBJECT#>
Department: <#DEPARTMENT#>
Priority: <#PRIORITY#>
Date Submitted: <#SUB_DATE#>

Ticket Key: <#TICKET_KEY#>

---------------------------
EOF;

$lang['staff_new_guest_ticket_sub'] = "Ticket ID #<#TICKET_ID#>";

$lang['staff_new_guest_ticket'] = <<<EOF
&Nu;έ&alpha; &sigma;&chi;ό&lambda;&iota;&alpha; &epsilon;&pi;&iota;&sigma;&kappa;&epsilon;&pi;&tau;ώ&nu; &tau;&omega;&nu; &epsilon;&iota;&sigma;&iota;&tau;&eta;&rho;ί&omega;&nu; &sigma;&alpha;&sigmaf;, έ&chi;&epsilon;&iota; &delta;&eta;&mu;&iota;&omicron;&upsilon;&rho;&gamma;&eta;&theta;&epsilon;ί &sigma;&tau;&eta;&nu; &upsilon;&pi;&eta;&rho;&epsilon;&sigma;ί&alpha; &sigma;&alpha;&sigmaf;. &Alpha;&kappa;&omicron;&lambda;&omicron;&upsilon;&theta;&omicron;ύ&nu; &omicron;&iota; &lambda;&epsilon;&pi;&tau;&omicron;&mu;έ&rho;&epsilon;&iota;&epsilon;&sigmaf; &tau;&omicron;&upsilon; &epsilon;&iota;&sigma;&iota;&tau;&eta;&rho;ί&omicron;&upsilon;.

---------------------------

Ticket ID: <#TICKET_ID#>
Member: <#MEMBER#> (Guest)
Subject: <#SUBJECT#>
Department: <#DEPARTMENT#>
Priority: <#PRIORITY#>
Date Submitted: <#SUB_DATE#>

---------------------------

<#MESSAGE#>

---------------------------


&Mu;&pi;&omicron;&rho;&epsilon;ί&tau;&epsilon; &nu;&alpha; &delta;&epsilon;ί&tau;&epsilon; &tau;&omicron; &epsilon;&iota;&sigma;&iota;&tau;ή&rho;&iota;ό &sigma;&alpha;&sigmaf;, &chi;&rho;&eta;&sigma;&iota;&mu;&omicron;&pi;&omicron;&iota;ώ&nu;&tau;&alpha;&sigmaf; &alpha;&upsilon;&tau;ό&nu; &tau;&omicron;&nu; &sigma;ύ&nu;&delta;&epsilon;&sigma;&mu;&omicron; : <#TICKET_LINK#>
EOF;

$lang['ticket_escl_sub'] = "Ticket ID #<#TICKET_ID#> &Kappa;&lambda;&iota;&mu;&alpha;&kappa;ώ&theta;&eta;&kappa;&epsilon;";

$lang['ticket_escl'] = <<<EOF
Έ&nu;&alpha; &alpha;&pi;ό &tau;&alpha; &epsilon;&iota;&sigma;&iota;&tau;ή&rho;&iota;ά &sigma;&alpha;&sigmaf; έ&chi;&epsilon;&iota; &kappa;&lambda;&iota;&mu;&alpha;&kappa;&omega;&theta;&epsilon;ί. &Omicron;&iota; &delta;&iota;&alpha;&chi;&epsilon;&iota;&rho;&iota;&sigma;&tau;έ&sigmaf; &theta;&alpha; &epsilon;&pi;&alpha;&nu;&epsilon;&xi;&epsilon;&tau;ά&sigma;&epsilon;&iota; &tau;&omicron; &epsilon;&iota;&sigma;&iota;&tau;ή&rho;&iota;ό &sigma;&alpha;&sigmaf; &sigma;ύ&nu;&tau;&omicron;&mu;&alpha;. &Alpha;&kappa;&omicron;&lambda;&omicron;&upsilon;&theta;&omicron;ύ&nu; &omicron;&iota; &lambda;&epsilon;&pi;&tau;&omicron;&mu;έ&rho;&epsilon;&iota;&epsilon;&sigmaf; &tau;&omicron;&upsilon; &epsilon;&iota;&sigma;&iota;&tau;&eta;&rho;ί&omicron;&upsilon;.

---------------------------

Ticket ID: <#TICKET_ID#>
Subject: <#SUBJECT#>
Department: <#DEPARTMENT#>
Priority: <#PRIORITY#>
Date Submitted: <#SUB_DATE#>

---------------------------


&Mu;&pi;&omicron;&rho;&epsilon;ί&tau;&epsilon; &nu;&alpha; &delta;&epsilon;ί&tau;&epsilon; &tau;&omicron; &epsilon;&iota;&sigma;&iota;&tau;ή&rho;&iota;ό &sigma;&alpha;&sigmaf;, &chi;&rho;&eta;&sigma;&iota;&mu;&omicron;&pi;&omicron;&iota;ώ&nu;&tau;&alpha;&sigmaf; &alpha;&upsilon;&tau;ό&nu; &tau;&omicron;&nu; &sigma;ύ&nu;&delta;&epsilon;&sigma;&mu;&omicron; : <#TICKET_LINK#>
EOF;

$lang['ticket_close_sub'] = "Ticket ID #<#TICKET_ID#> Closed";

$lang['ticket_close'] = <<<EOF
Έ&nu;&alpha; &alpha;&pi;ό &tau;&alpha; &epsilon;&iota;&sigma;&iota;&tau;ή&rho;&iota;ά &sigma;&alpha;&sigmaf; έ&chi;&epsilon;&iota; &kappa;&lambda;&epsilon;ί&sigma;&epsilon;&iota;. &Alpha;&kappa;&omicron;&lambda;&omicron;&upsilon;&theta;&omicron;ύ&nu; &omicron;&iota; &lambda;&epsilon;&pi;&tau;&omicron;&mu;έ&rho;&epsilon;&iota;&epsilon;&sigmaf; &tau;&omicron;&upsilon; &epsilon;&iota;&sigma;&iota;&tau;&eta;&rho;ί&omicron;&upsilon;.

---------------------------

Ticket ID: <#TICKET_ID#>
Subject: <#SUBJECT#>
Department: <#DEPARTMENT#>
Priority: <#PRIORITY#>
Date Submitted: <#SUB_DATE#>

---------------------------

&Mu;&pi;&omicron;&rho;&epsilon;ί&tau;&epsilon; &nu;&alpha; &delta;&epsilon;ί&tau;&epsilon; &tau;&omicron; &epsilon;&iota;&sigma;&iota;&tau;ή&rho;&iota;ό &sigma;&alpha;&sigmaf;, &chi;&rho;&eta;&sigma;&iota;&mu;&omicron;&pi;&omicron;&iota;ώ&nu;&tau;&alpha;&sigmaf; &alpha;&upsilon;&tau;ό&nu; &tau;&omicron;&nu; &sigma;ύ&nu;&delta;&epsilon;&sigma;&mu;&omicron; : <#TICKET_LINK#>

&Epsilon;ά&nu; &upsilon;&pi;ά&rho;&chi;&epsilon;&iota; &kappa;ά&tau;&iota; &pi;&omicron;&upsilon; &mu;&pi;&omicron;&rho;&omicron;ύ&mu;&epsilon; &nu;&alpha; &kappa;ά&nu;&omicron;&upsilon;&mu;&epsilon; &gamma;&iota;&alpha; &beta;&omicron;ή&theta;&epsilon;&iota;&alpha;, &pi;&alpha;&rho;&alpha;&kappa;&alpha;&lambda;&omicron;ύ&mu;&epsilon; &epsilon;&nu;&eta;&mu;&epsilon;&rho;ώ&sigma;&tau;&epsilon; &mu;&alpha;&sigmaf;.
EOF;

$lang['ticket_move_sub'] = "Ticket ID #<#TICKET_ID#> &Mu;&epsilon;&tau;&alpha;&kappa;&iota;&nu;ή&theta;&eta;&kappa;&epsilon;.";

$lang['ticket_move'] = <<<EOF
Έ&nu;&alpha; &alpha;&pi;ό &tau;&alpha; &epsilon;&iota;&sigma;&iota;&tau;ή&rho;&iota;ά &sigma;&alpha;&sigmaf; έ&chi;&epsilon;&iota; &mu;&epsilon;&tau;&alpha;&phi;&epsilon;&rho;&theta;&epsilon;ί &sigma;&epsilon; έ&nu;&alpha; &nu;έ&omicron; &tau;&mu;ή&mu;&alpha;. &Alpha;&kappa;&omicron;&lambda;&omicron;&upsilon;&theta;&omicron;ύ&nu; &omicron;&iota; &lambda;&epsilon;&pi;&tau;&omicron;&mu;έ&rho;&epsilon;&iota;&epsilon;&sigmaf; &tau;&omicron;&upsilon; &epsilon;&iota;&sigma;&iota;&tau;&eta;&rho;ί&omicron;&upsilon;.

---------------------------

Ticket ID: <#TICKET_ID#>
Subject: <#SUBJECT#>
Old Department: <#OLD_DEPARTMENT#>
New Department: <#NEW_DEPARTMENT#>
Priority: <#PRIORITY#>
Date Submitted: <#SUB_DATE#>

---------------------------
&Mu;&pi;&omicron;&rho;&epsilon;ί&tau;&epsilon; &nu;&alpha; &delta;&epsilon;ί&tau;&epsilon; &tau;&omicron; &epsilon;&iota;&sigma;&iota;&tau;ή&rho;&iota;ό &sigma;&alpha;&sigmaf;, &chi;&rho;&eta;&sigma;&iota;&mu;&omicron;&pi;&omicron;&iota;ώ&nu;&tau;&alpha;&sigmaf; &alpha;&upsilon;&tau;ό&nu; &tau;&omicron;&nu; &sigma;ύ&nu;&delta;&epsilon;&sigma;&mu;&omicron; :  <#TICKET_LINK#>

&Epsilon;ά&nu; &upsilon;&pi;ά&rho;&chi;&epsilon;&iota; &kappa;ά&tau;&iota; &pi;&omicron;&upsilon; &mu;&pi;&omicron;&rho;&omicron;ύ&mu;&epsilon; &nu;&alpha; &kappa;ά&nu;&omicron;&upsilon;&mu;&epsilon; &gamma;&iota;&alpha; &beta;&omicron;ή&theta;&epsilon;&iota;&alpha;, &pi;&alpha;&rho;&alpha;&kappa;&alpha;&lambda;&omicron;ύ&mu;&epsilon; &epsilon;&nu;&eta;&mu;&epsilon;&rho;ώ&sigma;&tau;&epsilon; &mu;&alpha;&sigmaf;.
EOF;

$lang['ticket_reply_sub'] = "Ticket ID #<#TICKET_ID#> &Alpha;&pi;&alpha;&nu;&tau;ή&theta;&eta;&kappa;&epsilon;.";

$lang['ticket_reply'] = <<<EOF
&Mu;&iota;&alpha; &alpha;&pi;ά&nu;&tau;&eta;&sigma;&eta; έ&chi;&epsilon;&iota; &gamma;ί&nu;&epsilon;&iota; &gamma;&iota;&alpha; &nu;&alpha; &tau;&omicron; &epsilon;&iota;&sigma;&iota;&tau;ή&rho;&iota;ό &sigma;&alpha;&sigmaf;. &Alpha;&kappa;&omicron;&lambda;&omicron;&upsilon;&theta;&omicron;ύ&nu; &omicron;&iota; &lambda;&epsilon;&pi;&tau;&omicron;&mu;έ&rho;&epsilon;&iota;&epsilon;&sigmaf; &tau;&omicron;&upsilon; &epsilon;&iota;&sigma;&iota;&tau;&eta;&rho;ί&omicron;&upsilon;.

---------------------------

<#REPLY#>

---------------------------

Ticket ID: <#TICKET_ID#>
Subject: <#SUBJECT#>
Department: <#DEPARTMENT#>
Priority: <#PRIORITY#>
Date Submitted: <#SUB_DATE#>

---------------------------

&Mu;&pi;&omicron;&rho;&epsilon;ί&tau;&epsilon; &nu;&alpha; &delta;&epsilon;ί&tau;&epsilon; &tau;&omicron; &epsilon;&iota;&sigma;&iota;&tau;ή&rho;&iota;ό &sigma;&alpha;&sigmaf;, &chi;&rho;&eta;&sigma;&iota;&mu;&omicron;&pi;&omicron;&iota;ώ&nu;&tau;&alpha;&sigmaf; &alpha;&upsilon;&tau;ό&nu; &tau;&omicron;&nu; &sigma;ύ&nu;&delta;&epsilon;&sigma;&mu;&omicron; :<#TICKET_LINK#>

&Epsilon;ά&nu; &upsilon;&pi;ά&rho;&chi;&epsilon;&iota; &kappa;ά&tau;&iota; &pi;&omicron;&upsilon; &mu;&pi;&omicron;&rho;&omicron;ύ&mu;&epsilon; &nu;&alpha; &kappa;ά&nu;&omicron;&upsilon;&mu;&epsilon; &gamma;&iota;&alpha; &beta;&omicron;ή&theta;&epsilon;&iota;&alpha;, &pi;&alpha;&rho;&alpha;&kappa;&alpha;&lambda;&omicron;ύ&mu;&epsilon; &epsilon;&nu;&eta;&mu;&epsilon;&rho;ώ&sigma;&tau;&epsilon; &mu;&alpha;&sigmaf;.
EOF;

$lang['staff_reply_ticket_sub'] = "Ticket ID #<#TICKET_ID#> &Alpha;&pi;&alpha;&nu;&tau;ή&theta;&eta;&kappa;&epsilon;.";

$lang['staff_reply_ticket'] = <<<EOF
&Mu;&iota;&alpha; &alpha;&pi;ά&nu;&tau;&eta;&sigma;&eta; &pi;&omicron;&upsilon; έ&chi;&epsilon;&iota; &sigma;&eta;&mu;&epsilon;&iota;&omega;&theta;&epsilon;ί &sigma;&epsilon; έ&nu;&alpha; &epsilon;&iota;&sigma;&iota;&tau;ή&rho;&iota;&omicron; &sigma;&tau;&eta;&nu; &upsilon;&pi;&eta;&rho;&epsilon;&sigma;ί&alpha; &sigma;&alpha;&sigmaf;. &Alpha;&kappa;&omicron;&lambda;&omicron;&upsilon;&theta;&omicron;ύ&nu; &omicron;&iota; &lambda;&epsilon;&pi;&tau;&omicron;&mu;έ&rho;&epsilon;&iota;&epsilon;&sigmaf; &tau;&omicron;&upsilon; &epsilon;&iota;&sigma;&iota;&tau;&eta;&rho;ί&omicron;&upsilon;.

---------------------------

<#REPLY#>

---------------------------

Ticket ID: <#TICKET_ID#>
Member: <#MEMBER#>
Subject: <#SUBJECT#>
Department: <#DEPARTMENT#>
Priority: <#PRIORITY#>
Date Submitted: <#SUB_DATE#>

---------------------------

&Mu;&pi;&omicron;&rho;&epsilon;ί&tau;&epsilon; &nu;&alpha; &delta;&epsilon;ί&tau;&epsilon; &tau;&omicron; &epsilon;&iota;&sigma;&iota;&tau;ή&rho;&iota;ό &sigma;&alpha;&sigmaf;, &chi;&rho;&eta;&sigma;&iota;&mu;&omicron;&pi;&omicron;&iota;ώ&nu;&tau;&alpha;&sigmaf; &alpha;&upsilon;&tau;ό&nu; &tau;&omicron;&nu; &sigma;ύ&nu;&delta;&epsilon;&sigma;&mu;&omicron; : <#TICKET_LINK#>
EOF;

$lang['announcement_sub'] = "<#TITLE#>";

$lang['announcement'] = <<<EOF
&Mu;&iota;&alpha; &nu;έ&alpha; &alpha;&nu;&alpha;&kappa;&omicron;ί&nu;&omega;&sigma;&eta; έ&chi;&epsilon;&iota; &gamma;ί&nu;&epsilon;&iota; &mu;&epsilon;  &tau;ί&tau;&lambda;&omicron; '<#TITLE#>'.

---------------------------

<#CONTENT#>

---------------------------

Έ&chi;&epsilon;&tau;&epsilon; &lambda;ά&beta;&epsilon;&iota; &alpha;&upsilon;&tau;ό &tau;&omicron; email &epsilon;&pi;&epsilon;&iota;&delta;ή έ&chi;&epsilon;&tau;&epsilon; &epsilon;&pi;&iota;&lambda;έ&xi;&epsilon;&iota; &nu;&alpha; &lambda;&alpha;&mu;&beta;ά&nu;&epsilon;&tau;&epsilon; email &epsilon;&iota;&delta;&omicron;&pi;&omicron;&iota;ή&sigma;&epsilon;&iota;&sigmaf; &gamma;&iota;&alpha; &nu;έ&epsilon;&sigmaf; &alpha;&nu;&alpha;&kappa;&omicron;&iota;&nu;ώ&sigma;&epsilon;&iota;&sigmaf; &sigma;&tau;&omicron; &pi;&rho;&omicron;&phi;ί&lambda; &sigma;&alpha;&sigmaf;. &Alpha;&nu; &theta;έ&lambda;&epsilon;&tau;&epsilon; &nu;&alpha; &delta;&iota;&alpha;&kappa;ό&psi;&epsilon;&iota; &tau;&alpha; &mu;&eta;&nu;ύ&mu;&alpha;&tau;&alpha; &eta;&lambda;&epsilon;&kappa;&tau;&rho;&omicron;&nu;&iota;&kappa;&omicron;ύ &tau;&alpha;&chi;&upsilon;&delta;&rho;&omicron;&mu;&epsilon;ί&omicron;&upsilon;, &sigma;ύ&nu;&delta;&epsilon;&sigma;&eta;&sigmaf; &kappa;&alpha;&iota; &tau;&eta;&nu; &epsilon;&nu;&eta;&mu;έ&rho;&omega;&sigma;&eta; &tau;&omicron;&upsilon; &lambda;&omicron;&gamma;&alpha;&rho;&iota;&alpha;&sigma;&mu;&omicron;ύ &sigma;&alpha;&sigmaf; &pi;&rho;&omicron;&tau;&iota;&mu;ή&sigma;&epsilon;&iota;&sigmaf;.
EOF;

$lang['reset_pass_val_sub'] = "&Epsilon;&pi;&alpha;&nu;&alpha;&phi;έ&rho;&epsilon;&tau;&epsilon; &tau;&omicron;&nu; &kappa;&omega;&delta;&iota;&kappa;ό &pi;&rho;ό&sigma;&beta;&alpha;&sigma;&eta;&sigmaf;";

$lang['reset_pass_val'] = <<<EOF
Έ&chi;&epsilon;&tau;&epsilon; &zeta;&eta;&tau;ή&sigma;&epsilon;&iota; &nu;&alpha; &epsilon;&pi;&alpha;&nu;&alpha;&phi;έ&rho;&epsilon;&tau;&epsilon; &tau;&omicron;&nu; &kappa;&omega;&delta;&iota;&kappa;ό &pi;&rho;ό&sigma;&beta;&alpha;&sigma;&eta;&sigmaf; &sigma;&tau;&omicron;  Celestyal Cruises.  &Gamma;&iota;&alpha; &nu;&alpha; &epsilon;&pi;&alpha;&nu;&alpha;&phi;έ&rho;&epsilon;&tau;&epsilon; &tau;&omicron;&nu; &kappa;&omega;&delta;&iota;&kappa;ό &sigma;&alpha;&sigmaf;, &kappa;ά&nu;&tau;&epsilon; &kappa;&lambda;&iota;&kappa; &sigma;&tau;&omicron;&nu; &pi;&alpha;&rho;&alpha;&kappa;ά&tau;&omega; &sigma;ύ&nu;&delta;&epsilon;&sigma;&mu;&omicron; &epsilon;&pi;&iota;&kappa;ύ&rho;&omega;&sigma;&eta;. &Epsilon;ά&nu; &delta;&epsilon;&nu; &zeta;&eta;&tau;ή&sigma;&alpha;&tau;&epsilon; &nu;&alpha; &epsilon;&pi;&alpha;&nu;&alpha;&phi;έ&rho;&epsilon;&tau;&epsilon; &tau;&omicron;&nu; &kappa;&omega;&delta;&iota;&kappa;ό &pi;&rho;ό&sigma;&beta;&alpha;&sigma;&eta;&sigmaf;, &pi;&alpha;&rho;&alpha;&kappa;&alpha;&lambda;&omicron;ύ&mu;&epsilon; &nu;&alpha; &alpha;&gamma;&nu;&omicron;ή&sigma;&epsilon;&iota; &alpha;&upsilon;&tau;ό &tau;&omicron; email.
---------------------------

Username: <#USER_NAME#>
Validation Link: <#VAL_LINK#>
EOF;

$lang['reply_pipe_closed_sub'] = "&Eta; &Alpha;&pi;ά&nu;&tau;&eta;&sigma;&eta; &delta;&epsilon;&nu; έ&gamma;&iota;&nu;&epsilon; &alpha;&pi;&omicron;&delta;&epsilon;&kappa;&tau;ή.";

$lang['reply_pipe_closed'] = <<<EOF
&Delta;&epsilon;&nu; ή&mu;&alpha;&sigma;&tau;&epsilon; &sigma;&epsilon; &theta;έ&sigma;&eta; &nu;&alpha; &delta;&epsilon;&chi;&theta;&omicron;ύ&mu;&epsilon; &tau;&omicron; email &sigma;&alpha;&sigmaf; &gamma;&iota;&alpha; &nu;&alpha; &pi;&rho;&omicron;&sigma;&theta;έ&sigma;&epsilon;&tau;&epsilon; &tau;&eta;&nu; &alpha;&pi;ά&nu;&tau;&eta;&sigma;ή &sigma;&alpha;&sigmaf; &sigma;&tau;&omicron; &epsilon;&iota;&sigma;&iota;&tau;ή&rho;&iota;ό, &epsilon;&pi;&epsilon;&iota;&delta;ή &tau;&omicron; &epsilon;&iota;&sigma;&iota;&tau;ή&rho;&iota;&omicron; &epsilon;ί&nu;&alpha;&iota; &kappa;&lambda;&epsilon;&iota;&sigma;&tau;ό.

---------------------------

Ticket ID: <#TICKET_ID#>
Subject: <#SUBJECT#>

---------------------------

&Alpha;&nu; &nu;&omicron;&mu;ί&zeta;&epsilon;&tau;&epsilon; ό&tau;&iota; &alpha;&upsilon;&tau;ό &tau;&omicron; &mu;ή&nu;&upsilon;&mu;&alpha; &epsilon;ί&nu;&alpha;&iota; έ&nu;&alpha; &sigma;&phi;ά&lambda;&mu;&alpha;, &pi;&alpha;&rho;&alpha;&kappa;&alpha;&lambda;ώ &epsilon;&pi;&iota;&kappa;&omicron;&iota;&nu;&omega;&nu;ή&sigma;&tau;&epsilon; &mu;&epsilon; έ&nu;&alpha; &delta;&iota;&alpha;&chi;&epsilon;&iota;&rho;&iota;&sigma;&tau;ή.
EOF;

$lang['ticket_pipe_rejected_sub'] = "&Mu;&eta; &alpha;&pi;&omicron;&delta;&epsilon;&kappa;&tau;ό &epsilon;&iota;&sigma;&iota;&tau;ή&rho;&iota;&omicron;.";

$lang['ticket_pipe_rejected'] = <<<EOF
&Delta;&epsilon;&nu; ή&mu;&alpha;&sigma;&tau;&epsilon; &sigma;&epsilon; &theta;έ&sigma;&eta; &nu;&alpha; &delta;&epsilon;&chi;&theta;&omicron;ύ&mu;&epsilon; &tau;&omicron; email &sigma;&alpha;&sigmaf; &gamma;&iota;&alpha; &nu;&alpha; &delta;&eta;&mu;&iota;&omicron;&upsilon;&rho;&gamma;ή&sigma;&epsilon;&tau;&epsilon; έ&nu;&alpha; &epsilon;&iota;&sigma;&iota;&tau;ή&rho;&iota;&omicron; &epsilon;&pi;&epsilon;&iota;&delta;ή &delta;&epsilon;&nu; έ&chi;&epsilon;&tau;&epsilon; &tau;&eta;&nu; ά&delta;&epsilon;&iota;&alpha; &nu;&alpha; &delta;&eta;&mu;&iota;&omicron;&upsilon;&rho;&gamma;ή&sigma;&epsilon;&iota; &epsilon;&iota;&sigma;&iota;&tau;ή&rho;&iota;&alpha; &sigma;&epsilon; &alpha;&upsilon;&tau;ό &tau;&omicron; &tau;&mu;ή&mu;&alpha;.

&Alpha;&nu; &nu;&omicron;&mu;ί&zeta;&epsilon;&tau;&epsilon; ό&tau;&iota; &alpha;&upsilon;&tau;ό &tau;&omicron; &mu;ή&nu;&upsilon;&mu;&alpha; &epsilon;ί&nu;&alpha;&iota; έ&nu;&alpha; &sigma;&phi;ά&lambda;&mu;&alpha;, &pi;&alpha;&rho;&alpha;&kappa;&alpha;&lambda;ώ &epsilon;&pi;&iota;&kappa;&omicron;&iota;&nu;&omega;&nu;ή&sigma;&tau;&epsilon; &mu;&epsilon; έ&nu;&alpha; &delta;&iota;&alpha;&chi;&epsilon;&iota;&rho;&iota;&sigma;&tau;ή.
EOF;

$lang['new_user_admin_val_sub'] = "&Nu;έ&alpha; &Kappa;&alpha;&tau;&alpha;&chi;ώ&rho;&eta;&sigma;&eta;: <#USER_NAME#>";

$lang['new_user_admin_val'] = <<<EOF
Έ&nu;&alpha; &nu;έ&omicron; &mu;έ&lambda;&omicron;&sigmaf; &tau;&eta;&sigmaf; έ&chi;&epsilon;&iota; &kappa;&alpha;&tau;&alpha;&gamma;&rho;&alpha;&phi;&epsilon;ί &omega;&sigmaf; &alpha;&nu;&alpha;&mu;έ&nu;&epsilon;&iota; admin &epsilon;&pi;&iota;&kappa;ύ&rho;&omega;&sigma;&eta;. &Alpha;&kappa;&omicron;&lambda;&omicron;&upsilon;&theta;&omicron;ύ&nu; &omicron;&iota; &lambda;&epsilon;&pi;&tau;&omicron;&mu;έ&rho;&epsilon;&iota;&epsilon;&sigmaf; &tau;&omicron;&upsilon; &mu;έ&lambda;&omicron;&upsilon;&sigmaf;.

---------------------------

Member: <#USER_NAME#>
Email: <#USER_EMAIL#>
Date Joined: <#JOIN_DATE#>

---------------------------
&Mu;&pi;&omicron;&rho;&epsilon;ί&tau;&epsilon; &nu;&alpha; &delta;&iota;&alpha;&chi;&epsilon;&iota;&rho;&iota;&sigma;&tau;&epsilon;ί&tau;&epsilon; &tau;&alpha; &mu;έ&lambda;&eta; &epsilon;&nu; &alpha;&nu;&alpha;&mu;&omicron;&nu;ή &tau;&eta;&sigmaf; &epsilon;&pi;&iota;&kappa;ύ&rho;&omega;&sigma;&eta;&sigmaf; &chi;&rho;&eta;&sigma;&iota;&mu;&omicron;&pi;&omicron;&iota;ώ&nu;&tau;&alpha;&sigmaf; &tau;&omicron;&nu; &pi;&alpha;&rho;&alpha;&kappa;ά&tau;&omega; &sigma;ύ&nu;&delta;&epsilon;&sigma;&mu;&omicron;: <#APPROVE_LINK#>
EOF;

?>