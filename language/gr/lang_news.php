<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | lang_news.php
#======================================================
*/

$lang = array(

'add_a_comment' => '&Pi;&rho;&omicron;&sigma;&theta;ή&kappa;&eta; &sigma;&chi;&omicron;&lambda;ί&omicron;&upsilon;',
'add_comment_button' => '&Pi;&rho;&omicron;&sigma;&theta;ή&kappa;&eta; &sigma;&chi;&omicron;&lambda;ί&omicron;&upsilon;',
'announcement' => '&Alpha;&nu;&alpha;&kappa;&omicron;ί&nu;&omega;&sigma;&eta;',
'comments' => '&Sigma;&chi;ό&lambda;&iota;&alpha;',
'confirm_delete' => '&Epsilon;ί&sigma;&tau;&epsilon; &sigma;ί&gamma;&omicron;&upsilon;&rho;&omicron;&iota; ό&tau;&iota; &theta;έ&lambda;&epsilon;&tau;&epsilon; &nu;&alpha; &delta;&iota;&alpha;&gamma;&rho;ά&psi;&epsilon;&tau;&epsilon; &alpha;&upsilon;&tau;ό &tau;&omicron; &sigma;&chi;ό&lambda;&iota;&omicron;;',
'delete' => '&Delta;&iota;&alpha;&gamma;&rho;&alpha;&phi;ή',
'edit' => '&Epsilon;&pi;&epsilon;&xi;&epsilon;&rho;&gamma;&alpha;&sigma;ί&alpha;',
'edit_comment' => '&Epsilon;&pi;&epsilon;&xi;&epsilon;&rho;&gamma;&alpha;&sigma;ί&alpha; &Sigma;&chi;&omicron;&lambda;ί&omicron;&upsilon;',
'edit_comment_button' => '&Epsilon;&pi;&epsilon;&xi;&epsilon;&rho;&gamma;&alpha;&sigma;ί&alpha; &Sigma;&chi;&omicron;&lambda;ί&omicron;&upsilon;',
'err_no_comment' => '&Pi;&alpha;&rho;&alpha;&kappa;&alpha;&lambda;ώ &epsilon;&iota;&sigma;ά&gamma;&epsilon;&tau;&epsilon; έ&nu;&alpha; &sigma;&chi;ό&lambda;&iota;&omicron;.',
'no_comments' => '&Delta;&epsilon;&nu; έ&chi;&omicron;&upsilon;&nu; &gamma;ί&nu;&epsilon;&iota; &sigma;&chi;ό&lambda;&iota;&alpha; &alpha;&kappa;ό&mu;&alpha; &gamma;&iota;&alpha; &alpha;&upsilon;&tau;ή &tau;&eta;&nu; &alpha;&nu;&alpha;&kappa;&omicron;ί&nu;&omega;&sigma;&eta;',
'no_news' => '&Delta;&epsilon;&nu; &upsilon;&pi;ά&rho;&chi;&omicron;&upsilon;&nu; &alpha;&nu;&alpha;&kappa;&omicron;&iota;&nu;ώ&sigma;&epsilon;&iota;&sigmaf; &gamma;&iota;&alpha; &epsilon;&mu;&phi;ά&nu;&iota;&sigma;&eta;',
'viewing_announcement' => '&Pi;&rho;&omicron;&beta;&omicron;&lambda;ή &Alpha;&nu;&alpha;&kappa;&omicron;ί&nu;&omega;&sigma;&eta;&sigmaf;',

);

?>