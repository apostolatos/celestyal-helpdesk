<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | lang_register.php
#======================================================
*/

$lang = array(

'captcha' => 'Captcha',
'create_account_button' => '&Delta;&eta;&mu;&iota;&omicron;&upsilon;&rho;&gamma;ί&alpha; &lambda;&omicron;&gamma;&alpha;&rho;&iota;&alpha;&sigma;&mu;&omicron;ύ',
'email_address' => '&Delta;&iota;&epsilon;ύ&theta;&upsilon;&nu;&sigma;&eta; Email',
'err_captcha_mismatch' => '&Omicron; &kappa;&omega;&delta;&iota;&kappa;ό&sigmaf; Captcha &pi;&omicron;&upsilon; &delta;ώ&sigma;&alpha;&tau;&epsilon; &delta;&epsilon;&nu; &tau;&alpha;&iota;&rho;&iota;ά&zeta;&epsilon;&iota; &mu;&epsilon; &tau;&eta;&nu; &epsilon;&iota;&kappa;ό&nu;&alpha;. &Pi;&alpha;&rho;&alpha;&kappa;&alpha;&lambda;ώ &delta;&omicron;&kappa;&iota;&mu;ά&sigma;&tau;&epsilon; &xi;&alpha;&nu;ά.',
'err_email_in_use' => '&Sigma;&upsilon;&gamma;&nu;ώ&mu;&eta;, &alpha;&lambda;&lambda;ά ό&tau;&iota; &tau;&omicron; email &epsilon;ί&nu;&alpha;&iota; ή&delta;&eta; &sigma;&epsilon; &chi;&rho;ή&sigma;&eta;. &Epsilon;&pi;&iota;&lambda;έ&xi;&tau;&epsilon; έ&nu;&alpha; ά&lambda;&lambda;&omicron; email.',
'err_no_cpfield' => '&Pi;&alpha;&rho;&alpha;&kappa;&alpha;&lambda;ώ &epsilon;&iota;&sigma;ά&gamma;&epsilon;&tau;&epsilon; &mu;&iota;&alpha; &tau;&iota;&mu;ή &gamma;&iota;&alpha; &tau;&omicron; &pi;&epsilon;&delta;ί&omicron;:',
'err_no_email_valid' => '&Pi;&rho;έ&pi;&epsilon;&iota; &nu;&alpha; &epsilon;&iota;&sigma;ά&gamma;&epsilon;&tau;&epsilon; &mu;&iota;&alpha; έ&gamma;&kappa;&upsilon;&rho;&eta; &delta;&iota;&epsilon;ύ&theta;&upsilon;&nu;&sigma;&eta; email.',
'err_no_pass_match' => '&Omicron; &kappa;&omega;&delta;&iota;&kappa;&omicron;ί &delta;&epsilon;&nu; &tau;&alpha;&iota;&rho;&iota;ά&zeta;&omicron;&upsilon;&nu;.',
'err_no_pass_short' => '&Pi;&lambda;&eta;&kappa;&tau;&rho;&omicron;&lambda;&omicron;&gamma;ή&sigma;&tau;&epsilon; έ&nu;&alpha; &kappa;&omega;&delta;&iota;&kappa;ό &pi;&rho;ό&sigma;&beta;&alpha;&sigma;&eta;&sigmaf;.',
'err_no_user_or_email' => '&Pi;&rho;έ&pi;&epsilon;&iota; &nu;&alpha; &pi;&lambda;&eta;&kappa;&tau;&rho;&omicron;&lambda;&omicron;&gamma;ή&sigma;&epsilon;&tau;&epsilon; έ&nu;&alpha; ό&nu;&omicron;&mu;&alpha; &chi;&rho;ή&sigma;&tau;&eta; ή &delta;&iota;&epsilon;ύ&theta;&upsilon;&nu;&sigma;&eta; &eta;&lambda;&epsilon;&kappa;&tau;&rho;&omicron;&nu;&iota;&kappa;&omicron;ύ &tau;&alpha;&chi;&upsilon;&delta;&rho;&omicron;&mu;&epsilon;ί&omicron;&upsilon;.',
'err_no_user_short' => '&Pi;&lambda;&eta;&kappa;&tau;&rho;&omicron;&lambda;&omicron;&gamma;ή&sigma;&tau;&epsilon; έ&nu;&alpha; ό&nu;&omicron;&mu;&alpha; &chi;&rho;ή&sigma;&tau;&eta;.',
'err_user_already_active' => '&Omicron; &lambda;&omicron;&gamma;&alpha;&rho;&iota;&alpha;&sigma;&mu;ό&sigmaf; &sigma;&alpha;&sigmaf; έ&chi;&epsilon;&iota; ή&delta;&eta; &epsilon;&pi;&iota;&kappa;&upsilon;&rho;&omega;&theta;&epsilon;ί. &Delta;&omicron;&kappa;&iota;&mu;ά&sigma;&tau;&epsilon; &nu;&alpha; &sigma;&upsilon;&nu;&delta;&epsilon;&theta;&epsilon;ί&tau;&epsilon;. :)',
'err_user_in_use' => '&Sigma;&upsilon;&gamma;&nu;ώ&mu;&eta;, &alpha;&lambda;&lambda;ά &tau;&omicron; ό&nu;&omicron;&mu;&alpha; &chi;&rho;ή&sigma;&tau;&eta; &pi;&omicron;&upsilon; &epsilon;ί&nu;&alpha;&iota; ή&delta;&eta; &sigma;&epsilon; &chi;&rho;ή&sigma;&eta;. &Pi;&alpha;&rho;&alpha;&kappa;&alpha;&lambda;ώ &epsilon;&pi;&iota;&lambda;έ&xi;&tau;&epsilon; έ&nu;&alpha; ά&lambda;&lambda;&omicron; ό&nu;&omicron;&mu;&alpha; &chi;&rho;ή&sigma;&tau;&eta;.',
'err_user_not_found' => '&Delta;&epsilon;&nu; &mu;&pi;&omicron;&rho;&omicron;ύ&sigma;&alpha;&mu;&epsilon; &nu;&alpha; &beta;&rho;&omicron;ύ&mu;&epsilon; &kappa;ά&pi;&omicron;&iota;&omicron; &mu;έ&lambda;&omicron;&sigmaf; &mu;&epsilon; &alpha;&upsilon;&tau;ά &tau;&alpha; &sigma;&tau;&omicron;&iota;&chi;&epsilon;&iota;ά &lambda;&omicron;&gamma;&alpha;&rho;&iota;&alpha;&sigma;&mu;&omicron;ύ.',
'forgot_password' => '&Xi;&epsilon;&chi;ά&sigma;&alpha;&tau;&epsilon; &tau;&omicron;&nu; &kappa;&omega;&delta;&iota;&kappa;ό &pi;&rho;ό&sigma;&beta;&alpha;&sigma;&eta;&sigmaf;',
'new_password' => '&Nu;έ&omicron;&sigmaf; &Kappa;&omega;&delta;&iota;&kappa;ό&sigmaf;',
'new_password_confirm' => '&Epsilon;&pi;&alpha;&lambda;&eta;&theta;&epsilon;ύ&sigma;&tau;&epsilon; &tau;&omicron;&nu; &nu;έ&omicron; &Kappa;&omega;&delta;&iota;&kappa;ό',
'optional' => '(&Pi;&rho;&omicron;&alpha;&iota;&rho;&epsilon;&tau;&iota;&kappa;ό)',
'or' => '&Eta;\'',
'password_confirm' => '&Epsilon;&pi;&alpha;&lambda;&eta;&theta;&epsilon;ύ&sigma;&tau;&epsilon; &tau;&omicron;&nu; &Kappa;&omega;&delta;&iota;&kappa;ό',
'register_new_account' => '&Epsilon;&gamma;&gamma;&rho;&alpha;&phi;ή &nu;έ&omicron;&upsilon; &lambda;&omicron;&gamma;&alpha;&rho;&iota;&alpha;&sigma;&mu;&omicron;ύ',
'resend_val_button' => '&Epsilon;&pi;&alpha;&nu;&alpha;&pi;&omicron;&sigma;&tau;&omicron;&lambda;ή &Epsilon;&pi;&iota;&kappa;ύ&rho;&omega;&sigma;&eta;&sigmaf;',
'reset_pass_button' => '&Epsilon;&pi;&alpha;&nu;&alpha;&phi;&omicron;&rho;ά &Kappa;&omega;&delta;&iota;&kappa;&omicron;ύ',
'reset_password' => '&Epsilon;&pi;&alpha;&nu;&alpha;&phi;&omicron;&rho;ά &Kappa;&omega;&delta;&iota;&kappa;&omicron;ύ',
'upgrade_account_button' => '&Alpha;&nu;&alpha;&beta;ά&theta;&mu;&iota;&sigma;&eta; &Lambda;&omicron;&gamma;&alpha;&rho;&iota;&alpha;&sigma;&mu;&omicron;ύ',
'upgrade_msg' => '&Epsilon;&pi;&epsilon;&iota;&delta;ή &epsilon;ί&sigma;&tau;&epsilon; ή&delta;&eta; &sigma;&upsilon;&nu;&delta;&epsilon;&delta;&epsilon;&mu;έ&nu;&omicron;&iota; &omega;&sigmaf; &epsilon;&pi;&iota;&sigma;&kappa;έ&pi;&tau;&epsilon;&sigmaf;, &mu;&pi;&omicron;&rho;&epsilon;ί&tau;&epsilon; &nu;&alpha; &alpha;&nu;&alpha;&beta;&alpha;&theta;&mu;ί&sigma;&epsilon;&tau;&epsilon; &tau;&omicron; &lambda;&omicron;&gamma;&alpha;&rho;&iota;&alpha;&sigma;&mu;ό &sigma;&alpha;&sigmaf; &gamma;&iota;&alpha; &nu;&alpha; &kappa;&alpha;&tau;&alpha;&chi;&omega;&rho;&eta;&theta;&epsilon;ί &mu;&iota;&alpha; &pi;&lambda;ή&rho;&omega;&sigmaf; &upsilon;&pi;ό&psi;&eta; &delta;&iota;&alpha;&tau;&eta;&rho;ώ&nu;&tau;&alpha;&sigmaf; &tau;&alpha;&upsilon;&tau;ό&chi;&rho;&omicron;&nu;&alpha; ό&lambda;&epsilon;&sigmaf; &tau;&iota;&sigmaf; &tau;&rho;έ&chi;&omicron;&upsilon;&sigma;&epsilon;&sigmaf; &epsilon;&iota;&sigma;&iota;&tau;ή&rho;&iota;&alpha;. &Gamma;&iota;&alpha; &nu;&alpha; &alpha;&nu;&alpha;&beta;&alpha;&theta;&mu;ί&sigma;&epsilon;&tau;&epsilon; &tau;&omicron; &lambda;&omicron;&gamma;&alpha;&rho;&iota;&alpha;&sigma;&mu;ό &sigma;&alpha;&sigmaf;, &alpha;&pi;&lambda;ά &sigma;&upsilon;&mu;&pi;&lambda;&eta;&rho;ώ&sigma;&tau;&epsilon; &tau;&eta;&nu; &pi;&alpha;&rho;&alpha;&kappa;ά&tau;&omega; &phi;ό&rho;&mu;&alpha; &kappa;&alpha;&iota; &kappa;ά&nu;&tau;&epsilon; &kappa;&lambda;&iota;&kappa; &sigma;&tau;&omicron; &kappa;&omicron;&upsilon;&mu;&pi;ί &Alpha;&nu;&alpha;&beta;ά&theta;&mu;&iota;&sigma;&eta; &Lambda;&omicron;&gamma;&alpha;&rho;&iota;&alpha;&sigma;&mu;&omicron;ύ.',

);

?>