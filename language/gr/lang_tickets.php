<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | lang_tickets.php
#======================================================
*/

$lang = array(

'article_suggestions' => '&Pi;&rho;&omicron;&tau;ά&sigma;&epsilon;&iota;&sigmaf; &gamma;&iota;&alpha; &pi;&alpha;&rho;ό&mu;&omicron;&iota;&alpha; ά&rho;&theta;&rho;&alpha;',
'attachment' => '(&sigma;&upsilon;&nu;&eta;&mu;&mu;έ&nu;&omicron; &alpha;&rho;&chi;&epsilon;ί&omicron;)',
'attachment_b' => '&sigma;&upsilon;&nu;&eta;&mu;&mu;έ&nu;&omicron; &alpha;&rho;&chi;&epsilon;ί&omicron;',
'attachment_max_size' => '&Mu;έ&gamma;&iota;&sigma;&tau;&omicron; &mu;έ&gamma;&epsilon;&theta;&omicron;&sigmaf; &sigma;&upsilon;&nu;&eta;&mu;&mu;έ&nu;&omicron;&upsilon; &alpha;&rho;&chi;&epsilon;ί&omicron;&upsilon;:',
'captcha' => 'Captcha',
'close' => '&Kappa;&lambda;&epsilon;ί&sigma;&iota;&mu;&omicron;',
'close_msg_a' => '&Alpha;&upsilon;&tau;ό &tau;&omicron; &epsilon;&iota;&sigma;&iota;&tau;ή&rho;&iota;&omicron; έ&kappa;&lambda;&epsilon;&iota;&sigma;&epsilon; &alpha;&pi;ό &tau;&omicron;&nu;',
'close_msg_b' => '&gamma;&iota;&alpha; &tau;&omicron;&nu; &epsilon;&xi;ή&sigmaf; &lambda;ό&gamma;&omicron;.',
'close_ticket' => '&Kappa;&lambda;&epsilon;ί&sigma;&iota;&mu;&omicron; &epsilon;&iota;&sigma;&iota;&tau;&eta;&rho;ί&omicron;&upsilon;',
'close_ticket_button' => '&Kappa;&lambda;&epsilon;ί&sigma;&iota;&mu;&omicron; &epsilon;&iota;&sigma;&iota;&tau;&eta;&rho;ί&omicron;&upsilon;',
'closing_ticket' => '&Kappa;&lambda;&epsilon;ί&sigma;&iota;&mu;&omicron; &epsilon;&iota;&sigma;&iota;&tau;&eta;&rho;ί&omicron;&upsilon;',
'confirm_close' => '&Epsilon;ί&sigma;&tau;&epsilon; &sigma;ί&gamma;&omicron;&upsilon;&rho;&omicron;&iota; ό&tau;&iota; &theta;έ&lambda;&epsilon;&tau;&epsilon; &nu;&alpha; &kappa;&lambda;&epsilon;ί&sigma;&epsilon;&tau;&epsilon; &alpha;&upsilon;&tau;ό &tau;&omicron; &epsilon;&iota;&sigma;&iota;&tau;ή&rho;&iota;&omicron;;',
'confirm_delete_reply' => '&Epsilon;ί&sigma;&tau;&epsilon; &sigma;ί&gamma;&omicron;&upsilon;&rho;&omicron;&iota; ό&tau;&iota; &theta;έ&lambda;&epsilon;&tau;&epsilon; &nu;&alpha; &delta;&iota;&alpha;&gamma;&rho;ά&psi;&epsilon;&tau;&epsilon; &alpha;&upsilon;&tau;ή &tau;&eta;&nu; &alpha;&pi;ά&nu;&tau;&eta;&sigma;&eta;;',
'confirm_escalate' => '&Epsilon;ί&sigma;&tau;&epsilon; &sigma;ί&gamma;&omicron;&upsilon;&rho;&omicron;&iota; ό&tau;&iota; &theta;έ&lambda;&epsilon;&tau;&epsilon; &nu;&alpha; &kappa;&lambda;&iota;&mu;&alpha;&kappa;&omega;&theta;&epsilon;ί &alpha;&upsilon;&tau;ό &tau;&omicron; &epsilon;&iota;&sigma;&iota;&tau;ή&rho;&iota;&omicron;;',
'continue_ticket_submit' => '&Sigma;&upsilon;&nu;&epsilon;&chi;ί&sigma;&tau;&epsilon; &mu;&epsilon; &tau;&eta;&nu; &Upsilon;&pi;&omicron;&beta;&omicron;&lambda;ή &tau;&omicron;&upsilon; &epsilon;&iota;&sigma;&iota;&tau;&eta;&rho;ί&omicron;&upsilon;',
'download_attachment' => '&Kappa;&alpha;&tau;&epsilon;&beta;ά&sigma;&tau;&epsilon; &tau;&omicron; &sigma;&upsilon;&nu;&eta;&mu;&mu;έ&nu;&omicron; &alpha;&rho;&chi;&epsilon;ί&omicron; :',
'edit' => '&Epsilon;&pi;&epsilon;&xi;&epsilon;&rho;&gamma;&alpha;&sigma;ί&alpha;',
'edit_reply' => '&Epsilon;&pi;&epsilon;&xi;&epsilon;&rho;&gamma;&alpha;&sigma;ί&alpha; &Alpha;&pi;ά&nu;&tau;&eta;&sigma;&eta;&sigmaf;',
'edit_reply_button' => '&Epsilon;&pi;&epsilon;&xi;&epsilon;&rho;&gamma;&alpha;&sigma;ί&alpha; &Alpha;&pi;ά&nu;&tau;&eta;&sigma;&eta;&sigmaf;',
'edit_ticket' => '&Epsilon;&pi;&epsilon;&xi;&epsilon;&rho;&gamma;&alpha;&sigma;ί&alpha; &epsilon;&iota;&sigma;&iota;&tau;&eta;&rho;ί&omicron;&upsilon;',
'edit_ticket_button' => '&Epsilon;&pi;&epsilon;&xi;&epsilon;&rho;&gamma;&alpha;&sigma;ί&alpha; &epsilon;&iota;&sigma;&iota;&tau;&eta;&rho;ί&omicron;&upsilon;',
'email' => 'Email',
'email_address' => '&Delta;&iota;&epsilon;ύ&theta;&upsilon;&nu;&sigma;&eta; Email',
'enter_close_reason' => '&Pi;&alpha;&rho;&alpha;&kappa;&alpha;&lambda;ώ &epsilon;&iota;&sigma;ά&gamma;&epsilon;&tau;&epsilon; έ&nu;&alpha;&nu; &lambda;ό&gamma;&omicron; &gamma;&iota;&alpha; &tau;&omicron; &kappa;&lambda;&epsilon;ί&sigma;&iota;&mu;&omicron; &alpha;&upsilon;&tau;&omicron;ύ &tau;&omicron;&upsilon; &epsilon;&iota;&sigma;&iota;&tau;&eta;&rho;ί&omicron;&upsilon;.',
'err_captcha_mismatch' => '&Omicron; &kappa;&omega;&delta;&iota;&kappa;ό&sigmaf; Captcha &pi;&omicron;&upsilon; &delta;ώ&sigma;&alpha;&tau;&epsilon; &delta;&epsilon;&nu; &tau;&alpha;&iota;&rho;&iota;ά&zeta;&epsilon;&iota; &mu;&epsilon; &tau;&eta;&nu; &epsilon;&iota;&kappa;ό&nu;&alpha;. &Pi;&alpha;&rho;&alpha;&kappa;&alpha;&lambda;ώ &delta;&omicron;&kappa;&iota;&mu;ά&sigma;&tau;&epsilon; &xi;&alpha;&nu;ά.',
'err_email_in_use' => '&Alpha;&upsilon;&tau;ό &tau;&omicron; &mu;ή&nu;&upsilon;&mu;&alpha; &epsilon;ί&nu;&alpha;&iota; ή&delta;&eta; &sigma;&epsilon; &chi;&rho;ή&sigma;&eta; &alpha;&pi;ό έ&nu;&alpha; &alpha;&pi;ό &tau;&alpha; &mu;έ&lambda;&eta; &mu;&alpha;&sigmaf;. &Pi;&alpha;&rho;&alpha;&kappa;&alpha;&lambda;ώ &chi;&rho;&eta;&sigma;&iota;&mu;&omicron;&pi;&omicron;&iota;ή&sigma;&tau;&epsilon; έ&nu;&alpha; ά&lambda;&lambda;&omicron; email ή &sigma;&upsilon;&nu;&delta;&epsilon;&theta;&epsilon;ί&tau;&epsilon;.',
'err_no_cdfield' => '&Pi;&alpha;&rho;&alpha;&kappa;&alpha;&lambda;ώ &epsilon;&iota;&sigma;ά&gamma;&epsilon;&tau;&epsilon; &mu;&iota;&alpha; &tau;&iota;&mu;ή &gamma;&iota;&alpha; &tau;&omicron; &pi;&epsilon;&delta;ί&omicron;:',
'err_no_depart' => 'Please select a department.',
'err_no_email' => '&Pi;&lambda;&eta;&kappa;&tau;&rho;&omicron;&lambda;&omicron;&gamma;ή&sigma;&tau;&epsilon; &mu;&iota;&alpha; έ&gamma;&kappa;&upsilon;&rho;&eta; &delta;&iota;&epsilon;ύ&theta;&upsilon;&nu;&sigma;&eta; email.',
'err_no_message' => '&Pi;&lambda;&eta;&kappa;&tau;&rho;&omicron;&lambda;&omicron;&gamma;ή&sigma;&tau;&epsilon; έ&nu;&alpha; &mu;ή&nu;&upsilon;&mu;&alpha;.',
'err_no_name' => '&Pi;&alpha;&rho;&alpha;&kappa;&alpha;&lambda;ώ &epsilon;&iota;&sigma;ά&gamma;&epsilon;&tau;&epsilon; έ&nu;&alpha; ό&nu;&omicron;&mu;&alpha;.',
'err_no_reason' => '&Pi;&alpha;&rho;&alpha;&kappa;&alpha;&lambda;ώ &epsilon;&iota;&sigma;ά&gamma;&epsilon;&tau;&epsilon; έ&nu;&alpha;&nu; &lambda;ό&gamma;&omicron;.',
'err_no_reply' => '&Pi;&lambda;&eta;&kappa;&tau;&rho;&omicron;&lambda;&omicron;&gamma;ή&sigma;&tau;&epsilon; &mu;&iota;&alpha; &alpha;&pi;ά&nu;&tau;&eta;&sigma;&eta;.',
'err_no_subject' => '&Pi;&lambda;&eta;&kappa;&tau;&rho;&omicron;&lambda;&omicron;&gamma;ή&sigma;&tau;&epsilon; έ&nu;&alpha; &theta;έ&mu;&alpha;.',
'err_upload_bad_type' => '&Omicron; &tau;ύ&pi;&omicron;&sigmaf; &alpha;&rho;&chi;&epsilon;ί&omicron;&upsilon; &sigma;&alpha;&sigmaf; &pi;&omicron;&upsilon; &pi;&rho;&omicron;&sigma;&pi;&alpha;&theta;&epsilon;ί&tau;&epsilon; &nu;&alpha; &mu;&epsilon;&tau;&alpha;&phi;&omicron;&rho;&tau;ώ&sigma;&epsilon;&tau;&epsilon; &delta;&epsilon;&nu; &epsilon;&pi;&iota;&tau;&rho;έ&pi;&epsilon;&tau;&alpha;&iota;.',
'err_upload_failed' => 'File upload failed. &Pi;&alpha;&rho;&alpha;&kappa;&alpha;&lambda;ώ &delta;&omicron;&kappa;&iota;&mu;ά&sigma;&tau;&epsilon; &xi;&alpha;&nu;ά.',
'err_upload_too_big' => '&Tau;&omicron; &alpha;&rho;&chi;&epsilon;ί&omicron; &pi;&omicron;&upsilon; &pi;&rho;&omicron;&sigma;&pi;&alpha;&theta;&epsilon;ί&tau;&epsilon; &nu;&alpha; &phi;&omicron;&rho;&tau;ώ&sigma;&epsilon;&tau;&epsilon; &upsilon;&pi;&epsilon;&rho;έ&beta;&eta; &tau;&omicron; &mu;έ&gamma;&iota;&sigma;&tau;&omicron; &mu;έ&gamma;&epsilon;&theta;&omicron;&sigmaf; &gamma;&iota;&alpha; &mu;&epsilon;&tau;&alpha;&phi;ό&rho;&tau;&omega;&sigma;&eta;.',
'escalate' => '&Kappa;&lambda;&iota;&mu;ά&kappa;&omega;&sigma;&eta;',
'guest_login' => '&Epsilon;ί&sigma;&omicron;&delta;&omicron;&sigmaf; &Epsilon;&pi;&iota;&sigma;&kappa;έ&pi;&tau;&eta;',
'guest_login_info' => '&Kappa;&alpha;&lambda;&omega;&sigma;ό&rho;&iota;&sigma;&epsilon;&sigmaf; &Epsilon;&pi;&iota;&sigma;&kappa;έ&pi;&tau;&eta;. &Pi;&alpha;&rho;&alpha;&kappa;&alpha;&lambda;ώ &sigma;&upsilon;&nu;&delta;&epsilon;&theta;&epsilon;ί&tau;&epsilon; &chi;&rho;&eta;&sigma;&iota;&mu;&omicron;&pi;&omicron;&iota;ώ&nu;&tau;&alpha;&sigmaf; &tau;&eta;&nu; &eta;&lambda;&epsilon;&kappa;&tau;&rho;&omicron;&nu;&iota;&kappa;ή &sigma;&alpha;&sigmaf; &delta;&iota;&epsilon;ύ&theta;&upsilon;&nu;&sigma;&eta; &kappa;&alpha;&iota; &epsilon;&iota;&sigma;&iota;&tau;ή&rho;&iota;&omicron; &kappa;&lambda;&epsilon;&iota;&delta;ί &kappa;ά&tau;&omega; &mu;&epsilon; &tau;&eta;&nu; &pi;&rho;ό&sigma;&beta;&alpha;&sigma;&eta; &sigma;&alpha;&sigmaf; &sigma;&chi;ό&lambda;&iota;&alpha; &tau;&omega;&nu; &epsilon;&iota;&sigma;&iota;&tau;&eta;&rho;ί&omega;&nu;. &Alpha;&nu; &epsilon;ί&sigma;&tau;&epsilon; ή&delta;&eta; &epsilon;&gamma;&gamma;&epsilon;&gamma;&rho;&alpha;&mu;&mu;έ&nu;&omicron; &mu;έ&lambda;&omicron;&sigmaf;, &mu;&pi;&omicron;&rho;&epsilon;ί&tau;&epsilon; &nu;&alpha; &sigma;&upsilon;&nu;&delta;&epsilon;&theta;&epsilon;ί&tau;&epsilon; &mu;έ&sigma;&omega; &tau;&omicron;&upsilon; &pi;&lambda;&alpha;&iota;&sigma;ί&omicron;&upsilon; &Epsilon;&iota;&sigma;ό&delta;&omicron;&upsilon;  &sigma;&tau;&alpha; &delta;&epsilon;&xi;&iota;ά.',
'guest_ticket_notification' => '&Kappa;&omicron;&iota;&nu;&omicron;&pi;&omicron;ί&eta;&sigma;&eta; Email &alpha;&pi;&omicron;  &Alpha;&pi;&alpha;&nu;&tau;ή&sigma;&epsilon;&iota;&sigmaf; &tau;&omicron;&upsilon; &Pi;&rho;&omicron;&sigma;&omega;&pi;&iota;&kappa;&omicron;ύ',
'history' => '&Iota;&sigma;&tau;&omicron;&rho;&iota;&kappa;ό',
'last_replier' => '&tau;&epsilon;&lambda;&epsilon;&upsilon;&tau;&alpha;ί&alpha; &alpha;&pi;ά&nu;&tau;&eta;&sigma;&eta; &chi;&rho;ή&sigma;&tau;&eta;',
'last_reply' => '&tau;&epsilon;&lambda;&epsilon;&upsilon;&tau;&alpha;ί&alpha; &alpha;&pi;ά&nu;&tau;&eta;&sigma;&eta;',
'name' => 'Ό&nu;&omicron;&mu;&alpha;',
'no_replies' => '&Sigma;&upsilon;&gamma;&nu;ώ&mu;&eta;, &alpha;&lambda;&lambda;ά &delta;&epsilon;&nu; έ&chi;&omicron;&upsilon;&nu; &gamma;ί&nu;&epsilon;&iota; &alpha;&pi;&alpha;&nu;&tau;ή&sigma;&epsilon;&iota;&sigmaf;  &sigma;&epsilon; &alpha;&upsilon;&tau;ό &tau;&omicron; &epsilon;&iota;&sigma;&iota;&tau;ή&rho;&iota;&omicron;.',
'no_suggestions_helped' => '&Kappa;&alpha;&mu;&iota;ά &alpha;&pi;ό &tau;&iota;&sigmaf; &pi;&alpha;&rho;&alpha;&pi;ά&nu;&omega; &pi;&rho;&omicron;&tau;ά&sigma;&epsilon;&iota;&sigmaf; &delta;&epsilon;&nu; &mu;&epsilon; &beta;&omicron;ή&theta;&eta;&sigma;&alpha;&nu; &nu;&alpha; &epsilon;&pi;&iota;&lambda;&upsilon;&theta;&epsilon;ί &tau;&omicron; &zeta;ή&tau;&eta;&mu;&alpha;.',
'open_ticket_button' => '&Upsilon;&pi;&omicron;&beta;&omicron;&lambda;ή',
'optional' => '(&Pi;&rho;&omicron;&alpha;&iota;&rho;&epsilon;&tau;&iota;&kappa;ά)',
'relevance' => '&Sigma;&upsilon;&nu;&alpha;&phi;ή',
'replies' => '&Alpha;&pi;&alpha;&nu;&tau;ή&sigma;&epsilon;&iota;&sigmaf;',
'select_depart' => '&Pi;&alpha;&rho;&alpha;&kappa;&alpha;&lambda;ώ &epsilon;&pi;&iota;&lambda;έ&xi;&tau;&epsilon; &mu;&iota;&alpha; &upsilon;&pi;&eta;&rho;&epsilon;&sigma;ί&alpha;.',
'send_reply' => '&Sigma;&tau;&epsilon;ί&lambda;&epsilon; &Alpha;&pi;ά&nu;&tau;&eta;&sigma;&eta;',
'send_reply_button' => 'Send Reply',
'suggestions_explained' => '&Alpha;&kappa;&omicron;&lambda;&omicron;&upsilon;&theta;&epsilon;ί &eta; &lambda;ί&sigma;&tau;&alpha; &tau;&omega;&nu; &pi;&iota;&theta;&alpha;&nu;ώ&nu; &lambda;ύ&sigma;&epsilon;&omega;&nu; &alpha;&pi;ό &tau;&eta;&nu; &Gamma;&nu;&omega;&sigma;&iota;&alpha;&kappa;ή &Beta;ά&sigma;&eta;. &Pi;&alpha;&rho;&alpha;&kappa;&alpha;&lambda;&omicron;ύ&mu;&epsilon; &nu;&alpha; &epsilon;&lambda;έ&gamma;&xi;&epsilon;&tau;&epsilon; &tau;&alpha; ά&rho;&theta;&rho;&alpha; &kappa;&alpha;&iota; &beta;&epsilon;&beta;&alpha;&iota;&omega;&theta;&epsilon;ί&tau;&epsilon; ό&tau;&iota; &delta;&epsilon;&nu; &theta;&alpha; &sigma;&alpha;&sigmaf; &beta;&omicron;&eta;&theta;ή&sigma;&omicron;&upsilon;&nu;,  &pi;&rho;&iota;&nu; &sigma;&upsilon;&nu;&epsilon;&chi;ί&sigma;&epsilon;&tau;&epsilon; &mu;&epsilon; &tau;&eta;&nu; &upsilon;&pi;&omicron;&beta;&omicron;&lambda;ή &tau;&omega;&nu; &epsilon;&iota;&sigma;&iota;&tau;&eta;&rho;ί&omega;&nu;. <i>&Tau;&omicron; &epsilon;&iota;&sigma;&iota;&tau;ή&rho;&iota;ό &sigma;&alpha;&sigmaf; &delta;&epsilon;&nu; έ&chi;&epsilon;&iota; &upsilon;&pi;&omicron;&beta;&lambda;&eta;&theta;&epsilon;ί &alpha;&kappa;ό&mu;&eta;.</i>',
'thumbs_down' => 'Thumbs Down',
'thumbs_up' => 'Thumbs Up',
'ticket_center_overview' => '&Kappa;έ&nu;&tau;&rho;&omicron; &Epsilon;&pi;&iota;&sigma;&kappa;ό&pi;&eta;&sigma;&eta;&sigmaf; &epsilon;&iota;&sigma;&iota;&tau;&eta;&rho;ί&omega;&nu;',
'ticket_id' => 'Ticket Id',
'ticket_key' => 'Ticket Key',
'viewing_ticket' => '&Pi;&rho;&omicron;&beta;&omicron;&lambda;ή &epsilon;&iota;&sigma;&iota;&tau;ή&rho;&iota;&omicron;&upsilon;',

);

?>