<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | ad_lang_error.php
#======================================================
*/

$lang = array(

'error' => 'Errore',
'error_has_occured' => 'Si &egrave; verificato un errore.',
'fill_form_completely' => 'Devi completare tutti i campi del form prima di poterlo inviare.',
'fill_form_lengths' => 'Uno o pi&ugrave; dei campi che hai completato non raggiunge la lunghezza minima richiesta.',
'login_no_admin' => 'Spiacente, non hai accesso al pannello di controllo amministrativo.',
'login_no_pass' => 'Spiacente, la password non &egrave; corretta.',
'login_no_user' => 'Non abbiamo trovato un utente registrato che corrisponde all\'username inserito.',
'must_login' => 'Devi effettuare il login prima di potere accedere al pannello di controllo amministrativo.',
'news_disabled' => 'Il sistema delle News &egrave; stato disabilitato. <a href=\'<! HD_URL !>/admin.php?section=manage&act=settings&code=find&group=news\'>Clicca qui</a> per gestire le tue impostazioni degli annunci.',
'no_article' => 'Spiacente, l\'articolo che stavi cercando non &egrave; stato trovato.',
'no_articles_found' => 'Spiacente, non abbiamo trovato alcun articolo che corrispondesse ai tuoi criteri di ricerca.',
'no_attachment' => 'Spiacente, l\'allegato che stavi cercando non &egrave; stato trovato.',
'no_canned' => 'Spiacente, il modello di risposta che stavi cercando non &egrave; stato trovato.',
'no_category' => 'Spiacente, la categoria che stavi cercando non &egrave; stata trovata.',
'no_create_lang' => 'Spiacente, non &egrave; stato possibile creare i file del linguaggio. Per favore applica CHMOD alla cartella \'languages\' con permessi 0777.',
'no_delete_default_lang' => 'Spiacente, ma non puoi cancellare il linguaggio di default.',
'no_delete_default_skin' => 'Spiacente, ma non puoi cancellare la skin di default.',
'no_department' => 'Spiacente, la sezione che stavi cercando non &egrave; stata trovata.',
'no_dfield' => 'Spiacente, il campo della sezione personalizzata che stavi cercando non &egrave; stato trovato.',
'no_export_templates' => 'Spiacente, la skin non pu&ograve; essere generata perch&egrave; non ci sono template da esportare.',
'no_group' => 'Spiacente, il gruppo che stavi cercando non &egrave; stato trovato.',
'no_lang' => 'Spiacente, il linguaggio che stavi cercando non &egrave; stato trovato.',
'no_member' => 'Spiacente, l\'utente che stavi cercando non &egrave; stato trovato.',
'no_members_found' => 'Spiacente, ma non abbiamo trovato alcun utente che corrisponde ai tuoi criteri di ricerca.',
'no_message' => 'Per favore, inserisci un messaggio.',
'no_mm_tickets' => 'Spiacente, ma non hai selezionato alcun ticket.',
'no_mm_valid_tickets' => 'Spiacente, ma nessun ticket che hai selezionato &egrave; valido per questa azione.',
'no_open_file' => 'Spiacente, il file non pu&ograve; essere aperto.',
'no_page' => 'Spiacente, la pagina che stavi cercando non &egrave; stata trovata.',
'no_perm' => 'Spiacente, non hai i privilegi sufficienti per accedere a quest\'area.',
'no_perm_banned' => 'Spiacente, non hai i privilegi sufficienti per accedere al sistema.',
'no_pfield' => 'Spiacente, il campo profilo personalizzato che stavi cercando non &egrave; stato trovato.',
'no_reply' => 'Spiacente, la risposta che stavi cercando non &egrave; stata trovata.',
'no_settings_found' => 'Spiacente, le impostazioni che stavi cercando non sono state trovate.',
'no_skin' => 'Spiacente, lo skin che stavi cercando non &egrave; stato trovato.',
'no_subject' => 'Per favore, inserisci un oggetto.',
'no_template' => 'Spiacente, il template che stavi cercando non &egrave; stato trovato.',
'no_ticket' => 'Spiacente, il ticket che stavi cercando non &egrave; stato trovato.',
'no_upload_lang_xml' => 'Spiacente, il file che hai cercato di caricare non &egrave; un file XML supportato dal sistema.',
'no_upload_move' => 'Spiacente, non abbiamo potuto caricare il tuo file. Per favore,  applica CHMOD alla tua cartella \'tmp\' con permessi 0777.',
'no_upload_size' => 'Spiacente, il file che hai cercato di caricare non contiene alcun dato.',
'no_upload_skin_xml' => 'Spiacente, il file che hai cercato di caricare non &egrave; un file skin valido per il nostro sistema.',
'not_writable' => 'Spiacente, il file che stai cercando di scrivere non &egrave; modificabile. Per favore applica al file CHMOD con permessi 0777.',
'not_writable_img_dir' => 'Spiacente, non abbiamo potuto creare la skin perch&egrave; la cartella ./images/ non &egrave; scrivibile. Per favore applica CHMOD con permessi 0777.',
'not_writable_skin' => 'Spiacente, non abbiamo potuto creare la skin perch&egrave; la cartella ./skin/ non &egrave; scrivibile. Per favore applica CHMOD con permessi 0777.',
'prune_no_days' => 'Spiacente, non hai inserito il valore dei giorni.',
'root_edit_mem' => 'Solo un amministratore pu&ograve; modificare questo utente.',
'ticket_closed_already' => 'Spiacente, il ticket &egrave; gi&agrave; stato chiuso.',
'ticket_closed_escalate' => 'Spiacente, il ticket &egrave; chiuso quindi non pu&ograve; essere promosso.',
'ticket_closed_hold' => 'Spiacente, il ticket &egrave; chiuso quindi non pu&ograve; essere messo in attesa.',
'ticket_closed_reply' => 'Spiacente, il ticket &egrave; chiuso quindi non puoi rispondere.',
'ticket_escalated_already' => 'Spiacente, il ticket &egrave; gi&agrave; stato promosso.',
'ticket_hold_already' => 'Spiacente, il ticket &egrave; gi&agrave; stato messo in attesa.',
'ticket_reopen_already' => 'Spiacente, ma il ticket &egrave; gi&agrave; aperto.',
'try_again' => 'Per favore, torna indietro e prova ancora. Se il problema persiste, contatta un amministratore.',
'upload_bad_type' => 'Il tipo di file che stai cercando di caricare non &egrave; permesso.',
'upload_failed' => 'Invio del file fallito. Per favore, riprova.',
'upload_too_big' => 'Il file che stai cercando di caricare eccede la massima dimensione consentita di upload.',

);

?>