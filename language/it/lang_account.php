<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | lang_account.php
#======================================================
*/

$lang = array(

'account_overview' => 'Panoramica del tuo account',
'account_settings' => 'Impostazioni dell\'account',
'change_email' => 'Modifica email',
'change_email_address' => 'Modifica l\'indirizzo email',
'change_email_button' => 'Modifica email',
'change_pass_button' => 'Modifica la password',
'change_password' => 'Modifica la password',
'current_password' => 'Password corrente',
'custom_profile_fields' => 'Campi personalizzati del profilo',
'disabled' => 'Disabilitato',
'dst_active' => 'Ora legale attiva',
'email' => 'Email',
'email_notifications' => 'Notifiche per email',
'email_preferences' => 'Preferenze email',
'email_staff_new_ticket' => 'Nuovi ticket nelle mie sezioni',
'email_staff_ticket_reply' => 'Nuove risposte nelle mie sezioni',
'email_type' => 'Tipo email',
'enabled' => 'Abilitato',
'err_login_no_pass' => 'La tua password non &egrave; corretta.',
'err_no_cpfield' => 'Per favore, inserisci un valore per il campo:',
'err_no_email_change' => 'Questo indirizzo email &egrave; identico al tuo indirizzo email corrente.',
'err_no_email_match' => 'I tuoi indirizzi email non corrispondono.',
'err_no_email_valid' => 'Per favore, inserisci un indirizzo email valido.',
'err_no_new_pass_short' => 'Per favore, inserisci una nuova password.',
'err_no_pass_match' => 'Le tue password non corrispondono.',
'err_no_pass_short' => 'Per favore, inserisci la password corrente.',
'general_info' => 'Informazioni generali',
'group' => 'Gruppo',
'html' => 'HTML',
'joined' => 'Collegato',
'language' => 'Linguaggio',
'last_visit' => 'Ultima visita',
'modify_account' => 'Modifica account',
'modify_account_information' => 'Modifica le informazioni account',
'new_email_address' => 'Nuovo indirizzo email',
'new_email_address_confirm' => 'Conferma il nuovo indirizzo email',
'new_password' => 'Nuova password',
'new_password_confirm' => 'Conferma la nuova password',
'new_reply' => 'Nuova risposta',
'new_ticket' => 'Nuovo ticket',
'notifications_for' => 'Notifiche per',
'optional' => '(Opzionale)',
'plain_text' => 'Testo semplice',
'rich_text_editor' => 'Rich Text Editor',
'skin' => 'Skin',
'time_is_now' => 'L\'ora corrente &egrave;:',
'time_zone' => 'Fuso orario',
'title' => 'Titolo',
'update_account' => 'Aggiorna Account',

);

?>