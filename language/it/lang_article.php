<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | lang_article.php
#======================================================
*/

$lang = array(

'add_a_comment' => 'Aggiungi un commento',
'add_comment_button' => 'Aggiungi commento',
'categories' => 'Categorie',
'confirm_delete' => 'Sei sicuro di voler cancellare questo commento?',
'delete' => 'Cancella',
'edit' => 'Modifica',
'edit_comment' => 'Modifica commento',
'edit_comment_button' => 'Modifica commento',
'err_no_comment' => 'Per favore, inserisci un commento.',
'full_star' => 'Stella piena',
'half_star' => 'Mezza stella',
'keywords_phrase' => 'Inserisci le parole chiave o una frase',
'no_articles' => 'Non ci sono articoli da visualizzare.',
'no_articles_found' => 'Non sono stati trovati articoli.',
'no_comments' => 'Non sono ancora stati lasciati commenti relativi a questo articolo.',
'no_star' => 'Nessuna stella',
'relevance' => 'Rilevanza',
'search_results' => 'Risultati della ricerca',
'stars' => 'Stelle',
'view_category' => 'Visualizza sezione',
'viewing_article' => 'Visualizza articolo',

);

?>