<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | lang_email_content.php
#======================================================
*/

$lang['header'] = <<<EOF
Gentile <#MEM_NAME#>,
EOF;

$lang['footer'] = <<<EOF
Cordiali saluti,

Il team di <#HD_NAME#>.

<#HD_URL#>
EOF;

$lang['change_email_val_sub'] = "Verifica la tua email";

$lang['change_email_val'] = <<<EOF
Hai richiesto che la tua email venga modificata dall'indirizzo attuale a questo indirizzo.
Per completare la modifica, devi verificare questa email cliccando sul link di validazione sottostante.

---------------------------

Link di validazione: <#VAL_LINK#>

---------------------------
EOF;

$lang['new_user_val_email_sub'] = "Verifica il tuo nuovo account";

$lang['new_user_val_email'] = <<<EOF
Benvenuto a <#HD_NAME#>.  Hai richiesto un nuovo account al nostro sistema. 
Per attivare il tuo account devi verificare questo indirizzo email cliccando sul link di validazione sottostante.

---------------------------

Username: <#USER_NAME#>

Link di validazione: <#VAL_LINK#>

---------------------------
EOF;

$lang['new_user_val_both_sub'] = "Verifica il tuo nuovo account";

$lang['new_user_val_both'] = <<<EOF
Benvenuto in <#HD_NAME#>. Hai richiesto un nuovo account al nostro sistema. 
Per attivare il tuo account devi verificare questo indirizzo email cliccando sul link di validazione sottostante. 
Inoltre, il tuo account dovr&agrave; essere manualmente approvato da un amministratore.


---------------------------

Username: <#USER_NAME#>

Link di validazione: <#VAL_LINK#>

---------------------------

Ricorda, oltre ad avere verificato il tuo indirizzo email, il tuo account dovr&agrave; essere manualmente approvato da un amministratore.
Quando il tuo account sar&agrave; approvato, ti verr&agrave; inviata una email.
EOF;

$lang['new_user_val_admin_sub'] = "Il tuo nuovo account";

$lang['new_user_val_admin'] = <<<EOF
Hai richiesto un nuovo account al nostro sistema.  
Prima che tu possa iniziare ad utilizzare il tuo account, un amministratore dovr&agrave; manualmente approvarlo.
Quando il tuo account sar&agrave; approvato, ti verr&agrave; inviata una email.
EOF;

$lang['acc_accivated_sub'] = "Account attivato";

$lang['acc_accivated'] = <<<EOF
Il tuo account &egrave; stato attivato con successo.  Ora puoi effettuare il login.
EOF;

$lang['acc_almost_accivated_sub'] = "Account in attesa di approvazione";

$lang['acc_almost_accivated'] = <<<EOF
Il tuo indirizzo email &egrave; stato verificato con successo, ma prima che tu possa iniziare ad utilizzare il tuo account, un amministratore dovr&agrave; manualmente approvarlo.
Quando il tuo account sar&agrave; approvato, ti verr&agrave; inviata una email.
EOF;

$lang['acc_approved_sub'] = "Account approvato";

$lang['acc_approved'] = <<<EOF
Il tuo account &egrave; stato approvato con successo da un amministratore.  Ora puoi effettuare il login.
EOF;

$lang['acc_almost_approved_sub'] = "Account in attesa di validazione dell\'indirizzo email";

$lang['acc_almost_approved'] = <<<EOF
Il tuo account &egrave; stato approvato con successo da un amministratore,
ma prima che tu possa iniziare ad utilizzare il tuo account, 
devi verificare questo indirizzo email cliccando sul link di validazione contenuto nella email che ti &egrave; stata inviata.
EOF;

$lang['new_ticket_sub'] = "Ticket ID #<#TICKET_ID#>";

$lang['new_ticket'] = <<<EOF
Hai inviato un nuovo ticket. Il nostro staff esaminer&agrave; il prima possibile il tuo ticket e risponder&agrave; di conseguenza.
Di seguito sono elencati i dettagli del ticket.

---------------------------

Ticket ID: <#TICKET_ID#>
Argomento: <#SUBJECT#>
Sezione: <#DEPARTMENT#>
Priorit&agrave;: <#PRIORITY#>
Data di invio: <#SUB_DATE#>

---------------------------

Puoi visualizzare il tuo ticket cliccando su questo link: <#TICKET_LINK#>
EOF;

$lang['staff_new_ticket_sub'] = "Ticket ID #<#TICKET_ID#>";

$lang['staff_new_ticket'] = <<<EOF
Un nuovo ticket &egrave; stato creato nella tua sezione. 
Di seguito sono elencati i dettagli del ticket.

---------------------------

Ticket ID: <#TICKET_ID#>
Argomento: <#SUBJECT#>
Sezione: <#DEPARTMENT#>
Priorit&agrave;: <#PRIORITY#>
Data di invio: <#SUB_DATE#>

---------------------------

<#MESSAGE#>

---------------------------

Puoi gestire questo ticket cliccando sul link: <#TICKET_LINK#>
EOF;

$lang['new_guest_ticket_sub'] = "Ticket ID #<#TICKET_ID#>";

$lang['new_guest_ticket'] = <<<EOF
Hai inviato un nuovo ticket come utente senza profilo.  Il nostro staff esaminer&agrave; il prima possibile il tuo ticket e risponder&agrave; di conseguenza.  
Di seguito sono elencati i dettagli del ticket.

---------------------------

Ticket ID: <#TICKET_ID#>
Argomento: <#SUBJECT#>
Sezione: <#DEPARTMENT#>
Priorit&agrave;: <#PRIORITY#>
Data di invio: <#SUB_DATE#>

Chiave del ticket: <#TICKET_KEY#>

---------------------------

Puoi visualizzare il tuo ticket cliccando su questo link: <#TICKET_LINK#>
EOF;

$lang['staff_new_guest_ticket_sub'] = "Ticket ID #<#TICKET_ID#>";

$lang['staff_new_guest_ticket'] = <<<EOF
Un nuovo ticket da utente senza profilo &egrave; stato creato nella tua sezione. 
Di seguito sono elencati i dettagli del ticket.

---------------------------

Ticket ID: <#TICKET_ID#>
Argomento: <#SUBJECT#>
Sezione: <#DEPARTMENT#>
Priorit&agrave;: <#PRIORITY#>
Data di invio: <#SUB_DATE#>
---------------------------

<#MESSAGE#>

---------------------------

Puoi gestire questo ticket cliccando sul link: <#TICKET_LINK#>
EOF;

$lang['ticket_escl_sub'] = "Ticket ID #<#TICKET_ID#> - Promosso";

$lang['ticket_escl'] = <<<EOF
Uno dei tuoi ticket &egrave; stato promosso.
Il nostro team esaminer&agrave; quanto prima il tuo ticket. 
Di seguito sono elencati i dettagli del ticket.

---------------------------

Ticket ID: <#TICKET_ID#>
Argomento: <#SUBJECT#>
Sezione: <#DEPARTMENT#>
Priorit&agrave;: <#PRIORITY#>
Data di invio: <#SUB_DATE#>

---------------------------

Puoi visualizzare il tuo ticket cliccando su questo link: <#TICKET_LINK#>
EOF;

$lang['ticket_close_sub'] = "Ticket ID #<#TICKET_ID#> - Chiuso";

$lang['ticket_close'] = <<<EOF
Uno dei tuoi ticket &egrave; stato chiuso.
Di seguito sono elencati i dettagli del ticket.

---------------------------

Ticket ID: <#TICKET_ID#>
Argomento: <#SUBJECT#>
Sezione: <#DEPARTMENT#>
Priorit&agrave;: <#PRIORITY#>
Data di invio: <#SUB_DATE#>
---------------------------

Puoi visualizzare il tuo ticket cliccando su questo link: <#TICKET_LINK#>

Se possiamo fare altro per aiutarti o se &egrave; necessaria ulteriore assistenza, siamo a disposizione.
EOF;

$lang['ticket_move_sub'] = "Ticket ID #<#TICKET_ID#> - Spostato";

$lang['ticket_move'] = <<<EOF
Uno dei tuoi ticket &egrave; stato spostato in un'altra sezione.
Di seguito sono elencati i dettagli del ticket.

---------------------------

Ticket ID: <#TICKET_ID#>
Argomento: <#SUBJECT#>
Vecchia sezione: <#OLD_DEPARTMENT#>
Nuova sezione: <#NEW_DEPARTMENT#>
Priorit&agrave;: <#PRIORITY#>
Data di invio: <#SUB_DATE#>

---------------------------

Puoi visualizzare il tuo ticket cliccando su questo link: <#TICKET_LINK#>

Ora il ticket verr&agrave; preso in carico dallo staff che segue la nuova sezione.
EOF;

$lang['ticket_reply_sub'] = "Ticket ID #<#TICKET_ID#> - Risposta";

$lang['ticket_reply'] = <<<EOF
E' stata inviata una risposta al tuo ticket.
Di seguito sono elencati i dettagli del ticket.

---------------------------

<#REPLY#>

---------------------------

Ticket ID: <#TICKET_ID#>
Argomento: <#SUBJECT#>
Sezione: <#DEPARTMENT#>
Priorit&agrave;: <#PRIORITY#>
Data di invio: <#SUB_DATE#>

---------------------------

Puoi visualizzare il tuo ticket cliccando su questo link: <#TICKET_LINK#>

Se necessaria ulteriore assistenza per questo argomento risponda a questo ticket.
EOF;

$lang['staff_reply_ticket_sub'] = "Ticket ID #<#TICKET_ID#> - Risposta";

$lang['staff_reply_ticket'] = <<<EOF
E' stata inviata una risposta ad un ticket della tua sezione.  
Di seguito sono elencati i dettagli del ticket.

---------------------------

<#REPLY#>

---------------------------

Ticket ID: <#TICKET_ID#>
Argomento: <#SUBJECT#>
Sezione: <#DEPARTMENT#>
Priorit&agrave;: <#PRIORITY#>
Data di invio: <#SUB_DATE#>

---------------------------

Puoi gestire questo ticket cliccando sul link: <#TICKET_LINK#>
EOF;

$lang['announcement_sub'] = "<#TITLE#>";

$lang['announcement'] = <<<EOF
E' stato inserito un nuovo comunicato intitolato '<#TITLE#>'.

---------------------------

<#CONTENT#>

---------------------------

Hai ricevuto questa email perch&egrave; hai scelto nel tuo profilo di ricevere notifiche via email per nuovi comunicati.
Se non desideri ricevere queste mail, esegui il login sul nostro sistema e aggiorna le preferenze del tuo account.
EOF;

$lang['reset_pass_val_sub'] = "Resetta la tua password";

$lang['reset_pass_val'] = <<<EOF
Hai richiesto di resettare la tua password su <#HD_NAME#>.  
Per procedere al cambio della tua password, clicca sul link di validazione sottostante.  
Se non hai richiesto di resettare la tua password, per favore ignora questa email.

---------------------------

Username: <#USER_NAME#>

Link di validazione per cambio password: <#VAL_LINK#>
EOF;

$lang['reply_pipe_closed_sub'] = "Risposta non accettata";

$lang['reply_pipe_closed'] = <<<EOF
Non abbiamo potuto accettare la tua email e aggiungere la tua risposta perch&egrave; il ticket &egrave; chiuso.

---------------------------

Ticket ID: <#TICKET_ID#>
Argomento: <#SUBJECT#>

---------------------------

Se pensi che questo messaggio sia un errore, per favore contattaci.
EOF;

$lang['ticket_pipe_rejected_sub'] = "Ticket non accettato";

$lang['ticket_pipe_rejected'] = <<<EOF
Non abbiamo potuto accettare la tua email e creare un ticket perch&egrave; non hai i permessi sufficienti per
creare ticket in questa sezione.


Se pensi che questo messaggio sia un errore, per favore contattaci.
EOF;

$lang['new_user_admin_val_sub'] = "Nuova registrazione: <#USER_NAME#>";

$lang['new_user_admin_val'] = <<<EOF
Un nuovo membro si &egrave; registrato ed &egrave; in attesa dell'approvazione di un amministratore.
Di seguito sono elencati i dettagli.

---------------------------

Utente: <#USER_NAME#>
Email: <#USER_EMAIL#>
Data di registrazione: <#JOIN_DATE#>

---------------------------

Puoi gestire i membri in attesa di approvazione cliccando questo link: <#APPROVE_LINK#>
EOF;

?>