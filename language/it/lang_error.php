<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | lang_error.php
#======================================================
*/

$lang = array(

'already_rated' => 'Spiacente, hai gi&agrave; valutato questo articolo.',
'already_rated_reply' => 'Spiacente, hai gi&agrave; valutato questa risposta.',
'already_validated' => 'Il tuo account &egrave; gi&agrave; stato attivato.',
'banned_kb' => 'Spiacente, non hai i permessi sufficienti per accedere alla Knowledge Base.',
'banned_kb_comment' => 'Spiacente, non hai i permessi sufficienti per commentare gli articoli.',
'banned_kb_rate' => 'Spiacente, non hai i permessi sufficienti per valutare gli articoli.',
'banned_news_comment' => 'Spiacente, non hai i permessi sufficienti per commentare le novit&agrave;.',
'banned_ticket' => 'Spiacente, non hai i permessi sufficienti per accedere al Centro Ticket.',
'banned_ticket_escalate' => 'Spiacente, non hai i permessi sufficienti per promuovere un ticket.',
'banned_ticket_open' => 'Spiacente, non hai i permessi sufficienti per  aprire un ticket.',
'banned_ticket_rate' => 'Spiacente, non hai i permessi sufficienti per valutare le risposte dello staff.',
'error' => 'Errore',
'error_has_occured' => 'Si &egrave; verificato un errore.',
'fill_form_completely' => 'Devi compilare completamente il form prima di inviarlo.',
'fill_form_lengths' => 'Uno o pi&ugrave; dei campi che hai compilato non raggiunge la lunghezza minima richiesta.',
'invalid_rate_value' => 'Spiacente, devi scegliere un voto tra 1 e 5.',
'invalid_rate_value_reply' => 'Spiacente, devi scegliere pollice su o pollice gi&ugrave;.',
'kb_comment_disabled' => 'Spiacente, ma l\'invio di commenti &egrave; stato disabilitato.',
'kb_disabled' => 'Spiacente, ma la Knowledge Base &egrave; stata disabilitata.',
'kb_rating_disabled' => 'Spiacente, ma la valutazione &egrave; stata disabilitata.',
'login_must_val' => 'Spiacente, devi attivare il tuo account cliccando sul link contenuto nella email che ti abbiamo inviato. Se non hai ricevuto nessuna email, <a href=\\\"index.php?act=register&code=sendval\\\">clicca qui</a>.',
'login_must_val_admin' => 'Spiacente, devi aspettare che un amministratore approvi manualmente il tuo account. Riceverai una email quando il tuo account sar&agrave; attivato completamente.',
'login_no_pass' => 'Spiacente, la password non &egrave; corretta. <a href=\\\"index.php?act=register&code=forgot\\\">Hai dimenticato la password?</a>',
'login_no_user' => 'Non abbiamo trovato un utente con lo username indicato.',
'logout_no_key' => 'Chiave di logout non valida.',
'must_be_guest' => 'Spiacente, solo gli utenti senza profilo possono accedere a questa pagina.',
'must_be_user' => 'Spiacente, devi essere un utente con profilo completo per accedere a questa pagina.',
'new_tickets_disabled' => 'Spiacente, il sistema &egrave; temporaneamente disabilitato per aprire nuovi ticket.',
'news_comment_disabled' => 'Spiacente, i commenti sono stati disabilitati.',
'news_disabled' => 'Spiacente, la pagina delle novit&agrave; &egrave; stata disabilitata.',
'no_announcement' => 'Spiacente, il comunicato che stavi cercando non &egrave; stato trovato.',
'no_article' => 'Spiacente, l\'articolo che stavi cercando non &egrave; stato trovato.',
'no_category' => 'Spiacente, la categoria che stavi cercando non &egrave; stata trovata.',
'no_comment' => 'Spiacente, il commento che stavi cercando non &egrave; stato trovato.',
'no_department' => 'Spiacente, la sezione che stavi cercando non &egrave; stata trovata.',
'no_email_val_key' => 'Spiacente, non abbiamo trovato la tua chiave di validazione.',
'no_member' => 'Spiacente, l\'Utente che stavi cercando non &egrave; stato trovato.',
'no_page' => 'Spiacente, la pagina che stavi cercando non &egrave; stata trovata.',
'no_pass_match' => 'Le tue password non corrispondono.',
'no_perm_access' => 'Spiacente, non hai i permessi sufficienti per accedere a questa area.',
'no_perm_banned' => 'Spiacente, non hai i permessi sufficienti per accedere al sistema.',
'no_perm_com_delete' => 'Spiacente, non hai i permessi sufficienti per cancellare questo commento.',
'no_perm_com_edit' => 'Spiacente, non hai i permessi sufficienti per modificare questo commento.',
'no_perm_reply_delete' => 'Spiacente, non hai i permessi sufficienti per cancellare questa risposta.',
'no_perm_reply_edit' => 'Spiacente, non hai i permessi sufficienti per modificare questa risposta.',
'no_perm_ticket_edit' => 'Spiacente, non hai i permessi sufficienti per modificare questo ticket.',
'no_reply' => 'Spiacente, la risposta che stavi cercando non &egrave; stata trovata.',
'no_staff_rate_reply' => 'Spiacente, solo le risposte dello staff possono essere valutate.',
'no_ticket' => 'Spiacente, il ticket che stavi cercando non &egrave; stato trovato.',
'no_ticket_guest' => 'Spiacente, non &egrave; stato trovato alcun ticket corrispondente all\'indirizzo email e alla chiave del ticket.',
'no_valid_email' => 'Per favore, inserisci un indirizzo email valido.',
'no_valid_tkey' => 'Per favore, inserisci una chiave del ticket valida.',
'registration_disabled' => 'Spiacente, il sistema non accetta (temporaneamente) nuove registrazioni.',
'reply_rating_disabled' => 'Spiacete, la valutazione &egrave; stata disabilitata.',
'ticket_closed_already' => 'Spiacente, il ticket &egrave; gi&agrave; stato chiuso.',
'ticket_closed_escalate' => 'Spiacente, il ticket &egrave; chiuso quindi non puoi promuoverlo.',
'ticket_closed_reply' => 'Spiacente, il ticket &egrave; chiuso quindi non puoi rispondere.',
'ticket_escalate_perm' => 'Spiacente, non hai i permessi sufficienti per promuovere questo ticket.',
'ticket_escalated_already' => 'Spiacente, ma il ticket &egrave; gi&agrave; stato promosso.',
'ticket_no_close_perm' => 'Spiacente, non hai i permessi sufficienti per chiudere il ticket.',
'token_mismatch' => 'Il token del tuo form non pu&ograve; essere verificato.',
'try_again' => 'Per favore, torna indietro e prova ancora. Se il problema persiste, contattaci.',

);

?>