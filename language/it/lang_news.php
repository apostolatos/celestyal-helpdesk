<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | lang_news.php
#======================================================
*/

$lang = array(

'add_a_comment' => 'Aggiungi un commento',
'add_comment_button' => 'Aggiungi Commento',
'announcement' => 'Comunicato',
'comments' => 'Commenti',
'confirm_delete' => 'Vuoi davvero cancellare questo commento?',
'delete' => 'Cancella',
'edit' => 'Modifica',
'edit_comment' => 'Modifica il commento',
'edit_comment_button' => 'Modifica Commento',
'err_no_comment' => 'Per favore, inserisci un commento.',
'no_comments' => 'Non ci sono ancora commenti per questo annuncio.',
'no_news' => 'Non ci sono comunicati da visualizzare.',
'viewing_announcement' => 'Si sta visualizzando l\'annuncio',

);

?>