<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | lang_redirect.php
#======================================================
*/

$lang = array(

'add_rating_success' => 'La tua valutazione &egrave; stata aggiunta con successo.',
'almost_acc_activate' => 'Il tuo indirizzo email &egrave; stato verificato con successo. Un amministratore deve approvare manualmente il tuo account prima che tu possa iniziare ad utilizzarlo. Quando il tuo account sar&agrave; attivo, riceverai una email.',
'change_val_email' => 'Prima che tu possa modificare la tua email, devi prima cliccare sul link di attivazione contenuto nella email inviata al tuo indirizzo.',
'click_here' => 'Clicca qui',
'delete_comment_success' => 'Il commento &egrave; stato cancellato con successo.',
'edit_comment_success' => 'Il commento &egrave; stato modificato con successo.',
'login_success' => 'Hai eseguito con successo l\'operazione di login.',
'logout_success' => 'Hai eseguito con successo l\'operazione di logout.',
'new_user_no_val' => 'Il tuo account &egrave; stato creato con successo. Ora puoi accedere al sistema tramite il login.',
'new_user_val_admin' => 'Un amministratore deve approvare manualmente il tuo account prima che tu possa iniziare ad utilizzarlo. Quando il tuo account sar&agrave; attivato, riceverai una email.',
'new_user_val_both' => 'Prima che tu possa iniziare ad utilizzare il tuo account, un amministratore deve approvarlo manualmente E devi cliccare sul link di attivazione contenuto nella email inviata al tuo indirizzo.',
'new_user_val_email' => 'Prima che tu possa iniziare ad utilizzare il tuo account, devi prima cliccare sul link di attivazione contenuto nella email inviata al tuo indirizzo.',
'new_user_val_resend' => 'Una nuova mail di attivazione &egrave; stata inviata con successo al tuo indirizzo.',
'please_wait' => 'Per favore, attendi',
'reply_delete_success' => 'La risposta &egrave; stata cancellata con successo.',
'reply_edit_success' => 'La risposta &egrave; stata modificata con successo.',
'reset_pass_email_sent' => 'Per resettare la tua password, clicca sul link contenuto nella email che ti &egrave; stata inviata al tuo indirizzo.',
'reset_pass_success' => 'La tua password &egrave; stata resettata con successo. Ora puoi eseguire il login con la nuova password.',
'submit_comment_success' => 'Il tuo commento &egrave; stato aggiunto con successo.',
'submit_reply_success' => 'La tua risposta &egrave; stata aggiunta con successo.',
'submit_ticket_success' => 'Il tuo ticket &egrave; stato inviato con successo.',
'success_acc_activate' => 'Il tuo account &egrave; stato attivato con successo. Ora puoi accedere al sistema tramite il login.',
'thank_you' => 'Grazie.',
'ticket_close_success' => 'Il tuo ticket &egrave; stato chiuso con successo.',
'ticket_edit_success' => 'Il ticket &egrave; stato aggiornato con successo.',
'ticket_escalate_success' => 'Il tuo ticket &egrave; stato promosso con successo.',
'transfer_you' => 'Per favore, attendi mentre ti trasferiamo. Se non vuoi aspettare,',
'update_my_account' => 'Il tuo account &egrave; stato aggiornato con successo.',
'update_my_email' => 'Il tuo indirizzo email &egrave; stato aggiornato con successo.',
'update_my_pass' => 'La tua password &egrave; stata aggiornata con successo. A seconda delle tue impostazioni dei cookies, potresti dover eseguire nuovamente l\'operazione di login.',

);

?>