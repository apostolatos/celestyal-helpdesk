<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | lang_register.php
#======================================================
*/

$lang = array(

'captcha' => 'Antispam',
'create_account_button' => 'Crea un account',
'email_address' => 'Indirizzo email',
'err_captcha_mismatch' => 'Il codice antispam che hai inserito non corrisponde all\'immagine. Per favore, prova ancora.',
'err_email_in_use' => 'Spiacente, questo indirizzo email &egrave; gi&agrave; in uso. Per favore, scegli un altro indirizzo email.',
'err_no_cpfield' => 'Per favore, inserisci un valore per il campo:',
'err_no_email_valid' => 'Devi inserire un indirizzo email valido.',
'err_no_pass_match' => 'Le tue password non coincidono.',
'err_no_pass_short' => 'Per favore, inserisci una password.',
'err_no_user_or_email' => 'Devi inserire uno username o un indirizzo email.',
'err_no_user_short' => 'Per favore, inserisci uno username.',
'err_user_already_active' => 'Il tuo account &egrave; gi&agrave; stato validato. Prova ad eseguire il login. :)',
'err_user_in_use' => 'Spiacente, questo username &egrave; gi&agrave; in uso. Per favore, scegli un altro username.',
'err_user_not_found' => 'Non abbiamo trovato alcun membro con le informazioni di account fornite.',
'forgot_password' => 'Password dimenticata',
'new_password' => 'Nuova password',
'new_password_confirm' => 'Conferma la nuova password',
'optional' => '(Opzionale)',
'or' => 'o',
'password_confirm' => 'Conferma la password',
'register_new_account' => 'Registra un nuovo account',
'resend_val_button' => 'Reinvia l\'email di validazione',
'reset_pass_button' => 'Resetta Password',
'reset_password' => 'Resetta la password',
'upgrade_account_button' => 'Upgrade Account',
'upgrade_msg' => 'Dato che sei gi&agrave; loggato come utente senza profilo, puoi eseguire l\'upgrade del tuo account da account senza profilo ad account completamente registrato mantenendo i tuoi ticket. Per eseguire l\'upgrade del tuo account, completa il form sottostante e premi Upgrade Account.',

);

?>