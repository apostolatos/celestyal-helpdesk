<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | lang_tickets.php
#======================================================
*/

$lang = array(

'article_suggestions' => 'Suggerimento articoli',
'attachment' => '(Allegato)',
'attachment_b' => 'Allegato',
'attachment_max_size' => 'Dimensione max allegato:',
'captcha' => 'Antispam',
'close' => 'Chiudi',
'close_msg_a' => 'Questo ticket &egrave; stato chiuso da',
'close_msg_b' => 'per la seguente ragione.',
'close_ticket' => 'Chiudi il ticket',
'close_ticket_button' => 'Chiudi Ticket',
'closing_ticket' => 'Ticket in chiusura',
'confirm_close' => 'Sei sicuro di voler chiudere questo ticket?',
'confirm_delete_reply' => 'Sei sicuro di voler cancellare questa risposta?',
'confirm_escalate' => 'Sei sicuro che vuoi promuovere questo ticket?',
'continue_ticket_submit' => 'Continua con l\'invio del ticket',
'download_attachment' => 'Scarica allegato:',
'edit' => 'Modifica',
'edit_reply' => 'Modifica la risposta',
'edit_reply_button' => 'Modifica risposta',
'edit_ticket' => 'Modifica il ticket',
'edit_ticket_button' => 'Modifica Ticket',
'email' => 'Email',
'email_address' => 'Indirizzo email',
'enter_close_reason' => 'Per favore, inserisci una motivazione per la chiusura di questo ticket.',
'err_captcha_mismatch' => 'Il codice antispam che hai inserito non corrisponde all\'immagine. Per favore, prova ancora.',
'err_email_in_use' => 'Questo indirizzo email &egrave; gi&agrave; utilizzato da uno dei nostri membri. Per favore, utilzza un altro indirizzo email o oppure esegui il login.',
'err_no_cdfield' => 'Per favore, inserisci un valore per il campo:',
'err_no_depart' => 'Per favore, scegli una sezione.',
'err_no_email' => 'Per favore, inserisci un indirizzo email valido.',
'err_no_message' => 'Per favore, inserisci un messaggio.',
'err_no_name' => 'Per favore, inserisci un nome.',
'err_no_reason' => 'Per favore, inserisci una motivazione.',
'err_no_reply' => 'Per favore, inserisci una risposta.',
'err_no_subject' => 'Per favore, inserisci un argomento.',
'err_upload_bad_type' => 'Il tipo del file che stai cercando di caricare non &egrave; accettato dal sistema.',
'err_upload_failed' => 'Caricamento del file fallito. Per favore, prova ancora.',
'err_upload_too_big' => 'Il file che stai cercando di caricare eccede la dimensione massima di caricamento consentita.',
'escalate' => 'Promuovi',
'guest_login' => 'Login come Utente senza profilo',
'guest_login_info' => 'Benvenuto Utente senza profilo. Per favore, esegui il login usando il tuo indirizzo email e la chiave del ticket sottostante per visualizzare il ticket associato . Se sei gi&agrave; un utente registrato, per favore esegui l\'operazione di login dal pannello apposito.',
'guest_ticket_notification' => 'Notificami via email le risposte da parte dello staff',
'history' => 'Storico',
'last_replier' => 'Ultimo utente che ha risposto',
'last_reply' => 'Ultima risposta',
'name' => 'Nome',
'no_replies' => 'Spiacente, non ci sono risposte a questo ticket.',
'no_suggestions_helped' => 'Nessuno dei suggerimenti proposti mi &egrave; servito per risolvere il problema.',
'open_ticket_button' => 'Invia Ticket',
'optional' => '(Opzionale)',
'relevance' => 'Rilevanza',
'replies' => 'Risposte',
'select_depart' => 'Per favore, scegli una sezione.',
'send_reply' => 'Invia una risposta',
'send_reply_button' => 'Invia Risposta',
'suggestions_explained' => 'Di seguito ti vengono proposti degli articoli della Knowledge Base che potrebbero rispondere alla tua richiesta di supporto. Per favore, esamina gli articoli e verifica che non sono di aiuto per il tuo problema, prima di procedere con l\'invio di un ticket. <i>Il tuo ticket non &egrave; ancora stato inviato.</i>',
'thumbs_down' => 'Pollice gi&ugrave;',
'thumbs_up' => 'Pollice su',
'ticket_center_overview' => 'Panoramica del Centro Ticket',
'ticket_id' => 'Ticket ID',
'ticket_key' => 'Chiave del ticket',
'viewing_ticket' => 'Ticket in visualizzazione',

);

?>