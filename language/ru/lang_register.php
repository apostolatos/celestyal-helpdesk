<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | lang_register.php
#======================================================
*/

$lang = array(

'captcha' => 'Captcha',
'create_account_button' => 'Создать аккаунт',
'email_address' => 'Email адресс',
'err_captcha_mismatch' => 'The Captcha code you entered did not match the image. Please try again.',
'err_email_in_use' => 'Sorry, that email is already in use. Please choose another email.',
'err_no_cpfield' => 'Please enter a value for the field:',
'err_no_email_valid' => 'You must enter a valid email address.',
'err_no_pass_match' => 'Your passwords do not match.',
'err_no_pass_short' => 'Пожайлуста, введите пароль.',
'err_no_user_or_email' => 'You must enter a username or email address.',
'err_no_user_short' => 'Пожайлуста, введите имя пользователя.',
'err_user_already_active' => 'Your account has already been validated. Try logging in. :)',
'err_user_in_use' => 'Sorry, that username is already in use. Please choose another username.',
'err_user_not_found' => 'К сожалению такой пользовалель в базе не не регестрировался.',
'forgot_password' => 'Напомнить пароль',
'new_password' => 'Новый пароль',
'new_password_confirm' => 'Новый пароль, ещё раз',
'optional' => '(Дополнительно)',
'or' => 'или',
'password_confirm' => 'Пароль ещё раз',
'register_new_account' => 'Register A New Account',
'resend_val_button' => 'Выслать повторно',
'reset_pass_button' => 'Сбросить пароль',
'reset_password' => 'Сбросить пароль',
'upgrade_account_button' => 'Обновить Аккаунт',
'upgrade_msg' => 'Because you are already logged in as a guest, you can upgrade your account to a fully registered account while keeping all your current tickets. To upgrade your account, simply fill out the form below and click Upgrade Account.',

);

?>