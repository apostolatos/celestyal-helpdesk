<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | ad_lang_error.php
#======================================================
*/

$lang = array(

'error' => 'Hata',
'error_has_occured' => 'Hata! Bir hata oluştu.',
'fill_form_completely' => 'Göndermeden önce formu tamamen doldurmanız gerekiyor.',
'fill_form_lengths' => 'Bir veya daha fazla alanda gerekli olan uzunluktan daha kısa veri girdiniz.',
'login_no_admin' => 'Üzgünüz, yönetici paneline erişim yetkiniz yok.',
'login_no_pass' => 'Üzgünüz, şifre hatalı.',
'login_no_user' => 'Belirttiğiniz kullanıcı adı sistemimizde bulunamadı.',
'must_login' => 'Yönetici paneline erişmek için yönetici girişi yapmalısınız.',
'news_disabled' => 'Duyuru sistemi devre dışı. Duyuru ayarlarınızı değiştirmek için <a href=\'<! HD_URL !>/admin.php?section=manage&amp;act=settings&amp;code=find&amp;group=news\'>buraya tıklayınız</a>.',
'no_article' => 'Üzgünüz, istediğiniz yazı bulunamadı.',
'no_articles_found' => 'Üzgünüz, arama kriterlerinize uygun yazı bulunamadı.',
'no_attachment' => 'Üzgünüz, istediğiniz ek dosya bulunamadı.',
'no_canned' => 'Üzgünüz, istediğiniz hazır cevap bulunamadı.',
'no_category' => 'Üzgünüz, istediğiniz kategori bulunamadı.',
'no_create_lang' => 'Üzgünüz, dil dosyaları oluşturulamadı. \'languages\' klasörünü CHMOD yaparak 0777 veriniz.',
'no_delete_default_lang' => 'Üzgünüz, varsayılan dil silinemez.',
'no_delete_default_skin' => 'Üzgünüz, varsayılan tema silinemez.',
'no_department' => 'Üzgünüz, istediğiniz departman bulunamadı.',
'no_dfield' => 'Üzgünüz, istediğiniz özel departman alanı bulunamadı.',
'no_export_templates' => 'Üzgünüz, tema paketi oluşturulamadı, çünkü hiç şablon yok.',
'no_group' => 'Üzgünüz, istediğiniz grup bulunamadı.',
'no_lang' => 'Üzgünüz, istediğiniz dil bulunamadı.',
'no_member' => 'Üzgünüz, istediğiniz üye bulunamadı.',
'no_members_found' => 'Üzgünüz, arama kriterlerinize uygun üye bulunamadı.',
'no_message' => 'Lütfen bir mesaj giriniz.',
'no_mm_tickets' => 'Üzgünüz, hiç sorun bildirimi seçmediniz.',
'no_mm_valid_tickets' => 'Üzgünüz, seçtiğiniz sorun bildirimleri bu işlem için uygun değil.',
'no_open_file' => 'Üzgünüz, dosya açılamadı.',
'no_page' => 'Üzgünüz, istediğiniz sayfa bulunamadı.',
'no_perm' => 'Üzgünüz, bu alana erişim yetkiniz yok.',
'no_perm_banned' => 'Üzgünüz, bu destek sistemine erişim yetkiniz yok.',
'no_pfield' => 'Üzgünüz, özel profil alanı bulunamadı.',
'no_reply' => 'Üzgünüz, istediğiniz cevap bulunamadı.',
'no_settings_found' => 'Üzgünüz, istediğiniz ayarlar bulunamadı.',
'no_skin' => 'Üzgünüz, istediğiniz tema bulunamadı.',
'no_subject' => 'Lütfen bir cevap giriniz.',
'no_template' => 'Üzgünüz, istediğiniz şablon bulunamadı.',
'no_ticket' => 'Üzgünüz, istediğiniz sorun bildirimi bulunamadı.',
'no_upload_lang_xml' => 'Üzgünüz, yüklediğiniz dosya geçerli bir Trellis Desk XML Dil Dosyası değil.',
'no_upload_move' => 'Üzgünüz, dosya yüklenemiyor. \'tmp\' klasörüne CHMOD 0777 yaparak yazma hakkı veriniz.',
'no_upload_size' => 'Üzgünüz, yüklediğiniz dosya boş.',
'no_upload_skin_xml' => 'Üzgünüz, yüklediğiniz dosya geçerli bir Trellis Desk XML Tema dosyası değil.',
'not_writable' => 'Üzgünüz, düzenlemek istediğiniz dosyaya yazma hakkı yok. CHMOD 0777 yaparak yazma hakkı veriniz.',
'not_writable_img_dir' => 'Üzgünüz, tema oluşturulamıyor. ./images/ klasörüne CHMOD 0777 yaparak yazma hakkı veriniz.',
'not_writable_skin' => 'Üzgünüz, tema oluşturulamıyor. ./skin/ klasörüne CHMOD 0777 yaparak yazma hakkı veriniz.',
'prune_no_days' => 'Üzgünüz, gün toplamı belirtmediniz.',
'root_edit_mem' => 'Sadece yönetici bu üyenin bilgilerini düzenleyebilir.',
'ticket_closed_already' => 'Üzgünüz, sorun bildirimi zaten kapatılmış.',
'ticket_closed_escalate' => 'Üzgünüz, sorun bildirimi kapalı olduğundan yükseltme yapılamaz.',
'ticket_closed_hold' => 'Üzgünüz, sorun bildirimi kapalı olduğundan askıya alınamaz.',
'ticket_closed_reply' => 'Üzgünüz, sorun bildirimi kapalı olduğundan cevaplanamaz.',
'ticket_escalated_already' => 'Üzgünüz, bu sorun bildirimi zaten yükseltilmiş.',
'ticket_hold_already' => 'Üzgünüz, bu sorun bildirimi zaten yükseltilmiş.',
'ticket_reopen_already' => 'Üzgünüz, bu sorun bildirimi zaten açık.',
'try_again' => 'Lütfen geri giderek tekrar deneyiniz. Eğer problem çözülmezse yönetici ile iletişime geçiniz.',
'upload_bad_type' => 'Yüklemek istediğiniz dosya türü sistem tarafından kabul edilmiyor.',
'upload_failed' => 'Dosya yüklenemedi. Lütfen tekrar deneyiniz.',
'upload_too_big' => 'Dosya boyutu izin verilen en yüksek boyuttan büyük.',

);

?>