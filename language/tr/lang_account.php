<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | lang_account.php
#======================================================
*/

$lang = array(

'account_overview' => 'Hesap Görünümü',
'account_settings' => 'Hesap Ayarları',
'change_email' => 'Email Değiştir',
'change_email_address' => 'Email Adresi Değiştir',
'change_email_button' => 'Email Değiştir',
'change_pass_button' => 'Şifre Değiştir',
'change_password' => 'Şifre Değiştir',
'current_password' => 'Eski Şifre',
'custom_profile_fields' => 'Ek Profil Alanları',
'disabled' => 'Kapalı',
'dst_active' => 'DST Aktif',
'email' => 'Email',
'email_notifications' => 'Email Bildirimleri',
'email_preferences' => 'Email Tercihleri,',
'email_staff_new_ticket' => 'Departmanımdaki Yeni Görevler',
'email_staff_ticket_reply' => 'Departmanımdaki Yeni Cevaplar',
'email_type' => 'Email Tipi',
'enabled' => 'Açık',
'err_login_no_pass' => 'Eski şifreniz hatalı.',
'err_no_cpfield' => 'Bu alan için bir değer girmelisiniz :',
'err_no_email_change' => 'Bu e-mail adresi eski e-mail adresinizle aynı.',
'err_no_email_match' => 'E-Mail adresleriniz eşleşmiyor.',
'err_no_email_valid' => 'Geçerli bir e-mail adresi giriniz.',
'err_no_new_pass_short' => 'Yeni bir şifre giriniz.',
'err_no_pass_match' => 'Yeni şifreleriniz eşleşmiyor.',
'err_no_pass_short' => 'Eski şifrenizi giriniz.',
'general_info' => 'Genel Bilgi',
'group' => 'Grup',
'html' => 'HTML',
'joined' => 'Birleştirilmiş',
'language' => 'Dil',
'last_visit' => 'Son Ziyaret',
'modify_account' => 'Hesap Güncelle',
'modify_account_information' => 'Hesap Bilgisi Güncelle',
'new_email_address' => 'Yeni E-Mail Adresi',
'new_email_address_confirm' => 'Yeni E-Mail Adresini Onayla',
'new_password' => 'Yeni Şifre',
'new_password_confirm' => 'Yeni Şifreyi Onayla',
'new_reply' => 'Yeni Cevap',
'new_ticket' => 'Yeni Sorun Bildirimi',
'notifications_for' => 'Bildiriler',
'optional' => '(Opsiyonel)',
'plain_text' => 'Düz Metin',
'rich_text_editor' => 'Zengin Metin',
'skin' => 'Görünüm Şablonu',
'time_is_now' => 'Saat :',
'time_zone' => 'Zaman Dilimi',
'title' => 'Başlık',
'update_account' => 'Hesabı Güncelle',

);

?>