<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | lang_article.php
#======================================================
*/

$lang = array(

'add_a_comment' => 'Yorum Ekle',
'add_comment_button' => 'Yorum Ekle',
'categories' => 'Kategoriler',
'confirm_delete' => 'Bu yorumu silmek istediğinizden emin misiniz ?',
'delete' => 'Sil',
'edit' => 'Düzelt',
'edit_comment' => 'Yorum Düzelt',
'edit_comment_button' => 'Yorum Düzelt',
'err_no_comment' => 'Lütfen yorumunuzu yazınız.',
'full_star' => 'Tam Yıldız',
'half_star' => 'Yarım Yıldız',
'keywords_phrase' => 'Anahtar kelime ve deyimleri giriniz',
'no_articles' => 'Gösterilecek yazı yok.',
'no_articles_found' => 'Yazı bulunamadı.',
'no_comments' => 'Bu yazıya yorum eklenmedi.',
'no_star' => 'Yıldızsız',
'relevance' => 'İlişki',
'search_results' => 'Arama Sonuçları',
'stars' => 'Yıldız',
'view_category' => 'Kategori Göster',
'viewing_article' => 'Yazı',

);

?>