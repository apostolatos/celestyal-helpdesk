<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | lang_email_content.php
#======================================================
*/

$lang['header'] = <<<EOF
<div style="font-family:verdana,tahoma,helvetica,times;font-size:12px;">
<table cellpadding="10" cellspacing="1" border="0">
<tr>
	<td style="font-size:12px;">
		<b>Sayın <#MEM_NAME#>,</b>
	</td>
</tr>
<tr>
	<td style="font-size:12px;">
EOF;

$lang['footer'] = <<<EOF
	</td>
</tr>
<tr>
	<td style="font-size:12px;">
	İyi Çalışmalar,

	<#HD_NAME#>
	<a href="<#HD_URL#>"><#HD_URL#></a>
	</td>
</tr>
<tr>
	<td>
		<table cellpadding="5" cellspacing="1" border="0" width="500">
			<tr><td style="font-size:16px;color:white;" colspan="10" bgcolor="gray"><b>Boss Yazılım</b></td></tr>
			<tr><td style="font-size:12px;" nowrap>Adres	</td><td style="font-size:12px;">:</td><td style="font-size:12px;">Şirinyalı Mah. Eski Lara Cad. Villa Murat 191/2 / Antalya          </td></tr>
			<tr><td style="font-size:12px;" nowrap>Web		</td><td style="font-size:12px;">:</td><td style="font-size:12px;"><a href="http://www.bossyazilim.com">http://www.bossyazilim.com</a></td></tr>
			<tr><td style="font-size:12px;" nowrap>E-Mail	</td><td style="font-size:12px;">:</td><td style="font-size:12px;"><a href="mailto:info@bossyazilim.com">info@bossyazilim.com</a>     </td></tr>
			<tr><td style="font-size:12px;" nowrap>Telefon </td><td style="font-size:12px;">:</td><td style="font-size:12px;">+90 242 316 1055                                                   </td></tr>
			<tr><td style="font-size:12px;" nowrap>Faks	</td><td style="font-size:12px;">:</td><td style="font-size:12px;">+90 242 316 1058</td></tr>
		</table>
	</td>
</tr>
</table>
</div>
EOF;

$lang['change_email_val_sub'] = "Mail Adresi Doğrulama";

$lang['change_email_val'] = <<<EOF
E-mail adresinizin değiştirilmesini istediniz.<br>
Lütfen aşağıdaki bağlantıya tıklayarak onay işlemini tamamlayınız.<br>
<br>
---------------------------<br>
<br>
<#VAL_LINK#><br>
<br>
---------------------------<br>
EOF;

$lang['new_user_val_email_sub'] = "Hesap Doğrulama";

$lang['new_user_val_email'] = <<<EOF
<#HD_NAME#><br>
<br>
Hoşgeldiniz,<br>
<br>
Destek sistemimizde yeni üye hesabınız açıldı.  <br>
Hesabınızı aktif hale getirmek için aşağıdaki bağlantıya tıklayıp e-mail adresinizi doğrulamanız gerekiyor.<br>
<br>
---------------------------<br>
<br>
Kullanıcı Adı	: <#USER_NAME#><br>
Onay Bağlantısı : <a href='<#VAL_LINK#>'><#VAL_LINK#></a><br>
<br>
---------------------------
EOF;

$lang['new_user_val_both_sub'] = "Hesap Doğrulama";

$lang['new_user_val_both'] = <<<EOF
Destek sistemimizde yeni üye hesabınız açıldı.  <br>
Hesabınızı aktif hale getirmek için aşağıdaki bağlantıya tıklayıp e-mail adresinizi doğrulamanız gerekiyor.<br>
<br>
Doğrulama işlemini sistem yöneticisi de yapabilir. Yönetici doğrulama yaparsa size bu bir e-mail mesajıyla bildirilecektir. Bu durumda sizin yapmanıza gerek kalmaz.<br>
<br>
---------------------------<br>
<br>
Kullanıcı Adı	: <#USER_NAME#><br>
Onay Bağlantısı : <#VAL_LINK#><br>
<br>
---------------------------<br>
<br>
EOF;

$lang['new_user_val_admin_sub'] = "Yeni Hesap Eklendi";

$lang['new_user_val_admin'] = <<<EOF
Destek sistemimizde yeni bir kullanıcı hesabı açtınız.<br>
Hesabınızı kullanmaya başlayabilmeniz için hesabınızın yönetici tarafından onaylanması gerekmektedir.<br>
Yönetici tarafından onaylama yapıldığında size bir mesaj gönderilecektir.<br>
<br>
EOF;

$lang['acc_accivated_sub'] = "Hesap aktifleştirildi";

$lang['acc_accivated'] = <<<EOF
Hesabınız aktifleştirildi. Artık hesabınızı kullanarak sistemimize giriş yapabilirsiniz.<br>
EOF;

$lang['acc_almost_accivated_sub'] = "Account Waiting Approval";

$lang['acc_almost_accivated'] = <<<EOF
E-mail adresiniz onaylandı.  <br>
Hesabınızı kullanmaya başlayabilmeniz için hesabınızın yönetici tarafından onaylanması gerekmektedir.<br>
Yönetici tarafından onaylama yapıldığında size bir mesaj gönderilecektir.<br>
EOF;

$lang['acc_approved_sub'] = "Account Approved";

$lang['acc_approved'] = <<<EOF
Hesabınız yönetici tarafından onaylandı.  You may now login.<br>
EOF;

$lang['acc_almost_approved_sub'] = "Account Waiting Email Validation";

$lang['acc_almost_approved'] = <<<EOF
Hesabınız yönetici tarafından onaylandı. <br>
Hesabınızı kullanmaya başlamak için e-mail adresinize gönderilen mesajın içindeki onay bağlantısına tıklamanız gerekmektedir.<br>
EOF;

$lang['new_ticket_sub'] = "Ticket ID #<#TICKET_ID#>";

$lang['new_ticket'] = <<<EOF
Yeni bir sorun bildirimi eklediniz. Görevli personelimiz bu bildirimi inceleyip en kısa zamanda cevaplandıracaktır. <br>
Bildirim detayları aşağıdadır :<br>
<br>
---------------------------<br>
<br>
Sorun Bildirim No	:  <#TICKET_ID#><br>
Konu				:  <#SUBJECT#><br>
Departman			:  <#DEPARTMENT#><br>
Önem				:  <#PRIORITY#><br>
Eklendiği Tarih		:  <#SUB_DATE#><br>
<br>
---------------------------<br>
<br>
Sorun bildiriminize bu adresten ulaşabilirsiniz : <#TICKET_LINK#>
EOF;

$lang['staff_new_ticket_sub'] = "Ticket ID #<#TICKET_ID#>";

$lang['staff_new_ticket'] = <<<EOF
Bağlı olduğunuz departmanda bir sorun bildirildi.  Sorun bildirim detayları aşağıdadır.<br>
<br>
---------------------------<br>
<br>
Sorun Bildirim No	:  <#TICKET_ID#><br>
Üye					:  <#MEMBER#><br>
Konu				:  <#SUBJECT#><br>
Departman			:  <#DEPARTMENT#><br>
Önem				:  <#PRIORITY#><br>
Eklendiği Tarih		:  <#SUB_DATE#><br>
<br>
---------------------------<br>
<br>
<#MESSAGE#><br>
<br>
---------------------------<br>
<br>
Bu sorun bildirimine direk erişmek için : <#TICKET_LINK#>
EOF;

$lang['new_guest_ticket_sub'] = "Ticket ID #<#TICKET_ID#>";

$lang['new_guest_ticket'] = <<<EOF
Yeni bir sorun bildirimi eklediniz. Görevli personelimiz en kısa zamanda bu bildiriminize cevap verecektir. Sorun bildirim detayları aşağıdadır.<br>
<br>
---------------------------<br>
<br>
Sorun Bildirim No	:  <#TICKET_ID#><br>
Konu				:  <#SUBJECT#><br>
Departman			:  <#DEPARTMENT#><br>
Önem				:  <#PRIORITY#><br>
Eklendiği Tarih		:  <#SUB_DATE#><br>
<br>
Sorun Bildirim Anahtarı :  <#TICKET_KEY#><br>
<br>
---------------------------<br>
<br>
Bu sorun bildirimine direk erişmek için : <#TICKET_LINK#>
EOF;

$lang['staff_new_guest_ticket_sub'] = "Ticket ID #<#TICKET_ID#>";

$lang['staff_new_guest_ticket'] = <<<EOF
Bir ziyaretçi sizin departmanınızda sorun bildirdi. Sorun bildirim detayları aşağıdadır.<br>
<br>
---------------------------<br>
<br>
Sorun Bildirim No	:  <#TICKET_ID#><br>
Üye					:  <#MEMBER#> (Guest)<br>
Konu				:  <#SUBJECT#><br>
Departman			:  <#DEPARTMENT#><br>
Önem				:  <#PRIORITY#><br>
Eklendiği Tarih		:  <#SUB_DATE#><br>
<br>
---------------------------<br>
<br>
<#MESSAGE#><br>
<br>
---------------------------<br>
<br>
Bu sorun bildirimine direk erişmek için : <#TICKET_LINK#>
EOF;

$lang['ticket_escl_sub'] = "Ticket ID #<#TICKET_ID#> Escalated";

$lang['ticket_escl'] = <<<EOF
Sorun bildiriminiz bir üst kademeye gönderildi. Görevli arkadaşlarımız sorun bildiriminizi inceleyerek en kısa zamanda size cevap vereceklerdir.  Sorun bildirim detayları aşağıdadır.<br>
<br>
---------------------------<br>
<br>
Sorun Bildirim No	:  <#TICKET_ID#><br>
Konu				:  <#SUBJECT#><br>
Departman			:  <#DEPARTMENT#><br>
Önem				:  <#PRIORITY#><br>
Eklendiği Tarih		:  <#SUB_DATE#><br>
<br>
---------------------------<br>
<br>
Bu sorun bildirimine direk erişmek için : <#TICKET_LINK#>
EOF;

$lang['ticket_close_sub'] = "Ticket ID #<#TICKET_ID#> Closed";

$lang['ticket_close'] = <<<EOF
Sorun bildiriminiz kapatıldı. Sorun bildirim detayları aşağıdadır.<br>
<br>
---------------------------<br>
<br>
Sorun Bildirim No	:  <#TICKET_ID#><br>
Konu				:  <#SUBJECT#><br>
Departman			:  <#DEPARTMENT#><br>
Önem				:  <#PRIORITY#><br>
Eklendiği Tarih		:  <#SUB_DATE#><br>
<br>
---------------------------<br>
<br>
Bu sorun bildirimine direk erişmek için : <#TICKET_LINK#><br>
<br>
Yardımcı olabileceğimiz başka bir sorunuz veya sorununuz olursa lütfen iletiniz.
EOF;

$lang['ticket_move_sub'] = "Ticket ID #<#TICKET_ID#> Moved";

$lang['ticket_move'] = <<<EOF
Sorun bildiriminiz başka bir departmana kaydırıldı.  Sorun bildirim detayları aşağıdadır.<br>
<br>
---------------------------<br>
<br>
Sorun Bildirim No	:  <#TICKET_ID#><br>
Konu				:  <#SUBJECT#><br>
Eski Departman		:  <#OLD_DEPARTMENT#><br>
Yeni Departman		:  <#NEW_DEPARTMENT#><br>
Önem				:  <#PRIORITY#><br>
Eklendiği Tarih		:  <#SUB_DATE#><br>
<br>
---------------------------<br>
<br>
Bu sorun bildirimine direk erişmek için : <#TICKET_LINK#><br>
<br>
If there is anything we can do to be of assistance, please let us know.
EOF;

$lang['ticket_reply_sub'] = "Ticket ID #<#TICKET_ID#> Reply";

$lang['ticket_reply'] = <<<EOF
A reply has been made to your ticket.  Sorun bildirim detayları aşağıdadır.<br>
<br>
---------------------------<br>
<br>
<#REPLY#><br>
<br>
---------------------------<br>
<br>
Sorun Bildirim No	:  <#TICKET_ID#><br>
Konu				:  <#SUBJECT#><br>
Departman			:  <#DEPARTMENT#><br>
Önem				:  <#PRIORITY#><br>
Eklendiği Tarih		:  <#SUB_DATE#><br>
<br>
---------------------------<br>
<br>
Bu sorun bildirimine direk erişmek için : <#TICKET_LINK#><br>
<br>
Yardımcı olabileceğimiz başka bir sorunuz veya sorununuz olursa lütfen iletiniz.
EOF;

$lang['staff_reply_ticket_sub'] = "Ticket ID #<#TICKET_ID#> Reply";

$lang['staff_reply_ticket'] = <<<EOF
bağlı olduğunuz departmanda bulunan bir sorun bildirimine cevap verildi. Sorun bildirim detayları aşağıdadır.<br>
<br>
---------------------------<br>
<br>
<#REPLY#><br>
<br>
---------------------------<br>
<br>
Sorun Bildirim No	:  <#TICKET_ID#><br>
Üye					:  <#MEMBER#><br>
Konu				:  <#SUBJECT#><br>
Departman			:  <#DEPARTMENT#><br>
Önem				:  <#PRIORITY#><br>
Eklendiği Tarih		:  <#SUB_DATE#><br>
<br>
---------------------------<br>
<br>
Bu sorun bildirimine direk erişmek için : <#TICKET_LINK#>
EOF;

$lang['announcement_sub'] = "<#TITLE#>";

$lang['announcement'] = <<<EOF
<b>'<#TITLE#>'</b> başlıklı yeni bir duyuru yapıldı.<br>
<br>
---------------------------<br>
<br>
<#CONTENT#><br>
<br>
---------------------------<br>
<br>
Bu duyuru mesajını profilinizde <b>Email Bildirimleri</b> seçeneği açık olduğu için gönderdik.<br />
Eğer bu tip mailleri almak istemiyorsanız <b>Hesabım</b> bölümüne girip sözkonusu ayarı <b>Kapalı</b> konuma getiriniz.<br />
EOF;

$lang['reset_pass_val_sub'] = "Reset Your Password";

$lang['reset_pass_val'] = <<<EOF
<#HD_NAME#> sisteminde bulunan hesabınızın şifresini değiştirmek için istek gönderdiniz. Bu işlemi tamamlamak için aşağıdaki onay bağlantısına tıklamalısınız. Eğer şifre değiştirme talebinde bulunmadıysanız bu mesajı görmezden geliniz.<br>
<br>
---------------------------<br>
Kullanıcı Adı	: <#USER_NAME#><br>
Validation Link	: <#VAL_LINK#>
EOF;

$lang['reply_pipe_closed_sub'] = "Reply Not Accepted";

$lang['reply_pipe_closed'] = <<<EOF
E-mail mesajınızı kabul edilemedi ve cevap olarak sorun bildirimine eklenemedi. Çünkü sorun bildirimi kapatılmış.<br />
<br>
---------------------------<br>
<br>
Sorun Bildirim No	:  <#TICKET_ID#><br>
Konu				:  <#SUBJECT#><br>
<br>
---------------------------<br>
<br>
Bunun bir hata olduğunu düşünüyorsanız lütfen yöneticiye bildiriniz.
EOF;

$lang['ticket_pipe_rejected_sub'] = "Ticket Not Accepted";

$lang['ticket_pipe_rejected'] = <<<EOF
E-mail mesajınızı kabul edilemedi ve yeni bir sorun bildirimi eklenemedi. Çünkü belirttiğiniz departmanda mail yoluyla sorun bildirimi eklemek için yetkili değilsiniz.<br />
<br>
Bunun bir hata olduğunu düşünüyorsanız lütfen yöneticiye bildiriniz.
EOF;

$lang['new_user_admin_val_sub'] = "New Registration: <#USER_NAME#>";

$lang['new_user_admin_val'] = <<<EOF
Yeni bir üye kaydı yapıldı ve onay bekliyor. Üye Detayları aşağıdadır.<br>
<br>
---------------------------<br>
<br>
Üye				:  <#USER_NAME#><br>
Email			: <#USER_EMAIL#><br>
Üyelik Tarihi	: <#JOIN_DATE#><br>
<br>
---------------------------<br>
<br>
Bu üye kaydını direk onaylamak için : <#APPROVE_LINK#>
EOF;
?>
