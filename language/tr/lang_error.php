<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | lang_error.php
#======================================================
*/

$lang = array(

'already_rated' => 'Üzgünüm, bu yazıyı zaten puanladınız.',
'already_rated_reply' => 'Üzgünüz, bu cevabı zaten puanladınız.',
'already_validated' => 'Hesabınız zaten aktifleştirildi.',
'banned_kb' => 'Üzgünüz, bilgi arşivine erişmek için yetkili değilsiniz.',
'banned_kb_comment' => 'Üzgünüz, yazılara yorum yapmak için yetkili değilsiniz.',
'banned_kb_rate' => 'Üzgünüz, yazıları puanlamak için yetkili değilsiniz.',
'banned_news_comment' => 'Üzgünüz, haberleri yorumlamak için yetkili değilsiniz.',
'banned_ticket' => 'Üzgünüz, sorun bildirim listesine erişmek için yetkili değilsiniz.',
'banned_ticket_escalate' => 'Üzgünüz, sorun bildirimini yükseltmek için yetkili değilsiniz.',
'banned_ticket_open' => 'Üzgünüz, yeni sorun bildirimi için yetkili değilsiniz.',
'banned_ticket_rate' => 'Üzgünüz, görevlilerin cevaplarını puanlamak için yetkili değilsiniz.',
'error' => 'Hata',
'error_has_occured' => 'Hata! Bir hata oluştu.',
'fill_form_completely' => 'Göndermeden önce bütün gerekli alanları doldurmalısınız.',
'fill_form_lengths' => 'Doldurduğunuz alanlardan bir veya birkaçı en az girilmesi gereken veri uzunluğundan az veri içeriyor.',
'invalid_rate_value' => 'Üzgünüz, 1 ile 5 arasında bir puan seçmelisiniz.',
'invalid_rate_value_reply' => 'Üzgünüz, puanı yukarı veya aşağı seçmelisiniz.',
'kb_comment_disabled' => 'Üzgünüz, yorum sistemi kullanım dışıdır.',
'kb_disabled' => 'Üzgünüz, bilgi arşivi kullanım dışıdır.',
'kb_rating_disabled' => 'Üzgünüz, puanlama sistemi kullanım dışıdır.',
'login_must_val' => 'Üzgünüz, e-mail adresinize gönderilen mesajın içindeki onay bağlantısına tıklayıp hesabınızı doğrulamanız gerekiyor. Eğer sözkonusu mesaj adresinize ulaşmadıysa, <a href="index.php?act=register&amp;code=sendval">buraya tıklayınız</a>.',
'login_must_val_admin' => 'Üzgünüz, sistem yöneticisi hesabınızı onaylayana kadar bekleyiniz. Onaylama işlemi yapıldığında e-mail adresinize bir mesaj gelecektir.',
'login_no_pass' => 'Üzgünüz, girdiğiniz şifre hatalı. Şifrenizi unuttuysanız <a href="index.php?act=register&amp;code=forgot">buraya tıklayınız</a>.',
'login_no_user' => 'Girdiğiniz kullanıcı adı sistemde bulunamadı.',
'logout_no_key' => 'Hatalı çıkış anahtarı.',
'must_be_guest' => 'Üzgünüz, bu sayfaya sadece misafir ziyaretçiler erişebilir.',
'must_be_user' => 'Üzgünüz, bu sayfaya erişmek için kullanıcı adınız ve şifrenizle sisteme giriş yapmalısınız.',
'new_tickets_disabled' => 'Üzgünüz, yeni sorun bildirimi ekleme işlemi geçici olarak devre dışıdır.',
'news_comment_disabled' => 'Üzgünüz, yorum sistemi devre dışıdır.',
'news_disabled' => 'Üzgünüz, haberler sayfası devre dışıdır.',
'no_announcement' => 'Üzgünüz, ulaşmaya çalıştığınız bildiri bulunamıyor.',
'no_article' => 'Üzgünüz, ulaşmaya çalıştığınız yazı bulunamıyor.',
'no_category' => 'Üzgünüz, ulaşmaya çalıştığınız kategori bulunamıyor.',
'no_comment' => 'Üzgünüz, ulaşmaya çalıştığınız yorum bulunamıyor.',
'no_department' => 'Üzgünüz, ulaşmaya çalıştığınız departman bulunamıyor.',
'no_email_val_key' => 'Üzgünüz, onaylama anahtar kelimeniz bulunamıyor.',
'no_member' => 'Üzgünüz, ulaşmaya çalıştığınız üye bilgisi bulunamıyor.',
'no_page' => 'Üzgünüz, ulaşmaya çalıştığınız sayfa bulunamıyor.',
'no_pass_match' => 'Şifreleriniz aynı değil.',
'no_perm_access' => 'Üzgünüz, bu alana erişmek için yetkiniz yok.',
'no_perm_banned' => 'Üzgünüz, bu destek sistemine erişmek için yetkiniz yok.',
'no_perm_com_delete' => 'Üzgünüz, bu yorumu silmek için yetkiniz yok.',
'no_perm_com_edit' => 'Üzgünüz, bu yorumu düzeltmek için yetkiniz yok.',
'no_perm_reply_delete' => 'Üzgünüz, bu cevabı silmek için yetkiniz yok.',
'no_perm_reply_edit' => 'Üzgünüz, bu cevabı düzeltmek için yetkiniz yok.',
'no_perm_ticket_edit' => 'Üzgünüz, bu sorun bildirimini düzeltmek için yetkiniz yok.',
'no_reply' => 'Üzgünüz, the reply you were looking for could not be found.',
'no_staff_rate_reply' => 'Üzgünüz, only staff replies can be rated.',
'no_ticket' => 'Üzgünüz, ulaşmaya çalıştığınız sorun bildirimi bulunamıyor.',
'no_ticket_guest' => 'Üzgünüz, girdiğiniz e-mail adresi ve sorun bildirim anahtarı ile eşleşen bir sorun bildirimi bulunamadı.',
'no_valid_email' => 'Lütfen geçerli bir e-mail adresi giriniz.',
'no_valid_tkey' => 'Lütfen geçerli bir sorun bildirim anahtar kelimesi giriniz.',
'registration_disabled' => 'Üzgünüz, yeni üye kaydı yönetici tarafından durduruldu.',
'reply_rating_disabled' => 'Üzgünüz, puanlama sistemi devre dışıdır.',
'ticket_closed_already' => 'Üzgünüz, sorun bildirimi zaten kapatıldı.',
'ticket_closed_escalate' => 'Üzgünüz, zaten kapatılmış bir sorun bildirimini yükseltemezsiniz.',
'ticket_closed_reply' => 'Üzgünüz, zaten kapatılmış bir sorun bildirimini cevaplayamazsınız.',
'ticket_escalate_perm' => 'Üzgünüz, bu sorun bildirimini yükseltmek için yetkiniz yok.',
'ticket_escalated_already' => 'Üzgünüz, bu sorun bildirimi zaten yükseltilmiş.',
'ticket_no_close_perm' => 'Üzgünüz, bu sorun bildirimini kapatmak için yetkiniz yok.',
'token_mismatch' => 'Form anahtar kelimeniz hatalı.',
'try_again' => 'Lütfen geri dönüp tekrar deneyiniz. Problem çözülmezse sistem yöneticisi ile iletişime geçiniz.',

);

?>