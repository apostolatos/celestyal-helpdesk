<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | lang_news.php
#======================================================
*/

$lang = array(

'add_a_comment' => 'Yorum Ekle',
'add_comment_button' => 'Yorum Ekle',
'announcement' => 'Duyuru',
'comments' => 'Yorumlar',
'confirm_delete' => 'Bu yorumu silmek istediğinizden emin misiniz ?',
'delete' => 'Sil',
'edit' => 'Düzelt',
'edit_comment' => 'Yorumu Düzelt',
'edit_comment_button' => 'Yorumu Düzelt',
'err_no_comment' => 'Yorum ekle.',
'no_comments' => 'Bu duyuruya henüz yorum yapılmadı.',
'no_news' => 'Gösterilecek duyuru bulunamadı.',
'viewing_announcement' => 'Duyuru',

);

?>