<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | lang_redirect.php
#======================================================
*/

$lang = array(

'add_rating_success' => 'Puanlamanız eklendi.',
'almost_acc_activate' => 'E-mail adresiniz doğrulandı. Hesabınızı kullanabilmeniz için yönetici tarafından onaylanması gerekiyor. Bu onaylama işlemi yapıldığında bir mail mesajıyla bilgilendirileceksiniz.',
'change_val_email' => 'E-mail adresinizi değiştirebilmemiz için aşağıdaki bağlantıya tıklamanız gerekiyor.',
'click_here' => 'buraya tıklayınız',
'delete_comment_success' => 'Yorum silindi.',
'edit_comment_success' => 'Yorum düzenlendi.',
'login_success' => 'Giriş başarılı.',
'logout_success' => 'Çıkış başarılı.',
'new_user_no_val' => 'Hesabınız oluşturuldu. Artık sistemimize giriş yapabilirsiniz.',
'new_user_val_admin' => 'Hesabınızı kullanmaya başlayabilmeniz için, yönetici tarafından onaylanması gerekiyor. Bu onaylama işlemi yapıldığında bir mail mesajıyla bilgilendirileceksiniz.',
'new_user_val_both' => 'Hesabınızı kullanmaya başlayabilmeniz için, yönetici tarafından onaylanması ve sizin de gönderilen e-mail mesajındaki onay bağlantısına tıklamanız gerekiyor.',
'new_user_val_email' => 'Hesabınızı kullanmaya başlayabilmeniz için, size gönderilen e-mail mesajındaki onay bağlantısına tıklamanız gerekiyor.',
'new_user_val_resend' => 'Doğrulama için gerekli olan e-mail mesajı adresinize gönderildi.',
'please_wait' => 'Lütfen Bekleyiniz',
'reply_delete_success' => 'Cevap silindi.',
'reply_edit_success' => 'Cevap güncellendi.',
'reset_pass_email_sent' => 'Şifrenizi sıfırlamak için, size gönderilen e-mail mesajındaki onay bağlantısına tıklamanız gerekiyor.',
'reset_pass_success' => 'Şifreniz değiştirildi. Artık sistemimize giriş yapabilirsiniz.',
'submit_comment_success' => 'Yorumunuz eklendi.',
'submit_reply_success' => 'Cevabınız eklendi.',
'submit_ticket_success' => 'Sorun bildiriminiz gönderildi.',
'success_acc_activate' => 'Hesabınız aktifleştirildi. Artık sistemimize giriş yapabilirsiniz.',
'thank_you' => 'Teşekkür ederiz.',
'ticket_close_success' => 'Sorun bildiriminiz kapatıldı.',
'ticket_edit_success' => 'Sorun bildirimi güncellendi.',
'ticket_escalate_success' => 'Sorun bildiriminiz yükseltildi.',
'transfer_you' => 'Yönlendirme yapılırken lütfen bekleyiniz. Beklemek istemiyorsanız,',
'update_my_account' => 'Hesabınız güncellendi.',
'update_my_email' => 'E-mail adresiniz güncellendi.',
'update_my_pass' => 'Şifreniz güncellendi. Çerez ayarlarınıza göre değişen bir süre sonunda yeniden giriş yapmanız gerekecektir.',

);

?>