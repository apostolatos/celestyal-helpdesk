<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | lang_register.php
#======================================================
*/

$lang = array(

'captcha' => 'Grafiksel Onay',
'create_account_button' => 'Hesap Oluştur',
'email_address' => 'Email Adresi',
'err_captcha_mismatch' => 'Girdiğiniz kod resimdeki ile aynı değil. Lütfen tekrar deneyiniz.',
'err_email_in_use' => 'Bu e-mail adresi zaten veritabanımızda mevcut. Lütfen başka bir e-mail adresi kullanınız.',
'err_no_cpfield' => 'Bu alan için bir değer girmelisiniz :',
'err_no_email_valid' => 'Lütfen geçerli bir e-mail adresi giriniz.',
'err_no_pass_match' => 'Şifreleriniz eşleşmiyor.',
'err_no_pass_short' => 'Lütfen şifrenizi giriniz.',
'err_no_user_or_email' => 'Lütfen kullanıcı adınızı veya e-mail adresinizi giriniz.',
'err_no_user_short' => 'Kullanıcı adı giriniz.',
'err_user_already_active' => 'Hesabınız zaten onaylanmış. Girebilirsiniz.. :)',
'err_user_in_use' => 'Bu kullanıcı adı zaten veritabanımızda mevcut. Lütfen başka bir kullanıcı adı kullanınız.',
'err_user_not_found' => 'Girdiğiniz bilgilerle veritabanımızda bir kullanıcı bulamadık.',
'forgot_password' => 'Şifremi Unuttum',
'new_password' => 'Yeni Şifre',
'new_password_confirm' => 'Yeni Şifre (Tekrar)',
'optional' => '(Seçimli)',
'or' => 'VEYA',
'password_confirm' => 'Şifre (Tekrar)',
'register_new_account' => 'Yeni Üye Kaydı Yap',
'resend_val_button' => 'Onay mailini tekrar gönder',
'reset_pass_button' => 'Şifre Sıfırla',
'reset_password' => 'Şifre Sıfırla',
'upgrade_account_button' => 'Hesabı Güncelle',
'upgrade_msg' => 'Çünkü zaten misafir olarak giriş yaptınız. Sorun bildirimlerinizi kaybetmeden bu hesabı kalıcı bir hesaba çevirebilirsiniz. Hesabınızı kalıcı hesaba dönüştürmek için, aşağıdaki formu doldurup Hesabı Güncelle tuşuna basınız.',

);

?>