<?php

/*
#======================================================
|    | Trellis Desk Language File
|    | lang_tickets.php
#======================================================
*/

$lang = array(

'article_suggestions' => 'Makale Önerileri',
'attachment' => '(Ek Dosya)',
'attachment_b' => 'Ek Dosya',
'attachment_max_size' => 'En büyük ek dosya boyutu :',
'captcha' => 'Grafiksel Onay',
'close' => 'Kapat',
'close_msg_a' => 'Aşağıdaki sebeple ',
'close_msg_b' => ' sorun bildirimi kapatıldı.',
'close_ticket' => 'Sorun Bildirimini Kapat',
'close_ticket_button' => 'Sorun Bildirimini Kapat',
'closing_ticket' => 'Sorun Bildirimi Kapatılıyor',
'confirm_close' => 'Bu sorun bildirimini kapatmak istediğinizden emin misiniz ?',
'confirm_delete_reply' => 'Bu cevabı silmek istediğinizden emin misiniz ?',
'confirm_escalate' => 'Bu sorun bildirimini yükseltmek istediğinizden emin misiniz ?',
'continue_ticket_submit' => 'Sorun Bildirimini Ekle',
'download_attachment' => 'Ek dosyayı indir :',
'edit' => 'Düzenle',
'edit_reply' => 'Cevabı Düzenle',
'edit_reply_button' => 'Cevabı Düzenle',
'edit_ticket' => 'Sorun Bildirimini Düzenle',
'edit_ticket_button' => 'Sorun Bildirimini Düzenle',
'email' => 'Email',
'email_address' => 'Email Adresi',
'enter_close_reason' => 'Lütfen bu sorun bildirimini kapatmak için bir sebep giriniz.',
'err_captcha_mismatch' => 'Grafiksel onay kodunu düzgün girmediniz. Lütfen tekrar deneyiniz.',
'err_email_in_use' => 'Bu e-mail adresi başka bir kullanıcımız tarafından sisteme kaydedildi. Lütfen başka bir e-mail adresi kullanınız.',
'err_no_cdfield' => 'Lütfen bu alan için bir değer giriniz :',
'err_no_depart' => 'Lütfen bir departman seçiniz.',
'err_no_email' => 'Lütfen bir geçerli bir e-mail adresi giriniz.',
'err_no_message' => 'Lütfen bir mesaj giriniz.',
'err_no_name' => 'Lütfen bir isim giriniz.',
'err_no_reason' => 'Lütfen bir sebep giriniz.',
'err_no_reply' => 'Lütfen bir cevap giriniz.',
'err_no_subject' => 'Lütfen bir konu giriniz.',
'err_upload_bad_type' => 'Yüklemeye çalıştığınız dosya türünün sisteme yüklenmesine izin verilmiyor.',
'err_upload_failed' => 'Dosya yüklemede hata oluştu. Lütfen tekrar deneyiniz.',
'err_upload_too_big' => 'Yüklemeye çalıştığınız dosyanın boyutu izin verilen en yüksek boyutun üstünde.',
'escalate' => 'Yükselt',
'guest_login' => 'Ziyaretçi Girişi',
'guest_login_info' => 'Hoşgeldiniz. E-mail adresinizi ve Sorun Bildirim Anahtar Kelimenizi girerek Sorun Bildiriminize ulaşabilirsiniz. Eğer kayıtlı bir üyemizseniz lütfen sağ tarafta bulunan giriş bölümünden giriş yapınız.',
'guest_ticket_notification' => 'Görevlilerin Cevaplamalarını E-Mail ile Sorun Bildirene Gönder',
'history' => 'Geçmiş',
'last_replier' => 'Son Cevaplayan',
'last_reply' => 'Son Cevap',
'name' => 'Adı',
'no_replies' => 'Bu sorun bildirimine hiç cevap verilmedi.',
'no_suggestions_helped' => 'Aşağıdaki tavsiyelerden hiç biri sorunumu çözmüyor.',
'open_ticket_button' => 'Sorun Bildir',
'optional' => '(Seçimlik)',
'relevance' => 'Uygunluk',
'replies' => 'Cevaplar',
'select_depart' => 'Lütfen bir departman seçiniz.',
'send_reply' => 'Cevap Gönder',
'send_reply_button' => 'Cevabı Gönder',
'suggestions_explained' => 'Sorununuzla ilgili olabileceğini düşündüğümüz bilgi arşivi yazıları aşağıda listelendi. Lütfen sorun bildirimi eklemeden önce sorununuzun çözümünün bu yazılarda olmadığından emin olunuz. <i>Sorun bildiriminiz henüz kaydedilmedi.</i>',
'thumbs_down' => 'Kötü',
'thumbs_up' => 'İyi',
'ticket_center_overview' => 'Sorun Bildirimleri Önizleme',
'ticket_id' => 'Sorun Bildirim No',
'ticket_key' => 'Sorun Bildirim Anahtarı',
'viewing_ticket' => 'Sorun Bildirimi Gösteriliyor',

);

?>