<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="Description Here">
		<title>LET IT BE Motor Yacht: Preview of Main Deck </title>
		<link rel="stylesheet" type="text/css" href="css/demo01-bootstrap.min.css" />
	</head>
	<body>
<!--NAV BAR-->
    <div style=" position: absolute; top: 0; left: 0; right: 0; z-index: 10; margin-top: 20px; margin-bottom: -90px; ">
      <div class="container">


<div class="navbar  navbar-inverse" role="navigation">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/">LET IT BE</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">


	  
      <ul class="nav navbar-nav navbar-right">
        <li><a href="about-let-it-be-motor-yacht.php">About LET IT BE</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Yacht Areas <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="upper-deck-let-it-be.php">Upper Deck</a></li>
            <li><a href="main-deck-let-it-be.php">Main Deck</a></li>
            <li class="divider"></li>
            <li><a href="main-salon-let-it-be.php">Main Salon</a></li>
            <li><a href="dining-area-let-it-be.php">Dining Area</a></li>
            <li><a href="upper-salon-let-it-be.php">Interior Upper Salon</a></li>
            <li class="divider"></li>
            <li><a href="deckplan-let-it-be.php">Deck Plan</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Cabins <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="master-cabin-let-it-be.php">Master Cabin</a></li>
            <li><a href="VIP-cabin-let-it-be.php">VIP Cabin</a></li>
            <li><a href="twin-cabin-let-it-be.php">Twin Cabin</a></li>
          </ul>
        </li>
        <li><a href="tech-specs-let-it-be.php">Technical info</a></li>
        <li><a href="contact-us-let-it-be.php">Contact Info</a></li>

		</ul>    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</div>
</div>
</div>
<!--NAV BAR-->




<!--Carousel-->

   <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container" style="margin-top:100px">

 

      <div class="row">
        <div class="col-md-7">
          <h2  style="margin-top:170px; margin-bottom:20px">VIP Cabin  <span class="text-muted"> LET IT BE</span></h2>
          <p class="lead">

	
  	
The two VIP cabins each feature a large double bed, a spacious en-suite bathroom, large walk-in closet and audio-visual system.
<p> <p class="lead">VIP guests will relish the beauty and comfort found throughout the yacht.</p>

        </div>
        <div class="col-md-5">
          <img class="img-responsive" data-src="img/VIP.jpg" alt="Let It Be Outside" src="img/VIP.jpg">
        </div>
      </div>

      <hr>

 <h2  style="margin-bottom:20px"><span class="text-muted">{ Click to enlarge...}</span></h2>
	 
	  
		<div class="row" style="margin-bottom:20px">
			<div class="col-md-12">
						<a href="img/VIP_065_MG_5101.jpg" data-toggle="lightbox" data-gallery="multiimages" data-title="Let It Be Motor Yacht Upper Deck" class="col-sm-6">
							<img src="img/VIP_065_MG_5101.jpg" class="img-responsive">
						</a>
						<a href="img/VIP_025_MG_5089.jpg" data-toggle="lightbox" data-gallery="multiimages" data-title="Let It Be Motor Yacht Upper Deck" class="col-sm-6">
							<img src="img/VIP_025_MG_5089.jpg" class="img-responsive">
						</a>
<br/>&nbsp;
<br/>						<a href="img/VIP_060_MG_4013.jpg" data-toggle="lightbox" data-gallery="multiimages" data-title="Let It Be Motor Yacht Upper Deck" class="col-sm-4">
							<img src="img/VIP_060_MG_4013.jpg" class="img-responsive">
						</a>
						<a href="img/VIP_089_MG_4006.jpg" data-toggle="lightbox" data-gallery="multiimages" data-title="Let It Be Motor Yacht Upper Deck" class="col-sm-4">
							<img src="img/VIP_089_MG_4006.jpg" class="img-responsive">
						</a>
						<a href="img/VIP_066_MG_3963.jpg" data-toggle="lightbox" data-gallery="multiimages" data-title="Let It Be Motor Yacht Upper Deck" class="col-sm-4">
							<img src="img/VIP_066_MG_3963.jpg" class="img-responsive">
						</a>
<br/>&nbsp;
<br/>
						

									<a href="img/VIP_031_MG_5098.jpg" data-toggle="lightbox" data-gallery="multiimages" data-title="Let It Be Motor Yacht Upper Deck" class="col-sm-12">
							<img src="img/VIP_031_MG_5098.jpg" class="img-responsive">
						</a>

			</div>
		</div>




      <hr class="featurette-divider">

      <!-- FOOTER -->
      <footer>
        <p class="pull-right"><a href="#">Back to top</a></p>
        <p>&copy; 2014 Motor Yacht LET IT BE </p>
      </footer>

    </div><!-- /.container -->



		<script src="js/jquery.js"></script>	
		<script src="js/lighbox.js"></script>
		<script src="js/bootstrap.js"></script>
		<script type="text/javascript">
			$(document).ready(function ($) {

				// delegate calls to data-toggle="lightbox"
				$(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
					event.preventDefault();
					return $(this).ekkoLightbox({
						always_show_close: true
					});
				});

			});
		</script>
	</body>
</html>