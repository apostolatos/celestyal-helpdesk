<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="Description Here">
		<title>LET IT BE Motor Yacht: Preview of Dining Area </title>
		<link rel="stylesheet" type="text/css" href="css/demo01-bootstrap.min.css" />
	</head>
	<body>
<!--NAV BAR-->
    <div style=" position: absolute; top: 0; left: 0; right: 0; z-index: 10; margin-top: 20px; margin-bottom: -90px; ">
      <div class="container">


<div class="navbar  navbar-inverse" role="navigation">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/">LET IT BE</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">


	  
      <ul class="nav navbar-nav navbar-right">
        <li><a href="about-let-it-be-motor-yacht.html">About LET IT BE</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Yacht Areas <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="upper-deck-let-it-be.html">Upper Deck</a></li>
            <li><a href="main-deck-let-it-be.html">Main Deck</a></li>
            <li class="divider"></li>
            <li><a href="main-salon-let-it-be.html">Main Salon</a></li>
            <li><a href="dining-area-let-it-be.html">Dining Area</a></li>
            <li><a href="upper-salon-let-it-be.html">Interior Upper Salon</a></li>
            <li class="divider"></li>
            <li><a href="deckplan-let-it-be.html">Deck Plan</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Cabins <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="master-cabin-let-it-be.html">Master Cabin</a></li>
            <li><a href="VIP-cabin-let-it-be.html">VIP Cabin</a></li>
            <li><a href="twin-cabin-let-it-be.html">Twin Cabin</a></li>
          </ul>
        </li>
        <li><a href="tech-specs-let-it-be.html">Technical info</a></li>
        <li><a href="contact-us-let-it-be.html">Contact Info</a></li>

		</ul>    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</div>
</div>
</div>
<!--NAV BAR-->




<!--Carousel-->

   <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container" style="margin-top:100px">

 
	  
		<div class="row" style="margin-bottom:20px">
			<div class="col-md-12">
			<h2>Lower Deck</h2>
			
						<a href="img/DP_Upper.jpg" data-toggle="lightbox" data-gallery="multiimages" data-title="Let It Be Motor Lower Deck Deck Plan  " class="col-sm-12">
							<img src="img/DP_Upper.jpg" class="img-responsive">
						</a>
			<H3> VIP Cabins | Twin Cabins </H3>

<br/>&nbsp;
<br/>
						<a href="img/DP_VIP_cabin.jpg" data-toggle="lightbox" data-gallery="multiimages" data-title="Let It Be Motor Yacht VIP Cabin  " class="col-sm-6">
							<img src="img/DP_VIP_cabin.jpg" class="img-responsive">
						</a>
										<a href="img/DP_Twin_cabin.jpg" data-toggle="lightbox" data-gallery="multiimages" data-title="Let It Be Motor Yacht Twin Cabins  " class="col-sm-6">
							<img src="img/DP_Twin_cabin.jpg" class="img-responsive">
						</a>


			</div>
		</div>




      <hr class="featurette-divider"> 
	  
		<div class="row" style="margin-bottom:20px">
			<div class="col-md-12">
			<h2>Main Deck</h2>
			
						<a href="img/DP_main.jpg" data-toggle="lightbox" data-gallery="multiimages" data-title="Let It Be Motor Yacht Main Deck Plan  " class="col-sm-12">
							<img src="img/DP_main.jpg" class="img-responsive">
						</a>
			<H3> Dining Area | Main Salon | Master Cabin </H3>

<br/>&nbsp;
<br/>
						<a href="img/dine_019_MG_4136.jpg" data-toggle="lightbox" data-gallery="multiimages" data-title="Let It Be Motor Yacht Dining Area  " class="col-sm-4">
							<img src="img/dine_019_MG_4136.jpg" class="img-responsive">
						</a>
										<a href="img/mains_034_MG_4125.jpg" data-toggle="lightbox" data-gallery="multiimages" data-title="Let It Be Motor Yacht Main Salon  " class="col-sm-4">
							<img src="img/mains_034_MG_4125.jpg" class="img-responsive">
						</a>
						<a href="img/DP_Master_cabin.jpg" data-toggle="lightbox" data-gallery="multiimages" data-title="Let It Be Motor Yacht Master Cabin  " class="col-sm-4">
							<img src="img/DP_Master_cabin.jpg" class="img-responsive">
						</a>


			</div>
		</div>
      <hr class="featurette-divider"> 
	  
		<div class="row" style="margin-bottom:20px">
			<div class="col-md-12">
			<h2>Upper Deck</h2>
			
						<a href="img/DP_lower.jpg" data-toggle="lightbox" data-gallery="multiimages" data-title="Let It Be Motor Yacht Upper Deck Plan  " class="col-sm-12">
							<img src="img/DP_lower.jpg" class="img-responsive">
						</a>
			<H3> Sunbathing Area | Interior Upper Salon</H3>

<br/>&nbsp;
<br/>
						<a href="img/upper_LET-IT-BE0046JPEG.jpg" data-toggle="lightbox" data-gallery="multiimages" data-title="Let It Be Motor Yacht Sunbathing Area  " class="col-sm-6">
							<img src="img/upper_LET-IT-BE0046JPEG.jpg" class="img-responsive">
						</a>
						<a href="img/uppers_088_MG_4242.jpg" data-toggle="lightbox" data-gallery="multiimages" data-title="Let It Be Motor Yacht Interior Upper Salon Area  " class="col-sm-6">
							<img src="img/uppers_088_MG_4242.jpg" class="img-responsive">
						</a>


			</div>
		</div>




      <hr class="featurette-divider">

      <!-- FOOTER -->
      <footer>
        <p class="pull-right"><a href="#">Back to top</a></p>
        <p>&copy; 2014 Motor Yacht LET IT BE </p>
      </footer>

    </div><!-- /.container -->



		<script src="js/jquery.js"></script>	
		<script src="js/lighbox.js"></script>
		<script src="js/bootstrap.js"></script>
		<script type="text/javascript">
			$(document).ready(function ($) {

				// delegate calls to data-toggle="lightbox"
				$(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
					event.preventDefault();
					return $(this).ekkoLightbox({
						always_show_close: true
					});
				});

			});
		</script>
	</body>
</html>