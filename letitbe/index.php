<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="Description Here">
		<title>LET IT BE Motor Yacht: Smooth Cruising in East Mediterranean  </title>
		<link rel="stylesheet" type="text/css" href="css/demo01-bootstrap.min.css" />
	</head>
	<body>
<!--NAV BAR-->
    <div style=" position: absolute; top: 0; left: 0; right: 0; z-index: 10; margin-top: 20px; margin-bottom: -90px; ">
      <div class="container">


<div class="navbar  navbar-inverse" role="navigation">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/">LET IT BE</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">


	  
      <ul class="nav navbar-nav navbar-right">
        <li><a href="about-let-it-be-motor-yacht.php">About LET IT BE</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Yacht Areas <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="upper-deck-let-it-be.php">Upper Deck</a></li>
            <li><a href="main-deck-let-it-be.php">Main Deck</a></li>
            <li class="divider"></li>
            <li><a href="main-salon-let-it-be.php">Main Salon</a></li>
            <li><a href="dining-area-let-it-be.php">Dining Area</a></li>
            <li><a href="upper-salon-let-it-be.php">Interior Upper Salon</a></li>
            <li class="divider"></li>
            <li><a href="deckplan-let-it-be.php">Deck Plan</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Cabins <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="master-cabin-let-it-be.php">Master Cabin</a></li>
            <li><a href="VIP-cabin-let-it-be.php">VIP Cabin</a></li>
            <li><a href="twin-cabin-let-it-be.php">Twin Cabin</a></li>
          </ul>
        </li>
        <li><a href="tech-specs-let-it-be.php">Technical info</a></li>
        <li><a href="contact-us-let-it-be.php">Contact Info</a></li>

		</ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</div>
</div>
</div>
<!--NAV BAR-->


<!--Carousel-->
<div class="container-fluid">
<div class="row">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
        <li class="" data-target="#carousel-example-generic" data-slide-to="1"></li>
        <li class="" data-target="#carousel-example-generic" data-slide-to="2"></li>
        <li class="" data-target="#carousel-example-generic" data-slide-to="3"></li>
        <li class="" data-target="#carousel-example-generic" data-slide-to="4"></li>
      </ol>
      <div class="carousel-inner">

        <div class="item active">
          <img src="img/boot01-01.jpg"  alt="First slide">
		  <div class="container-fluid">
            <div class="carousel-caption">
              <h1 style="color: #34466a">Luxury Vacation in East Med. with "LET IT BE" </h1>
              <p>Let it Be is a well designed luxurious yacht, where comfort and stability were highly ranked on the list of concerns combined with a smooth solid ride.</p>
            </div>
          </div>
		  </div>
        <div class="item">
          <img src="img/boot01-02.jpg"  alt="First slide">
		  <div class="container-fluid">
            <div class="carousel-caption">
              <h1 style="color: #34466a">Sun bathing and relaxation.</h1>
              <p>The ideal place for "Al fresco" dining and breakfast while contemplating the beauty of the Mediterranean sea.</p>
            </div>
          </div>
        </div>
        <div class="item">
          <img src="img/boot01-03.jpg"  alt="First slide">
		  <div class="container-fluid">
            <div class="carousel-caption">
              <h1 style="color: #34466a">afternoon tea, coffee, a drink or casual dining... </h1>
              <p>&nbsp;</p>
            </div>
          </div>
        </div>
        <div class="item">
          <img src="img/boot01-04.jpg"  alt="First slide">
		  <div class="container-fluid">
            <div class="carousel-caption">
              <h1 style="color: #34466a">Meet the beauty of the Mediterranean sea</h1>
              <p>&nbsp;.</p>
            </div>
          </div>
        </div>
        <div class="item">
          <img src="img/boot01-05.jpg"  alt="First slide">
		  <div class="container-fluid">
            <div class="carousel-caption">
              <h1 style="color: #34466a">Relax in the modern living area</h1>
              <p>&nbsp;</p>
            </div>
          </div>
        </div>
      </div>
      <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
      </a>
      <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
      </a>
    </div>
</div>
</div>


<!--Carousel-->

   <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container" style="margin-top:50px">

      <!-- Three columns of text below the carousel -->

       <div class="row text-center">
        <div class="col-lg-4">
          <img class="img-circle" data-src="img/main01-01.jpg" src="img/main01-01.jpg" alt="Generic placeholder image">
          <h2>Main Deck</h2>
          <p><a class="btn btn-default" href="main-deck-let-it-be.php" role="button">View details &raquo;</a></p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <img class="img-circle" data-src="img/salon01-01.jpg" src="img/salon01-01.jpg" alt="Generic placeholder image">
          <h2>Main Salon</h2>
          <p><a class="btn btn-default" href="main-salon-let-it-be.php" role="button">View details &raquo;</a></p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <img class="img-circle" data-src="img/intsalon01-01.jpg" src="img/intsalon01-01.jpg" alt="Generic placeholder image">
          <h2>Interior Upper Salon</h2>
          <p><a class="btn btn-default" href="upper-salon-let-it-be.php" role="button">View details &raquo;</a></p>
        </div><!-- /.col-lg-4 -->
      </div><!-- /.row -->     <!-- /END THE FEATURETTES -->


      <!-- START THE FEATURETTES -->


			
			
			
			
			
      <hr >

      <div class="row">
        <div class="col-md-7">
          <h2  style="margin-top:100px; margin-bottom:20px">Our Cabins ... <span class="text-muted">fine elegance and luxury.</span></h2>
          <p class="lead">Master cabin with queen size bed, en-suite bathroom, mini bar. <br/>Two VIP cabins with large double bed. <br/>Twin Cabins for more guests</p>
		  <a href="master-cabin-let-it-be.php" class="btn btn-primary btn-lg">Master Cabin</a>
		  <a href="VIP-cabin-let-it-be.php" class="btn btn-primary btn-lg">VIP Cabin</a>
		  <a href="twin-cabin-let-it-be" class="btn btn-primary btn-lg">Twin Cabin</a>
        </div>
        <div class="col-md-5">
          <img class="img-responsive" data-src="img/Cab01-01.jpg" alt="Let It Be Suite" src="img/Cab01-01.jpg">
        </div>
      </div>

      <hr>

      <div class="row featurette">
        <div class="col-md-5">
          <img class="img-responsive" data-src="img/Dine01-01.jpg" src="img/Dine01-01.jpg" alt="Generic placeholder image">
        </div>
        <div class="col-md-7">
          <h2 style="margin-top:100px; margin-bottom:20px">Dining Area <span class="text-muted">See for yourself.</span></h2>
          <p class="lead">Gourmet meals, matched to your personal preferences can be served in the dining area. Please advise us in advance of your culinary requests and we will comply to your needs. </p>
		   <a href="dining-area-let-it-be.php" class="btn btn-primary pull-right">More</a>
        </div>
      </div>

      <hr class="featurette-divider">

      <div class="row">
        <div class="col-md-7">
          <h2 style="margin-top:100px; margin-bottom:20px">Upper Deck <span class="text-muted">Checkmate.</span></h2>
          <p class="lead">Sun bathing and relaxation are the rules of the wide shaded aft deck.
The ideal place for "Al fresco" dining and breakfast while contemplating the beauty of the Mediterranean sea.</p>
		   <a href="upper-deck-let-it-be.php" class="btn btn-primary pull-right">More</a>

        </div>
        <div class="col-md-5">
          <img class="img-responsive" data-src="img/upper01-01.jpg"  src="img/upper01-01.jpg" alt="Generic placeholder image">
        </div>
      </div>



      <hr class="featurette-divider">

      <!-- FOOTER -->
      <footer>
        <p class="pull-right"><a href="#">Back to top</a></p>
        <p>&copy; 2014 Motor Yacht LET IT BE </p>
      </footer>

    </div><!-- /.container -->



		<script src="js/jquery.js"></script>	
		<script src="js/bootstrap.js"></script>
		<script>
		$('.carousel').carousel({
  interval: 2500
})

		</script>
	</body>
</html>