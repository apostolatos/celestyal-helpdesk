<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="Description Here">
		<title>LET IT BE Motor Yacht: Preview of Main Deck </title>
		<link rel="stylesheet" type="text/css" href="css/demo01-bootstrap.min.css" />
	</head>
	<body>
<!--NAV BAR-->
    <div style=" position: absolute; top: 0; left: 0; right: 0; z-index: 10; margin-top: 20px; margin-bottom: -90px; ">
      <div class="container">


<div class="navbar  navbar-inverse" role="navigation">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/">LET IT BE</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">


	  
      <ul class="nav navbar-nav navbar-right">
        <li><a href="about-let-it-be-motor-yacht.php">About LET IT BE</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Yacht Areas <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="upper-deck-let-it-be.php">Upper Deck</a></li>
            <li><a href="main-deck-let-it-be.php">Main Deck</a></li>
            <li class="divider"></li>
            <li><a href="main-salon-let-it-be.php">Main Salon</a></li>
            <li><a href="dining-area-let-it-be.php">Dining Area</a></li>
            <li><a href="upper-salon-let-it-be.php">Interior Upper Salon</a></li>
            <li class="divider"></li>
            <li><a href="deckplan-let-it-be.php">Deck Plan</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Cabins <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="master-cabin-let-it-be.php">Master Cabin</a></li>
            <li><a href="VIP-cabin-let-it-be.php">VIP Cabin</a></li>
            <li><a href="twin-cabin-let-it-be.php">Twin Cabin</a></li>
          </ul>
        </li>
        <li><a href="tech-specs-let-it-be.php">Technical info</a></li>
        <li><a href="contact-us-let-it-be.php">Contact Info</a></li>

		</ul>    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</div>
</div>
</div>
<!--NAV BAR-->




<!--Carousel-->

   <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container" style="margin-top:100px">


      <hr>

 <h2  style="margin-bottom:20px">Technical Specifications <span class="text-muted">LET IT BE</span></h2>
	 
	  





      <hr class="featurette-divider">

<table  class="table table-striped table-bordered table-condensed ">
  <tr>
    <th class="technical-textbold" scope="col"><h4>SPECIFICATIONS</h4></th>
    <th class="technical-textbold" scope="col"></th>
  </tr>
 
  <tr>
    <td>BUILDER:
    <td scope="col">TECHNOMARINE</td>
  </tr>
  <tr>
    <td>YEAR OF BUILD:</td>
    <td>1998</td>
  </tr>
  <tr>
    <td>REFITTED:</td>
    <td>2001 &amp; 2005 </td>
  </tr>
  <tr>
    <td>L.O.A.:</td>
    <td>37M (118FT) </td>
  </tr>
  <tr>
    <td>BEAM:</td>
    <td>7,33M (24FT) </td>
  </tr>
  <tr>
    <td>DRAFT:</td>
    <td>2,5M(8,24FT)</td>
  </tr>
  <tr>
    <td> HULL CONSTRUCTION: </td>
    <td>G.R.P</td>
  </tr>
  <tr>
    <td> SUPERSTRUCTURE:</td>
    <td>Aluminum</td>
  </tr>
  <tr>
    <td> ENGINES:</td>
    <td>MTU 2X2252 HP </td>
  </tr>
  <tr>
    <td> GENERATORS:</td>
    <td>2x ONAN   1x45kw &amp; 1x65kw</td>
  </tr>
  <tr>
    <td class="technical-text ">SPEED:</td>
    <td>20 knts CRUISING / 24knts MAX </td>
  </tr>
  <tr>
    <td> FUEL CONSUMPTION: </td>
    <td>850 lt/h INCLUDING GENERATORS </td>
  </tr>
  <tr>
    <td> FUEL CAPACITY:</td>
    <td>25.000 lt </td>
  </tr>
  <tr>
    <td> WATER CAPACITY: </td>
    <td>3.000 lt </td>
  </tr>
  <tr>
    <td> WATER MAKER: </td>
    <td>2xhydromare 300LT/H </td>
  </tr>
  <tr>
    <td> STABILIZERS:</td>
    <td>REXROTH MANESMAN </td>
  </tr>
  <tr>
    <td> AIR CONDITIONING: </td>
    <td>250.000 BTU </td>
  </tr>
  <tr>
    <td class="technical-text"> CREW</td>
    <td class="technical-text">7</td>
  </tr>
</table>



<br />
<br />
<table  class="table table-striped table-bordered table-condensed ">
  <tr>
    <th class="technical-textbold" scope="col">ACCOMMODATION</th>
    <th class="technical-textbold" scope="col"> </th>
  </tr>
  
  <tr>
    <td class="technical-text">ACCOMMODATES TEN GUESTS IN: </td>
    <td class="technical-text">ONE
      MASTER SUITE ON MAIN DECK<br/>TWO VIP CABINS AND TWO TWIN CABINS 
	  <br/> AFT
      DECK SITTING / DINING AREA
	  <br/>SUNBATHING AREAS ON UPPER DECK AND
      WC ON MAIN DECK
	  <br/>SEPARATE CAPTAIN AND CREW QUARTERS
      FOR 7 FORWARD
	  <br/>FULLY AIRCONDITIONED THROUGHOUT -
      INDIVIDUALLY CONTROLLED







	  </td>
  </tr>
 
</table>
<br />
<br />
<table  class="table table-striped table-bordered table-condensed ">
  <tr>
    <th  class="technical-textbold" scope="col">TENDER/TOYS/ENTERTAINMENT </th>
  </tr>
  <tr>
    <td class="technical-text"> TENDER   LOMAC 4.5m WITH YAMAHA 60HP
	<br/>2ND PLASTIMAR 2,60 WITH YAMAHA 2.5HP
	<br/> SNORKELING EQUIPMENT 
	<br/>FISHING EQUIPMENT
	<br/>AUDIO
      VISUAL SYSTEM / SATELLITE TV THROUGHOUT (IN TWIN CABINS AUDIO SYSTEM ONLY) 
	<br/>HOME
      CINEMA IN MAIN DECK SALON
	
	
	
	
	
	
	
	
	</td>
  </tr>

</table>
<br />
<br />
<table  class="table table-striped table-bordered table-condensed ">
  <tr>
    <th width="68" class="technical-textbold" scope="col">SAFETY</th>
  </tr>
  <tr>
    <td class="technical-text">2 LIFE RAFTS FOR 12 PERSONS EACH </td>
  </tr>
  <tr>
    <td class="technical-text"> SMOKE DETECTORS </td>
  </tr>
  <tr>
    <td class="technical-text"> CLOSE CIRCUIT IN TV SYSTEM </td>
  </tr>
  <tr>
    <td class="technical-text"> FULL FIREFIGHTING EQUIPMENT </td>
  </tr>
</table>
<br />
<br />
<table  class="table table-striped table-bordered table-condensed ">
  <tr>
    <th colspan="2" class="technical-textbold" scope="col">NAVIGATION / COMMUNICATION </th>
  </tr>
  <tr>
    <td width="220" class="technical-text"> 2 RADARS </td>
    <td width="230" class="technical-text">DEPTH SOUNDER </td>
  </tr>
  <tr>
    <td class="technical-text"> 2 GPS / PLOTERS </td>
    <td class="technical-text"> SPEED LOG </td>
  </tr>
  <tr>
    <td class="technical-text"> AUTOPILOT</td>
    <td class="technical-text"> WIND SPEED / DIRECTION </td>
  </tr>
  <tr>
    <td class="technical-text"> NAVTEX</td>
    <td class="technical-text"> SSB GMDSS </td>
  </tr>
  <tr>
    <td class="technical-text"> WEATHER FAX </td>
    <td class="technical-text"> SATCOM (PHONE-FAX-DATA) </td>
  </tr>
  <tr>
    <td class="technical-text"></td>
    <td class="technical-text "> GSM PHONE </td>
  </tr>
</table>




	  
	      <hr class="featurette-divider">
  
	  
      <!-- FOOTER -->
      <footer>
        <p class="pull-right"><a href="#">Back to top</a></p>
        <p>&copy; 2014 Motor Yacht LET IT BE </p>
      </footer>

    </div><!-- /.container -->



		<script src="js/jquery.js"></script>	
		<script src="js/lighbox.js"></script>
		<script src="js/bootstrap.js"></script>
		<script type="text/javascript">
			$(document).ready(function ($) {

				// delegate calls to data-toggle="lightbox"
				$(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
					event.preventDefault();
					return $(this).ekkoLightbox({
						always_show_close: true
					});
				});

			});
		</script>
	</body>
</html>