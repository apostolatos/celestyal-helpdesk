<div class="hidden megaMenu container_12 container" id="MegaMenu-44">
	<div id="content">
		<div class="grid_6">
			<div class="box-bg">
				<div class="megabox1Left">					
					<h2>{$language['help_desk']}</h2>
					<div class="float-left"> 
						<a href="/{$input[lang]}/form/6/contact-us.html"><span>{$language['contact_form']}</span></a>
						<div class="sep-mega"></div>
						<a href="/{$input[lang]}/cruising/298/louis-headquarters-phones.html"><span>{$language['louis_headquarters_phones']}</span></a>
						<div class="sep-mega"></div>
						<a href="/{$input[lang]}/cruising/298/louis-headquarters-phones.html"><span>{$language['ship_phones']}</span></a>
						<div class="sep-mega"></div>
						<a href="/{$input[lang]}/"><span>  </span></a>
						<div class="sep-mega"></div>
					</div>
				</div>
			</div>
			<div class="box-bg">
				<div id="width73" class="megabox1Left">
					<h2>{$language[press_area]}</h2>
					<img src="/images/site/mega-menu/press.jpg" />
					<div class="float-left">
						<a href="/{$input[lang]}/category/brochures/index.html" class="str-exc"><span class="normal">{$language['brochures']}</span></a>
						<div class="sep-mega"></div>
						<a href="/{$input[lang]}/category/professional-photo-gallery/index.html" class="str-exc"><span class="normal">{$language['professional_photo_gallery']}</span></a>
						<div class="sep-mega"></div>
						 <a href="/{$input[lang]}/category/news/index.html" class="str-exc"><span class="normal">{$lang[news]}</span></a>
						<div class="sep-mega"></div> 
						<a href="/{$input[lang]}/videos.html" class="str-exc"><span class="normal">{$language['videos']}</span></a>
						<div class="seperator"></div>
					</div>
				</div>
				<div id="width20" class="megabox1Right"> 
				<a href="{$td_domain}/{$input[lang]}/category/press-area/index.html">
					<span class="seemore spacer-right">{$language[see_all]}</span>
				</a>
				<br/>
				</div>
			</div>
		</div>
		<div class="grid_6">
			<div  class="box-bg">
				<div class="ContentLeft"> 
					<h2>{$language['i_want']}</h2>
					 <a href="/{$input[lang]}/form/3/a-group-offer.html"><span>{$language['a_group_offer']}</span></a>
					<div class="sep-mega"></div>
					<a href="/{$input[lang]}/form/7/a-meeting-convetion-group-offer.html"><span>{$language['meeting_convetion']}</span></a>
					<div class="sep-mega"></div>
					<a href="/{$input[lang]}/form/2/to-charter-our-ship.html"><span>{$language['to_charter_on_your_ships']}</span></a>
					<div class="sep-mega"></div>
				</div>
			</div>
			<div class="box-bg float-left" id="support-left"> 
				<a href="{$td_domain}/{$input[lang]}/category/greek-hospitallity/index.html">
					<h2>{$language[site_help]}</h2>
				</a>
				<div class="float-left"> 
					<a href="/{$input[lang]}/cruising/133/booking-conditions.html" class="str-exc"><span class="normal">{$language['booking_conditions']}</span></a>
					<div class="sep-mega"></div>
					<a href="/{$input[lang]}/category/terms-and-conditions-of-carriage/index.html" class="str-exc"><span class="normal">{$language['terms_and_conditions']}</span></a>
					<div class="sep-mega"></div>
					<a href="/{$input[lang]}/cruising/129/privacy-policy.html" class="str-exc"><span class="normal">{$language['privacy_policy']}</span></a>
					<div class="sep-mega"></div>
					<a href="/{$input[lang]}/cruising/134/sitemap.html" class="str-exc"><span class="normal">{$language['sitemap']}</span></a>
					<div class="seperator"></div>
				</div>
			</div>
			<div class="box-bg float-left" id="support-right">
				<a href="{$td_domain}/{$input[lang]}/form/5/travel-agents-registration.html">
					<h2>{$language[travel_agents]}</h2>
				</a>
				<div class="float-left"> 
					<a href="/{$input[lang]}/form/5/travel-agents-registration.html" class="str-exc">
					<span class="normal">{$language['travel_agents_registration']}</span></a>
					<div class="sep-mega"></div>
					<div id="trav-agents">
						<a class="dest-more-left button blue small float-right travel-btn-{$input[lang]}" style="text-align:center;" href=" http://www.louiscruises.com/AgencyLogin.html" target="_blank">{$language['travel_agents_booking_area']}</a>
					</div>
				</div>
			</div>
		</div>
		<div class="grid_12">
			<div class="MegaButtons"> 
				<a href="/{$input[lang]}/category/terms-and-conditions-of-carriage/index.html" class="button red large">{$language['conditions_of_carriage']}</a>
			</div>
		</div>
	</div>
</div>
