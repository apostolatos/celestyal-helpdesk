<footer class="greyBg">
	<!-- footer Rest -->
	<div class="restFooter">
		<div class="container">
			<div class="row">
				<div class="col-lg-2 col-lg-offset-0 col-md-2 col-md-offset-0 hidden-sm hidden-xs">
					<ul class="footerNav">
						<li style="color: #fff;font-size: 18px;" class="bold">
							{$language['why_celestyal']}
						</li><!-- <div class=""> -->
						<ul class="menu">
							<li class="first leaf menu-mlid-811">
								<a href="{$domain}/{$menu_alias['our_mission']}">{$language['our_mission']}</a>
							</li>
							<li class="last leaf menu-mlid-812">
								<a href="{$domain}/{$menu_alias['our_fleet']}">{$language['our_fleet']}</a>
							</li>
						</ul><!-- </div> -->
					</ul>
					<ul class="footerNav">
						<li style="color: #fff;font-size: 18px; margin-top: 10px;" class="bold">
							{$language['travel_agents_booking_area']}
						</li>
						<ul class="menu">
							<li class="first leaf menu-mlid-811">
								<a target="_blank" href="http://helpdesk.celestyalcruises.com/en/form/5/travel-agents-registration.html">{$language['registration_form']}</a>
							</li>
							<li class="last leaf menu-mlid-812">
								<a target="_blank" href="https://pro.celestyalcruises.com/AgencyLogin.html">{$language['booking_area']}</a>
							</li>
						</ul><!-- </div> -->
					</ul>
				</div>

				<div class="col-lg-2 col-lg-offset-0 col-md-2 col-md-offset-0 hidden-sm hidden-xs">
					<ul class="footerNav">
						<li style="color: #fff;font-size: 18px;" class="bold">
							{$language['cruises']}
						</li>
						<ul class="menu">
							<li class="first leaf">
								<a href="{$domain}/greece-turkey-cruises">{$language['itineraries']}</a>
							</li>
							<li class="leaf">
								<a href="{$domain}/{$menu_alias['destinations']}">{$language['destinations']}</a>
							</li>
							<li class="leaf">
								<a href="{$domain}/{$menu_alias['excursions']}">{$language['excursions']}</a>
							</li>
							<li class="last leaf">
								<a href="{$domain}/{$menu_alias['drinks_packages']}">{$language['drinks_packages']}</a>
							</li>
						</ul>
					</ul>
				</div>
				
				<!--
				classWrap|col-lg-2 col-lg-offset-1 col-md-2 col-md-offset-1 col-sm-2 col-sm-offset-1 col-xs-2 col-xs-offset-1
				menuClass|footerNav
				title|Cruises
				-->

				<div class="col-lg-2 col-md-2 hidden-sm hidden-xs">
					<ul class="footerNav">
						<li style="color: #fff;font-size: 18px;" class="bold">
							{$language['experience']}
						</li>
						<ul class="menu">
							<li class="first leaf">
								<a href="{$domain}/{$menu_alias['themed_cruises']}">{$language['themed_cruises']}</a>
							</li>
							<li class="leaf">
								<a href="{$domain}/{$menu_alias['on_board']}">{$language['onboard']}</a>
							</li>
							<li class="last leaf">
								<a href="{$domain}/{$menu_alias['ashore']}">{$language['ashore']}</a>
							</li>
						</ul>
					</ul>

				</div>

				<div class="col-lg-2 col-md-2 hidden-sm hidden-xs">
					<ul class="footerNav">
						<li style="color: #fff;font-size: 18px;" class="bold">
							{$language['cruising_guide']}
						</li>
						<ul class="menu">
							<li class="first leaf">
								<a href="{$domain}/{$menu_alias['new_to_cruising']}">{$language['new_to_cruising']}</a>
							</li>
						</ul>
					</ul>
				</div>

				<div class="col-lg-4 col-lg-offset-0 col-md-4 col-md-offset-0 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0">
					<div class="row mob40">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<p class="bold white mb15">
								{$language['on_the_web']}:
							</p>
							<ul class="soc">
								<li>
									<a href="https://www.facebook.com/CelestyalCruisesGreece" target="_blank"><img src="http://americas.celestyalcruises.com//sites/default/files/facebook.svg"></a>
								</li>
								<li>
									<a href="https://twitter.com/celestyalcruise" target="_blank"><img src="http://americas.celestyalcruises.com//sites/default/files/twitter.svg"></a>
								</li>
								<li>
									<a href="https://www.youtube.com/channel/UC33aSFFR8EcXzBYJFpAZcIw" target="_blank"><img src="http://americas.celestyalcruises.com//sites/default/files/youtube.svg"></a>
								</li>
								<li>
									<a href="https://www.instagram.com/celestyalcruises/" target="_blank"><img src="http://americas.celestyalcruises.com//sites/default/files/instagram.svg"></a>
								</li>
								<li>
									<a href="https://www.pinterest.com/celestyalcruise/" target="_blank"><img src="http://americas.celestyalcruises.com//sites/default/files/pinterest.svg"></a>
								</li>
								<li>
									<a href="https://www.linkedin.com/company/celestyal-cruises" target="_blank"><img src="http://americas.celestyalcruises.com//sites/default/files/linked-in.svg"></a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="row mt45">
				<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 rightT mob40">
					<!-- PUT HERE THE AEGEAN MILES CRAP -->

					<!-- PUT HERE THE AEGEAN MILES CRAP -->
					<img src="http://americas.celestyalcruises.com/sites/all/themes/celestyal/media/clia-logo.svg" class="cliaLogo">
					<a target="_blank" href="http://yourcubacruise.com/"><img src="http://americas.celestyalcruises.com/sites/all/themes/celestyal/media/cuba_footer.svg" class="cliaLogo"></a>
					<!--<img class="hidden-xs hidden-sm" src="/sites/all/themes/celestyal/media/celestyal_logo_white.svg" /> -->
					<img src="http://americas.celestyalcruises.com/sites/all/themes/celestyal/media/logo_footer_en.svg" class="hidden-xs hidden-sm">
				</div>
				<div class="visible-sm visible-xs col-sm-12 col-xs-12">
					<img width="221" src="http://americas.celestyalcruises.com/sites/all/themes/celestyal/media/logo_footer_en.svg">
				</div>
			</div>
		</div>
	</div>

	<div class="footerBottom">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<p class="white">
						Celestyal Cruises &copy; 2016 <a target="_blank" href="http://www.celestyalcruises.com/en/cruising/133/booking-conditions.html" class="white">{$language['booking_conditions']}</a>
						<a target="_blank" href="http://www.celestyalcruises.com/en/category/terms-and-conditions-of-carriage/index.html" class="white">{$language['conditions_of_carriage']}</a>
					</p>
				</div>
			</div>
		</div>
	</div>
</footer>