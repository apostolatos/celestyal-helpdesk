<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Celestyal Cruises | {if $depart_title}{$depart_title}{else}{$language['thank_you_for_contact']}{/if}</title>
		<meta name="description" content="">
		<meta name="author" content="">
		
		<link rel="shortcut icon" href="http://www.celestyalcruises.gr/sites/default/files/favicon.ico" type="image/vnd.microsoft.icon" />
		
		<!-- Bootstrap -->
		<link rel="stylesheet" href="/includes/css/minified.css.php" />
		<link rel="stylesheet" href="/includes/css/styles.css" />
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		
		<link href="/includes/scripts/kendo/styles/kendo.common.min.css" rel="stylesheet" />
		<link href="/includes/scripts/kendo/styles/kendo.metro.min.css" rel="stylesheet" />
		
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic&subset=latin,greek,cyrillic' rel='stylesheet' type='text/css'>
		
		<script src="/includes/scripts/head.load.min.js"></script>
		<script src="/includes/scripts/jquery-1.8.2.min.js"></script>
		<script src="https://maps.googleapis.com/maps/api/js"></script>
		<script src="/includes/scripts/isotope.min.js"></script>
		<!--[if lt IE 9]>
		<style>
		.thumbnail img {
		width:auto !important
		}
		</style>
		<![endif]-->
	</head>
