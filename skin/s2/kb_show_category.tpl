<div class="container-fluid nopadd white-bg">
	<div class="row row-banner">
		<div class="banner-container">
			<img alt="" src="/images/site/opportunities.jpg">
		</div>
	</div>
	<div class="container">
		<div style="padding-bottom:0px; position: relative;" class="row article-pane">
			<div class="col-lg-12">
				<header style="text-align:center">
					<!-- <h1>{$lang['view_category']}</h1> -->
					<h1>Questions and Answers concerning the Current Greek Financial Situation and Its Potential Impact on Celestyal Cruises and the Company’s 2015 Cruise Season</h1>
					<div class="social-plugins">
						
					</div>
				</header>
			</div>
		</div>
	</div>
</div>

<div class="container faq">
	<br />
	<div class="row">
		<div class="col-sm-12">
			{foreach $articles $a}
			
			<div class="accordion-group">
				<div class="accordion-heading">
					<a class="accordion-toggle btn btn-default collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse{$a['count']}"> {$a['name']} </a>
				</div>
				<div class="accordion-body collapse" id="collapse{$a['count']}" style="height: 0px;">
					<div class="accordion-inner">
						<p>
							{$a['description']}
						</p>
						<br />
					</div>
				</div>
			</div>
			{/foreach}
		</div>
	</div>
	<br />
</div>

{literal}

<style>
	.faq .accordion-toggle.btn {
		background-color: #7bb6d9;
	    border: 1px solid #7bb6d9;
	    color: #fff;
	    font-size: 18px !important;
	    font-weight: 300;
	    line-height: 35px;
	    margin-bottom: 10px;
	    margin-right: 2px;
	    padding: 0 30px;
	    text-align: left;
	    white-space: unset;
	    width: 100%;
	}
	
	.faq .accordion-toggle.btn.collapsed {
		background-color: #FFFFFF;
		border: 1px solid #cfc08e;
		color: #cfc08e;
	}
	
	.faq .accordion-toggle.btn:hover {
		 background-color: #7bb6d9;
		 border: 1px solid #7bb6d9;
		 color: #fff;
	}
</style>

{/literal}

<!--
<div class='content_block'>
<h2>{$lang['view_category']}</h2>
{foreach $articles $a}
<h3><a href='{$td_url}/index.php?act=article&amp;code=view&amp;id={$a['id']}'>{$a['name']}</a><span class='date'> -- {$a['date']}</span></h3>
<p><div class='desc'>{$a['description']}</div></p>
{/foreach}
</div>
-->