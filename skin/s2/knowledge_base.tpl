<div class="container-fluid nopadd white-bg">
	<div class="row row-banner">
		<div class="banner-container">
			<img alt="" src="/images/site/opportunities.jpg">
		</div>
	</div>
	<div class="container">
		<div style="padding-bottom:0px; position: relative;" class="row article-pane">
			<div class="col-lg-12">
				<header style="text-align:center">
					<h1>{$lang['knowledge_base']}</h1>
					<div class="social-plugins">
						
					</div>
				</header>
			</div>
		</div>
	</div>
</div>

<div class="container">
	<h2>{$lang['knowledge_base']}</h2>
	<h3 style='margin-top: 4px'>{$lang['search']}</h3>
	<!--
	<form action='{$td_url}/index.php?act=kb&amp;code=search' method='post'>
	{$token_kb_search}
	<p>
	<textarea name='keywords' id='keywordsb' cols='70' rows='2' onfocus="clear_value(this, '{$lang['keywords_phrase']}')" onblur="reset_value(this, '{$lang['keywords_phrase']}')">{$lang['keywords_phrase']}</textarea>
	</p>
	<p>
	<input type='submit' class='submit' name='submit' id='searchb' value='{$lang['search_button']}' />
	</p>
	</form>
	-->
	<h3>{$lang['categories']}</h3>
	<br />
	<div class="row">
		<div class="col-sm-12">
			<div class="accordion" id="accordion2">
				{foreach $cats_left $cl}
				<div class="accordion-group">
					<div class="accordion-heading">
						<a style="width:100%;" class="accordion-toggle btn btn-default" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne"> 
							{$cl['name']} ({$cl['articles']})
						</a>
					</div>
					<div class="accordion-body collapse" id="collapseOne" style="height: 0px;">
						<div class="accordion-inner">
							Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
						</div>
					</div>
				</div>
				{/foreach}
			</div>
		</div>
		<div class="col-sm-12">
			<div class="accordion" id="accordion2">
				{foreach $cats_right $cr}
				<div class="accordion-group">
					<div class="accordion-heading">
						<a style="width:100%;" class="accordion-toggle btn btn-default" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne"> {$cr['name']} ({$cr['articles']}) </a>
					</div>
				</div>
				{/foreach}
			</div>
		</div>
	</div>
	
	
	
	<!--
	<table width='100%' cellpadding='3' cellspacing='1' class='padnotop'>
	<tr>
	<td width='50%' valign='top'> {foreach $cats_left $cl} <span class='ctitle'><a href='{$td_url}/index.php?act=kb&amp;code=cat&amp;id={$cl['id']}'>{$cl['name']} ({$cl['articles']})</a></span>
	<div class='bldesc'>
	{$cl['description']}
	</div> {/foreach} </td>
	<td width='50%' valign='top'> {foreach $cats_right $cr} <span class='ctitle'><a href='{$td_url}/index.php?act=kb&amp;code=cat&amp;id={$cr['id']}'>{$cr['name']} ({$cr['articles']})</a></span>
	<div class='bldesc'>
	{$cr['description']}
	</div> {/foreach} </td>
	</tr>
	</table>
	-->
	
</div>

{literal}
<script>
	head.ready(function() {
		$(".collapse").collapse()
	}); 
</script>

<style>
	.accordion-toggle.btn {
		border: 1px solid #cfc08e;
	    color: #cfc08e;
	    font-size: 20px;
	    font-weight: 300;
	    line-height: 42px;
	    margin-right: 2px;
	    padding: 0 30px;
	    text-align: center;
	    margin-bottom: 10px;
	}
	
	/*
	.accordion-toggle.btn:hover {
	    background-color: #7bb6d9;
	    border: 1px solid #7bb6d9;
	    color: #fff;
	}
	*/
}
</style>

{/literal}