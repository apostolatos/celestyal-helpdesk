<nav class="navbar navbar-celestyal" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header"> </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="menu-navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">{$language['plan_your_cruise']}</a>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="/{$input['lang']}/cruises.html"> 
                                <i> <img src="/images/site/submenu/cruises.png" alt=""> </i> <span>{$language['cruises']}</span> 
                            </a>
                        </li>
                        <li>
                            <a href="/{$input['lang']}/cruise-destinations/index.html"> 
                                <i> <img src="/images/site/submenu/destinations.png" alt=""> </i> <span>{$language['destinations']}</span> 
                            </a>
                        </li>
                        <li>
                            <a href="/{$input['lang']}/cruise-ships/index.html"> 
                                <i> <img src="/images/site/submenu/ships.png" alt=""> </i> <span>{$language['ships']}</span> 
                            </a>
                        </li>
                        <li>
                            <a href="/{$input['lang']}/cruise-excursions/index.html">
                                 <i> <img src="/images/site/submenu/shore.png" alt=""> </i> <span>{$language['tours_shore_excursions']}</span> 
                            </a>
                        </li>
                        <li>
                            <a href="/{$input['lang']}/cruising/296/how-to-book.html">
                                 <i> <img src="/images/site/submenu/book.png" alt=""> </i> <span>{$language['how_to_book']}</span> 
                            </a>
                        </li>
                        <li>
                            <a href="/{$input['lang']}/cruising/128/new-to-cruising-learn-more.html"> <i> <img src="/images/site/submenu/new_to_cruises.png" alt=""> </i> <span>{$language['new_to_cruising']}</span> </a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">{$language['welcome_on_board']}</span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="/{$input['lang']}/cruising/76/restaurants.html"> 
                                <i> <img src="/images/site/submenu/gastronomy.png" alt=""> </i> <span>{$language['culinary_n_gastronomy']}</span>
                            </a>
                        </li>
                        <li>
                            <a href="/{$input['lang']}/cruising/78/all-inclusive-drink-packages.html"> 
                                <i> <img src="/images/site/submenu/drinks.png" alt=""> </i> <span>{$language['drink_packages']}</span> 
                            </a>
                        </li>
                        <li>
                            <a href="/{$input['lang']}/category/day-and-night-life/index.html"> 
                                <i> <img src="/images/site/submenu/nightlife.png" alt=""> </i> <span>{$language['day_n_night_life']}</span> 
                            </a>
                        </li>
                        <li>
                            <a href="/{$input['lang']}/category/day-and-night-life/index.html"> 
                                <i> <img src="/images/site/submenu/moments.png" alt=""> </i> <span>{$language['moments_of_life']}</span> 
                            </a>
                        </li>
                        <li>
                            <a href="/{$input['lang']}/category/special-services/index.html"> 
                                <i> <img src="/images/site/submenu/special.png" alt=""> </i> <span>{$language['special_services']}</span>
                            </a>
                        </li>
                        <!--
                        <li>
                            <a href="#"> <i> <img src="/images/site/submenu/media.png" alt=""> </i> <span>Media</span> </a>
                        </li>
                        -->
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">{$language['customer_support']}</span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="/{$input['lang']}/form/6/contact-us.html"> 
                                <i> <img src="/images/site/submenu/contact.png" alt=""> </i> <span>{$language['contact_us']}</span> 
                            </a>
                        </li>
                        <li>
                            <a href="/{$input['lang']}/category/before-you-cruise/index.html"> 
                                <i> <img src="/images/site/submenu/before.png" alt=""> </i> <span>{$language['before_you_cruise']}</span> 
                            </a>
                        </li>
                        <li>
                            <a href="/{$input['lang']}/login.html"> 
                                <i> <img src="/images/site/submenu/booked.png" alt=""> </i> <span>{$language['home_already_booked']}..</span> 
                            </a>
                        </li>
                        <li>
                            <a href="/{$input['lang']}/category/terms-and-conditions-of-carriage/index.html">
                                <i> <img src="/images/site/submenu/conditions.png" alt=""> </i> <span>{$language['terms_and_conditions']}</span> 
                            </a>
                        </li>
                    </ul>
                </li>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>
