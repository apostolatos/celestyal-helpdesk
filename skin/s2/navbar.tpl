<header class="hidden-sm hidden-xs" id="headerTop">
	<div class="container">
		<div class="row">
			<!-- logo -->
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
				<div id="logoPosition">
					<a href="">
						<img alt="Celestyal Cruises" src="http://www.celestyalcruises.gr/sites/default/files/celestyal_logo_final.svg"> 
					</a>
				</div>
			</div>
			<!-- top Nav -->
			<div class="col-lg-8 col-lg-offset-1 col-md-8 col-md-offset-1 col-sm-8 col-sm-offset-1 col-xs-8 col-xs-offset-1">
				<div class="row">
					<!-- Phone - Search -->
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rightT">
						<span id="regionHeader"> </span>
						<span class="phone icon">
							<a href="callto://+30 210 45 83 499" data-phone="+30 210 45 83 499">+30 210 45 83 499</a>
						</span>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div id="topNavPosition">
							<ul>
								<li>
									<a href="{$domain}/{$menu_alias['news']}" class="restNav">{$language['news']}</a>
								</li>
								<li>
									<a href="{$domain}/{$menu_alias['company']}" class="restNav">{$language['about_us']}</a>
								</li>
								<li>
									<a href="http://booking.celestyalcruises.com/{$input['lang']}/login.html" class="restNav">{$language['manage_booking']}</a>
								</li>
								<li>
									<a href="{$domain}/{$menu_alias['contact']}" class="restNav">{$language['contact']}</a>
								</li>
							</ul>
							<div id="searchPosition">
								<div class="input-group stylish-input-group">
									<input type="text" placeholder="Αναζήτηση" class="form-control">
									<span class="input-group-addon">
										<button type="submit">
											<span class="glyphicon glyphicon-search"> </span>
										</button> 
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>

<header class="hidden-sm hidden-xs" id="headerNav">
	<div class="container"> </div>
</header>

<div class="relative">
	<div data-num-elements="0.35" class="slider home top height slick-initialized slick-slider" style="height: 184.8px;">
		<div class="slick-list draggable" aria-live="polite">
			<div class="slick-track">
				<div class="sliderInner slick-slide slick-current slick-active" data-slick-index="0" aria-hidden="false" style="position: relative; left: 0px; top: 0px; z-index: 999; opacity: 1;" tabindex="-1" role="option" aria-describedby="slick-slide00">
					<div class="bgimg bgimg-on" style="background: transparent url('http://www.celestyalcruises.gr/sites/default/files/our-vision-header_2.jpg') no-repeat scroll 50% 50%;">
						<img style="height: 500px; width: 1600px; display: none;" src="http://www.celestyalcruises.gr/sites/default/files/our-vision-header_2.jpg">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>