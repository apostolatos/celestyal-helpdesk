<div class="main_column_inner">
	<div class="left_column">
		<h2 class="title">404 Status Code (Page Not Found).</h2>
		<div id="h3bottom" class="excursion-info-block not-found">
			<ul class="links">
				<li>
					<a href="/{$FRONT_LANG}/index.html">{$language[home]}</a>
				</li>
				<li>
					<a href="/{$FRONT_LANG}/cruises/cruise-2013/index.html">{$language[cruises]} 2013</a>
				</li>
				<li>
					<a href="/{$FRONT_LANG}/cruises/cruise-2013/index.html#cruises-from/GREECE">{$language[cruises_from]} {$language[Greece]}</a>
				</li>
				<li>
					<a href="/{$FRONT_LANG}/cruises/cruise-2013/index.html#cruises-from/TURKEY">{$language[cruises_from]} {$language[turkey]}</a>
				</li>
				<li>
					<a href="/{$FRONT_LANG}/cruises-from-cyprus.html">{$language[cruises_from]} {$language[Cyprus]}</a>
				</li>
				<li>
					<a href="/{$FRONT_LANG}/category/greek-hospitallity/index.html">{$language[greek_hospitality]}</a>
				</li>
				<li>
					<a href="/{$FRONT_LANG}/category/momments-of-life/index.html">{$language[moments_of_life]}</a>
				</li>
				<li>
					<a href="/{$FRONT_LANG}/category/fun-entertainment/index.html">{$language[fun_n_entertainment]}</a>
				</li>
				<li>
					<a href="/{$FRONT_LANG}/category/during-you-cruise/index.html">{$language[before_you_cruise]}</a>
				</li>
				<li>
					<a href="/{$FRONT_LANG}/category/before-you-cruise/index.html">{$language[during_your_cruise]}</a>
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="seperator"> </div>
<div class="seperator"> </div>
