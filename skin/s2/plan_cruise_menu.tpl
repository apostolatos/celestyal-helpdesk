<div class="hidden megaMenu container_12 container" id="MegaMenu-42">
	<div id="content">
		<div class="grid_6">
			<div class="box-bg">
				<div class="megabox1Left">
					<h2>{$language['find_a_louis_cruise']}</h2>
					<img src="/images/site/mega-menu/find-cruise.jpg" alt="" title="" />
					<div class="float-left"> 
						<a href="/{$input[lang]}/cruises/cruise-2013/index.html"><span>{$language['cruise_calendar']} 2013</span></a>
						<div class="sep-mega"> </div>
						<a href="/{$input[lang]}/cruises/cruise-2014/index.html"><span>{$language['cruise_calendar']} 2014</span></a>
						<div class="sep-mega"> </div>
						<a href="/{$input[lang]}/cruises/cruise-2013/index.html#!cruises-from/GREECE"><span>{$language['cruises_from']} {$language['Greece']}</span></a>
						<div class="sep-mega"> </div>
						<a href="/{$input[lang]}/cruises/cruise-2013/index.html#!cruises-from/TURKEY"><span>{$language['cruises_from']} {$language['turkey']}</span></a>
						<div class="sep-mega"> </div>
						<!--
						<a href="/{$input[lang]}/cruises/cruise-2013/index.html#!cruises-from/CYPRUS"><span>{$language['cruises_from']} {$language['Cyprus']}</span></a>
						<div class="sep-mega"> </div>
						-->
					</div>
				</div>
				<div class="megabox1Right">
					<a href="/{$input[lang]}/cruises/cruise-2013/index.html">
						<span class="seemore">{$language['see_all_cruises']}</span>
					</a>
					<br/>
					<br/>
					<a class="dest-more-left button blue small float-right" style="text-align:center;" href="#">{$language['deals_inside']}</a>
				</div>
			</div>
			<div class="box-bg-big">
				<h2 class="float-left">{$language['louis_cruise_ships']}</h2>
				<a href="/{$input[lang]}/cruise-ships/index.html" class="float-right">
					<span class="seemore spacer-right spacer-bottom">{$language['see_all_ships']}</span>
				</a>
				<div class="float-left">
					<div class="float-left spacer-right">
					<a href="/{$input[lang]}/cruise-ships/cruise-ship/LO/louis_olympia.html" class="spacer-bottom">
						<img src="/images/site/mega-menu/olympia.jpg" width="202" height="93" alt="olympia" />
						<div class="ship-over">
						<span>Louis Olympia</span>
						</div>
					</a> 
					</div>
					<div class="float-left">
					<a href="/{$input[lang]}/cruise-ships/cruise-ship/OQ/louis-aura.html">
						<img src="/images/site/mega-menu/queen.jpg" width="202" height="93" alt="Louis Aura" />
						<div class="ship-over">
						<span>Louis Aura</span>
						</div>
					</a> 
					</div>
				</div>
			</div>
		</div>
		<div class="grid_6">
			<div  class="box-bg">
				<div class="MegaDestinationsLeft">
					<h2 id="wid265">{$language['east_mediterranean_destinations']}</h2>
					<img src="/images/site/mega-menu/shore.jpg" alt="" title="" />
					<div class="float-left mega-dest">
						<a href="/{$input[lang]}/cruise-destinations/index.html" class="SearchFilter"><span class="seemore-no-under">{$language['all_destinations']}</span></a>
						<div class="sep-mega"> </div>
						<a href="/{$input[lang]}/cruise-destinations/from/Greece/index.html" class="SearchFilter"><span class="seemore-no-under">{$language['greece_and_greek_islands']}</span></a>
						<div class="sep-mega"> </div>
						<a href="/{$input[lang]}/cruise-destinations/from/Turkey/index.html" class="SearchFilter"><span class="seemore-no-under">{$language['turkey']}</span></a>
						<div class="sep-mega"> </div>
						<a href="/{$input[lang]}/cruise-destinations/index.html"><span class="seemore-no-under">{$language['ports_of_departure']}</span></a>
						<br/>
					</div>
				</div>
				<div class="MegaDestinationsRight">
					<a href="/{$input[lang]}/cruise-destinations/index.html"><span class="seemore">{$language['see_all_places']}</span></a>
					<div class="seperator"> </div>
					<div class="seperator"> </div>
					<div class="seperator"> </div>
					<h3 id="arrow">{$language['top_destinations_mega']}</h3>
					<a href="/{$input[lang]}/cruise-destinations/cruise-port/IST/cruise-to-istanbul.html"><span class="top-dest-mega">{$language['istanbul']}, </span></a>
					<a href="/{$input[lang]}/cruise-destinations/cruise-port/MYK/cruise-to-mykonos-greece.html"><span class="top-dest-mega">{$language['mykonos']}, </span></a>
					<a href="/{$input[lang]}/cruise-destinations/cruise-port/SAN/cruise-to-santorini-greece.html"><span class="top-dest-mega">{$language['santorini']}, </span></a>
					<a href="/{$input[lang]}/cruise-destinations/cruise-port/KUS/cruise-to-kusadasi.html"><span class="top-dest-mega">{$language['Kusadasi']}, </span></a> 
					<a href="/{$input[lang]}/cruise-destinations/cruise-port/PAT/cruise-to-patmos-greece.html"><span class="top-dest-mega">{$language['patmos']}, </span></a>
					<a href="/{$input[lang]}/cruise-destinations/cruise-port/ROD/cruise-to-rhodes.html"><span class="top-dest-mega">{$language['rhodes']}</span></a>
				</div>
			</div>
			<div class="box-bg-big">
				<div class="megabox1Left">
					<h2>{$language['tours_shore_excursions']}</h2>
					<img src="/images/site/mega-menu/shore.jpg" alt="" title="" />
					<span class="normal mega-exc">
						{$language['turkey']}:
						<a href="/{$input[lang]}/cruise-excursions/shore-excursion/IST-15/religious-visit-to-orthodox-churches-and-greek-patriarchate.html" class="str-exc SearchFilter">{$language['hagia_sophia']}, </a>
						<a href="/{$input[lang]}/cruise-excursions/shore-excursion/IST-13/discover-the-hidden-secrets-of-sultanahmet-square.html" class="str-exc SearchFilter">{$language['blue_mosque']}, </a>
						<a href="/{$input[lang]}/cruise-excursions/shore-excursion/KUS-02/ancient-ephesus-through-the-ages-hellenistic-and-roman.html" class="str-exc SearchFilter">{$language['ephesus']} </a>
					</span>
					<div class="sep-mega"> </div>
					<span class="normal mega-exc">
						{$language['patmos']}:
						<a href="/{$input[lang]}/cruise-excursions/shore-excursion/PAT-01/religious-visit-to-st-johns-monastery-grotto-of-apocalypse.html" class="str-exc SearchFilter">{$language['st_john_monastery_n_grotto']}</a>
					</span>
					<div class="sep-mega"> </div>
					<span class="normal mega-exc">
						{$language['heraklion']}:
						<a href="/{$input[lang]}/cruise-excursions/shore-excursion/HER-02/minoan-palace-of-knossos-1st-european-civilization.html" class="str-exc SearchFilter">{$language['knossos_palace']}, </a>
						<a href="/{$input[lang]}/cruise-excursions/shore-excursion/HER-07/venetian-heraklion-and-the-unique-archaeological-museum.html" class="str-exc SearchFilter">{$language['city_tour']} </a>
					</span>
					<div class="sep-mega"> </div>
					<span class="normal mega-exc">
						{$language['santorini']}:
						<a href="/{$input[lang]}/cruise-excursions/shore-excursion/SAN-01/spectacular-oia-village-perched-on-the-caldera-rim.html" class="str-exc SearchFilter">{$language['oia']}, </a>
						<a href="/{$input[lang]}/cruise-excursions/shore-excursion/SAN-02/cruise-to-santorinis-islands-and-volcanos.html" class="str-exc SearchFilter">{$language['volcano_tour']} </a>
					</span>
				</div>
				<div class="megabox1Right" id="mega-exc-right"> 
					<a href="/{$input[lang]}/cruise-excursions/index.html"><span class="seemore">{$language['see_all_shorex']}</span></a>
				</div>
				<div id="img-new"> </div>
				<a href="/{$input[lang]}/cruises/cruise-2013/index.html" class="button red large float-right spacer-right" id="marg5">{$language['discounted_packages']}</a>
			</div>
		</div>
		<div class="grid_6">
			<div class="MegaButtons"> 
				<a href="#" class="button yellow large">{$language['how_to_book']}</a>
			</div>
		</div>
		<div class="grid_6">
			<div class="MegaButtons"> 
				<a href="#" class="button red large">{$language['book_your_vacation_here']}</a>
			</div>
		</div>
	</div>
</div>