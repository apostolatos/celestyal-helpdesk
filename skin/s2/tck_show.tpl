{literal} 
<script type="text/javascript">

	function sure_close()
	{
		if ( confirm("{/literal}{$lang['confirm_close']}{literal}") )
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function sure_escalate()
	{
		if ( confirm("{/literal}{$lang['confirm_escalate']}{literal}") )
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function sure_delete_reply()
	{
		if ( confirm("{/literal}{$lang['confirm_delete_reply']}{literal}") )
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function validate_form(form)
	{
		if ( ! form.message.value )
		{
			alert('{/literal}{$lang['err_no_message']}{literal}');
			form.message.focus();
			return false;
		}
	}

	function amithumbsup(rate)
	{
		document.images['thumbsup_'+rate].src = '{/literal}{$img_url}{literal}/thumbs_up_hover.gif';
	}

	function unamithumbsup(rate)
	{
		document.images['thumbsup_'+rate].src = '{/literal}{$img_url}{literal}/thumbs_up.gif';
	}

	function amithumbsdown(rate)
	{
		document.images['thumbsdown_'+rate].src = '{/literal}{$img_url}{literal}/thumbs_down_hover.gif';
	}

	function unamithumbsdown(rate)
	{
		document.images['thumbsdown_'+rate].src = '{/literal}{$img_url}{literal}/thumbs_down.gif';
	}
	
	function reply() 
	{
		document.getElementById("reply").submit();
	}
	
	$(document).ready(function()
	{
		$('a.reply').click(function(e){
			e.preventDefault();
		})
	})
	
</script>
<script language='javascript' type='text/javascript' src='{/literal}{$td_url}{literal}/includes/tinymce/tiny_mce.js'></script>
<script language='javascript' type='text/javascript'>						
/*tinyMCE.init({
    mode: 'exact',
    theme: 'advanced',
    elements: 'message',
    plugins: 'inlinepopups,safari,spellchecker',
    dialog_type: 'modal',
    forced_root_block: false,
    force_br_newlines: true,
    force_p_newlines: false,
    theme_advanced_toolbar_location: 'top',
    theme_advanced_toolbar_align: 'left',
    theme_advanced_path_location: 'bottom',
    theme_advanced_disable: 'styleselect,formatselect',
    theme_advanced_buttons1: 'bold,italic,underline,strikethrough,separator,forecolor,backcolor,separator,bullist,numlist,separator,outdent,indent,separator,link,unlink,image,separator,undo,redo,separator,spellchecker,separator,removeformat,cleanup,code',
    theme_advanced_buttons2: '',
    theme_advanced_buttons3: '',
    theme_advanced_resize_horizontal: false,
    theme_advanced_resizing: true
});*/

function canned(cid) {
    var xmlHttp;

    try {
        xmlHttp = new XMLHttpRequest();
    } catch (e) {
        try {
            xmlHttp = new ActiveXObject('Msxml2.XMLHTTP');
        } catch (e) {
            try {
                xmlHttp = new ActiveXObject('Microsoft.XMLHTTP');
            } catch (e) {
                alert('Sorry, your browser does not support AJAX.  Therefore, this feature will not work.');
                show_hide('canned_drop');
                return false;
            }
        }
    }

    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState == 4) {
            tinyMCE.execCommand('mceInsertContent', false, xmlHttp.responseText);
        }
    }

    xmlHttp.open('GET', '{/literal}{$td_url}{literal}/admin.php?section=manage&act=canned&code=get&id=' + cid, true);
    xmlHttp.send(null);

    show_hide('canned_drop');
}
</script> 
{/literal}
<div id="myDiv">
	<div class="bluestripbig">
		<div style="float:right" class="blinks"><a href="{$td_url}/index.php?act=tickets&amp;code=print&amp;id={$t['id']}">{$lang['print']}</a>{if $t['links']} | {$t['links']}{/if}</div>
		<h3>{$lang['viewing_ticket']}</h3>
	</div>
	<div class="sboutline">
		<table class="ticket-show" width="100%" cellpadding="3" cellspacing="1">
			<tr>
				<td class="row1" width="20%">{$lang['ticket_id']}</td>
				<td class="row3" width="30%">{$t['id']}</td>
				<td class="row1" width="20%">{$lang['replies']}</td>
				<td class="row3" width="30%">{$t['replies']}</td>
			</tr>
			<tr>
				<td class="row1">{$lang['priority']}</td>
				<td class="row3">{$t['priority']}</td>
				<td class="row1">{$lang['last_reply']}</td>
				<td class="row3">{$t['last_reply']}</td>
			</tr>
			<tr>
				<td class="row1">{$lang['department']}</td>
				<td class="row3">{$cache['depart'][ $t['did'] ]['name']}</td>
				<td class="row1">{$lang['last_replier']}</td>
				<td class="row3">{$t['last_mname']}</td>
			</tr>
			<tr>
				<td class="row1">{$lang['submitted']}</td>
				<td class="row3">{$t['date']}</td>
				<td class="row1">{$lang['status']}</td>
				<td class="row3">{$t['status_human']}</td>
			</tr>
			{if $cdfields_left}
			{foreach $cdfields_left $cdfl}
			<tr>
				<td class="row1">{$cdfl['name']}</td>
				<td class="row3"{if $cdfl['colspan']} colspan="{$cdfl['colspan']}"{/if}>
				{$cdfl['value']}
				</td>
				{if $cdfl['colspan']}
			</tr>
			{/if}
			{if $cdfields_right[ $cdfl['count'] ]}
				<td class="row1">{$cdfields_right[ $cdfl['count'] ]['name']}</td>
				<td class="row3">{$cdfields_right[ $cdfl['count'] ]['value']}</td>
			</tr>
			{/if}
			{/foreach}
			{/if}
		</table>
	</div>
	<div class="seperator"></div>
	<div class="seperator"></div>
	{if $t['close_reason']}
	<div id="smallerror">
		<p>{$lang['close_msg_a']} {$t['close_mname']} {$lang['close_msg_b']}
		<br />
		<br />
		{$t['close_reason']}
		</p>
	</div>
	{/if}
	<h3>{$t['subject']}{$t['edit_link']}</h3>
	<p> {$t['message']} </p>
	{if $t['attach_id']}
	<p class="addesc">{$lang['download_attachment']} <a href="{$td_url}/index.php?act=tickets&amp;code=attachment&amp;id={$t['attach_id']}">{$t['original_name']}</a> ({$t['size']})
	<p> {/if} <br />
		{if $replies}
		{foreach $replies $r} 
		<a name="reply{$r['id']}"></a>
	<div class="{$r['class']}">
		<div style="float:left">{$r['mname']} -- {$r['date']}</div>
		<div align="right">{$r['time_ago']}{$r['rate_imgs']}</div>
	</div>
	<p> {$r['message']} </p>
	{if $r['attach_id']}
	<p class="addesc">{$lang['download_attachment']} <a href="{$td_url}/index.php?act=tickets&amp;code=attachment&amp;id={$r['attach_id']}">{$r['original_name']}</a> ({$r['size']})
	<p> {/if}
		{/foreach}
		{else}
	<div class="seperator"></div>
	<div class="seperator"></div>
	<div class="disabled">
		<p>{$lang['no_replies']}</p>
	</div>
	{/if}
	<br />
	<div class="blueFooter"></div> 
	<br />
	{if $t['status'] != 6}
	<h3>{$lang['send_reply']}</h3>
	<div class="main-selection">
		<form name="reply" id="reply" action="{$td_url}/index.php?act=tickets&amp;code=reply&amp;id={$t['id']}" method="post" onsubmit="return validate_form(this)" enctype="multipart/form-data">
			{$token_ticket_reply}
			{if $error}
			<div id="smallerror">
				<p>{$error}</p>
			</div>
			{/if}
			<table cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td><textarea style="width:98%" name="message" id="message" cols="70" rows="8">{$input['message']}</textarea></td>
				</tr>
				<tr>
					<td>
						<a class="next button blue larger float-right spacer-right" href="#" onclick="document.getElementById('reply').submit();" data-template="DtsShopRequestMessage" data-div="myDiv">{$lang['send_reply_button']}</a>
					</td>
				</tr>
				{if $cache['config']['ticket_attachments'] && $member['g_ticket_attach'] && $cache['depart'][ $t['did'] ]['can_attach']}
				<input type="file" name="attachment" id="attachment" size="32" />
				{$upload_info}
				{/if}
			</table>
		</form>
		{/if} 
	</div>
</div>
<div class="seperator"></div>
<div class="seperator"></div>