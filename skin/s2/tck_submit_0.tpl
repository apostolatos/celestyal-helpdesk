<div class="container-fluid ">
	<div class="row row-banner">
		<div class="banner-container">
			<img src="/images/site/veria.jpg" alt="">
		</div>
	</div>
</div>
<div class="container">
	<div class="row article-pane" style="padding-bottom:0px; position: relative;">
		<div class="col-lg-12">
			{if $error}
			<div class="alert alert-danger" style="margin-top:20px;">
				{$error} {$error_extra}
			</div>
			{/if}
			<header style="text-align:center">
				<h1>{$depart_title}</h1>
				<h2>{$depart_desc}</h2>
			</header>
		</div>
	</div>
</div>
<div id="myDiv" class="container">
	Here
</div>
<div id="loading" style="display:none;">
	<div class="inner">
		<div id="popup-msg">
			<h3>{$language['thank_you_for_contact']}</h3>
			<h4>{$language['please_wait']}...</h4>
			<h5>{$language['we_will_reply']}</h5>
		</div>
	</div>
</div>
{literal}
<script>
	head.ready(function()
	{
		$('#carousel-presentation').carousel(
		{
			interval : 20000000
		})
		$('[id^=thumb-selector-]').click(function()
		{
			var id_selector = $(this).attr("id");
			var id = id_selector.substr(id_selector.length - 1);
			id = parseInt(id);
			$('#carousel-presentation').carousel(id);
			$('[id^=thumb-selector-]').removeClass('selected');
			$(this).addClass('selected');
		});
		// update while slide
		$('#carousel-presentation').on('slide.bs.carousel', function(e)
		{
			var id = $('.item.active').data('slide-number');
			id = parseInt(id);
			$('[id^=thumb-selector-]').removeClass('selected');
			$('[id^=thumb-selector-' + id + ']').addClass('selected');
		});
	});
</script>
{/literal}