<div class="content_block">
	<h1>{$lang['open_ticket']}</h1>
	{if $error}
	<div class="critical">{$error}</div>
	{/if}
	<form action="{$td_url}/index.php?act=tickets&amp;code=open&amp;step=2" method="get" onsubmit="return validate_form(this)">
		<input type="hidden" name="act" value="tickets"/>
		<input type="hidden" name="code" value="open"/>
		<input type="hidden" name="step" value="2"/>
		{$token_sub_a}
		<div class="groupbox">{$lang['select_depart']}</div>
		<div class="option1">
			<table width="100%" cellpadding="4" cellspacing="0">
				{$depart_opts}
			</table>
		</div>
		<div class="formtail">
			<input type="submit" name="submit" id="send" value="{$lang['open_ticket_button']}" class="button" />
		</div>
	</form>
</div>