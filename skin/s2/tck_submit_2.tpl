<div id="topDestinations" class="homeTitles">
	<div class="center">
		<div class="slider-nav-destinations" id="tabs">
			{foreach $all_departments $department}
			<div class="inline{if $department[id] == $input['department']} active{/if}">
				<h5><a href="/{$input['lang']}/form/{$department[id]}/contact.html">{$department[name]}</a></h5>
			</div>
			{/foreach}
		</div>
	</div>
	<div class="container">
		<div id="ajax-message"> </div>
		{if $error}
		<div class="alert alert-danger" style="margin-top:20px; border: 1px dashed #0f90c0;">
			{$error} {$error_extra}
		</div>
		{/if}
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 center">
				<h2>{$depart_title}</h2>
				<h4>{$depart_desc}</h4>
			</div>
		</div>
	</div>
	
</div>

<div id="myDiv" class="container contact-form mt100" data-validator="kendoValidator">
	<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12">
		<form id="contact" action="{$td_domain}/{if $input['lang']}{$input['lang']}{else}en{/if}/form/submit.html" name="ticket" method="post" class="webform-client-form webform-client-form-101" enctype="multipart/form-data">
			<div>
				<input type="hidden" value="{$input['department']}" name="department" />
				<input type="hidden" name="lang" value="{$input['lang']}" />
				<input type="hidden" name="country" value="{$country}" />
				<input type="hidden" name="browser" value="{$browser_name}" />
				<input type="hidden" name="group" value="{$input['group_perm']}" />

				<div class="row">
					<div class="field cf col-lg-6 col-md-6 col-sm-6 col-xs-12 relative">
						<div class="form-item webform-component webform-component-textfield">
							<label for="name" class="element-invisible">{$lang['name']}</label>
							<input type="text" class="form-text" name="name" id="name" value="{$input['name']}" size="35" required placeholder="{$lang['name']}*" validationMessage="{$lang['the_field']} {$lang['name']} {$lang['is_required']}" />
						</div>
					</div>
					<div class="field cf col-lg-6 col-md-6 col-sm-6 col-xs-12 relative">
						<div class="form-item webform-component webform-component-textfield">
							<label for="email" class="element-invisible">{$lang['email']}</label>
							<input type="email" class="form-text" name="email" id="email" value="{$input['email']}" size="35" required placeholder="{$lang['email']}*" data-email-msg="{$lang['email_not_valid']}" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="field cf col-lg-6 col-md-6 col-sm-6 col-xs-12 relative">
						<div class="form-item webform-component webform-component-textfield">
							<label for="subject" class="element-invisible">{$lang['subject']}</label>
							<input type="text" class="form-text" name="subject" id="subject" value="{$input['subject']}" size="35" required placeholder="{$lang['subject']}*" validationMessage="{$lang['the_field']} {$lang['subject']} {$lang['is_required']}" />
						</div>
					</div>
					<div class="field cf col-lg-6 col-md-6 col-sm-6 col-xs-12 relative">
						<div class="form-item webform-component webform-component-textfield">
							<label id="priority-label" for="priority" class="element-invisible">{$lang['priority']}</label>
							<div class="dropdown dropdown-mod priority">
								<select name="priority" id="priority" class="form-text">
									{$priority_drop}
								</select>
							</div>
						</div>
					</div>
				</div>
				{if $cdfields}
				<div class="row">
					{foreach $cdfields $cdf}
					{if $cdf['type'] == "textfield"}
					<div class="field cf col-lg-6 col-md-6 col-sm-6 col-xs-12 relative">
						<div class="form-item webform-component webform-component-textfield">
							<label class="element-invisible"><span class="cdf_{$cdf['fkey']} ad-ch">{$cdf['name']}</span></label>
							<input type="text" class="{if $cdf['datepicker'] and $cdf['rangeselection']} range_{$cdf['fkey']} date-start{elseif $cdf['datepicker']} datepicker{/if}{if $cdf['hidden']} hidden{/if} form-text" name="cdf_{$cdf['fkey']}" id="cdf_{$cdf['fkey']}" value="{$cdf['value']}" size="45"{if $cdf['required']} required  validationMessage="{$lang['the_field']} {$cdf['name']} {$lang['is_required']}" {/if} placeholder="{$cdf['name']}*" />
						</div>
					</div>
					{elseif $cdf['type'] == "textarea"}
					<label><span class="cdf_{$cdf['fkey']} ad-ch">{$cdf['name']}</span></label>
					<div class="form-textarea-wrapper">
						<textarea name="cdf_{$cdf['fkey']}" id="cdf_{$cdf['fkey']}" class="form-textarea" rows="1" cols="60">{$cdf['value']}</textarea>
					</div>
					{elseif $cdf['type'] == "dropdown"}
					<div class="field cf col-lg-6 col-md-6 col-sm-6 col-xs-12 relative{if $cdf['hidden']} hidden{/if}">
						<div class="form-item webform-component webform-component-textfield">
							<label class="element-invisible">{$cdf['name']}</label>
							<div class="dropdown dropdown-mod priority">
								<select name="cdf_{$cdf['fkey']}" id="cdf_{$cdf['fkey']}" class="form-text">
									{$cdf['options']}
								</select>
							</div>
						</div>
					</div>
					{/if}
					{/foreach}
				</div>
				{/if}
				{literal}
				<script>
					$ ( '#priority input' ).on ( 'click' , priorityHandler );
					function priorityHandler ( event )
					{
						caret_Str = '<span class="d-caret" style="position:absolute; right:10px"></span>';
						label = $ ( this ).attr ( 'data-value' );
						$ ( '#priority-btn' ).html ( label + caret_Str );
					}
				</script>
				{/literal}
				<!-- RIGHT PANE -->
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="row">
						<div class="form-textarea-wrapper">
							<label for="message" class="element-invisible">{$lang[message]}</label>
							<textarea class="form-textarea" rows="1" cols="60" name="message" id="message" onfocus="if(this.value=='Enter Your Message'){this.value=''}; return false;" placeholder="{$lang[message]}">{$input['message']}</textarea>
						</div>
						{if $cache['config']['use_captcha'] && $member['id'] == 0}
						<div class="row">
							<div class="field cf col-lg-6 col-md-6 col-sm-6 col-xs-12 col-md-offset-3 relative">
								<div class="form-item webform-component webform-component-textfield">
									<label for="captcha" class="element-invisible">Captcha</label>
									<input type="text" name="captcha" placeholder="Captcha" id="captcha-form" class="form-text" autocomplete="off" />
								</div>
							</div>
						</div>
						{/if}
						<div class="row">
							<div class="field cf col-lg-6 col-md-6 col-sm-6 col-xs-12 col-md-offset-3 relative">
								<div class="form-item webform-component webform-component-textfield">
									<br />
									<img src="{$new_captcha}" id="captcha2" alt="Captcha" />
									<small class="captcha-small">Not readable? <a href="#" id="change_captcha" onclick="
										document.getElementById('captcha2').src='{$new_captcha}?'+Math.random();
										document.getElementById('captcha-form').focus();"
										id="change-image">Change text.</a>. 
									</small>
								</div>
							</div>
						</div>
					</div>
                    <br />
					<!-- ENDS RIGHT -->
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<a class="next btn apply-btn center-block" href="#">{$lang['open_ticket_button']}</a>
					</div>
				</div>
		</form>
	</div>
</div>
{literal}
<script>
	head.js ( '/includes/scripts/ticket.js' );
</script>
{/literal}