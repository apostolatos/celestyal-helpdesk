{literal}
<script type="text/javascript">
	function send ( )
	{
		document.forms [ "ticket" ].submit ( );
	}

</script>
{/literal}

<form action="{$td_domain}/{$input['lang']}/form/submit.html" method="post" name="ticket">
	{$token_sub_c}
	<input type="hidden" name="final" value="1" />
	<input type="hidden" name="subject" value="{$input['subject']}" />
	<input type="hidden" name="department" value="{$input['department']}" />
	<input type="hidden" name="priority" value="{$input['priority']}" />
	<input type="hidden" name="message" value="{$input['message']}" />
	{if $member['id'] == 0}
	<input type="hidden" name="name" value="{$input['name']}" />
	<input type="hidden" name="email" value="{$input['email']}" />
	<input type="hidden" name="guest_email" value="{$input['guest_email']}" />
	{/if}
	{$cdfields}
	{$attach_field}
	<div class="container contact-form mt100">
		<h3>{$lang['article_suggestions']}</h3>
		<p class="suggestions-explained">
			{$lang['suggestions_explained']}
		</p>
		<br />
		<div class="blueFooter"> </div>
		<div class="themeList">
			{foreach $suggestions $a}
			<div class="block">
				<div class="content float-left">
					<h3><a href="{$td_domain}/{$input["lang"]}/form/article/view/{$a["id"]}.html" target="_blank">{$a["name"]} -- {$a["date"]} -- {$lang["relevance"]} {$a["score"]}</a></h3>
					<p>
						{$a["description"]}
					</p>
				</div>
			</div>
			{/foreach}
		</div>

		<div class="themeList">
			<h2>{$lang['continue_ticket_submit']}</h2>
			<p>
				{$lang[vno_suggestions_helped']}
			</p>
		</div>

		{if $error}
		<div id="smallerror">
			<p>
				{$error}
			</p>
		</div>
		{/if}
		<div class="main-selection">
			{if $cache['config']['use_captcha'] && $member['id'] == 0}
			<table width="100%" cellpadding="0" cellspacing="0" class="fakep">
				<tr>
					<td class="row1" width="1"><label for="captcha">{$lang['captcha']}</label></td>
					<td style="padding-left:5px">
					<input type="text" name="captcha" class="childrenage UserData blue k-invalid" id="captcha" size="12" />
					<img src="{$td_url}/index.php?act=captcha&amp;code=create&amp;width=100&amp;height=18&amp;fontsize=8" alt="{$lang['captcha']}" style="vertical-align:middle;" /></td>
				</tr>
				<tr>
					<td colspan="2"><a class="next button blue larger float-right spacer-right" href="#" onclick="javascript:send()" data-template="DtsShopRequestMessage" data-div="myDiv">{$lang['open_ticket_button']}</a></td>
				</tr>
			</table>
			{/if}

		</div>
	</div>
</form>