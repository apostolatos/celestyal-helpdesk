<div class="hidden megaMenu container_12 container" id="MegaMenu-43">
	<div id="content">
		<div class="grid_6">
			<div  class="box-bg">
				<div class="megabox1Left">
					<h2>{$language['greek_hospitality']}</h2>
					<img src="/images/site/mega-menu/hospitality.jpg" alt="" title="" />
					<div class="float-left">
						<a href="/{$input[lang]}/cruising/78/all-inclusive-drink-packages.html" class="str-exc"><span class="normal">{$language['all_inclusive_drink_packages']}</span></a>
						<div class="sep-mega"></div>
						<a href="/{$input[lang]}/cruising/76/restaurants.html" class="str-exc"><span class="normal">{$language['restaurants']}</span></a>
						<div class="sep-mega"></div>
						<a href="/{$input[lang]}/cruising/75/cabin-facilities.html" class="str-exc"><span class="normal">{$language['cabin_facilities']}</span></a>
						<div class="sep-mega"></div>
						<a href="/{$input[lang]}/cruising/77/greek-amusement-facilites.html" class="str-exc"><span class="normal">{$language['amusement_program']}</span></a>
						<div class="seperator"></div>
					</div>
				</div>
				<div class="megabox1Right">
					<a href="/{$input[lang]}/category/greek-hospitallity/index.html"><span class="seemore">{$language['see_what_louis']}</span></a>
					<br/>
					<br/>
					<a class="dest-more-left button blue small float-right" href="/{$input[lang]}/cruising/307/whats-included.html" style="text-align:center;">{$language['whats_included_button']}</a>
				</div>
			</div>
			<div class="box-bg">
				<div class="megabox1Left"> 
					<h2>{$language['moments_of_life']}</h2>
					 <img src="/images/site/mega-menu/moments.jpg" alt="" title="" />
					<div class="float-left"> <a href="/{$input[lang]}/cruising/91/honeymoon-package.html" class="str-exc"><span class="normal">{$language['honeymoon_package']}</span></a>
						<div class="sep-mega"></div>
						<a href="/{$input[lang]}/cruising/82/vow-renewal-package.html" class="str-exc"><span class="normal">{$language['vow_renewal_package']}</span></a>
						<div class="sep-mega"></div>
						<a href="/{$input[lang]}/cruising/86/birthday-on-board.html" class="str-exc"><span class="normal">{$language['birthday_on_board']}</span></a>
						<div class="seperator"></div>
					</div>
				</div>
				<div class="megabox1Right">			
					<a href="/{$input[lang]}/category/momments-of-life/index.html"><span class="seemore">{$language['celebrate_with_us']}</span></a>
					<br/>
					<div class="float-left" style="padding-top:22px;">
						<a href="/{$input[lang]}/cruising/85/a-touch-of-class.html" class="str-exc"><span class="normal">{$language['a_touch_of_class']}</span></a>
						<div class="sep-mega"></div>
						<a href="/{$input[lang]}/cruising/261/champagne-breakfast.html" class="str-exc"><span class="normal">{$language['champagne_breakfast']}</span></a>
						<div class="sep-mega"></div>
						<a href="/{$input[lang]}/cruising/81/celebrate-in-style.html" class="str-exc"><span class="normal">{$language['celebrate_in_style']}</span></a>
						<div class="sep-mega"></div>
						<a href="/{$input[lang]}/cruising/262/bon-voyage-gifts.html" class="str-exc"><span class="bold">{$language['bon_voyage_gifts']}</span></a>
						<div class="sep-mega"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="grid_6">
			<div  class="box-bg">
				<div class="ContentLeft">
					<h2>{$language['fun_n_entertainment']}</h2>
					<img src="/images/site/mega-menu/fun.jpg" class="float-left" alt=""/> 
					<a href="/{$input[lang]}/cruising/263/shore-excursions.html"><span>{$language['shore_excursions']}</span></a>
					<br/>
					<div class="sep-mega"></div>
					<a href="/{$input[lang]}/cruising/116/on-board-shopping.html"><span>{$language['on_board_shopping']}</span></a>
					<br/>
					<div class="sep-mega"></div>
					<a href="/{$input[lang]}/cruising/119/hairdressing-and-styling.html"><span>{$language['hairdressing_n_styling']}</span></a>
					<br/>
					<div class="sep-mega"></div>
					<a href="/{$input[lang]}/cruising/264/spa-and-beauty.html"><span>{$language['spa_n_beauty']}</span></a>
					<br/>
					<div class="sep-mega"></div>
				</div>
				<div class="ContentRight"> 
					<a href="/{$input[lang]}/category/fun-entertainment/index.html" ><span class="seemore">{$language['see_all_fun']}</span></a>
					<div class="seperator"></div>
					<div class="seperator"></div>
					<div class="seperator"></div>
					<h2 id="arrow-welcome">
						<a title="" href="/{$input[lang]}/cruising/118/bars.html">{$language[bars]}, </a>
						<a title="" href="/{$input[lang]}/cruising/279/night-clubbing.html">{$language[night_clubbing]}, </a>
						<a title="" href="/{$input[lang]}/cruising/117/casino.html">{$language[casino]}</a>
					</h2>
				</div>
			</div>
			<div class=" box-bg float-left" id="welcome-left">
				<h2>{$language['before_you_cruise']}</h2>
				<div class="float-left">
					<a href="/{$input[lang]}/category/how-to-reach-the-port/index.html" class="str-exc"><span class="normal">{$language['reach_the_ports']}</span></a>
					<div class="sep-mega"></div>
					<a href="/{$input[lang]}/cruising/121/travel-documents-visa.html" class="str-exc"><span class="normal">{$language['travel_documents']}</span></a>
					<div class="sep-mega"></div>
					<a href="/{$input[lang]}/cruising/122/special-needs-medical-issues.html" class="str-exc"><span class="normal">{$language['special_needs_medical']}</span></a>
					<div class="sep-mega"></div>
					<a href="/{$input[lang]}/cruising/123/what-to-pack.html" class="str-exc"><span class="normal">{$language['what_to_pack']}</span></a>
					<div class="seperator"></div>
				</div>
			</div>
			<div class="box-bg float-left" id="welcome-right">
				<h2>{$language['during_your_cruise']}</h2>
				<div class="float-left"> 
					<a href="/{$input[lang]}/cruising/127/board-on-the-ship.html" class="str-exc"><span class="normal">{$language['board_on_the_ship']}</span></a>
					<div class="sep-mega"></div>
					<a href="/{$input[lang]}/cruising/124/safety-on-board.html" class="str-exc"><span class="normal">{$language['safety_on_board']}</span></a>
					<div class="sep-mega"></div>
					<a href="/{$input[lang]}/cruising/265/louis-cashless-system.html" class="str-exc"><span class="normal">{$language['louis_cashless_system']}</span></a>
					<div class="sep-mega"></div>
					<a href="/{$input[lang]}/cruising/126/disembarkation-procedures.html" class="str-exc"><span class="normal">{$language['disembarkation_procedures']}</span></a>
					<div class="seperator"></div>
				</div>
			</div>
		</div>
		<div class="grid_12">
			<div class="MegaButtons"> 
				<a href="/{$input[lang]}/cruising/128/new-to-cruising-learn-more.html" class="button red large">{$language['new_to_cruising']}</a>
			</div>
		</div>
	</div>
</div>
