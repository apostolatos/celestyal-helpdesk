	
	{include header.tpl}
	<body class="{$input['lang']}">
		<div id="site-wrapper">
			{include navbar.tpl}
			<div id="site-canvas">
				<div class="content pages">
					{if $sub_tpl == 'tck_submit_3.tpl'}
					<div class="container" style="margin-top:40px;">
						<div class="alert alert-success" style="text-align:center; border: 1px dashed #0f90c0;">
							<h3>{$language['thank_you_for_contact']}</h3>
							<br />
							{$language['we_will_reply']}
							<br />
							Please wait while we transfer you. If you do not wish to wait, click here.
						</div>
					</div>
					{else}
					{include $sub_tpl}
					{/if}
				</div>
				<footer>
					{include footer.tpl}
				</footer>
			</div>
		</div>
		<script>
			head.js('/includes/scripts/bootstrap.min.js',
					'/includes/scripts/main.js', 
					'/includes/scripts/langsHandler.js', 
					'/includes/scripts/lightbox.js', 
					'/includes/scripts/mcmsFront.js', 
					'/includes/scripts/kendo/kendo.core.min.js', 
					'/includes/scripts/kendo/kendo.validator.min.js',
					'/includes/scripts/kendo/kendo.data.min.js', 
					'/includes/scripts/kendo/kendo.binder.min.js',
					'/includes/scripts/kendo/kendo.web.min.js');
		</script>
		<!-- Google Code for Celestyal - All Visitors -->
		<!-- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. For instructions on adding this tag and more information on the above requirements, read the setup guide: google.com/ads/remarketingsetup -->
		<script type="text/javascript">
			/* <![CDATA[ */
			var google_conversion_id = 997442487;
			var google_conversion_label = "-C5HCNKMtlcQt4fP2wM";
			var google_custom_params = window.google_tag_params;
			var google_remarketing_only = true;
			/* ]]> */
		</script>
		<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
		<noscript>
		<div style="display:inline;">
			<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/997442487/?value=1.00&amp;currency_code=EUR&amp;label=-C5HCNKMtlcQt4fP2wM&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
	</body>
</html>