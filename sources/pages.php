<?php

/*
 #======================================================
 |    Trellis Desk
 |    =====================================
 |    By DJ Tarazona (dj@accord5.com)
 |    (c) 2010 ACCORD5
 |    http://www.trellisdesk.com/
 |    =====================================
 |    Email: sales@accord5.com
 #======================================================
 |    @ Version: v1.0.4 Final Build 10440094
 |    @ Version Int: 104.4.0.094
 |    @ Version Num: 10440094
 |    @ Build: 0094
 #======================================================
 |    | Custom Pages :: Sources
 #======================================================
 */

class pages {

	#=======================================
	# @ Auto Run
	# Function that is run automatically
	# when the file is required.
	#=======================================

	function auto_run() {
		#=============================
		# Initialize
		#=============================

		$this -> show_page();
	}

	#=======================================
	# @ Show Page
	# Display a custom page.
	#=======================================

	function show_page() {
		#=============================
		# Grab Page
		#=============================

		$this -> ifthd -> core -> db -> construct(array('select' => 'all', 'from' => 'pages', 'where' => array('id', '=', intval($this -> ifthd -> input['id'])), 'limit' => array(0, 1), ));

		$this -> ifthd -> core -> db -> execute();

		if (!$this -> ifthd -> core -> db -> get_num_rows()) {
			$this -> ifthd -> log('error', "Custom Page Not Found ID: " . intval($this -> ifthd -> input['id']));

			$this -> ifthd -> skin -> error('no_page');
		}

		$p = $this -> ifthd -> core -> db -> fetch_row();

		#=============================
		# Fix Up Information
		#=============================

		if ($p['type']) {
			$p['template'] .= '.tpl';

			$this -> ifthd -> core -> template -> set_var('template', $p['template']);
		} else {
			$p['content'] = $this -> ifthd -> prepare_output($this -> ifthd -> remove_dbl_spaces($this -> ifthd -> convert_html($p['content'])), 0, 0, 1);
		}

		$this -> ifthd -> core -> template -> set_var('p', $p);

		#=============================
		# Custom Profile Fields / jim
		#=============================

		//if (is_array($this -> ifthd -> core -> cache['dfields'])) {
		$cdfields = array();
		// Initialize for Security
		$row_count = 0;
		// Initialize for Security

		foreach ($this->ifthd->core->cache['dfields'] as $id => $f) {

			$f_perm = unserialize($f['departs']);

			//if ($f_perm[$this -> ifthd -> input['department']]) {
			$row_count++;

			($row_count & 1) ? $f['class'] = 1 : $f['class'] = 2;

			if (!$f['required']) {
				$f['optional'] = $this -> ifthd -> lang['optional'];
			}

			if ($error) {
				$f['value'] = $this -> ifthd -> input['cdf_' . $f['fkey']];
			}

			if ($f['type'] == 'textfield') {
				$cdfields[] = $f;
			} elseif ($f['type'] == 'textarea') {
				$cdfields[] = $f;
			} elseif ($f['type'] == 'dropdown') {
				$options = explode("\n", $f['extra']);

				while (list(, $opt) = each($options)) {
					$our_opt = explode("=", $opt);

					if ($our_opt[0] == $f['value']) {
						$f['options'] .= "<option value='" . $our_opt[0] . "' selected='selected'>" . $our_opt[1] . "</option>";
					} else {
						$f['options'] .= "<option value='" . $our_opt[0] . "'>" . $our_opt[1] . "</option>";
					}
				}

				$cdfields[] = $f;
			} elseif ($f['type'] == 'checkbox') {
				$cdfields[] = $f;
			} elseif ($f['type'] == 'radio') {
				$options = explode("\n", $f['extra']);

				while (list(, $opt) = each($options)) {
					$our_opt = explode("=", $opt);

					if ($our_opt[0] == $f['value']) {
						$f['options'] .= "<label for='cdf_" . $f['fkey'] . "_" . $our_opt[0] . "'><input type='radio' name='cdf_" . $f['fkey'] . "' id='cdf_" . $f['fkey'] . "_" . $our_opt[0] . "' value='" . $our_opt[0] . "' class='radio' checked='checked' /> " . $our_opt[1] . "</label>&nbsp;&nbsp;";
					} else {
						$f['options'] .= "<label for='cdf_" . $f['fkey'] . "_" . $our_opt[0] . "'><input type='radio' name='cdf_" . $f['fkey'] . "' id='cdf_" . $f['fkey'] . "_" . $our_opt[0] . "' value='" . $our_opt[0] . "' class='radio' /> " . $our_opt[1] . "</label>&nbsp;&nbsp;";
					}
				}

				$cdfields[] = $f;

			}
			//}

			$optional = "";
			// Reset
			$f['options'] = "";

			// Reset
		}//endforeach

		/*
		 print '<pre style="color:#ffffff; background:#000000">';
		 print_r($cdfields);
		 print '</pre>';
		 */
		$this -> ifthd -> core -> template -> set_var('priority_drop', $this -> ifthd -> build_priority_drop($this -> ifthd -> input['priority']));
		$this -> ifthd -> core -> template -> set_var('cdfields', $cdfields);
		//assign variable //jim
		//}

		$sql = '';

		#=============================
		# Do Output
		#=============================

		$this -> nav = array("<a href='{$this->ifthd->core->cache['config']['hd_url']}/index.php?act=pages&amp;id={$p['id']}'>{$p['name']}</a>");

		$this -> ifthd -> core -> template -> set_var('sub_tpl', 'page_show.tpl');

		$this -> ifthd -> skin -> do_output(array('nav' => $this -> nav, 'title' => $p['name']));
	}

}
?>